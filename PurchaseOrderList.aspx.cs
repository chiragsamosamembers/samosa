﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;

public partial class PurchaseOrderList : System.Web.UI.Page
{
    public ReportDocument rptdoc;
    ForPurchaseOrder fpoclass = new ForPurchaseOrder();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fpoclass.selectallPurchaseorderdata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvpo.Visible = true;
            gvpo.DataSource = dtdata;
            gvpo.DataBind();
        }
        else
        {
            gvpo.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Sales Order Found.";
        }
    }

    protected void gvpo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.strpono = e.CommandArgument.ToString();
                Response.Redirect("~/PurchaseOrder.aspx?mode=update&pono=" + li.strpono + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.strpono = e.CommandArgument.ToString();
                DataTable dtcheck = new DataTable();
                dtcheck = fpoclass.selectbillinvoicedatafrompcno(li);
                if (dtcheck.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Purchase Order Used in Purchase Challan No. " + dtcheck.Rows[0]["pcno"].ToString() + " So You Cant delete this Purchase Order.First delete that Purchase Challan then try to delete this Purchase Order.');", true);
                    return;
                }
                else
                {
                    DataTable dtdata1 = new DataTable();
                    dtdata1 = fpoclass.selectallPurchaseorderdatafromponostring(li);
                    li.sono = Convert.ToInt64(dtdata1.Rows[0]["sono"].ToString());
                    DataTable dtdata = new DataTable();
                    dtdata = fpoclass.selectallPurchaseorderitemdatafromponostring(li);
                    for (int c = 0; c < dtdata.Rows.Count; c++)
                    {
                        li.vid = Convert.ToInt64(dtdata.Rows[c]["vid"].ToString());
                        li.id = Convert.ToInt64(dtdata.Rows[c]["id"].ToString());
                        li.itemname = dtdata.Rows[c]["itemname"].ToString();
                        li.qty = Convert.ToDouble(dtdata.Rows[c]["qty"].ToString());
                        if (li.vid != 0)
                        {
                            DataTable dtqty = new DataTable();
                            dtqty = fpoclass.selectqtyremainusedfromsono(li);
                            if (dtqty.Rows.Count > 0)
                            {
                                li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain1"].ToString()) + li.qty;
                                li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused1"].ToString()) - li.qty;
                                fpoclass.updateqtyduringdelete(li);
                            }
                        }
                    }

                    fpoclass.deletepomasterdatastring(li);
                    fpoclass.deletepoitemsdatafromponostring(li);
                    fpoclass.updateisusedn(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.strpono + " Purchase Order Deleted.";
                    faclass.insertactivity(li);
                    fillgrid();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
        else if (e.CommandName == "print")
        {
            string Xrepname = "purchase order";
            string pono = e.CommandArgument.ToString();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?pono=" + pono + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }
    protected void gvpo_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void btnpo_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/PurchaseOrder.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
}