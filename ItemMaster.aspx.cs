﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ItemMaster : System.Web.UI.Page
{
    ForItemMaster fimclass = new ForItemMaster();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "update")
            {
                fillewaybillunitdrop();
                filleditdata();
            }
            else
            {
                fillewaybillunitdrop();
                txtuser.Text = Request.Cookies["ForLogin"]["username"];
            }
        }
    }

    public void fillewaybillunitdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fimclass.selectewaybillunit(li);
        if (dtdata.Rows.Count > 0)
        {
            drpunitewaybill.DataSource = dtdata;
            drpunitewaybill.DataTextField = "name";
            drpunitewaybill.DataBind();
            drpunitewaybill.Items.Insert(0,"--SELECT--");
        }
        else
        {
            drpunitewaybill.Items.Clear();
            drpunitewaybill.Items.Insert(0, "--SELECT--");
        }
    }

    public void filleditdata()
    {
        li.id = Convert.ToInt64(Request["id"].ToString());
        DataTable dtdata = new DataTable();
        dtdata = fimclass.selectitemdatafromid(li);
        if (dtdata.Rows.Count > 0)
        {
            txtitemname.Text = dtdata.Rows[0]["itemname"].ToString();
            ViewState["name"] = dtdata.Rows[0]["itemname"].ToString();
            txtdescription.Text = dtdata.Rows[0]["description"].ToString();
            txtunit.Text = dtdata.Rows[0]["unit"].ToString();
            drpunitewaybill.SelectedValue = dtdata.Rows[0]["unitewaybill"].ToString();
            txthsncode.Text = dtdata.Rows[0]["hsncode"].ToString();
            ViewState["hsncode"] = dtdata.Rows[0]["hsncode"].ToString();
            txtsalesrate.Text = dtdata.Rows[0]["salesrate"].ToString();
            txtpurchaserate.Text = dtdata.Rows[0]["purchaserate"].ToString();
            txtvaluationrate.Text = dtdata.Rows[0]["valuationrate"].ToString();
            txtvattyppe.Text = dtdata.Rows[0]["vattype"].ToString();
            txtgsttype.Text = dtdata.Rows[0]["gsttype"].ToString();
            txtvatdesc.Text = dtdata.Rows[0]["vatdesc"].ToString();
            txtgstdesc.Text = dtdata.Rows[0]["gstdesc"].ToString();
            txtrreason.Text = dtdata.Rows[0]["reason"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            if (dtdata.Rows[0]["isblock"].ToString() == "Y")
            {
                chkblock.Checked = true;
            }
            ViewState["id"] = dtdata.Rows[0]["id"].ToString();
            fillgrid();
            DataTable dtfillop = new DataTable();
            dtfillop = fimclass.selectopitembalancedata(li);
            if (dtfillop.Rows.Count > 0)
            {
                txtopeningstock.Text = dtfillop.Rows[0]["opqty"].ToString();
                txtopstockvalue.Text = dtfillop.Rows[0]["opamount"].ToString();
                txtinward.Text = dtfillop.Rows[0]["totp"].ToString();
                txtoutward.Text = dtfillop.Rows[0]["tots"].ToString();
                txtclosingstock.Text = dtfillop.Rows[0]["clqty"].ToString();
                txtclstockvalue.Text = "remain";
            }
            else
            {
                txtopeningstock.Text = "0";
                txtopstockvalue.Text = "0";
                txtinward.Text = "0";
                txtoutward.Text = "0";
                txtclosingstock.Text = "0";
                txtclstockvalue.Text = "remain";
            }
            btnsaves.Text = "Update";
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getvattype(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallvattype(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        li.itemname = txtitemname.Text;
        li.description = txtdescription.Text;
        li.unit = txtunit.Text;
        li.unitewaybill = drpunitewaybill.SelectedItem.Text;
        var cc1 = li.unitewaybill.IndexOf("-");
        if (cc1 != -1)
        {
            li.unitewaybill1 = li.unitewaybill.Split('-')[0];
        }
        else
        {
        }
        li.hsncode = txthsncode.Text;
        if (txtsalesrate.Text.Trim() != string.Empty)
        {
            li.salesrate = Convert.ToDouble(txtsalesrate.Text);
        }
        else
        {
            li.salesrate = 0;
        }
        if (txtpurchaserate.Text.Trim() != string.Empty)
        {
            li.purchaserate = Convert.ToDouble(txtpurchaserate.Text);
        }
        else
        {
            li.purchaserate = 0;
        }
        if (txtvaluationrate.Text.Trim() != string.Empty)
        {
            li.valuationrate = Convert.ToDouble(txtvaluationrate.Text);
        }
        else
        {
            li.valuationrate = 0;
        }
        li.vattype = txtvattyppe.Text;
        li.gsttype = txtgsttype.Text;
        li.vatdesc = txtvatdesc.Text;
        li.gstdesc = txtgstdesc.Text;
        li.acyear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
        try
        {
            if (txtvattyppe.Text != string.Empty)
            {
                var cc = txtvattyppe.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.typename = txtvattyppe.Text;
                    double vatp = Convert.ToDouble(txtvattyppe.Text.Split('-')[0]);
                    double addvatp = Convert.ToDouble(txtvattyppe.Text.Split('-')[1]);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('VAT Type Data Wrong.Please change that and try again.');", true);
            return;
        }

        li.oprate = 0;
        li.qty = 0;
        li.opqty = 0;
        if (txtopeningstock.Text.Trim() != string.Empty)
        {
            li.opqty = Convert.ToDouble(txtopeningstock.Text);
        }
        li.rate = 0;
        li.amount = 0;
        li.reason = txtrreason.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        li.isblock = "N";
        if (chkblock.Checked == true)
        {
            li.isblock = "Y";
        }
        if (btnsaves.Text == "Save")
        {
            DataTable dtcheck = new DataTable();
            dtcheck = fimclass.checkduplicate(li);
            if (dtcheck.Rows.Count == 0)
            {
                fimclass.insertitemdata(li);
                li.activity = li.itemname + " New Item Inserted.";
                faclass.insertactivity(li);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name already exists.Please try again with new Item Name.');", true);
                return;
            }
        }
        else
        {
            if (txtitemname.Text.Trim() != ViewState["name"].ToString().Trim())
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtcheck = new DataTable();
                dtcheck = fimclass.checkduplicate(li);
                if (dtcheck.Rows.Count == 0)
                {
                    li.id = Convert.ToInt64(ViewState["id"].ToString());
                    fimclass.updateitemdata(li);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.itemname + " Item Data Updated.";
                    faclass.insertactivity(li);
                    //update itemname in all software
                    li.itemname1 = txtitemname.Text.Trim();
                    li.itemname = ViewState["name"].ToString().Trim();
                    fimclass.updateItemMaster1(li);
                    fimclass.updatePCItems(li);
                    fimclass.updatePerInvItems(li);
                    fimclass.updateCommInvItems(li);
                    fimclass.updatePIItems(li);
                    fimclass.updatePurchaseOrderItems(li);
                    fimclass.updateQuoItems(li);
                    fimclass.updaterritems(li);
                    fimclass.updateSalesOrderItems(li);
                    fimclass.updateSCItems(li);
                    fimclass.updateSIItems(li);
                    fimclass.updateStockJVItems(li);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Name already exists.Please try again with new Item Name.');", true);
                    return;
                }
            }
            else
            {
                li.id = Convert.ToInt64(ViewState["id"].ToString());
                fimclass.updateitemdata(li);
            }
            //if (txthsncode.Text.Trim() != ViewState["hsncode"].ToString().Trim())
            //{
            //    li.hsncode = ViewState["hsncode"].ToString();
            //    li.hsncode1 = txthsncode.Text;
            //    fimclass.updatehsncodeinledger_e(li);
            //}
        }
        Response.Redirect("~/ItemMasterList.aspx?pagename=ItemMasterList");
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.itemname = txtitemname.Text;
        DataTable dtitemop = new DataTable();
        dtitemop = fimclass.selectopbalancedata(li);
        if (dtitemop.Rows.Count > 0)
        {
            lblemptyop.Visible = false;
            gvitemop.Visible = true;
            gvitemop.DataSource = dtitemop;
            gvitemop.DataBind();
            double totqty = 0;
            double totamt = 0;
            for (int c = 0; c < dtitemop.Rows.Count; c++)
            {
                totqty = totqty + Convert.ToDouble(dtitemop.Rows[c]["qty"].ToString());
                totamt = totamt + Convert.ToDouble(dtitemop.Rows[c]["amount"].ToString());
            }
            lbltotopqty.Text = totqty.ToString();
            lbltotopamount.Text = totamt.ToString();
        }
        else
        {
            gvitemop.Visible = false;
            lblemptyop.Visible = true;
            lblemptyop.Text = "No Item Opening Balance Data Found.";
            lbltotopqty.Text = "0";
            lbltotopamount.Text = "0";
        }
    }

    protected void gvitemop_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            li.id = Convert.ToInt64(e.CommandArgument);
            fimclass.deleteopitemdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.id + " Item Opening balance has been Deleted.";
            faclass.insertactivity(li);
            fillgrid();
            DataTable dtfillop = new DataTable();
            dtfillop = fimclass.selectopitembalancedata(li);
            if (dtfillop.Rows.Count > 0)
            {
                txtopeningstock.Text = dtfillop.Rows[0]["opqty"].ToString();
                txtopstockvalue.Text = dtfillop.Rows[0]["opamount"].ToString();
                txtinward.Text = dtfillop.Rows[0]["tots"].ToString();
                txtoutward.Text = dtfillop.Rows[0]["totp"].ToString();
                txtclosingstock.Text = dtfillop.Rows[0]["clqty"].ToString();
                txtclstockvalue.Text = "remain";
            }
            else
            {
                txtopeningstock.Text = "0";
                txtopstockvalue.Text = "0";
                txtinward.Text = "0";
                txtoutward.Text = "0";
                txtclosingstock.Text = "0";
                txtclstockvalue.Text = "remain";
            }
        }
    }
    protected void gvitemop_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void btnopitem_Click(object sender, EventArgs e)
    {
        li.itemname = txtitemname.Text;
        li.opdate = Convert.ToDateTime(txtopdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.qty = Convert.ToDouble(txtopqty.Text);
        li.oprate = Convert.ToDouble(txtoprate.Text);
        li.amount = Convert.ToDouble(txtopamount.Text);
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        li.adjqty = 0;
        li.acyear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
        fimclass.insertopitemdata(li);
        li.activity = li.oprate + " New Item Opening Balance has been Inserted.";
        faclass.insertactivity(li);
        fillgrid();
        DataTable dtfillop = new DataTable();
        dtfillop = fimclass.selectopitembalancedata(li);
        if (dtfillop.Rows.Count > 0)
        {
            txtopeningstock.Text = dtfillop.Rows[0]["opqty"].ToString();
            txtopstockvalue.Text = dtfillop.Rows[0]["opamount"].ToString();
            txtinward.Text = dtfillop.Rows[0]["tots"].ToString();
            txtoutward.Text = dtfillop.Rows[0]["totp"].ToString();
            txtclosingstock.Text = dtfillop.Rows[0]["clqty"].ToString();
            txtclstockvalue.Text = "remain";
        }
        else
        {
            txtopeningstock.Text = "0";
            txtopstockvalue.Text = "0";
            txtinward.Text = "0";
            txtoutward.Text = "0";
            txtclosingstock.Text = "0";
            txtclstockvalue.Text = "remain";
        }
        txtopdate.Text = string.Empty;
        txtopqty.Text = string.Empty;
        txtoprate.Text = string.Empty;
        txtopamount.Text = string.Empty;
    }
    protected void txtopqty_TextChanged(object sender, EventArgs e)
    {
        calculation();
        Page.SetFocus(txtoprate);
    }
    protected void txtoprate_TextChanged(object sender, EventArgs e)
    {
        calculation();
        Page.SetFocus(txtopamount);
    }
    public void calculation()
    {
        double qty = 0;
        double amt = 0;
        if (txtopqty.Text != string.Empty)
        {
            qty = Convert.ToDouble(txtopqty.Text);
        }
        if (txtoprate.Text != string.Empty)
        {
            amt = Convert.ToDouble(txtoprate.Text);
        }
        txtopamount.Text = Math.Round((qty * amt), 2).ToString();
    }
}