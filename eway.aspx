﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="eway.aspx.cs" Inherits="eway" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="bootstrap/js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="bootstrap/css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="bootstrap/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="js/Jquery-ui.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <%--style="zoom:80%;"--%>
        <span style="color: white; background-color: Red">E-Way Bill Details</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    E-Way Bill No.</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtewaybillno" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Date</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtdate1" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Consolidated e-Way Bill No.</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtconsolidatedewaybillno" runat="server" CssClass="form-control"
                    Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Date</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="date2" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Sub Type</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtsubtype" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Document Type</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtdocumenttype" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <span style="color: white; background-color: Red; font-weight: bold; border-bottom-style: dashed">
            Transporter Details</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Mode</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtmode" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Distance(In Km)*</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtdistance" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Transporter Name</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txttransportername" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Vehicle Number</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtvehiclenumber" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Doc/Landing/RR/AirWay No.</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtdoclandingrrairwayno" runat="server" CssClass="form-control"
                    Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Date</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Transporter ID</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txttransporterid" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <span style="color: white; background-color: Red; font-weight: bold; border-bottom-style: dashed">
            Consignor Details(From)</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Consignor</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtconsignor" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Address1</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtaddress1" runat="server" CssClass="form-control" Width="150px"
                    TextMode="MultiLine" Height="50px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Address2</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtaddress2" runat="server" CssClass="form-control" Width="150px"
                    TextMode="MultiLine" Height="50px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Place</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtplace" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Pincode</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtpincode" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    State</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtstate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    GSTIN/UIN</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtgstinuin" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <span style="color: white; background-color: Red; font-weight: bold; border-bottom-style: dashed">
            Consignee Details(To)</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Consignee</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtconsignee" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Address1</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtconsigneeaddress1" runat="server" CssClass="form-control" Width="150px"
                    TextMode="MultiLine" Height="50px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Address2</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtconsigneeaddress2" runat="server" CssClass="form-control" Width="150px"
                    TextMode="MultiLine" Height="50px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    To Place(Destination)</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txttoplace" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Pincode</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtconsigneepincode" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    State</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtconsigneestate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    GSTIN/UIN</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtconsigneegstinuin" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
    </div>
    </form>
</body>
</html>
