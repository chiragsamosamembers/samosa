﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class SalesmanMaster : System.Web.UI.Page
{
    ForSalesmanMaster fsmclass = new ForSalesmanMaster();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "update")
            {
                filleditdata();
            }
            else
            {
                txtuser.Text = Request.Cookies["ForLogin"]["username"];
            }
        }
    }

    public void filleditdata()
    {
        li.id = Convert.ToInt64(Request["id"].ToString());
        DataTable dtdata = new DataTable();
        dtdata = fsmclass.selectsalesmandatafromid(li);
        if (dtdata.Rows.Count > 0)
        {
            txtcode.Text = dtdata.Rows[0]["id"].ToString();
            txtname.Text = dtdata.Rows[0]["name"].ToString();
            ViewState["name"] = dtdata.Rows[0]["name"].ToString();
            txtmobile.Text = dtdata.Rows[0]["mobileno"].ToString();
            txtemail.Text = dtdata.Rows[0]["emailid"].ToString();
            txtaddress.Text = dtdata.Rows[0]["address"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            ViewState["id"] = dtdata.Rows[0]["id"].ToString();
            btnsaves.Text = "Update";
        }
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        li.name = txtname.Text;
        li.mobileno = txtmobile.Text;
        li.emailid = txtemail.Text;
        li.address = txtaddress.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        if (btnsaves.Text == "Save")
        {
            DataTable dtcheck = new DataTable();
            dtcheck = fsmclass.checkduplicate(li);
            if (dtcheck.Rows.Count == 0)
            {
                fsmclass.insertsalesmandata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.name + "Salesman inserted.";
                faclass.insertactivity(li);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Salesman Name already exists.Please try again with new Salesman Name.');", true);
                return;
            }
        }
        else
        {
            if (txtname.Text.Trim() != ViewState["name"].ToString().Trim())
            {
                DataTable dtcheck = new DataTable();
                dtcheck = fsmclass.checkduplicate(li);
                if (dtcheck.Rows.Count == 0)
                {
                    li.id = Convert.ToInt64(ViewState["id"].ToString());
                    fsmclass.updatesalesmandata(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = "Salesman Updated.";
                    faclass.insertactivity(li);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Salesman Name already exists.Please try again with new Salesman Name.');", true);
                    return;
                }
            }
            else
            {
                li.id = Convert.ToInt64(ViewState["id"].ToString());
                fsmclass.updatesalesmandata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = "Salesman Updated.";
                faclass.insertactivity(li);
            }
        }
        Response.Redirect("~/SalesmanMasterList.aspx?pagename=SalesmanMasterList");
    }
}