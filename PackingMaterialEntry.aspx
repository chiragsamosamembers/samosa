﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PackingMaterialEntry.aspx.cs" Inherits="PackingMaterialEntry" Culture="hi-IN"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Tools Return Entry</span><br />
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Voucher No.</label>
                            </div>
                            <div class="col-md-2" style="width: 21.466667% !important">
                                <asp:TextBox ID="txtvoucherno" runat="server" CssClass="form-control" Width="150px"
                                    ReadOnly="true" placeholder="Auto"></asp:TextBox></div>
                            <div class="col-md-1">
                                <label class="control-label">
                                    Date<span style="color: Red; font-size: 20px;">*</span></label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtvdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                            <div class="col-md-1" style="width: 11.73333333% !important">
                                <label class="control-label">
                                    User</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" ReadOnly="true"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Name<span style="color: Red; font-size: 20px;">*</span></label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="432px"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtname"
                                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                    ServiceMethod="Getacname">
                                </asp:AutoCompleteExtender>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">
                                    Client Code<span style="color: Red; font-size: 20px;">*</span></label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" Width="150px"
                                    AutoPostBack="true" OnTextChanged="txtname_TextChanged"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtclientcode"
                                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                    ServiceMethod="GetClientname">
                                </asp:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Delivery Challn No.</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtdcno" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Returnable Material Challan No.</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtreturnablematerialchallanno" runat="server" CssClass="form-control"
                                    Width="150px"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtreturnablematerialchallanno"
                                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                    ServiceMethod="Getrrcno">
                                </asp:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Tools Delivery Challan No.</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txttoolsdeliverychallanno" runat="server" CssClass="form-control"
                                    Width="150px"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txttoolsdeliverychallanno"
                                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                    ServiceMethod="Getdelcno">
                                </asp:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Transport Name</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txttransportname" runat="server" CssClass="form-control" Width="432px"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    LR No.</label>
                            </div>
                            <div class="col-md-2" style="width: 21.466667% !important">
                                <asp:TextBox ID="txtlrno" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                            <div class="col-md-1">
                                <label class="control-label">
                                    Date</label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtlrdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Vehicle No.</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtvehicleno" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Driver Name</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtdrivername" runat="server" CssClass="form-control" Width="432px"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Driver License No.</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtdriverlicenseno" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Driver Mobile No.</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtdrivermobileno" runat="server" CssClass="form-control" Width="150px" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <asp:Button ID="btnsave" runat="server" Text="Save" class="btn btn-default forbutton"
                                    OnClick="btnsave_Click"  ValidationGroup="valtool"/>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                    ShowSummary="false" ValidationGroup="valtool" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter date."
                                    Text="*" ValidationGroup="valtool" ControlToValidate="txtvdate"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter name."
                                    Text="*" ValidationGroup="valtool" ControlToValidate="txtname"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter client code."
                                    Text="*" ValidationGroup="valtool" ControlToValidate="txtclientcode"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtlrdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtvdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtlrdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtlrdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
