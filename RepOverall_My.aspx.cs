﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class RepOverall : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    //protected void txtccode_TextChanged(object sender, EventArgs e)
    //{
    //    if (txtacname.Text.Trim() != string.Empty)
    //    {
    //        string cc = txtacname.Text;
    //        //txtclientcode.Text = cc.Split('-')[1];
    //        txtacname.Text = cc.Split('-')[0];
    //        //SessionMgt.FirstName = cc.Split('-')[1];
    //    }
    //    else
    //    {
    //        //txtname.Text = string.Empty;
    //        txtacname.Text = string.Empty;
    //        //SessionMgt.FirstName = string.Empty;
    //    }
    //    Page.SetFocus(txtfromdate);
    //}

    protected void btnpreview_Click(object sender, EventArgs e)
    {
        if (drptype.SelectedItem.Text == "Simple" && RadioButtonList1.SelectedItem.Text == "Excel")
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('You can not export Simple report in excel.');", true);
            return;
        }
        //string Xrepname = "Sales Outstanding Report";
        //string Xrepname = "Overall Report with no";
        //string Xrepname = "Overall Report";
        string Xrepname = drptype.SelectedValue;
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        if (txtacname.Text.IndexOf("-") > 0)
        {
            li.acname = txtacname.Text.Split('-')[0];
            li.name = txtacname.Text.Split('-')[1];
            //Int64 vno = Convert.ToInt64(txtvono.Text);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?acname=" + li.acname + "&cname=" + li.name + "&fromdate=" + li.fromdate + "&todate=" + li.todate + "&rtype=" + RadioButtonList1.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }

    protected void btnoverall1_Click(object sender, EventArgs e)
    {
        string Xrepname = "overall cid challanwise report";
        if (txtccode1.Text.IndexOf("-") > 0)
        {
            li.acname = txtccode1.Text.Split('-')[0];
            li.name = txtccode1.Text.Split('-')[1];
            //Int64 vno = Convert.ToInt64(txtvono.Text);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?acname=" + li.acname + "&cname=" + li.name + "&rtype=" + rdotype1.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }

    protected void btnprocessdata_Click(object sender, EventArgs e)
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connma"].ConnectionString);
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select distinct(acyear) from acyear where acyear='2017-2018'", con);
        DataTable dtacyear = new DataTable();
        da.Fill(dtacyear);
        for (int a = 0; a < dtacyear.Rows.Count; a++)
        {
            string dbname = "AT" + dtacyear.Rows[a]["acyear"].ToString().Split('-')[0] + dtacyear.Rows[a]["acyear"].ToString().Split('-')[1];
            DataTable dtdt = new DataTable();
            SqlDataAdapter da1 = new SqlDataAdapter("select * FROM master.dbo.sysdatabases where name ='" + dbname + "'", con);
            da1.Fill(dtdt);
            con.Close();
            if (dtdt.Rows.Count > 0)
            {
                li.ccode = Convert.ToInt64(txtccode1.Text);
                SqlConnection con111 = new SqlConnection("Data Source=CHIRAG-PC;Initial Catalog=" + dbname + ";Integrated Security=True");
                li.acyear = dtacyear.Rows[a]["acyear"].ToString();
                SqlDataAdapter da2 = new SqlDataAdapter("select * from SCItems inner join SCMaster on SCItems.strscno=SCMaster.strscno where SCItems.ccode=" + li.ccode + " and SCMaster.cno=" + li.cno + " and SCMaster.ctype<>'RM' and SCItems.vid<>1", con111);
                DataTable dtsc = new DataTable();
                da2.Fill(dtsc);
                //if (dtsc.Rows.Count > 0)
                //{
                for (int b = 0; b < dtsc.Rows.Count; b++)
                {
                    li.scid = Convert.ToInt64(dtsc.Rows[b]["vid"].ToString());
                    SqlDataAdapter daso = new SqlDataAdapter("select * from SalesOrderItems where id=" + li.scid + "", con111);
                    DataTable dtso = new DataTable();
                    daso.Fill(dtso);
                    if (dtso.Rows.Count > 0)
                    {
                        li.id = Convert.ToInt64(dtso.Rows[0]["id"].ToString());
                        li.sono = Convert.ToInt64(dtso.Rows[0]["sono"].ToString());
                        li.sodate = Convert.ToDateTime(dtso.Rows[0]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        li.itemname = dtso.Rows[0]["itemname"].ToString();
                        if (li.itemname == "Fusible Link")
                        {
                            string dd = "";
                        }
                        li.descr1 = dtso.Rows[0]["descr1"].ToString();
                        li.descr2 = dtso.Rows[0]["descr2"].ToString();
                        li.descr3 = dtso.Rows[0]["descr3"].ToString();
                        li.unit = dtso.Rows[0]["unit"].ToString();
                        li.qty = Convert.ToDouble(dtso.Rows[0]["qty"].ToString());
                        li.rate = Convert.ToDouble(dtso.Rows[0]["rate"].ToString());
                        li.type = "Ordered";
                    }
                    else
                    {
                        li.id = Convert.ToInt64(dtsc.Rows[b]["id"].ToString());
                        li.sono = 0;
                        li.sodate = Convert.ToDateTime(System.DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        li.itemname = dtsc.Rows[b]["itemname"].ToString();
                        if (li.itemname == "Fusible Link")
                        {
                            string dd = "";
                        }
                        li.descr1 = dtsc.Rows[b]["descr1"].ToString();
                        li.descr2 = dtsc.Rows[b]["descr2"].ToString();
                        li.descr3 = dtsc.Rows[b]["descr3"].ToString();
                        li.unit = dtsc.Rows[b]["unit"].ToString();
                        li.qty = 0;
                        li.rate = 0;
                        li.type = "Non-Ordered";
                    }
                    string nodetail = "";
                    double rfs = 0;
                    string remarks = "";
                    li.qty1 = 0;
                    li.rate1 = 0;
                    li.id = Convert.ToInt64(dtsc.Rows[b]["id"].ToString());
                    SqlDataAdapter da4 = new SqlDataAdapter("select * from SCItems where id=" + li.id + "", con111);
                    DataTable dt4 = new DataTable();
                    da4.Fill(dt4);
                    if (dt4.Rows.Count > 0)
                    {
                        //for (int z = 0; z < dt4.Rows.Count; z++)
                        //{

                        SqlDataAdapter da41 = new SqlDataAdapter("select isnull(sum(qty),0) as totscq,isnull(sum(basicamount),0) as totsc from SCItems where id=" + li.id + "", con111);
                        DataTable dt41 = new DataTable();
                        da41.Fill(dt41);

                        nodetail = nodetail + dt4.Rows[0]["strscno"].ToString() + "-" + dt4.Rows[0]["qty"].ToString();
                        li.qty1 = Convert.ToDouble(dtsc.Rows[0]["qty"].ToString());
                        li.rate1 = Convert.ToDouble(dtsc.Rows[0]["basicamount"].ToString());
                        li.vid = Convert.ToInt64(dtsc.Rows[b]["id"].ToString());
                        SqlDataAdapter da5 = new SqlDataAdapter("select * from SIItems where vid=" + li.vid + "", con111);
                        DataTable dt5 = new DataTable();
                        da5.Fill(dt5);
                        if (dt5.Rows.Count > 0)
                        {
                            SqlDataAdapter da51 = new SqlDataAdapter("select isnull(sum(qty),0) as totsiq,isnull(sum(basicamount),0) as totsi from SIItems where vid=" + li.vid + "", con111);
                            DataTable dt51 = new DataTable();
                            da51.Fill(dt51);
                            nodetail = nodetail + "( " + dt5.Rows[0]["strsino"].ToString() + " )\n";
                            li.qty1 = Convert.ToDouble(dt51.Rows[0]["totsiq"].ToString());
                            li.rate1 = Convert.ToDouble(dt51.Rows[0]["totsi"].ToString());
                        }
                        SqlDataAdapter dafg = new SqlDataAdapter("select isnull(sum(qty),0) as totqty from rritems where vid=@id", con);
                        dafg.SelectCommand.Parameters.AddWithValue("@id", li.vid);
                        DataTable dtfg = new DataTable();
                        dafg.Fill(dtfg);
                        if (dtfg.Rows.Count > 0)
                        {
                            rfs = rfs + Convert.ToDouble(dtfg.Rows[0]["totqty"].ToString());
                        }

                        SqlDataAdapter dafg1 = new SqlDataAdapter("select retremarks from rritems where vid=@id", con);
                        dafg1.SelectCommand.Parameters.AddWithValue("@id", li.vid);
                        DataTable dtfg1 = new DataTable();
                        dafg1.Fill(dtfg1);
                        for (int ff = 0; ff < dtfg1.Rows.Count; ff++)
                        {
                            remarks = remarks + dtfg1.Rows[ff]["retremarks"].ToString() + ",";
                        }
                        //li.type = "Ordered";
                        //SqlCommand cmd1 = new SqlCommand("update tempoverall set qty1=@qty1,rate1=@rate1,nodetail=@nodetail,rfs=@rfs,remarks=@remarks,type=@type where id=@id", con);
                        //cmd1.Parameters.AddWithValue("@id", li.id);
                        //cmd1.Parameters.AddWithValue("@qty1", li.qty1);
                        //cmd1.Parameters.AddWithValue("@rate1", li.rate1);
                        //cmd1.Parameters.AddWithValue("@nodetail", nodetail);
                        //cmd1.Parameters.AddWithValue("@rfs", rfs);
                        //cmd1.Parameters.AddWithValue("@remarks", remarks);
                        //cmd1.Parameters.AddWithValue("@type", li.type);
                        //con.Open();
                        //cmd1.ExecuteNonQuery();
                        //con.Close();

                        //}
                    }
                    SqlCommand cmd = new SqlCommand("insert into tempoverall (id,sono,sodate,itemname,descr1,descr2,descr3,unit,qty,rate,qty1,rfs,type,rate1,nodetail,remarks,acyear) values (@id,@sono,@sodate,@itemname,@descr1,@descr2,@descr3,@unit,@qty,@rate,@qty1,@rfs,@type,@rate1,@nodetail,@remarks,@acyear)", con);
                    cmd.Parameters.AddWithValue("@id", li.id);
                    cmd.Parameters.AddWithValue("@sono", li.sono);
                    cmd.Parameters.AddWithValue("@sodate", li.sodate);
                    cmd.Parameters.AddWithValue("@itemname", li.itemname);
                    cmd.Parameters.AddWithValue("@descr1", li.descr1);
                    cmd.Parameters.AddWithValue("@descr2", li.descr2);
                    cmd.Parameters.AddWithValue("@descr3", li.descr3);
                    cmd.Parameters.AddWithValue("@unit", li.unit);
                    cmd.Parameters.AddWithValue("@qty", li.qty);
                    cmd.Parameters.AddWithValue("@rate", li.rate);
                    cmd.Parameters.AddWithValue("@type", li.type);
                    cmd.Parameters.AddWithValue("@acyear", li.acyear);

                    cmd.Parameters.AddWithValue("@qty1", li.qty1);
                    cmd.Parameters.AddWithValue("@rfs", li.rfs);
                    cmd.Parameters.AddWithValue("@rate1", li.rate1);
                    cmd.Parameters.AddWithValue("@nodetail", nodetail);
                    cmd.Parameters.AddWithValue("@remarks", remarks);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }

                da2 = new SqlDataAdapter("select * from SCItems inner join SCMaster on SCItems.strscno=SCMaster.strscno where SCItems.ccode=" + li.ccode + " and SCMaster.cno=" + li.cno + " and SCMaster.ctype<>'RM' and SCItems.vid=1", con111);
                dtsc = new DataTable();
                da2.Fill(dtsc);
                //if (dtsc.Rows.Count > 0)
                //{
                for (int b = 0; b < dtsc.Rows.Count; b++)
                {
                    li.scid = Convert.ToInt64(dtsc.Rows[b]["vid"].ToString());
                    li.id = Convert.ToInt64(dtsc.Rows[b]["id"].ToString());
                    li.sono = 0;
                    li.sodate = Convert.ToDateTime(System.DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    li.itemname = dtsc.Rows[b]["itemname"].ToString();
                    if (li.itemname == "Fusible Link")
                    {
                        string dd = "";
                    }
                    li.descr1 = dtsc.Rows[b]["descr1"].ToString();
                    li.descr2 = dtsc.Rows[b]["descr2"].ToString();
                    li.descr3 = dtsc.Rows[b]["descr3"].ToString();
                    li.unit = dtsc.Rows[b]["unit"].ToString();
                    li.qty = 0;
                    li.rate = 0;
                    li.type = "Non-Ordered";
                    string nodetail = "";
                    double rfs = 0;
                    string remarks = "";
                    li.qty1 = 0;
                    li.rate1 = 0;
                    li.id = Convert.ToInt64(dtsc.Rows[b]["id"].ToString());
                    SqlDataAdapter da4 = new SqlDataAdapter("select * from SCItems where id=" + li.id + "", con111);
                    DataTable dt4 = new DataTable();
                    da4.Fill(dt4);
                    if (dt4.Rows.Count > 0)
                    {
                        //for (int z = 0; z < dt4.Rows.Count; z++)
                        //{

                        SqlDataAdapter da41 = new SqlDataAdapter("select isnull(sum(qty),0) as totscq,isnull(sum(basicamount),0) as totsc from SCItems where id=" + li.id + "", con111);
                        DataTable dt41 = new DataTable();
                        da41.Fill(dt41);

                        nodetail = nodetail + dt4.Rows[0]["strscno"].ToString() + "-" + dt4.Rows[0]["qty"].ToString();
                        li.qty1 = Convert.ToDouble(dtsc.Rows[0]["qty"].ToString());
                        li.rate1 = Convert.ToDouble(dtsc.Rows[0]["basicamount"].ToString());
                        li.vid = Convert.ToInt64(dtsc.Rows[b]["id"].ToString());
                        SqlDataAdapter da5 = new SqlDataAdapter("select * from SIItems where vid=" + li.vid + "", con111);
                        DataTable dt5 = new DataTable();
                        da5.Fill(dt5);
                        if (dt5.Rows.Count > 0)
                        {
                            SqlDataAdapter da51 = new SqlDataAdapter("select isnull(sum(qty),0) as totsiq,isnull(sum(basicamount),0) as totsi from SIItems where vid=" + li.vid + "", con111);
                            DataTable dt51 = new DataTable();
                            da51.Fill(dt51);
                            nodetail = nodetail + "( " + dt5.Rows[0]["strsino"].ToString() + " )\n";
                            li.qty1 = Convert.ToDouble(dt51.Rows[0]["totsiq"].ToString());
                            li.rate1 = Convert.ToDouble(dt51.Rows[0]["totsi"].ToString());
                        }
                        SqlDataAdapter dafg = new SqlDataAdapter("select isnull(sum(qty),0) as totqty from rritems where vid=@id", con);
                        dafg.SelectCommand.Parameters.AddWithValue("@id", li.vid);
                        DataTable dtfg = new DataTable();
                        dafg.Fill(dtfg);
                        if (dtfg.Rows.Count > 0)
                        {
                            rfs = rfs + Convert.ToDouble(dtfg.Rows[0]["totqty"].ToString());
                        }

                        SqlDataAdapter dafg1 = new SqlDataAdapter("select retremarks from rritems where vid=@id", con);
                        dafg1.SelectCommand.Parameters.AddWithValue("@id", li.vid);
                        DataTable dtfg1 = new DataTable();
                        dafg1.Fill(dtfg1);
                        for (int ff = 0; ff < dtfg1.Rows.Count; ff++)
                        {
                            remarks = remarks + dtfg1.Rows[ff]["retremarks"].ToString() + ",";
                        }
                        //li.type = "Ordered";
                        //SqlCommand cmd1 = new SqlCommand("update tempoverall set qty1=@qty1,rate1=@rate1,nodetail=@nodetail,rfs=@rfs,remarks=@remarks,type=@type where id=@id", con);
                        //cmd1.Parameters.AddWithValue("@id", li.id);
                        //cmd1.Parameters.AddWithValue("@qty1", li.qty1);
                        //cmd1.Parameters.AddWithValue("@rate1", li.rate1);
                        //cmd1.Parameters.AddWithValue("@nodetail", nodetail);
                        //cmd1.Parameters.AddWithValue("@rfs", rfs);
                        //cmd1.Parameters.AddWithValue("@remarks", remarks);
                        //cmd1.Parameters.AddWithValue("@type", li.type);
                        //con.Open();
                        //cmd1.ExecuteNonQuery();
                        //con.Close();

                        //}
                    }
                    SqlCommand cmd = new SqlCommand("insert into tempoverall (id,sono,sodate,itemname,descr1,descr2,descr3,unit,qty,rate,qty1,rfs,type,rate1,nodetail,remarks,acyear) values (@id,@sono,@sodate,@itemname,@descr1,@descr2,@descr3,@unit,@qty,@rate,@qty1,@rfs,@type,@rate1,@nodetail,@remarks,@acyear)", con);
                    cmd.Parameters.AddWithValue("@id", li.id);
                    cmd.Parameters.AddWithValue("@sono", li.sono);
                    cmd.Parameters.AddWithValue("@sodate", li.sodate);
                    cmd.Parameters.AddWithValue("@itemname", li.itemname);
                    cmd.Parameters.AddWithValue("@descr1", li.descr1);
                    cmd.Parameters.AddWithValue("@descr2", li.descr2);
                    cmd.Parameters.AddWithValue("@descr3", li.descr3);
                    cmd.Parameters.AddWithValue("@unit", li.unit);
                    cmd.Parameters.AddWithValue("@qty", li.qty);
                    cmd.Parameters.AddWithValue("@rate", li.rate);
                    cmd.Parameters.AddWithValue("@type", li.type);
                    cmd.Parameters.AddWithValue("@acyear", li.acyear);

                    cmd.Parameters.AddWithValue("@qty1", li.qty1);
                    cmd.Parameters.AddWithValue("@rfs", li.rfs);
                    cmd.Parameters.AddWithValue("@rate1", li.rate1);
                    cmd.Parameters.AddWithValue("@nodetail", nodetail);
                    cmd.Parameters.AddWithValue("@remarks", remarks);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }


                //SqlDataAdapter da3 = new SqlDataAdapter("select * from tempoverall where acyear='" + li.acyear + "'", con111);
                //DataTable dt3 = new DataTable();
                //da3.Fill(dt3);
                //for (int c = 0; c < dt3.Rows.Count; c++)
                //{
                //    string nodetail = "";
                //    double rfs = 0;
                //    string remarks = "";
                //    li.qty1 = 0;
                //    li.rate1 = 0;
                //    li.id = Convert.ToInt64(dt3.Rows[c]["id"].ToString());
                //    if (li.id == 2331)
                //    {
                //        string cc = "";
                //    }


                //}
                //}
                //SqlDataAdapter da6 = new SqlDataAdapter("select SCItems.* from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno where scitems.vid not in (select SalesOrderItems.id from SCItems inner join SalesOrderItems on SalesOrderItems.id=SCItems.vid inner join SalesOrder on SalesOrder.sono=SalesOrderItems.sono where SalesOrder.ccode=" + li.ccode + ") and SCItems.ccode=" + li.ccode + " and scmaster.ctype<>'RM'", con111);
                //DataTable dt6 = new DataTable();
                //da6.Fill(dt6);
                //for (int d = 0; d < dt6.Rows.Count; d++)
                //{
                //    string nodetail = "";
                //    double rfs = 0;
                //    string remarks = "";
                //    li.id = 0;
                //    li.sono = 0;
                //    li.sodate = Convert.ToDateTime(dt6.Rows[d]["udate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //    li.itemname = dt6.Rows[d]["itemname"].ToString();
                //    li.descr1 = dt6.Rows[d]["descr1"].ToString();
                //    li.descr2 = dt6.Rows[d]["descr2"].ToString();
                //    li.descr3 = dt6.Rows[d]["descr3"].ToString();
                //    li.unit = dt6.Rows[d]["unit"].ToString();
                //    li.qty = 0;
                //    li.rate = 0;
                //    li.qty1 = 0;
                //    li.rate1 = 0;
                //    li.id = Convert.ToInt64(dt6.Rows[d]["id"].ToString());
                //    //SqlDataAdapter da41 = new SqlDataAdapter("select isnull(sum(qty),0) as totscq,isnull(sum(basicamount),0) as totsc from SCItems where vid=" + li.id + "", con111);
                //    //DataTable dt41 = new DataTable();
                //    //da41.Fill(dt41);
                //    //for (int z = 0; z < dt6.Rows.Count; z++)
                //    //{
                //    nodetail = nodetail + dt6.Rows[d]["strscno"].ToString() + "-" + dt6.Rows[d]["qty"].ToString();
                //    li.qty1 = Convert.ToDouble(dt6.Rows[0]["qty"].ToString());
                //    li.rate1 = Convert.ToDouble(dt6.Rows[0]["basicamount"].ToString());
                //    li.vid = Convert.ToInt64(dt6.Rows[d]["id"].ToString());
                //    SqlDataAdapter da5 = new SqlDataAdapter("select * from SIItems where vid=" + li.vid + "", con111);
                //    DataTable dt5 = new DataTable();
                //    da5.Fill(dt5);
                //    if (dt5.Rows.Count > 0)
                //    {
                //        SqlDataAdapter da51 = new SqlDataAdapter("select isnull(sum(qty),0) as totsiq,isnull(sum(basicamount),0) as totsi from SIItems where vid=" + li.vid + "", con111);
                //        DataTable dt51 = new DataTable();
                //        da51.Fill(dt51);
                //        nodetail = nodetail + "( " + dt5.Rows[0]["strsino"].ToString() + " )\n";
                //        li.qty1 = Convert.ToDouble(dt51.Rows[0]["totsiq"].ToString());
                //        li.rate1 = Convert.ToDouble(dt51.Rows[0]["totsi"].ToString());
                //    }
                //    SqlDataAdapter dafg = new SqlDataAdapter("select isnull(sum(qty),0) as totqty from rritems where vid=@id", con111);
                //    dafg.SelectCommand.Parameters.AddWithValue("@id", li.id);
                //    DataTable dtfg = new DataTable();
                //    dafg.Fill(dtfg);
                //    if (dtfg.Rows.Count > 0)
                //    {
                //        rfs = rfs + Convert.ToDouble(dtfg.Rows[0]["totqty"].ToString());
                //    }

                //    SqlDataAdapter dafg1 = new SqlDataAdapter("select retremarks from rritems where vid=@id", con111);
                //    dafg1.SelectCommand.Parameters.AddWithValue("@id", li.id);
                //    DataTable dtfg1 = new DataTable();
                //    dafg1.Fill(dtfg1);
                //    for (int ff = 0; ff < dtfg1.Rows.Count; ff++)
                //    {
                //        remarks = remarks + dtfg1.Rows[ff]["retremarks"].ToString() + ",";
                //    }
                //    //}
                //    li.type = "Non-Ordered";
                //    SqlDataAdapter daf = new SqlDataAdapter("select * from tempoverall order by id desc", con111);
                //    DataTable dtf = new DataTable();
                //    daf.Fill(dtf);
                //    li.id = Convert.ToInt64(dtf.Rows[0]["id"].ToString()) + 1;
                //    SqlCommand cmd = new SqlCommand("insert into tempoverall (id,sono,sodate,itemname,descr1,descr2,descr3,unit,qty,rate,qty1,rfs,type,rate1,nodetail,remarks,acyear) values (@id,@sono,@sodate,@itemname,@descr1,@descr2,@descr3,@unit,@qty,@rate,@qty1,@rfs,@type,@rate1,@nodetail,@remarks,@acyear)", con);
                //    cmd.Parameters.AddWithValue("@id", li.id);
                //    cmd.Parameters.AddWithValue("@sono", li.sono);
                //    cmd.Parameters.AddWithValue("@sodate", li.sodate);
                //    cmd.Parameters.AddWithValue("@itemname", li.itemname);
                //    cmd.Parameters.AddWithValue("@descr1", li.descr1);
                //    cmd.Parameters.AddWithValue("@descr2", li.descr2);
                //    cmd.Parameters.AddWithValue("@descr3", li.descr3);
                //    cmd.Parameters.AddWithValue("@unit", li.unit);
                //    cmd.Parameters.AddWithValue("@qty", li.qty);
                //    cmd.Parameters.AddWithValue("@rate", li.rate);
                //    cmd.Parameters.AddWithValue("@acyear", li.acyear);
                //    cmd.Parameters.AddWithValue("@qty1", li.qty1);
                //    cmd.Parameters.AddWithValue("@rfs", li.rfs);
                //    cmd.Parameters.AddWithValue("@type", li.type);
                //    cmd.Parameters.AddWithValue("@rate1", li.rate1);
                //    cmd.Parameters.AddWithValue("@nodetail", nodetail);
                //    cmd.Parameters.AddWithValue("@remarks", remarks);
                //    con.Open();
                //    cmd.ExecuteNonQuery();
                //    con.Close();


                //}
            }
        }
        btnpreview.Visible = true;
    }

    //protected void btnprocessdata_Click(object sender, EventArgs e)
    //{
    //    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
    //    SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["connma"].ConnectionString);
    //    string cz = Request.Cookies["Forcon"]["conc"];
    //    cz = cz.Replace(":", ";");
    //    SqlConnection con = new SqlConnection(cz);
    //    SqlDataAdapter da = new SqlDataAdapter("select distinct(acyear) from acyear where acyear='2017-2018'", con);
    //    DataTable dtacyear = new DataTable();
    //    da.Fill(dtacyear);
    //    for (int a = 0; a < dtacyear.Rows.Count; a++)
    //    {
    //        string dbname = "AT" + dtacyear.Rows[a]["acyear"].ToString().Split('-')[0] + dtacyear.Rows[a]["acyear"].ToString().Split('-')[1];
    //        DataTable dtdt = new DataTable();
    //        SqlDataAdapter da1 = new SqlDataAdapter("select * FROM master.dbo.sysdatabases where name ='" + dbname + "'", con);
    //        da1.Fill(dtdt);
    //        con.Close();
    //        if (dtdt.Rows.Count > 0)
    //        {
    //            li.ccode = Convert.ToInt64(txtccode1.Text);
    //            SqlConnection con111 = new SqlConnection("Data Source=CHIRAG-PC;Initial Catalog=" + dbname + ";Integrated Security=True");
    //            li.acyear = dtacyear.Rows[a]["acyear"].ToString();
    //            SqlDataAdapter da2 = new SqlDataAdapter("select * from SalesOrder inner join SalesOrderItems on SalesOrder.sono=SalesOrderItems.sono where SalesOrder.ccode=" + li.ccode + " and SalesOrder.cno=" + li.cno + "", con111);
    //            DataTable dt2 = new DataTable();
    //            da2.Fill(dt2);
    //            if (dt2.Rows.Count > 0)
    //            {
    //                for (int b = 0; b < dt2.Rows.Count; b++)
    //                {
    //                    li.id = Convert.ToInt64(dt2.Rows[b]["id"].ToString());
    //                    li.sono = Convert.ToInt64(dt2.Rows[b]["sono"].ToString());
    //                    li.sodate = Convert.ToDateTime(dt2.Rows[b]["sodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
    //                    li.itemname = dt2.Rows[b]["itemname"].ToString();
    //                    if (li.itemname == "Fusible Link")
    //                    {
    //                        string dd = "";
    //                    }
    //                    li.descr1 = dt2.Rows[b]["descr1"].ToString();
    //                    li.descr2 = dt2.Rows[b]["descr2"].ToString();
    //                    li.descr3 = dt2.Rows[b]["descr3"].ToString();
    //                    li.unit = dt2.Rows[b]["unit"].ToString();
    //                    li.qty = Convert.ToDouble(dt2.Rows[b]["qty"].ToString());
    //                    li.rate = Convert.ToDouble(dt2.Rows[b]["rate"].ToString());
    //                    li.type = "Ordered";
    //                    string nodetail = "";
    //                    double rfs = 0;
    //                    string remarks = "";
    //                    li.qty1 = 0;
    //                    li.rate1 = 0;
    //                    SqlDataAdapter da4 = new SqlDataAdapter("select * from SCItems where vid=" + li.id + "", con111);
    //                    DataTable dt4 = new DataTable();
    //                    da4.Fill(dt4);
    //                    if (dt4.Rows.Count > 0)
    //                    {
    //                        for (int z = 0; z < dt4.Rows.Count; z++)
    //                        {

    //                            SqlDataAdapter da41 = new SqlDataAdapter("select isnull(sum(qty),0) as totscq,isnull(sum(basicamount),0) as totsc from SCItems where vid=" + li.id + "", con111);
    //                            DataTable dt41 = new DataTable();
    //                            da41.Fill(dt41);

    //                            nodetail = nodetail + dt4.Rows[z]["strscno"].ToString() + "-" + dt4.Rows[z]["qty"].ToString();
    //                            li.qty1 = Convert.ToDouble(dt41.Rows[0]["totscq"].ToString());
    //                            li.rate1 = Convert.ToDouble(dt41.Rows[0]["totsc"].ToString());
    //                            li.vid = Convert.ToInt64(dt4.Rows[z]["id"].ToString());
    //                            SqlDataAdapter da5 = new SqlDataAdapter("select * from SIItems where vid=" + li.vid + "", con111);
    //                            DataTable dt5 = new DataTable();
    //                            da5.Fill(dt5);
    //                            if (dt5.Rows.Count > 0)
    //                            {
    //                                SqlDataAdapter da51 = new SqlDataAdapter("select isnull(sum(qty),0) as totsiq,isnull(sum(basicamount),0) as totsi from SIItems where vid=" + li.vid + "", con111);
    //                                DataTable dt51 = new DataTable();
    //                                da51.Fill(dt51);
    //                                nodetail = nodetail + "( " + dt5.Rows[0]["strsino"].ToString() + " )\n";
    //                                li.qty1 = Convert.ToDouble(dt51.Rows[0]["totsiq"].ToString());
    //                                li.rate1 = Convert.ToDouble(dt51.Rows[0]["totsi"].ToString());
    //                            }
    //                            SqlDataAdapter dafg = new SqlDataAdapter("select isnull(sum(qty),0) as totqty from rritems where vid=@id", con);
    //                            dafg.SelectCommand.Parameters.AddWithValue("@id", li.id);
    //                            DataTable dtfg = new DataTable();
    //                            dafg.Fill(dtfg);
    //                            if (dtfg.Rows.Count > 0)
    //                            {
    //                                rfs = rfs + Convert.ToDouble(dtfg.Rows[0]["totqty"].ToString());
    //                            }

    //                            SqlDataAdapter dafg1 = new SqlDataAdapter("select retremarks from rritems where vid=@id", con);
    //                            dafg1.SelectCommand.Parameters.AddWithValue("@id", li.id);
    //                            DataTable dtfg1 = new DataTable();
    //                            dafg1.Fill(dtfg1);
    //                            for (int ff = 0; ff < dtfg1.Rows.Count; ff++)
    //                            {
    //                                remarks = remarks + dtfg1.Rows[ff]["retremarks"].ToString() + ",";
    //                            }
    //                            li.type = "Ordered";
    //                            SqlCommand cmd1 = new SqlCommand("update tempoverall set qty1=@qty1,rate1=@rate1,nodetail=@nodetail,rfs=@rfs,remarks=@remarks,type=@type where id=@id", con);
    //                            cmd1.Parameters.AddWithValue("@id", li.id);
    //                            cmd1.Parameters.AddWithValue("@qty1", li.qty1);
    //                            cmd1.Parameters.AddWithValue("@rate1", li.rate1);
    //                            cmd1.Parameters.AddWithValue("@nodetail", nodetail);
    //                            cmd1.Parameters.AddWithValue("@rfs", rfs);
    //                            cmd1.Parameters.AddWithValue("@remarks", remarks);
    //                            cmd1.Parameters.AddWithValue("@type", li.type);
    //                            con.Open();
    //                            cmd1.ExecuteNonQuery();
    //                            con.Close();
    //                        }
    //                    }
    //                    SqlCommand cmd = new SqlCommand("insert into tempoverall (id,sono,sodate,itemname,descr1,descr2,descr3,unit,qty,rate,type,acyear) values (@id,@sono,@sodate,@itemname,@descr1,@descr2,@descr3,@unit,@qty,@rate,@type,@acyear)", con);
    //                    cmd.Parameters.AddWithValue("@id", li.id);
    //                    cmd.Parameters.AddWithValue("@sono", li.sono);
    //                    cmd.Parameters.AddWithValue("@sodate", li.sodate);
    //                    cmd.Parameters.AddWithValue("@itemname", li.itemname);
    //                    cmd.Parameters.AddWithValue("@descr1", li.descr1);
    //                    cmd.Parameters.AddWithValue("@descr2", li.descr2);
    //                    cmd.Parameters.AddWithValue("@descr3", li.descr3);
    //                    cmd.Parameters.AddWithValue("@unit", li.unit);
    //                    cmd.Parameters.AddWithValue("@qty", li.qty);
    //                    cmd.Parameters.AddWithValue("@rate", li.rate);
    //                    cmd.Parameters.AddWithValue("@type", li.type);
    //                    cmd.Parameters.AddWithValue("@acyear", li.acyear);
    //                    con.Open();
    //                    cmd.ExecuteNonQuery();
    //                    con.Close();
    //                }
    //                //SqlDataAdapter da3 = new SqlDataAdapter("select * from tempoverall where acyear='" + li.acyear + "'", con111);
    //                //DataTable dt3 = new DataTable();
    //                //da3.Fill(dt3);
    //                //for (int c = 0; c < dt3.Rows.Count; c++)
    //                //{
    //                //    string nodetail = "";
    //                //    double rfs = 0;
    //                //    string remarks = "";
    //                //    li.qty1 = 0;
    //                //    li.rate1 = 0;
    //                //    li.id = Convert.ToInt64(dt3.Rows[c]["id"].ToString());
    //                //    if (li.id == 2331)
    //                //    {
    //                //        string cc = "";
    //                //    }


    //                //}
    //            }
    //            SqlDataAdapter da6 = new SqlDataAdapter("select SCItems.* from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno where scitems.vid not in (select SalesOrderItems.id from SCItems inner join SalesOrderItems on SalesOrderItems.id=SCItems.vid inner join SalesOrder on SalesOrder.sono=SalesOrderItems.sono where SalesOrder.ccode=" + li.ccode + ") and SCItems.ccode=" + li.ccode + " and scmaster.ctype<>'RM'", con111);
    //            DataTable dt6 = new DataTable();
    //            da6.Fill(dt6);
    //            for (int d = 0; d < dt6.Rows.Count; d++)
    //            {
    //                string nodetail = "";
    //                double rfs = 0;
    //                string remarks = "";
    //                li.id = 0;
    //                li.sono = 0;
    //                li.sodate = Convert.ToDateTime(dt6.Rows[d]["udate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
    //                li.itemname = dt6.Rows[d]["itemname"].ToString();
    //                li.descr1 = dt6.Rows[d]["descr1"].ToString();
    //                li.descr2 = dt6.Rows[d]["descr2"].ToString();
    //                li.descr3 = dt6.Rows[d]["descr3"].ToString();
    //                li.unit = dt6.Rows[d]["unit"].ToString();
    //                li.qty = 0;
    //                li.rate = 0;
    //                li.qty1 = 0;
    //                li.rate1 = 0;
    //                li.id = Convert.ToInt64(dt6.Rows[d]["id"].ToString());
    //                //SqlDataAdapter da41 = new SqlDataAdapter("select isnull(sum(qty),0) as totscq,isnull(sum(basicamount),0) as totsc from SCItems where vid=" + li.id + "", con111);
    //                //DataTable dt41 = new DataTable();
    //                //da41.Fill(dt41);
    //                //for (int z = 0; z < dt6.Rows.Count; z++)
    //                //{
    //                nodetail = nodetail + dt6.Rows[d]["strscno"].ToString() + "-" + dt6.Rows[d]["qty"].ToString();
    //                li.qty1 = Convert.ToDouble(dt6.Rows[0]["qty"].ToString());
    //                li.rate1 = Convert.ToDouble(dt6.Rows[0]["basicamount"].ToString());
    //                li.vid = Convert.ToInt64(dt6.Rows[d]["id"].ToString());
    //                SqlDataAdapter da5 = new SqlDataAdapter("select * from SIItems where vid=" + li.vid + "", con111);
    //                DataTable dt5 = new DataTable();
    //                da5.Fill(dt5);
    //                if (dt5.Rows.Count > 0)
    //                {
    //                    SqlDataAdapter da51 = new SqlDataAdapter("select isnull(sum(qty),0) as totsiq,isnull(sum(basicamount),0) as totsi from SIItems where vid=" + li.vid + "", con111);
    //                    DataTable dt51 = new DataTable();
    //                    da51.Fill(dt51);
    //                    nodetail = nodetail + "( " + dt5.Rows[0]["strsino"].ToString() + " )\n";
    //                    li.qty1 = Convert.ToDouble(dt51.Rows[0]["totsiq"].ToString());
    //                    li.rate1 = Convert.ToDouble(dt51.Rows[0]["totsi"].ToString());
    //                }
    //                SqlDataAdapter dafg = new SqlDataAdapter("select isnull(sum(qty),0) as totqty from rritems where vid=@id", con111);
    //                dafg.SelectCommand.Parameters.AddWithValue("@id", li.id);
    //                DataTable dtfg = new DataTable();
    //                dafg.Fill(dtfg);
    //                if (dtfg.Rows.Count > 0)
    //                {
    //                    rfs = rfs + Convert.ToDouble(dtfg.Rows[0]["totqty"].ToString());
    //                }

    //                SqlDataAdapter dafg1 = new SqlDataAdapter("select retremarks from rritems where vid=@id", con111);
    //                dafg1.SelectCommand.Parameters.AddWithValue("@id", li.id);
    //                DataTable dtfg1 = new DataTable();
    //                dafg1.Fill(dtfg1);
    //                for (int ff = 0; ff < dtfg1.Rows.Count; ff++)
    //                {
    //                    remarks = remarks + dtfg1.Rows[ff]["retremarks"].ToString() + ",";
    //                }
    //                //}
    //                li.type = "Non-Ordered";
    //                SqlDataAdapter daf = new SqlDataAdapter("select * from tempoverall order by id desc", con111);
    //                DataTable dtf = new DataTable();
    //                daf.Fill(dtf);
    //                li.id = Convert.ToInt64(dtf.Rows[0]["id"].ToString()) + 1;
    //                SqlCommand cmd = new SqlCommand("insert into tempoverall (id,sono,sodate,itemname,descr1,descr2,descr3,unit,qty,rate,qty1,rfs,type,rate1,nodetail,remarks,acyear) values (@id,@sono,@sodate,@itemname,@descr1,@descr2,@descr3,@unit,@qty,@rate,@qty1,@rfs,@type,@rate1,@nodetail,@remarks,@acyear)", con);
    //                cmd.Parameters.AddWithValue("@id", li.id);
    //                cmd.Parameters.AddWithValue("@sono", li.sono);
    //                cmd.Parameters.AddWithValue("@sodate", li.sodate);
    //                cmd.Parameters.AddWithValue("@itemname", li.itemname);
    //                cmd.Parameters.AddWithValue("@descr1", li.descr1);
    //                cmd.Parameters.AddWithValue("@descr2", li.descr2);
    //                cmd.Parameters.AddWithValue("@descr3", li.descr3);
    //                cmd.Parameters.AddWithValue("@unit", li.unit);
    //                cmd.Parameters.AddWithValue("@qty", li.qty);
    //                cmd.Parameters.AddWithValue("@rate", li.rate);
    //                cmd.Parameters.AddWithValue("@acyear", li.acyear);
    //                cmd.Parameters.AddWithValue("@qty1", li.qty1);
    //                cmd.Parameters.AddWithValue("@rfs", li.rfs);
    //                cmd.Parameters.AddWithValue("@type", li.type);
    //                cmd.Parameters.AddWithValue("@rate1", li.rate1);
    //                cmd.Parameters.AddWithValue("@nodetail", nodetail);
    //                cmd.Parameters.AddWithValue("@remarks", remarks);
    //                con.Open();
    //                cmd.ExecuteNonQuery();
    //                con.Close();


    //            }
    //        }
    //    }
    //    btnpreview.Visible = true;
    //}
}