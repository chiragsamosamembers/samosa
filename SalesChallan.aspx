﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SalesChallan.aspx.cs" Inherits="SalesChallan" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Sales Challan</span><br />
        <div class="row">
            <div class="col-md-1">
                <asp:Button ID="btnfirst" runat="server" Text="First" OnClick="btnfirst_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnprevious" runat="server" Text="Previous" OnClick="btnprevious_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnnext" runat="server" Text="Next" OnClick="btnnext_Click" /></div>
            <div class="col-md-1">
                <asp:Button ID="btnlast" runat="server" Text="Last" OnClick="btnlast_Click" /></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Challan No.</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtchallanno" runat="server" CssClass="form-control" Width="150px"
                    onkeypress="javascript:return isNumber (event)" AutoPostBack="True" OnTextChanged="txtchallanno_TextChanged"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Date</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtscdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Type<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="drpchallantype" runat="server" CssClass="form-control" Width="150px"
                    AutoPostBack="True" OnSelectedIndexChanged="drpchallantype_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    User</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" ReadOnly="true"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Name<span style="color: Red; font-weight: bolder;" class="glyphicon glyphicon-search">*</span></label>
            </div>
            <div class="col-md-5">
                <%--<asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="393px" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txtname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetAccountname">
                -</asp:AutoCompleteExtender>--%>
                <asp:DropDownList ID="drpacname" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    Client Code<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtccode" runat="server" CssClass="form-control" Width="150px" AutoPostBack="true"
                    OnTextChanged="txtname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtccode"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Order No.<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtsalesorder" runat="server" CssClass="form-control" Width="150px"
                    Text="0" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    Date</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtsodate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    LR No.</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtlrno" runat="server" CssClass="form-control" Width="150px" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Date</label>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtlrdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Despatched by</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtdespatchedby" runat="server" CssClass="form-control" Width="393px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Freight Rs.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtfreightrs" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Sales Order</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtsono" runat="server" CssClass="form-control" Width="150px" onkeypress="javascript:return isNumber (event)"
                            placeholder="Alt + s"></asp:TextBox>
                        <%--OnTextChanged="txtsono_TextChanged"--%>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtsono"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getsono">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Status<span style="color: Red;">*</span></label></div>
            <div class="col-md-2">
                <asp:DropDownList ID="drpstatus" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                <asp:Label ID="lblcount" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="22px"></asp:Label></div>
            <div class="col-md-2">
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    Signed Or Not</label></div>
            <div class="col-md-1">
                <asp:CheckBox ID="chksigned" runat="server" />
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtsignremark" runat="server" CssClass="form-control" Width="370px"
                    placeholder="Sign Remarks"></asp:TextBox></div>
        </div>
    </div>
    <div class="row" style="height: 10px;">
    </div>
    <div class="row" style="border: 1px solid;" runat="server" visible="false">
        <div class="col-md-4 text-center" style="border-right: 1px solid;">
            <label class="control-label">
                Item Name</label>
        </div>
        <div class="col-md-1 text-center" style="border-right: 1px solid;">
            <label class="control-label">
                Bill Qty</label>
        </div>
        <div class="col-md-1 text-center" style="border-right: 1px solid;">
            <label class="control-label">
                Stock Qty(<asp:Label ID="lblcloseqty" runat="server" ForeColor="Red"></asp:Label>)</label>
        </div>
        <div class="col-md-1 text-center" style="border-right: 1px solid;">
            <label class="control-label">
                Rate</label>
        </div>
        <div class="col-md-1 text-center" style="border-right: 1px solid;">
            <label class="control-label">
                Amount</label>
        </div>
        <div class="col-md-1 text-center">
            <label class="control-label">
                Unit
            </label>
        </div>
    </div>
    <div class="row" style="border: 1px solid;" runat="server" visible="false">
        <div class="col-md-4 text-center" style="border-right: 1px solid;">
            <label class="control-label">
                Description 1</label>
        </div>
        <div class="col-md-4 text-center" style="border-right: 1px solid;">
            <label class="control-label">
                Description 2</label>
        </div>
        <div class="col-md-3 text-center">
            <label class="control-label">
                Description 3</label>
        </div>
    </div>
    <div class="row" style="border: 1px solid;" runat="server" visible="false">
        <div class="col-md-4" style="border-right: 1px solid;">
            <%--<asp:TextBox ID="txtitemname" runat="server" CssClass="form-control" placeholder="Item Name"
                Width="200px" AutoPostBack="true" OnTextChanged="txtitemname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtitemname"
                MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                ServiceMethod="Getitemname">
            </asp:AutoCompleteExtender>--%>
            <asp:DropDownList ID="drpitemname" runat="server" CssClass="form-control" AutoPostBack="True"
                OnSelectedIndexChanged="drpitemname_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div class="col-md-1" style="border-right: 1px solid;">
            <asp:TextBox ID="txtbillqty" runat="server" CssClass="form-control" placeholder="Bill Qty"
                Width="90px" AutoPostBack="True" OnTextChanged="txtbillqty_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
        </div>
        <div class="col-md-1" style="border-right: 1px solid;">
            <asp:TextBox ID="txtstockqty" runat="server" CssClass="form-control" placeholder="Stock Qty"
                Width="90px" onkeypress="javascript:return isNumber (event)" AutoPostBack="True"
                OnTextChanged="txtstockqty_TextChanged"></asp:TextBox>
        </div>
        <div class="col-md-1" style="border-right: 1px solid;">
            <asp:TextBox ID="txtrate" runat="server" CssClass="form-control" placeholder="Rate"
                Width="90px" AutoPostBack="True" OnTextChanged="txtrate_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
        </div>
        <div class="col-md-1" style="border-right: 1px solid;">
            <asp:TextBox ID="txtbasicamt" runat="server" CssClass="form-control" placeholder="Basic Amt."
                Width="90px" onkeypress="javascript:return isNumber (event)" ReadOnly="true"></asp:TextBox>
        </div>
        <div class="col-md-1">
            <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" placeholder="Unit"
                Width="90px" onkeypress="return checkQuote();"></asp:TextBox>
        </div>
    </div>
    <div class="row" style="border: 1px solid;" runat="server" visible="false">
        <div class="col-md-4" style="border-right: 1px solid;">
            <asp:TextBox ID="txtdescription1" runat="server" CssClass="form-control" placeholder="Description 1"
                Width="200px" onkeypress="return checkQuote();"></asp:TextBox>
        </div>
        <div class="col-md-4" style="border-right: 1px solid;">
            <asp:TextBox ID="txtdescription2" runat="server" CssClass="form-control" placeholder="Description 2"
                Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
        </div>
        <div class="col-md-3">
            <asp:TextBox ID="txtdescription3" runat="server" CssClass="form-control" placeholder="Description 3"
                Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
        </div>
        <div class="col-md-1">
            <asp:Button ID="btnadd" runat="server" Text="Add" Height="30px" BackColor="#F05283"
                ValidationGroup="valgvsaleschallan" OnClick="btnadd_Click" />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                ShowSummary="false" ValidationGroup="valgvsaleschallan" />
        </div>
    </div>
    
    <div class="row table-responsive">
        <div class="col-md-12">
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                <asp:GridView ID="gvscitemlist" runat="server" AutoGenerateColumns="False" Width="1300px"
                    BorderStyle="None" AllowSorting="false" Height="0px" CssClass="table table-bordered"
                    OnRowDeleting="gvscitemlist_RowDeleting" OnRowCommand="gvscitemlist_RowCommand">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgbtnselect11" CommandName="update1" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                    ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete123();"
                                    CommandArgument='<%# bind("vid") %>' CausesValidation="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label><br />
                                Description 1
                                <asp:TextBox ID="txtdesc1" runat="server" Text='<%# bind("descr1") %>' ReadOnly="true"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VNo." SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblvno" ForeColor="Black" runat="server" Text='<%# bind("vno") %>'></asp:Label><br />
                                Description 2
                                <asp:TextBox ID="txtdesc2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label><br />
                                Description 3
                                <asp:TextBox ID="txtdesc3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                <asp:Label ID="lblqtyremain" runat="server" ForeColor="Green" Text='<%# bind("qtyremain") %>'></asp:Label>
                                <asp:Label ID="lblqtyused" runat="server" ForeColor="Red" Text='<%# bind("qtyused") %>'></asp:Label>
                                <asp:TextBox ID="txtgridqty" runat="server" Text='<%# bind("qty") %>' Width="40px"
                                    AutoPostBack="True" OnTextChanged="txtgridqty_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stock Qty" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblgvstockqty" ForeColor="Black" runat="server" Text='<%# bind("stockqty") %>'></asp:Label>
                                <asp:TextBox ID="txtgvstockqty" runat="server" Text='<%# bind("stockqty") %>' Width="40px"
                                    AutoPostBack="True" OnTextChanged="txtgvstockqty_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>(<asp:Label
                                        ID="lblgvcloseqty" runat="server" ForeColor="Red"></asp:Label>)
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'></asp:Label><asp:TextBox
                                    ID="txtgridrate" runat="server" Text='<%# bind("rate") %>' Width="50px" AutoPostBack="True"
                                    OnTextChanged="txtgridrate_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("basicamount") %>'></asp:Label>
                                <asp:TextBox ID="txtgridamount" runat="server" Text='<%# bind("basicamount") %>'
                                    Width="50px" ReadOnly="true"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VAT Type" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblvattype" ForeColor="Black" runat="server" Text='<%# bind("taxtype") %>'></asp:Label><asp:TextBox
                                    ID="txtgridtaxtype" runat="server" Text='<%# bind("taxtype") %>' Width="50px"
                                    AutoPostBack="True" OnTextChanged="txtgridtaxtype_TextChanged"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtgridtaxtype"
                                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                    ServiceMethod="Getvattype">
                                </asp:AutoCompleteExtender>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VAT%" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblvatp" ForeColor="Black" runat="server" Text='<%# bind("vatp") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Add VAT%" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lbladdvatp" ForeColor="Black" runat="server" Text='<%# bind("addtaxp") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CST%" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblcstp" ForeColor="Black" runat="server" Text='<%# bind("cstp") %>'></asp:Label><asp:TextBox
                                    ID="txtgridcstp" runat="server" Text='<%# bind("cstp") %>' Width="30px" AutoPostBack="True"
                                    OnTextChanged="txtgridcstp_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CST Amt" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblcstamt" ForeColor="Black" runat="server" Text='<%# bind("cstamt") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VAT Amt" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblvatamt" ForeColor="Black" runat="server" Text='<%# bind("vatamt") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Add VAT Amt" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lbladdvatamt" ForeColor="Black" runat="server" Text='<%# bind("addtaxamt") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lbltotamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'
                                    Visible="false"></asp:Label>
                                <asp:TextBox ID="txtgridunit" runat="server" Text='<%# bind("unit") %>' Width="60px"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="E-Unit" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:TextBox ID="txtgrideunit" runat="server" Text='<%# bind("eunit") %>' Width="60px"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks(For Packing Slip)" SortExpression="Csnm">
                            <ItemTemplate>
                                <%--<asp:Label ID="lbldesc3" ForeColor="Black" runat="server" Text='<%# bind("descr3") %>'></asp:Label>--%>
                                <asp:TextBox ID="txtremarks" runat="server" Text='<%# bind("remarks") %>'></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="Returned Qty" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:TextBox ID="txtgvreturnqty" runat="server" Text='<%# bind("rfs") %>' Width="120px"></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Returned Remarks" SortExpression="Csnm">
                            <ItemTemplate>
                                <asp:TextBox ID="txtgvretremarks" runat="server" Text='<%# bind("retremarks") %>'></asp:TextBox>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                    ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                    CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VID" SortExpression="Csnm" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblvid" ForeColor="Black" runat="server" Text='<%# bind("vid") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#4c4c4c" />
                    <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                    <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                </asp:GridView>
            </asp:Panel>
        </div>
    </div>
    <div class="row" style="height: 10px;">
        <div class="col-md-1">
            <label class="control-label">
                E-Way</label>
        </div>
        <div class="col-md-1">
            <asp:DropDownList ID="drpeway" runat="server" CssClass="form-control">
                <%-- AutoPostBack="True"
                                        OnSelectedIndexChanged="drpeway_SelectedIndexChanged"--%>
                <asp:ListItem>YES</asp:ListItem>
                <asp:ListItem>NO</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <asp:Button ID="btnsave" runat="server" Text="Save" Height="30px" BackColor="#F05283"
                ValidationGroup="valsaleschallan" OnClick="btnsave_Click" /></div>
        <div class="col-md-8">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                ShowSummary="false" ValidationGroup="valsaleschallan" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter sales challan date."
                Text="*" ValidationGroup="valsaleschallan" ControlToValidate="txtscdate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select name."
                Text="*" ValidationGroup="valsaleschallan" ControlToValidate="drpacname" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter so no."
                Text="*" ValidationGroup="valsaleschallan" ControlToValidate="txtsono"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter client code."
                Text="*" ValidationGroup="valsaleschallan" ControlToValidate="txtccode"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter order date."
                Text="*" ValidationGroup="valsaleschallan" ControlToValidate="txtsodate"></asp:RequiredFieldValidator>--%>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please enter lr no."
            Text="*" ValidationGroup="valsaleschallan" ControlToValidate="txtlrno"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter lr date."
            Text="*" ValidationGroup="valsaleschallan" ControlToValidate="txtlrdate"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please enter dispatched by."
            Text="*" ValidationGroup="valsaleschallan" ControlToValidate="txtdespatchedby"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please enter freight rs."
            Text="*" ValidationGroup="valsaleschallan" ControlToValidate="txtfreightrs"></asp:RequiredFieldValidator>--%>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="Please enter sales order."
                Text="*" ValidationGroup="valsaleschallan" ControlToValidate="txtsalesorder"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please select item name."
                Text="*" ValidationGroup="valgvsaleschallan" ControlToValidate="drpitemname"
                InitialValue="--SELECT--"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please enter bill qty."
                Text="*" ValidationGroup="valgvsaleschallan" ControlToValidate="txtbillqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please enter stock qty."
                Text="*" ValidationGroup="valgvsaleschallan" ControlToValidate="txtstockqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please enter rate."
                Text="*" ValidationGroup="valgvsaleschallan" ControlToValidate="txtrate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Please enter basic amount."
                Text="*" ValidationGroup="valgvsaleschallan" ControlToValidate="txtbasicamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Please enter unit."
                Text="*" ValidationGroup="valgvsaleschallan" ControlToValidate="txtunit"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Please select status."
                Text="*" ValidationGroup="valsaleschallan" InitialValue="--SELECT--" ControlToValidate="drpstatus"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select type."
                Text="*" ValidationGroup="valsaleschallan" InitialValue="--SELECT--" ControlToValidate="drpchallantype"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Please enter description1."
                Text="*" ValidationGroup="valgvsaleschallan" ControlToValidate="txtdescription1"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Please enter description2."
                Text="*" ValidationGroup="valgvsaleschallan" ControlToValidate="txtdescription2"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Please enter description3."
                Text="*" ValidationGroup="valgvsaleschallan" ControlToValidate="txtdescription3"></asp:RequiredFieldValidator>--%>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Sales Challan Date must be dd-MM-yyyy" ValidationGroup="valsaleschallan"
                ForeColor="Red" ControlToValidate="txtscdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Sales Order Date must be dd-MM-yyyy" ValidationGroup="valsaleschallan"
                ForeColor="Red" ControlToValidate="txtsodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Lr Date must be dd-MM-yyyy" ValidationGroup="valsaleschallan" ForeColor="Red"
                ControlToValidate="txtlrdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="row">
        <asp:LinkButton ID="lnkselectionpopup" runat="server" AccessKey="s" OnClick="lnkselectionpopup_Click"></asp:LinkButton>
        <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
            TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="500px" align="center"
            Width="95%" Style="display: none">
            <div class="row">
                <div class="col-md-12 text-center">
                    <span style="color: Red;"><b>Sales Order Selection</b></span></div>
            </div>
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-md-1 text-left">
                    <b>A/C Name :</b></div>
                <div class="col-md-7">
                    <asp:Label ID="lblpopupacname" runat="server" Font-Bold="true"></asp:Label></div>
            </div>
            <div class="row">
                <asp:Label ID="lblemptyso" runat="server" ForeColor="Red"></asp:Label>
                <asp:CheckBox ID="chkall" runat="server" Text="Select All" Visible="false" />
                <asp:Button ID="btnselectitem" runat="server" Text="Ok" BackColor="Red" ForeColor="White"
                    OnClick="btnselectitem_Click" />
                <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                    <asp:GridView ID="gvsalesorder" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvsalesorder_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("sono") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Purchase Chlln No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblscno" ForeColor="Black" runat="server" Text='<%# bind("sono") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                <ItemTemplate>
                                    <asp:Label ID="lblpcdate" runat="server" ForeColor="#505050" Text='<%# bind("sodate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblstatus" ForeColor="Black" runat="server" Text='<%# bind("status") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                    <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvsoitemlistselection" runat="server" AutoGenerateColumns="False"
                        Width="1300px" BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”"
                        Height="0px" CssClass="table table-bordered">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%--<asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />--%>
                                    <asp:CheckBox ID="chkselect" runat="server" />
                                </ItemTemplate>
                                <%--<HeaderTemplate>
                                    <asp:CheckBox ID="chkall" runat="server" AutoPostBack="True" OnCheckedChanged="chkall_CheckedChanged" /></HeaderTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdesc1" runat="server" Text='<%# bind("descr1") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdesc2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdesc3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="basic Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbasicamt" ForeColor="Black" runat="server" Text='<%# bind("basicamount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
                <asp:Button ID="btnClose" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
            </div>
        </asp:Panel>
    </div>
    <div class="row">
        <asp:LinkButton ID="lnkewayselectionpopup" runat="server" OnClick="lnkewayselectionpopup_Click"></asp:LinkButton>
        <asp:LinkButton ID="lnkewaypopup" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel4"
            TargetControlID="lnkewaypopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel4" runat="server" CssClass="modalPopup" Height="500px" align="center"
            ScrollBars="Both" Width="95%" Style="display: none">
            <div class="row">
                <div class="col-md-10">
                    <span style="color: Red; font-weight: bold;"><b>Additional Details : Sales Taxable</b></span></div>
                <div class="col-md-2">
                    <asp:LinkButton ID="lnkclose" runat="server" OnClick="lnkclose_Click" Font-Bold="true"
                        ForeColor="Black">Close (X)</asp:LinkButton></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <span style="color: white; background-color: Red; font-weight: bold; border-bottom-style: dashed;">
                        <b>E-Way Bill Details</b></span></div>
            </div>
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        E-Way Bill No.</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtewaybillno" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                <div class="col-md-2">
                    <label class="control-label">
                        Date</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtdate1" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="Please Enter Date."
                        Text="*" ControlToValidate="txtdate1" ValidationGroup="valeway"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Consolidated e-Way Bill No.</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtconsolidatedewaybillno" runat="server" CssClass="form-control"
                        Width="150px"></asp:TextBox></div>
                <div class="col-md-2">
                    <label class="control-label">
                        Date</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtdate2" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Supply Type</label>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="drpsupplytype" runat="server" CssClass="form-control">
                        <asp:ListItem Value="O">Outward</asp:ListItem>
                        <asp:ListItem Value="I">Inward</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Sub Type</label>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="drpsubtype" runat="server" CssClass="form-control">
                        <asp:ListItem Value="1">Supply</asp:ListItem>
                        <asp:ListItem Value="2">Import</asp:ListItem>
                        <asp:ListItem Value="3">Export</asp:ListItem>
                        <asp:ListItem Value="4">Job Work</asp:ListItem>
                        <asp:ListItem Value="5">For Own Use</asp:ListItem>
                        <asp:ListItem Value="6">Job work Returns</asp:ListItem>
                        <asp:ListItem Value="7">Sales Return</asp:ListItem>
                        <asp:ListItem Value="8">Other</asp:ListItem>
                        <asp:ListItem Value="9">SKD/CKD</asp:ListItem>
                        <asp:ListItem Value="10">Line Sales</asp:ListItem>
                        <asp:ListItem Value="11">Recipient Not Known</asp:ListItem>
                        <asp:ListItem Value="12">Exhibition or Fairs</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <label class="control-label">
                        Document Type</label>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="drpdocumenttype" runat="server" CssClass="form-control">
                        <asp:ListItem Value="CHN">Delivery Challan</asp:ListItem>
                        <asp:ListItem Value="INV">Tax Invoice</asp:ListItem>
                        <asp:ListItem Value="BIL">Bill of Supply</asp:ListItem>
                        <asp:ListItem Value="BOE">Bill of Entry</asp:ListItem>
                        <asp:ListItem Value="CNT">Credit Note</asp:ListItem>
                        <asp:ListItem Value="OTH">Others</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <span style="color: white; background-color: Red; font-weight: bold; border-bottom-style: dashed;">
                Transporter Details</span><br />
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Mode</label>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="drpmode" runat="server" CssClass="form-control">
                        <asp:ListItem Value="1">Road</asp:ListItem>
                        <asp:ListItem Value="2">Rail</asp:ListItem>
                        <asp:ListItem Value="3">Air</asp:ListItem>
                        <asp:ListItem Value="4">Ship</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <label class="control-label">
                        Distance(In Km)*</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtdistance" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Please Enter Distance (In Km)."
                        Text="*" ControlToValidate="txtdistance" ValidationGroup="valeway"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Transporter Name</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txttransportername" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="Please Enter Transporter Name."
                            Text="*" ControlToValidate="txttransportername" ValidationGroup="valeway"></asp:RequiredFieldValidator>--%>
                </div>
                <div class="col-md-2">
                    <label class="control-label">
                        Vehicle Number</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtvehiclenumber" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Vehicle Type</label>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="drpvehicletype" runat="server" CssClass="form-control">
                        <asp:ListItem Value="R">Regular</asp:ListItem>
                        <asp:ListItem Value="O">Over Dimensional Cargo</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Doc/Landing/RR/AirWay No.</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtdoclandingrrairwayno" runat="server" CssClass="form-control"
                        Width="150px"></asp:TextBox></div>
                <div class="col-md-2">
                    <label class="control-label">
                        Transport Date</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="Please Enter Transport Date."
                        Text="*" ControlToValidate="txtdate" ValidationGroup="valeway"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Transporter ID</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txttransporterid" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
            </div>
            <span style="color: white; background-color: Red; font-weight: bold; border-bottom-style: dashed">
                Consignor Details(From)</span><br />
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Consignor</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtconsignor" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ErrorMessage="Please Enter Consignor."
                        Text="*" ControlToValidate="txtconsignor" ValidationGroup="valeway"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Address1</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtaddress1" runat="server" CssClass="form-control" Width="150px"
                        TextMode="MultiLine" Height="50px"></asp:TextBox></div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Address2</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtaddress2" runat="server" CssClass="form-control" Width="150px"
                        TextMode="MultiLine" Height="50px"></asp:TextBox></div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Place</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtplace" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                <div class="col-md-2">
                    <label class="control-label">
                        Pincode</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtpincode" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" ErrorMessage="Please Enter Consignor Pincode."
                        Text="*" ControlToValidate="txtpincode" ValidationGroup="valeway"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        State</label>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="drpfromstate" runat="server" CssClass="form-control">
                        <asp:ListItem Value="0">--SELECT--</asp:ListItem>
                        <asp:ListItem Value="35">ANDAMAN AND NICOBAR</asp:ListItem>
                        <asp:ListItem Value="37">ANDHRA PRADESH</asp:ListItem>
                        <asp:ListItem Value="12">ARUNACHAL PRADESH</asp:ListItem>
                        <asp:ListItem Value="18">ASSAM</asp:ListItem>
                        <asp:ListItem Value="10">BIHAR</asp:ListItem>
                        <asp:ListItem Value="4">CHANDIGARH</asp:ListItem>
                        <asp:ListItem Value="22">CHHATTISGARH</asp:ListItem>
                        <asp:ListItem Value="26">DADRA AND NAGAR HAVELI</asp:ListItem>
                        <asp:ListItem Value="25">DAMAN AND DIU</asp:ListItem>
                        <asp:ListItem Value="7">DELHI</asp:ListItem>
                        <asp:ListItem Value="30">GOA</asp:ListItem>
                        <asp:ListItem Value="24">GUJARAT</asp:ListItem>
                        <asp:ListItem Value="6">HARYANA</asp:ListItem>
                        <asp:ListItem Value="2">HIMACHAL PRADESH</asp:ListItem>
                        <asp:ListItem Value="1">JAMMU AND KASHMIR</asp:ListItem>
                        <asp:ListItem Value="20">JHARKHAND</asp:ListItem>
                        <asp:ListItem Value="29">KARNATAKA</asp:ListItem>
                        <asp:ListItem Value="32">KERALA</asp:ListItem>
                        <asp:ListItem Value="31">LAKSHADWEEP</asp:ListItem>
                        <asp:ListItem Value="23">MADHYA PRADESH</asp:ListItem>
                        <asp:ListItem Value="27">MAHARASHTRA</asp:ListItem>
                        <asp:ListItem Value="14">MANIPUR</asp:ListItem>
                        <asp:ListItem Value="17">MEGHALAYA</asp:ListItem>
                        <asp:ListItem Value="15">MIZORAM</asp:ListItem>
                        <asp:ListItem Value="13">NAGALAND</asp:ListItem>
                        <asp:ListItem Value="21">ORISSA</asp:ListItem>
                        <asp:ListItem Value="34">PONDICHERRY</asp:ListItem>
                        <asp:ListItem Value="3">PUNJAB</asp:ListItem>
                        <asp:ListItem Value="8">RAJASTHAN</asp:ListItem>
                        <asp:ListItem Value="11">SIKKIM</asp:ListItem>
                        <asp:ListItem Value="33">TAMIL NADU</asp:ListItem>
                        <asp:ListItem Value="36">TELANGANA</asp:ListItem>
                        <asp:ListItem Value="16">TRIPURA</asp:ListItem>
                        <asp:ListItem Value="9">UTTAR PRADESH</asp:ListItem>
                        <asp:ListItem Value="5">UTTARAKHAND</asp:ListItem>
                        <asp:ListItem Value="19">WEST BENGAL</asp:ListItem>
                        <asp:ListItem Value="97">OTHER TERRITORY</asp:ListItem>
                        <asp:ListItem Value="38">OTHER COUNTRIES</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please select From State."
                        Text="*" ControlToValidate="drpfromstate" ValidationGroup="valeway" InitialValue="0"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-2">
                    <label class="control-label">
                        GSTIN/UIN</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtgstinuin" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" ErrorMessage="Please Enter Consignor GSTIN/UIN."
                        Text="*" ControlToValidate="txtgstinuin" ValidationGroup="valeway"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Actual From State</label>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="drpactualfromstate" runat="server" CssClass="form-control">
                        <asp:ListItem Value="0">--SELECT--</asp:ListItem>
                        <asp:ListItem Value="35">ANDAMAN AND NICOBAR</asp:ListItem>
                        <asp:ListItem Value="37">ANDHRA PRADESH</asp:ListItem>
                        <asp:ListItem Value="12">ARUNACHAL PRADESH</asp:ListItem>
                        <asp:ListItem Value="18">ASSAM</asp:ListItem>
                        <asp:ListItem Value="10">BIHAR</asp:ListItem>
                        <asp:ListItem Value="4">CHANDIGARH</asp:ListItem>
                        <asp:ListItem Value="22">CHHATTISGARH</asp:ListItem>
                        <asp:ListItem Value="26">DADRA AND NAGAR HAVELI</asp:ListItem>
                        <asp:ListItem Value="25">DAMAN AND DIU</asp:ListItem>
                        <asp:ListItem Value="7">DELHI</asp:ListItem>
                        <asp:ListItem Value="30">GOA</asp:ListItem>
                        <asp:ListItem Value="24">GUJARAT</asp:ListItem>
                        <asp:ListItem Value="6">HARYANA</asp:ListItem>
                        <asp:ListItem Value="2">HIMACHAL PRADESH</asp:ListItem>
                        <asp:ListItem Value="1">JAMMU AND KASHMIR</asp:ListItem>
                        <asp:ListItem Value="20">JHARKHAND</asp:ListItem>
                        <asp:ListItem Value="29">KARNATAKA</asp:ListItem>
                        <asp:ListItem Value="32">KERALA</asp:ListItem>
                        <asp:ListItem Value="31">LAKSHADWEEP</asp:ListItem>
                        <asp:ListItem Value="23">MADHYA PRADESH</asp:ListItem>
                        <asp:ListItem Value="27">MAHARASHTRA</asp:ListItem>
                        <asp:ListItem Value="14">MANIPUR</asp:ListItem>
                        <asp:ListItem Value="17">MEGHALAYA</asp:ListItem>
                        <asp:ListItem Value="15">MIZORAM</asp:ListItem>
                        <asp:ListItem Value="13">NAGALAND</asp:ListItem>
                        <asp:ListItem Value="21">ORISSA</asp:ListItem>
                        <asp:ListItem Value="34">PONDICHERRY</asp:ListItem>
                        <asp:ListItem Value="3">PUNJAB</asp:ListItem>
                        <asp:ListItem Value="8">RAJASTHAN</asp:ListItem>
                        <asp:ListItem Value="11">SIKKIM</asp:ListItem>
                        <asp:ListItem Value="33">TAMIL NADU</asp:ListItem>
                        <asp:ListItem Value="36">TELANGANA</asp:ListItem>
                        <asp:ListItem Value="16">TRIPURA</asp:ListItem>
                        <asp:ListItem Value="9">UTTAR PRADESH</asp:ListItem>
                        <asp:ListItem Value="5">UTTARAKHAND</asp:ListItem>
                        <asp:ListItem Value="19">WEST BENGAL</asp:ListItem>
                        <asp:ListItem Value="97">OTHER TERRITORY</asp:ListItem>
                        <asp:ListItem Value="37">OTHER COUNTRIES</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="Please select Actual From State."
                        Text="*" ControlToValidate="drpactualfromstate" ValidationGroup="valeway" InitialValue="0"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <span style="color: white; background-color: Red; font-weight: bold; border-bottom-style: dashed">
                Consignee Details(To)</span><br />
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Consignee</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtconsignee" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server" ErrorMessage="Please Enter Consignee."
                        Text="*" ControlToValidate="txtconsignee" ValidationGroup="valeway"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Address1</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtconsigneeaddress1" runat="server" CssClass="form-control" Width="150px"
                        TextMode="MultiLine" Height="50px"></asp:TextBox></div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Address2</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtconsigneeaddress2" runat="server" CssClass="form-control" Width="150px"
                        TextMode="MultiLine" Height="50px"></asp:TextBox></div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        To Place(Destination)</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txttoplace" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                <div class="col-md-2">
                    <label class="control-label">
                        Pincode</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtconsigneepincode" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server" ErrorMessage="Please Enter Consignee Pincode."
                        Text="*" ControlToValidate="txtconsigneepincode" ValidationGroup="valeway"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        State</label>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="drptostate" runat="server" CssClass="form-control">
                        <asp:ListItem Value="0">--SELECT--</asp:ListItem>
                        <asp:ListItem Value="35">ANDAMAN AND NICOBAR</asp:ListItem>
                        <asp:ListItem Value="37">ANDHRA PRADESH</asp:ListItem>
                        <asp:ListItem Value="12">ARUNACHAL PRADESH</asp:ListItem>
                        <asp:ListItem Value="18">ASSAM</asp:ListItem>
                        <asp:ListItem Value="10">BIHAR</asp:ListItem>
                        <asp:ListItem Value="4">CHANDIGARH</asp:ListItem>
                        <asp:ListItem Value="22">CHHATTISGARH</asp:ListItem>
                        <asp:ListItem Value="26">DADRA AND NAGAR HAVELI</asp:ListItem>
                        <asp:ListItem Value="25">DAMAN AND DIU</asp:ListItem>
                        <asp:ListItem Value="7">DELHI</asp:ListItem>
                        <asp:ListItem Value="30">GOA</asp:ListItem>
                        <asp:ListItem Value="24">GUJARAT</asp:ListItem>
                        <asp:ListItem Value="6">HARYANA</asp:ListItem>
                        <asp:ListItem Value="2">HIMACHAL PRADESH</asp:ListItem>
                        <asp:ListItem Value="1">JAMMU AND KASHMIR</asp:ListItem>
                        <asp:ListItem Value="20">JHARKHAND</asp:ListItem>
                        <asp:ListItem Value="29">KARNATAKA</asp:ListItem>
                        <asp:ListItem Value="32">KERALA</asp:ListItem>
                        <asp:ListItem Value="31">LAKSHADWEEP</asp:ListItem>
                        <asp:ListItem Value="23">MADHYA PRADESH</asp:ListItem>
                        <asp:ListItem Value="27">MAHARASHTRA</asp:ListItem>
                        <asp:ListItem Value="14">MANIPUR</asp:ListItem>
                        <asp:ListItem Value="17">MEGHALAYA</asp:ListItem>
                        <asp:ListItem Value="15">MIZORAM</asp:ListItem>
                        <asp:ListItem Value="13">NAGALAND</asp:ListItem>
                        <asp:ListItem Value="21">ORISSA</asp:ListItem>
                        <asp:ListItem Value="34">PONDICHERRY</asp:ListItem>
                        <asp:ListItem Value="3">PUNJAB</asp:ListItem>
                        <asp:ListItem Value="8">RAJASTHAN</asp:ListItem>
                        <asp:ListItem Value="11">SIKKIM</asp:ListItem>
                        <asp:ListItem Value="33">TAMIL NADU</asp:ListItem>
                        <asp:ListItem Value="36">TELANGANA</asp:ListItem>
                        <asp:ListItem Value="16">TRIPURA</asp:ListItem>
                        <asp:ListItem Value="9">UTTAR PRADESH</asp:ListItem>
                        <asp:ListItem Value="5">UTTARAKHAND</asp:ListItem>
                        <asp:ListItem Value="19">WEST BENGAL</asp:ListItem>
                        <asp:ListItem Value="36">OTHER TERRITORY</asp:ListItem>
                        <asp:ListItem Value="37">OTHER COUNTRIES</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator44" runat="server" ErrorMessage="Please select To State."
                        Text="*" ControlToValidate="drptostate" ValidationGroup="valeway" InitialValue="0"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-2">
                    <label class="control-label">
                        GSTIN/UIN</label>
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtconsigneegstinuin" runat="server" CssClass="form-control" Width="200px"
                        AutoPostBack="true" OnTextChanged="txtconsigneegstinuin_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator43" runat="server" ErrorMessage="Please Enter Consignee GSTIN/UIN."
                        Text="*" ControlToValidate="txtconsigneegstinuin" ValidationGroup="valeway"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label">
                        Actual To State</label>
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="drpactualtostate" runat="server" CssClass="form-control">
                        <asp:ListItem Value="0">--SELECT--</asp:ListItem>
                        <asp:ListItem Value="35">ANDAMAN AND NICOBAR</asp:ListItem>
                        <asp:ListItem Value="37">ANDHRA PRADESH</asp:ListItem>
                        <asp:ListItem Value="12">ARUNACHAL PRADESH</asp:ListItem>
                        <asp:ListItem Value="18">ASSAM</asp:ListItem>
                        <asp:ListItem Value="10">BIHAR</asp:ListItem>
                        <asp:ListItem Value="4">CHANDIGARH</asp:ListItem>
                        <asp:ListItem Value="22">CHHATTISGARH</asp:ListItem>
                        <asp:ListItem Value="26">DADRA AND NAGAR HAVELI</asp:ListItem>
                        <asp:ListItem Value="25">DAMAN AND DIU</asp:ListItem>
                        <asp:ListItem Value="7">DELHI</asp:ListItem>
                        <asp:ListItem Value="30">GOA</asp:ListItem>
                        <asp:ListItem Value="24">GUJARAT</asp:ListItem>
                        <asp:ListItem Value="6">HARYANA</asp:ListItem>
                        <asp:ListItem Value="2">HIMACHAL PRADESH</asp:ListItem>
                        <asp:ListItem Value="1">JAMMU AND KASHMIR</asp:ListItem>
                        <asp:ListItem Value="20">JHARKHAND</asp:ListItem>
                        <asp:ListItem Value="29">KARNATAKA</asp:ListItem>
                        <asp:ListItem Value="32">KERALA</asp:ListItem>
                        <asp:ListItem Value="31">LAKSHADWEEP</asp:ListItem>
                        <asp:ListItem Value="23">MADHYA PRADESH</asp:ListItem>
                        <asp:ListItem Value="27">MAHARASHTRA</asp:ListItem>
                        <asp:ListItem Value="14">MANIPUR</asp:ListItem>
                        <asp:ListItem Value="17">MEGHALAYA</asp:ListItem>
                        <asp:ListItem Value="15">MIZORAM</asp:ListItem>
                        <asp:ListItem Value="13">NAGALAND</asp:ListItem>
                        <asp:ListItem Value="21">ORISSA</asp:ListItem>
                        <asp:ListItem Value="34">PONDICHERRY</asp:ListItem>
                        <asp:ListItem Value="3">PUNJAB</asp:ListItem>
                        <asp:ListItem Value="8">RAJASTHAN</asp:ListItem>
                        <asp:ListItem Value="11">SIKKIM</asp:ListItem>
                        <asp:ListItem Value="33">TAMIL NADU</asp:ListItem>
                        <asp:ListItem Value="36">TELANGANA</asp:ListItem>
                        <asp:ListItem Value="16">TRIPURA</asp:ListItem>
                        <asp:ListItem Value="9">UTTAR PRADESH</asp:ListItem>
                        <asp:ListItem Value="5">UTTARAKHAND</asp:ListItem>
                        <asp:ListItem Value="19">WEST BENGAL</asp:ListItem>
                        <asp:ListItem Value="36">OTHER TERRITORY</asp:ListItem>
                        <asp:ListItem Value="37">OTHER COUNTRIES</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator45" runat="server" ErrorMessage="Please select Actual To State."
                        Text="*" ControlToValidate="drpactualtostate" ValidationGroup="valeway" InitialValue="0"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <div class="row">
                <div class="col-Md-4">
                </div>
                <div class="col-Md-2">
                    <b>JSON File Path :</b>
                </div>
                <div class="col-Md-3">
                    <asp:Label ID="lblpathp" runat="server" Text="D:\Eway Bill JSON" Font-Bold="true"></asp:Label>
                </div>
                <div class="col-Md-3">
                </div>
            </div>
            <div class="row">
                <div class="col-Md-12">
                    <asp:Button ID="btnsaveewaydata" runat="server" Text="Save" CssClass="btn btn-default forbutton"
                        OnClick="btnsaveewaydata_Click" ValidationGroup="valeway" />
                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="valeway"
                        ShowSummary="false" ShowMessageBox="true" />
                </div>
            </div>
        </asp:Panel>
    </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtscdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtsodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtlrdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });



        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtscdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtscdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtsodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtsodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtlrdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtlrdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
