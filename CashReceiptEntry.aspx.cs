﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlClient;
using System.Data.SqlTypes;

public partial class CashReceiptEntry : System.Web.UI.Page
{
    ForBankReceipt fbrclass = new ForBankReceipt();
    ForLedger flclass = new ForLedger();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "insert")
            {
                fillacnamedrop();
                getbrno();
                txtvdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemscr"] = CreateTemplate();
                Session["dtpitemscr1"] = CreateTemplate1();
                txtvoucherno.ReadOnly = false;
            }
            else if (Request["mode"].ToString() == "ledger")
            {
                fillacnamedrop();
                filleditdata();
                txtvoucherno.ReadOnly = true;
                Session["dtpitemscr1"] = CreateTemplate1();
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
            }
            else
            {
                fillacnamedrop();
                filleditdata();
                txtvoucherno.ReadOnly = true;
                Session["dtpitemscr1"] = CreateTemplate1();
            }            
            Page.SetFocus(txtcashname);
            //fillbillcombo();
        }
    }

    public void fillacnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fbrclass.selectallacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.Items.Clear();
            drpacname.DataSource = dtdata;
            drpacname.DataTextField = "tt";
            drpacname.DataValueField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    public void getbrno()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.istype = "CR";
        //DataTable dtvno = new DataTable();
        //dtvno = fbrclass.selectunusedcashreceiptno(li);
        //if (dtvno.Rows.Count > 0)
        //{
        //    txtvoucherno.Text = dtvno.Rows[0]["vno"].ToString();
        //}
        //else
        //{
        //    txtvoucherno.Text = "";
        //}
        DataTable dtvno = new DataTable();
        dtvno = fbrclass.selectlastnostring(li);
        if (dtvno.Rows.Count > 0)
        {
            txtvoucherno.Text = (Convert.ToInt64(dtvno.Rows[0]["voucherno"].ToString()) + 1).ToString();
        }
        else
        {
            txtvoucherno.Text = "1";
        }
    }

    public void filleditdata()
    {
        li.istype = "CR";
        li.voucherno = Convert.ToInt64(Request["vno"].ToString());
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fbrclass.selectbr1datafromvno(li);
        if (dtdata.Rows.Count > 0)
        {
            txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
            txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtcashname.Text = dtdata.Rows[0]["name"].ToString();
            lbltotal.Text = dtdata.Rows[0]["total"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            DataTable dtitem = new DataTable();
            dtitem = fbrclass.selectbrdatafromvno(li);
            if (dtitem.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitem;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Data Found.";
            }
            counttotal();
            btnsaveall.Text = "Update";
        }
    }

    public void fillbillcombo()
    {
        //li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        //DataTable dtdata = new DataTable();
        //dtdata = fbrclass.selectallbillnoaa(li);
        //if (dtdata.Rows.Count > 0)
        //{
        //    combillno.DataSource = dtdata;
        //    combillno.DataTextField = "pino";
        //    combillno.DataBind();
        //}
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetBankname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        //dt = fbrclass.selectallcashname(li);
        dt = fbrclass.selectallcashnamefromacm(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetBillno(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallbillno(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    //protected void txtclientcode_TextChanged(object sender, EventArgs e)
    //{
    //    txtclientcode.Text=txtclientcode.Text.Split('-')[0];
    //}
    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("acname", typeof(string));
        dtpitems.Columns.Add("remarks", typeof(string));
        dtpitems.Columns.Add("ccode", typeof(Int64));
        dtpitems.Columns.Add("amount", typeof(double));
        dtpitems.Columns.Add("chequeno", typeof(string));
        dtpitems.Columns.Add("agbill", typeof(string));
        dtpitems.Columns.Add("sino", typeof(string));
        dtpitems.Columns.Add("paidamount", typeof(double));
        return dtpitems;
    }

    public DataTable CreateTemplate1()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("refid", typeof(Int64));
        dtpitems.Columns.Add("sino", typeof(Int64));
        dtpitems.Columns.Add("strsino", typeof(string));
        dtpitems.Columns.Add("sidate", typeof(DateTime));
        dtpitems.Columns.Add("billamount", typeof(double));
        dtpitems.Columns.Add("paidamount", typeof(double));
        dtpitems.Columns.Add("receivedamount", typeof(double));
        dtpitems.Columns.Add("remainamount", typeof(double));
        return dtpitems;
    }

    public DataTable CreateTemplatebr1()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("refid", typeof(Int64));
        dtpitems.Columns.Add("sino", typeof(Int64));
        dtpitems.Columns.Add("strsino", typeof(string));
        dtpitems.Columns.Add("sidate", typeof(DateTime));
        dtpitems.Columns.Add("billamount", typeof(double));
        dtpitems.Columns.Add("paidamount", typeof(double));
        dtpitems.Columns.Add("receivedamount", typeof(double));
        dtpitems.Columns.Add("remainamount", typeof(double));
        return dtpitems;
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemscr"];
            DataRow dr = dt.NewRow();
            if (rptlist.Rows.Count == 0)
            {
                dr["id"] = rptlist.Rows.Count + 1;
            }
            else if (rptlist.Rows.Count >= 1)
            {
                dr["id"] = Convert.ToInt64(dt.Rows[dt.Rows.Count - 1]["id"].ToString()) + 1;
            }
            dr["acname"] = drpacname.SelectedValue;
            dr["remarks"] = txtremarks.Text;
            if (txtclientcode.Text.Trim() != string.Empty)
            {
                var cc = txtclientcode.Text.IndexOf("-");
                if (cc != -1)
                {
                    dr["ccode"] = txtclientcode.Text.Split('-')[0];
                }
                else
                {
                    if (txtclientcode.Text != "0")
                    {
                        dr["ccode"] = txtclientcode.Text;
                    }
                    else
                    {
                        dr["ccode"] = 0;
                    }
                }
            }
            else
            {
                dr["ccode"] = 0;
            }
            dr["amount"] = txtamount.Text;
            dr["chequeno"] = "";
            dr["agbill"] = DropDownList1.SelectedItem.Text;
            dr["sino"] = SessionMgt.strsino;
            dr["paidamount"] = SessionMgt.paidamount;
            dt.Rows.Add(dr);
            Session["dtpitemscr"] = dt;
            if (txtclientcode.Text.Trim() != string.Empty)
            {
                var cc = txtclientcode.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.ccode = Convert.ToInt64(txtclientcode.Text.Split('-')[0]);
                }
                else
                {
                    if (txtclientcode.Text != "0")
                    {
                        li.ccode = Convert.ToInt64(txtclientcode.Text);
                    }
                    else
                    {
                        li.ccode = 0;
                    }
                }
            }
            else
            {
                li.ccode = 0;
            }
            li.voucherdate = Convert.ToDateTime(txtvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.acname = drpacname.SelectedValue;
            if (DropDownList1.SelectedItem.Text == "Y")
            {
                DataTable dtf = (DataTable)Session["dtpitemscr3"];
                DataTable dtbrag = (DataTable)Session["dtpitemscr1"];
                for (int c = 0; c < dtf.Rows.Count; c++)
                {
                    SessionMgt.issipi = "SI";
                    DataRow dr1 = dtbrag.NewRow();
                    dr1["refid"] = dtf.Rows[c]["refid"].ToString();
                    dr1["sino"] = Convert.ToInt64(dtf.Rows[c]["sino"].ToString());
                    dr1["strsino"] = dtf.Rows[c]["strsino"].ToString();
                    dr1["sidate"] = Convert.ToDateTime(dtf.Rows[c]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["billamount"] = Convert.ToDouble(dtf.Rows[c]["billamount"].ToString());
                    dr1["paidamount"] = Convert.ToDouble(dtf.Rows[c]["paidamount"].ToString());
                    dr1["receivedamount"] = Convert.ToDouble(dtf.Rows[c]["receivedamount"].ToString());
                    dr1["remainamount"] = Convert.ToDouble(dtf.Rows[c]["remainamount"].ToString());
                    dtbrag.Rows.Add(dr1);
                }
                Session["dtpitemscr1"] = dtbrag;
            }
            this.bindgrid();
        }
        else
        {
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            li.voucherdate = Convert.ToDateTime(txtvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.name = txtcashname.Text;
            li.acname = drpacname.SelectedValue;
            if (txtclientcode.Text.Trim() != string.Empty)
            {
                var cc = txtclientcode.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.ccode = Convert.ToInt64(txtclientcode.Text.Split('-')[0]);
                }
                else
                {
                    if (txtclientcode.Text != "0")
                    {
                        li.ccode = Convert.ToInt64(txtclientcode.Text);
                    }
                    else
                    {
                        li.ccode = 0;
                    }
                }
            }
            else
            {
                li.ccode = 0;
            }
            li.remarks = txtremarks.Text;
            li.amount = Convert.ToDouble(txtamount.Text);
            li.chequeno = "";
            li.agbill = DropDownList1.SelectedItem.Text;
            li.istype = "CR";
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fbrclass.insertbrdata(li);
            li.activity = "New Cash Receipt Item Inserted.";
            faclass.insertactivity(li);
            li.strvoucherno = "CR" + (Convert.ToInt64(txtvoucherno.Text)).ToString();
            li.refid = SessionMgt.id;
            li.debitcode = txtcashname.Text;
            li.creditcode = drpacname.SelectedValue;
            li.description = li.remarks;
            li.istype1 = "D";
            //flclass.insertledgerdata(li);
            li.debitcode = drpacname.SelectedValue;
            li.creditcode = txtcashname.Text;
            li.istype1 = "C";
            //flclass.insertledgerdata(li);
            // if (SessionMgt.sino != 0)
            DataTable dtqq = (DataTable)Session["dtpitemscr3"];
            if (DropDownList1.SelectedItem.Text == "Y")
            {
                //bank ac shadow data
                //bank ac shadow data
                for (int z = 0; z < dtqq.Rows.Count; z++)
                {
                    li.sino = Convert.ToInt64(dtqq.Rows[z]["sino"].ToString());
                    li.strsino = dtqq.Rows[z]["strsino"].ToString();
                    double receivedamt = 0;
                    double remaindamt = 0;
                    double paiddamt = 0;
                    DataTable dtsidata = new DataTable();
                    dtsidata = fbrclass.selectdatatfromsinostring(li);
                    if (dtsidata.Rows.Count > 0)
                    {
                        if (dtsidata.Rows[0]["receivedamount"].ToString().Trim() != string.Empty)
                        {
                            receivedamt = Convert.ToDouble(dtsidata.Rows[0]["receivedamount"].ToString());
                        }
                        if (dtsidata.Rows[0]["remainamount"].ToString().Trim() != string.Empty)
                        {
                            remaindamt = Convert.ToDouble(dtsidata.Rows[0]["remainamount"].ToString());
                        }
                        //if (lblgvpaidamount.Text.Trim() != string.Empty)
                        //{
                        paiddamt = Convert.ToDouble(dtqq.Rows[z]["paidamount"].ToString());
                        //}
                        remaindamt = remaindamt - paiddamt;
                        receivedamt = receivedamt + paiddamt;
                        li.receivedamount = receivedamt;
                        li.remainamount = remaindamt;
                        //li.sino = Convert.ToInt64(SessionMgt.sino);
                        li.sino = Convert.ToInt64(dtqq.Rows[z]["sino"].ToString());
                        SessionMgt.invoiceno = li.sino;
                        SessionMgt.issipi = "SI";
                        fbrclass.updatesidatastring(li);
                        li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                        li.istype = "CR";
                        //li.ccode = Convert.ToInt64(txtclientcode.Text.Split('-')[0]);
                        //li.amount = Convert.ToDouble(SessionMgt.paidamount);
                        li.amount = Convert.ToDouble(dtqq.Rows[z]["paidamount"].ToString());
                        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                        li.uname = Request.Cookies["ForLogin"]["username"];
                        li.udate = System.DateTime.Now;
                        li.issipi = "SI";
                        fbrclass.insertbankacshadowdata(li);
                        //
                    }
                }
            }

            DataTable dtitem = new DataTable();
            dtitem = fbrclass.selectbrdatafromvno(li);
            if (dtitem.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitem;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Data Found.";
            }

        }
        counttotal();
        SessionMgt.sino = 0;
        SessionMgt.paidamount = 0;
        fillacnamedrop();
        txtremarks.Text = string.Empty;
        txtclientcode.Text = string.Empty;
        txtamount.Text = string.Empty;
        DropDownList1.SelectedValue = "N";
        Page.SetFocus(drpacname);
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemscr"] as DataTable;
        rptlist.DataSource = dtsession;
        rptlist.DataBind();
    }

    protected void btnsaveall_Click(object sender, EventArgs e)
    {
        if (rptlist.Rows.Count > 0)
        {
            string xxyear = Request.Cookies["ForLogin"]["currentyear"];
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            li.voucherdate = Convert.ToDateTime(txtvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //int zz = xxyear.IndexOf(yyyear);
            if (li.voucherdate <= yyyear1 && li.voucherdate >= yyyear)
            {

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
                return;
            }
            li.name = txtcashname.Text;
            li.istype = "CR";
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.totbasicamount = Convert.ToDouble(lbltotal.Text);
            if (btnsaveall.Text == "Save")
            {
                if (rptlist.Rows.Count > 0)
                {
                    DataTable dtcheck = new DataTable();
                    dtcheck = fbrclass.selectbr1datafromvno(li);
                    if (dtcheck.Rows.Count == 0)
                    {
                        fbrclass.insertbr1data(li);
                        li.activity = li.voucherno + " New Cash Receipt Inserted.";
                        faclass.insertactivity(li);
                    }
                    else
                    {
                        li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                        fbrclass.updateisusedcr(li);
                        getbrno();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Voucher No. already exist.Try Again.');", true);
                        return;
                    }
                    for (int c = 0; c < rptlist.Rows.Count; c++)
                    {
                        li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                        Label lblid = (Label)rptlist.Rows[c].FindControl("lblid");
                        TextBox lblacname = (TextBox)rptlist.Rows[c].FindControl("lblacname");
                        TextBox lblremarks = (TextBox)rptlist.Rows[c].FindControl("lblremarks");
                        TextBox lblclientcode = (TextBox)rptlist.Rows[c].FindControl("lblclientcode");
                        TextBox lblamount = (TextBox)rptlist.Rows[c].FindControl("lblamount");
                        //Label lblchequeno = (Label)rptlist.Rows[c].FindControl("lblchequeno");
                        Label lblagbill = (Label)rptlist.Rows[c].FindControl("lblagbill");
                        Label lblgvsino = (Label)rptlist.Rows[c].FindControl("lblgvsino");
                        Label lblgvpaidamount = (Label)rptlist.Rows[c].FindControl("lblgvpaidamount");
                        li.acname = lblacname.Text;
                        li.remarks = lblremarks.Text;
                        //li.ccode = Convert.ToInt64(lblclientcode.Text);
                        //var cc = lblclientcode.Text.IndexOf("-");
                        //if (cc != -1)
                        //{
                        li.ccode = Convert.ToInt64(lblclientcode.Text);
                        //}
                        //else
                        //{
                        //    li.ccode = 0;
                        //}
                        li.amount = Convert.ToDouble(lblamount.Text);
                        li.agbill = lblagbill.Text;
                        li.chequeno = "";
                        fbrclass.insertbrdata(li);
                        li.strvoucherno = "CR" + (Convert.ToInt64(txtvoucherno.Text)).ToString();
                        li.refid = SessionMgt.id;
                        li.debitcode = txtcashname.Text;
                        li.creditcode = lblacname.Text;
                        li.description = li.remarks;
                        li.istype1 = "D";
                        //flclass.insertledgerdata(li);
                        li.debitcode = lblacname.Text;
                        li.creditcode = txtcashname.Text;
                        li.istype1 = "C";
                        //flclass.insertledgerdata(li);
                        //if (lblgvsino.Text != "0")
                        if (lblagbill.Text == "Y")
                        {
                            //bank ac shadow entry data
                            //li.sino = Convert.ToInt64(lblgvsino.Text);
                            li.refid = Convert.ToInt64(lblid.Text);
                            li.refid1 = Convert.ToInt64(lblid.Text);
                            DataTable dtqq = (DataTable)Session["dtpitemscr1"];
                            Session["dtpitemscr2"] = CreateTemplate1();
                            DataTable dtrow1 = new DataTable();
                            dtrow1 = (DataTable)Session["dtpitemscr2"];
                            DataView dtv = dtqq.DefaultView;
                            dtrow1 = dtqq.Select("refid=" + li.refid + "").CopyToDataTable();
                            //foreach (DataRow row in result)
                            //{
                            //    //Console.WriteLine("{0}, {1}", row[0], row[1]);                            
                            //    //dtrow1 = dtqq.Select().Where(p => (p["refid"]) == li.refid.ToString()).CopyToDataTable();
                            //    //dtrow1.ImportRow(row);
                            //}
                            //dtrow1 = fbrclass.selecttempbankacshadowdatatfromrefid(li);
                            for (int r = 0; r < dtrow1.Rows.Count; r++)
                            {
                                li.strsino = dtrow1.Rows[r]["strsino"].ToString();
                                double receivedamt = 0;
                                double remaindamt = 0;
                                double paiddamt = 0;
                                DataTable dtsidata = new DataTable();
                                dtsidata = fbrclass.selectdatatfromsinostring(li);
                                if (dtsidata.Rows.Count > 0)
                                {
                                    if (dtsidata.Rows[0]["receivedamount"].ToString().Trim() != string.Empty)
                                    {
                                        receivedamt = Convert.ToDouble(dtsidata.Rows[0]["receivedamount"].ToString());
                                    }
                                    if (dtsidata.Rows[0]["remainamount"].ToString().Trim() != string.Empty)
                                    {
                                        remaindamt = Convert.ToDouble(dtsidata.Rows[0]["remainamount"].ToString());
                                    }
                                    if (dtrow1.Rows[r]["paidamount"].ToString().Trim() != string.Empty)
                                    {
                                        paiddamt = Convert.ToDouble(dtrow1.Rows[r]["paidamount"].ToString());
                                    }
                                    remaindamt = remaindamt - paiddamt;
                                    receivedamt = receivedamt + paiddamt;
                                    li.receivedamount = receivedamt;
                                    li.remainamount = remaindamt;
                                    //li.sino = Convert.ToInt64(SessionMgt.sino);
                                    SessionMgt.invoiceno = li.sino;
                                    SessionMgt.issipi = "SI";
                                    fbrclass.updatesidatastring(li);
                                    li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                                    li.istype = "CR";
                                    //li.ccode = Convert.ToInt64(txtclientcode.Text.Split('-')[0]);
                                    li.amount = Convert.ToDouble(dtrow1.Rows[r]["paidamount"].ToString());
                                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                                    li.uname = Request.Cookies["ForLogin"]["username"];
                                    li.udate = System.DateTime.Now;
                                    li.issipi = "SI";
                                    li.refid = SessionMgt.id;
                                    fbrclass.insertbankacshadowdata(li);
                                    //
                                }
                            }
                        }

                    }
                    fbrclass.updateisusedcr(li);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Cash Receipt Data.');", true);
                    return;
                }
            }
            else
            {
                if (rptlist.Rows.Count > 0)
                {
                    li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                    li.type = "";
                    fbrclass.updatebr1data(li);
                    fbrclass.updatebrdatadateonly(li);
                    fbrclass.updatebankacshadowdatadateonly(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.voucherno + " Cash Receipt data Updated.";
                    faclass.insertactivity(li);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Cash Receipt Data.');", true);
                    return;
                }
            }
            counttotal();
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            fbrclass.deleteallledgerdatacr(li);
            for (int c = 0; c < rptlist.Rows.Count; c++)
            {
                Label lblid = (Label)rptlist.Rows[c].FindControl("lblid");
                TextBox lblacname = (TextBox)rptlist.Rows[c].FindControl("lblacname");
                TextBox lblremarks = (TextBox)rptlist.Rows[c].FindControl("lblremarks");
                TextBox lblclientcode = (TextBox)rptlist.Rows[c].FindControl("lblclientcode");
                TextBox lblamount = (TextBox)rptlist.Rows[c].FindControl("lblamount");
                //Label lblchequeno = (Label)rptlist.Rows[c].FindControl("lblchequeno");
                Label lblagbill = (Label)rptlist.Rows[c].FindControl("lblagbill");
                Label lblgvsino = (Label)rptlist.Rows[c].FindControl("lblgvsino");
                Label lblgvpaidamount = (Label)rptlist.Rows[c].FindControl("lblgvpaidamount");
                li.id = Convert.ToInt64(lblid.Text);
                li.acname = lblacname.Text;
                li.remarks = lblremarks.Text;
                //li.ccode = Convert.ToInt64(lblclientcode.Text);
                var cc = lblclientcode.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.ccode = Convert.ToInt64(lblclientcode.Text.Split('-')[0]);
                }
                else
                {
                    if (lblclientcode.Text != "0")
                    {
                        li.ccode = Convert.ToInt64(lblclientcode.Text);
                    }
                    else
                    {
                        li.ccode = 0;
                    }
                }
                li.amount = Convert.ToDouble(lblamount.Text);
                li.agbill = lblagbill.Text;
                li.chequeno = "";
                //fbrclass.insertbrdata(li);
                li.strvoucherno = "CR" + (Convert.ToInt64(txtvoucherno.Text)).ToString();
                //li.refid = SessionMgt.id;
                li.refid = Convert.ToInt64(lblid.Text);
                li.debitcode = txtcashname.Text;
                li.creditcode = lblacname.Text;
                li.description = li.remarks;
                li.istype1 = "D";
                flclass.insertledgerdata(li);
                li.debitcode = lblacname.Text;
                li.creditcode = txtcashname.Text;
                li.istype1 = "C";
                flclass.insertledgerdata(li);
                if (btnsaveall.Text == "Update")
                {
                    fbrclass.updatebrdata(li);
                }
            }            
            if (Request["mode"].ToString() != "ledger")
            {
                Response.Redirect("~/CashReceiptList.aspx?pagename=CashReceiptList");
            }
            else
            {
                object refUrl = ViewState["RefUrl"];
                string ch = (string)refUrl;
                ch = ch.Split('?')[0] + "?acname=" + SessionMgt.acname + "&ccode=" + SessionMgt.ccode + "&fromdate=" + SessionMgt.fromdate + "&todate=" + SessionMgt.todate + "";
                SessionMgt.acname = "";
                SessionMgt.ccode = 0;
                SessionMgt.fromdate = "";
                SessionMgt.todate = "";
                if (refUrl != null)
                    Response.Redirect(ch);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Cash Receipt Data.');", true);
            return;
        }
    }
    //protected void rptlist_ItemCommand(object source, RepeaterCommandEventArgs e)
    //{
    //    if (e.CommandName == "delete")
    //    {
    //        if (btnsaveall.Text == "Save")
    //        {
    //            DataTable dt = Session["dtpitems"] as DataTable;
    //            dt.Rows.Remove(dt.Rows[]);
    //            //Session["ddc"] = dt;
    //            Session["dtpitems"] = dt;
    //            this.bindgrid();
    //        }
    //        else
    //        {
    //        }
    //        //counttotal();
    //        //Label2.Text = gvitemlist.Rows.Count.ToString();
    //    }
    //}
    protected void rptlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = Session["dtpitemscr"] as DataTable;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            Label lblid = (Label)rptlist.Rows[e.RowIndex].FindControl("lblid");
            li.id = Convert.ToInt64(lblid.Text);
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            li.istype = "CR";
            fbrclass.deletetepshadowtabledatafroidnvno(li);
            //DataTable dtamount = new DataTable();
            //dtamount = fbrclass.selectamounttodebitfromacfromaccountname(li);
            //if (dtamount.Rows.Count > 0)
            //{
            ////////li.amount = Convert.ToDouble(dt.Rows[0]["amount"].ToString());
            ////////li.invoiceno = SessionMgt.invoiceno;
            ////////li.acname = dt.Rows[0]["acname"].ToString();
            ////////li.issipi = SessionMgt.issipi;
            ////////DataTable dtsi = new DataTable();
            ////////dtsi = fbrclass.selectamountforupdation(li);
            ////////li.receivedamount = Convert.ToDouble(dtsi.Rows[0]["receivedamount"].ToString()) - li.amount;
            ////////li.remainamount = Convert.ToDouble(dtsi.Rows[0]["remainamount"].ToString()) + li.amount;
            ////////fbrclass.updatesidata11(li);
            //}

            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            ////////li.voucherno = Convert.ToInt64(txtvoucherno.Text);

            ////////fbrclass.deletefromshadowtable(li);
            //Session["ddc"] = dt;
            DataTable dtrow1 = new DataTable();
            dtrow1 = (DataTable)Session["dtpitemscr1"];
            DataRow[] drow = dtrow1.Select("refid=" + li.id + "");
            //for (int c = 0; c < drow.Length; c++)
            foreach (DataRow dc in drow)
            {
                dtrow1.Rows.Remove(dc);
            }
            Session["dtpitemscr"] = dt;
            Session["dtpitemscr1"] = dtrow1;
            this.bindgrid();

        }
        else
        {
            if (rptlist.Rows.Count > 1)
            {
                li.istype = "CR";
                li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)rptlist.Rows[e.RowIndex].FindControl("lblid");
                li.id = Convert.ToInt64(lblid.Text);


                DataTable dtamount = new DataTable();
                dtamount = fbrclass.selectamounttodebitfromacfromaccountname(li);
                if (dtamount.Rows.Count > 0)
                {
                    for (int d = 0; d < dtamount.Rows.Count; d++)
                    {
                        li.amount = Convert.ToDouble(dtamount.Rows[d]["amount1"].ToString());
                        //li.invoiceno = Convert.ToInt64(dtamount.Rows[0]["invoiceno"].ToString());
                        li.strsino = dtamount.Rows[d]["invoiceno"].ToString();
                        li.acname = dtamount.Rows[d]["acname"].ToString();
                        li.issipi = dtamount.Rows[d]["issipi"].ToString();
                        DataTable dtsi = new DataTable();
                        dtsi = fbrclass.selectamountforupdationstring(li);
                        li.receivedamount = Convert.ToDouble(dtsi.Rows[0]["receivedamount"].ToString()) - li.amount;
                        li.remainamount = Convert.ToDouble(dtsi.Rows[0]["remainamount"].ToString()) + li.amount;
                        fbrclass.updatesidata11string(li);
                    }
                }


                fbrclass.deletebrdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.id + " Cash Receipt item deleted.";
                faclass.insertactivity(li);
                li.istype = "CR";
                //fbrclass.deletefromledgertable(li);
                fbrclass.deletefromshadowtable(li);
                DataTable dtitem = new DataTable();
                dtitem = fbrclass.selectbrdatafromvno(li);
                if (dtitem.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    rptlist.Visible = true;
                    rptlist.DataSource = dtitem;
                    rptlist.DataBind();
                }
                else
                {
                    rptlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Data Found.";
                    rptlist.DataSource = null;
                    rptlist.DataBind();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
        counttotal();
        //counttotal();
        //Label2.Text = gvitemlist.Rows.Count.ToString();
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["dtpitemscr3"] = CreateTemplate1();
        if (DropDownList1.SelectedItem.Text == "Y")
        {
            if (drpacname.SelectedItem.Text != "--SELECT--")
            {
                li.acname = drpacname.SelectedValue;
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                //txtbillno.Enabled = true;
                lblpopupname.Text = drpacname.SelectedValue;
                lblpopupadjamount.Text = txtamount.Text;
                DataTable dtsalesinv = new DataTable();
                //DataTable dtpurchaseinv = new DataTable();
                dtsalesinv = fbrclass.selectallunpaidsalesinvoice(li);
                //dtpurchaseinv = fbrclass.selectallunpaidpurchaseinvoice(li);
                DataTable dtdata = new DataTable();
                if (dtsalesinv.Rows.Count > 0)
                {
                    lblemptysi.Visible = false;
                    gvsalesinv.Visible = true;
                    gvsalesinv.DataSource = dtsalesinv;
                    gvsalesinv.DataBind();
                }
                else
                {
                    gvsalesinv.Visible = false;
                    lblemptysi.Visible = true;
                    lblemptysi.Text = "No Sales Invoice Found.";
                }
                //if (dtpurchaseinv.Rows.Count > 0)
                //{
                //    lblemptypi.Visible = false;
                //    gvpurchaseinv.Visible = true;
                //    gvpurchaseinv.DataSource = dtpurchaseinv;
                //    gvpurchaseinv.DataBind();
                //}
                //else
                //{
                //    gvpurchaseinv.Visible = false;
                //    lblemptypi.Visible = true;
                //    lblemptypi.Text = "No Sales Invoice Found.";
                //}
                ModalPopupExtender2.Show();
            }
            else
            {
                DropDownList1.SelectedValue = "N";
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Account Name.');", true);
                return;
            }
        }
        else
        {
            //txtbillno.Enabled = false;
            ModalPopupExtender2.Hide();
        }
    }

    protected void txtsiamountpaid_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        Label lblstrsino = (Label)currentRow.FindControl("lblstrsino");
        Label lblremainamount = (Label)currentRow.FindControl("lblremainamount");
        TextBox txtsiamountpaid = (TextBox)currentRow.FindControl("txtsiamountpaid");
        if (Convert.ToDouble(txtsiamountpaid.Text) <= Convert.ToDouble(lblremainamount.Text))
        {
            double receivedamt = 0;
            double remaindamt = 0;
            double paiddamt = 0;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            Label lblsino = (Label)currentRow.FindControl("lblsino");
            Label lblreceivedamount = (Label)currentRow.FindControl("lblreceivedamount");
            SessionMgt.sino = Convert.ToInt64(lblsino.Text);
            SessionMgt.strsino = lblstrsino.Text;
            SessionMgt.paidamount = Convert.ToDouble(txtsiamountpaid.Text);
            ////if (lblreceivedamount.Text.Trim() != string.Empty)
            ////{
            ////    receivedamt = Convert.ToDouble(lblreceivedamount.Text);
            ////}
            ////if (lblremainamount.Text.Trim() != string.Empty)
            ////{
            ////    remaindamt = Convert.ToDouble(lblremainamount.Text);
            ////}
            ////if (txtsiamountpaid.Text.Trim() != string.Empty)
            ////{
            ////    paiddamt = Convert.ToDouble(txtsiamountpaid.Text);
            ////}
            ////remaindamt = remaindamt - paiddamt;
            ////receivedamt = receivedamt + paiddamt;
            ////li.receivedamount = receivedamt;
            ////li.remainamount = remaindamt;
            ////li.sino = Convert.ToInt64(lblsino.Text);
            ////SessionMgt.invoiceno = li.sino;
            ////SessionMgt.issipi = "SI";
            ////fbrclass.updatesidata(li);
            ////li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            ////li.istype = "CR";
            ////li.ccode = Convert.ToInt64(txtclientcode.Text.Split('-')[0]);
            ////li.amount = Convert.ToDouble(txtsiamountpaid.Text);
            ////li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            ////li.uname = Request.Cookies["ForLogin"]["username"];
            ////li.udate = System.DateTime.Now;
            ////li.issipi = "SI";
            ////fbrclass.insertbankacshadowdata(li);


            ModalPopupExtender2.Hide();
        }
        else
        {
            DropDownList1.SelectedValue = "N";
            Page.SetFocus(txtamount);
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Paid amount must be less than or equal to remain amount.');", true);
            return;
        }
    }

    protected void txtpiamountpaid_TextChanged(object sender, EventArgs e)
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        Label lblpino = (Label)currentRow.FindControl("lblpino");
        Label lblreceivedamount = (Label)currentRow.FindControl("lblreceivedamount");
        Label lblremainamount = (Label)currentRow.FindControl("lblremainamount");
        TextBox txtsiamountpaid = (TextBox)currentRow.FindControl("txtsiamountpaid");
        double receivedamt = Convert.ToDouble(lblreceivedamount.Text);
        double remaindamt = Convert.ToDouble(lblremainamount.Text);
        double paiddamt = Convert.ToDouble(txtsiamountpaid.Text);
        remaindamt = remaindamt - paiddamt;
        receivedamt = receivedamt + paiddamt;
        li.receivedamount = receivedamt;
        li.remainamount = remaindamt;
        li.pino = Convert.ToInt64(lblpino.Text);
        fbrclass.updatepidata(li);
        ModalPopupExtender2.Hide();
    }

    public void counttotal()
    {
        double totamount = 0;
        for (int c = 0; c < rptlist.Rows.Count; c++)
        {
            TextBox lblamount = (TextBox)rptlist.Rows[c].FindControl("lblamount");
            totamount = totamount + Convert.ToDouble(lblamount.Text);
        }
        lbltotal.Text = totamount.ToString();
    }

    protected void drpacname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpacname.SelectedItem.Text != "--SELECT--")
        {
            li.acname = drpacname.SelectedValue;
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            string rptname;
            DataTable dtq = new DataTable();
            SqlDataAdapter datt = new SqlDataAdapter();
            datt = new SqlDataAdapter("select ACMaster.acname,ACMaster.city,ACMaster.opbalance from ACMaster where acname='" + li.acname + "' order by acname", con);
            DataTable dtactt = new DataTable();
            datt.Fill(dtactt);
            if (dtactt.Rows.Count > 0)
            {
                li.acname = dtactt.Rows[0]["acname"].ToString();
                li.city = dtactt.Rows[0]["city"].ToString();
                li.acname = dtactt.Rows[0]["acname"].ToString();
                li.city = dtactt.Rows[0]["city"].ToString();
                SqlDataAdapter dadebit = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where type='D' and istype!='OP' and debitcode='" + li.acname + "'", con);
                //dadebit.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dadebit.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtdebit = new DataTable();
                dadebit.Fill(dtdebit);
                SqlDataAdapter dacredit = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where type='C' and istype!='OP' and debitcode='" + li.acname + "'", con);
                //dacredit.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                //dacredit.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                DataTable dtcredit = new DataTable();
                dacredit.Fill(dtcredit);

                double debit = 0;
                double credit = 0;

                double finalamt = 0;
                finalamt = Convert.ToDouble(dtactt.Rows[0]["opbalance"].ToString()) + Convert.ToDouble(dtdebit.Rows[0]["totdebit"].ToString()) - Convert.ToDouble(dtcredit.Rows[0]["totcredit"].ToString());
                lblramount.Text = finalamt.ToString();
                if (finalamt == 0)
                {
                    SqlDataAdapter datt1 = new SqlDataAdapter();
                    datt1 = new SqlDataAdapter("select ACMaster.acname,ACMaster.city,ACMaster.opbalance from ACMaster where acname='" + li.acname + "' order by acname", con);
                    DataTable dtactt1 = new DataTable();
                    datt1.Fill(dtactt1);
                    if (dtactt1.Rows.Count > 0)
                    {
                        li.acname = dtactt1.Rows[0]["acname"].ToString();
                        li.city = dtactt1.Rows[0]["city"].ToString();
                        SqlDataAdapter dadebit1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from ledger where type='D' and istype!='OP' and debitcode='" + li.acname + "'", con);
                        //dadebit1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                        //dadebit1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                        DataTable dtdebit1 = new DataTable();
                        dadebit1.Fill(dtdebit1);
                        SqlDataAdapter dacredit1 = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from ledger where type='C' and istype!='OP' and debitcode='" + li.acname + "'", con);
                        //dacredit1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
                        //dacredit1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
                        DataTable dtcredit1 = new DataTable();
                        dacredit1.Fill(dtcredit1);

                        double debit1 = 0;
                        double credit1 = 0;

                        double finalamt1 = 0;
                        finalamt1 = Convert.ToDouble(dtactt1.Rows[0]["opbalance"].ToString()) + Convert.ToDouble(dtdebit1.Rows[0]["totdebit"].ToString()) - Convert.ToDouble(dtcredit1.Rows[0]["totcredit"].ToString());
                        lblramount.Text = finalamt1.ToString();
                    }
                }
            }
        }
        Page.SetFocus(txtremarks);
    }

    protected void btnok_Click(object sender, EventArgs e)
    {
        DataTable dtsave = (DataTable)Session["dtpitemscr"];
        double amt = 0;
        for (int c = 0; c < gvsalesinv.Rows.Count; c++)
        {
            TextBox txtsiamountpaid = (TextBox)gvsalesinv.Rows[c].FindControl("txtsiamountpaid");
            if (txtsiamountpaid.Text.Trim() != string.Empty)
            {
                amt = amt + Convert.ToDouble(txtsiamountpaid.Text);
            }
        }
        if (amt != Convert.ToDouble(lblpopupadjamount.Text))
        {
            DropDownList1.SelectedValue = "N";
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Adjust amount and entered amount is different.Please enter same amount and try again.');", true);
            return;
        }
        DataTable dt = (DataTable)Session["dtpitemscr3"];
        for (int c = 0; c < gvsalesinv.Rows.Count; c++)
        {
            SessionMgt.issipi = "SI";
            Label lblsino = (Label)gvsalesinv.Rows[c].FindControl("lblsino");
            Label lblstrsino = (Label)gvsalesinv.Rows[c].FindControl("lblstrsino");
            Label lblsidate = (Label)gvsalesinv.Rows[c].FindControl("lblsidate");
            Label lblbillamount = (Label)gvsalesinv.Rows[c].FindControl("lblbillamount");
            TextBox txtsiamountpaid = (TextBox)gvsalesinv.Rows[c].FindControl("txtsiamountpaid");
            Label lblreceivedamount = (Label)gvsalesinv.Rows[c].FindControl("lblreceivedamount");
            Label lblremainamount = (Label)gvsalesinv.Rows[c].FindControl("lblremainamount");
            if (txtsiamountpaid.Text.Trim() != string.Empty)
            {
                DataRow dr = dt.NewRow();
                SessionMgt.strsino = lblstrsino.Text;
                SessionMgt.paidamount = amt;
                if (rptlist.Rows.Count == 0)
                {
                    dr["refid"] = rptlist.Rows.Count + 1;
                }
                else if (rptlist.Rows.Count >= 1)
                {
                    dr["refid"] = Convert.ToInt64(dtsave.Rows[dtsave.Rows.Count - 1]["id"].ToString()) + 1;
                }
                dr["sino"] = Convert.ToInt64(lblsino.Text);
                dr["strsino"] = lblstrsino.Text;
                dr["sidate"] = Convert.ToDateTime(lblsidate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr["billamount"] = Convert.ToDouble(lblbillamount.Text);
                dr["paidamount"] = Convert.ToDouble(txtsiamountpaid.Text);
                if (lblreceivedamount.Text.Trim() != string.Empty)
                {
                    dr["receivedamount"] = Convert.ToDouble(lblreceivedamount.Text);
                }
                else
                {
                    dr["receivedamount"] = 0;
                }
                dr["remainamount"] = Convert.ToDouble(lblremainamount.Text);
                dt.Rows.Add(dr);
            }
        }
        Session["dtpitemscr3"] = dt;
    }

    protected void btnfirst_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.istype = "CR";
        SqlDataAdapter da = new SqlDataAdapter("select * from BankACMaster1 where istype='CR' order by voucherno", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.voucherno = Convert.ToInt64(dtdataq.Rows[0]["voucherno"].ToString());
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            DataTable dtdata = new DataTable();
            dtdata = fbrclass.selectbr1datafromvno(li);
            if (dtdata.Rows.Count > 0)
            {
                txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
                txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtcashname.Text = dtdata.Rows[0]["name"].ToString();
                lbltotal.Text = dtdata.Rows[0]["total"].ToString();
                DataTable dtitem = new DataTable();
                dtitem = fbrclass.selectbrdatafromvno(li);
                if (dtitem.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    rptlist.Visible = true;
                    rptlist.DataSource = dtitem;
                    rptlist.DataBind();
                }
                else
                {
                    rptlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Data Found.";
                }
                counttotal();
                btnsaveall.Text = "Update";
            }
        }
    }
    protected void btnlast_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.istype = "CR";
        SqlDataAdapter da = new SqlDataAdapter("select * from BankACMaster1 where istype='CR' order by voucherno desc", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.voucherno = Convert.ToInt64(dtdataq.Rows[0]["voucherno"].ToString());
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            DataTable dtdata = new DataTable();
            dtdata = fbrclass.selectbr1datafromvno(li);
            if (dtdata.Rows.Count > 0)
            {
                txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
                txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtcashname.Text = dtdata.Rows[0]["name"].ToString();
                lbltotal.Text = dtdata.Rows[0]["total"].ToString();
                DataTable dtitem = new DataTable();
                dtitem = fbrclass.selectbrdatafromvno(li);
                if (dtitem.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    rptlist.Visible = true;
                    rptlist.DataSource = dtitem;
                    rptlist.DataBind();
                }
                else
                {
                    rptlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Data Found.";
                }
                counttotal();
                btnsaveall.Text = "Update";
            }
        }
    }
    protected void btnnext_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter daa = new SqlDataAdapter("select * from BankACMaster1 where istype='CR' order by voucherno", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.voucherno = Convert.ToInt64(txtvoucherno.Text) + 1;
        for (Int64 i = li.voucherno; i <= Convert.ToInt64(dtdataqa.Rows[dtdataqa.Rows.Count - 1]["voucherno"].ToString()); i++)
        {
            li.voucherno = i;
            li.istype = "CR";
            SqlDataAdapter da = new SqlDataAdapter("select * from BankACMaster1 where istype='CR' and voucherno=" + li.voucherno + "", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtdata = new DataTable();
                dtdata = fbrclass.selectbr1datafromvno(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
                    txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtcashname.Text = dtdata.Rows[0]["name"].ToString();
                    lbltotal.Text = dtdata.Rows[0]["total"].ToString();
                    DataTable dtitem = new DataTable();
                    dtitem = fbrclass.selectbrdatafromvno(li);
                    if (dtitem.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        rptlist.Visible = true;
                        rptlist.DataSource = dtitem;
                        rptlist.DataBind();
                    }
                    else
                    {
                        rptlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Data Found.";
                    }
                    counttotal();
                    btnsaveall.Text = "Update";
                    return;
                }
            }
        }
    }
    protected void btnprevious_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter daa = new SqlDataAdapter("select * from BankACMaster1 where istype='CR' order by voucherno", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.voucherno = Convert.ToInt64(txtvoucherno.Text) - 1;
        for (Int64 i = li.voucherno; i >= Convert.ToInt64(dtdataqa.Rows[0]["voucherno"].ToString()); i--)
        {
            li.voucherno = i;
            li.istype = "CR";
            //li.voucherno = Convert.ToInt64(txtvoucherno.Text) - 1;
            SqlDataAdapter da = new SqlDataAdapter("select * from BankACMaster1 where istype='CR' and voucherno=" + li.voucherno + "", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtdata = new DataTable();
                dtdata = fbrclass.selectbr1datafromvno(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
                    txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtcashname.Text = dtdata.Rows[0]["name"].ToString();
                    lbltotal.Text = dtdata.Rows[0]["total"].ToString();
                    DataTable dtitem = new DataTable();
                    dtitem = fbrclass.selectbrdatafromvno(li);
                    if (dtitem.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        rptlist.Visible = true;
                        rptlist.DataSource = dtitem;
                        rptlist.DataBind();
                    }
                    else
                    {
                        rptlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Data Found.";
                    }
                    counttotal();
                    btnsaveall.Text = "Update";
                    return;
                }
            }
        }
    }

}