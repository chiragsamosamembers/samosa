﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PurchaseChallanList.aspx.cs" Inherits="PurchaseChallanList" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Purchase Challan List</span><br />
        <div class="row">
            <div class="col-md-12">
                <asp:Button ID="btnpurchasechallan" runat="server" Text="Add New Purchase Challan"
                    class="btn btn-default forbutton" OnClick="btnpurchasechallan_Click" /></div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvpurchasechallanlist" runat="server" AutoGenerateColumns="False"
                        Width="100%" BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”"
                        Height="0px" CssClass="table table-bordered" OnRowCommand="gvpurchasechallanlist_RowCommand"
                        OnRowDeleting="gvpurchasechallanlist_RowDeleting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("pcno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PC NO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblpcno" ForeColor="Black" runat="server" Text='<%# bind("pcno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PC Date." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblpcdate" ForeColor="Black" runat="server" Text='<%#Convert.ToDateTime(Eval("pcdate")).ToString("dd-MM-yyyy") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblacname" ForeColor="Black" runat="server" Text='<%# bind("acname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PO NO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblpono" ForeColor="Black" runat="server" Text='<%# bind("pono") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="PO NO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblpono1" ForeColor="Black" runat="server" Text='<%# bind("pono1") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="PO NO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblstringpono" ForeColor="Black" runat="server" Text='<%# bind("stringpono") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("pcno") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
