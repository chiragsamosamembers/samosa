﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ClientMasterList.aspx.cs" Inherits="ClientMasterList" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Client Master List</span><br />
        <div class="row">
            <div class="col-md-2">
                <asp:Button ID="btnclient" runat="server" Text="Add New Client" class="btn btn-default forbutton"
                    OnClick="btnclient_Click" /></div>
            <div class="col-md-3">
                <asp:TextBox ID="txtsearchclientname" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtsearchclientname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    Status</label></div>
            <div class="col-md-2">
                <asp:DropDownList ID="drpstatus" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnsearch" runat="server" Text="Search" class="btn btn-default forbutton"
                    OnClick="btnsearch_Click" />
            </div>
            <div class="col-md-1">
                <asp:Label ID="lblcount" runat="server" Text="Label" Font-Bold="true" Font-Size="27px"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btncidwise" runat="server" Text="CIDWise Report" class="btn btn-default forbutton"
                    OnClick="btncidwise_Click" />
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvaclient" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvaclient_RowCommand" OnRowDeleting="gvaclient_RowDeleting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CID" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcode" ForeColor="Black" runat="server" Text='<%# bind("code") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Client Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblname" ForeColor="Black" runat="server" Text='<%# bind("name") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcity" ForeColor="Black" runat="server" Text='<%# bind("city") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ref. Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblreferencename" ForeColor="Black" runat="server" Text='<%# bind("referencename") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Handled By" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblhandledby" ForeColor="Black" runat="server" Text='<%# bind("handledby") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblstatus" ForeColor="Black" runat="server" Text='<%# bind("status") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("code") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
