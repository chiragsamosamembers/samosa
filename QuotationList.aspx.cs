﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using Survey.Classes;

public partial class QuotationList : System.Web.UI.Page
{
    public ReportDocument rptdoc;
    ForQuotationMaster fqmclass = new ForQuotationMaster();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        DataTable dtdata = new DataTable();
        dtdata = fqmclass.selectlastquono();
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvso.Visible = true;
            gvso.DataSource = dtdata;
            gvso.DataBind();
        }
        else
        {
            gvso.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Data Found.";
        }
    }

    protected void gvso_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.quono = Convert.ToDouble(e.CommandArgument);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Response.Redirect("~/Quotation.aspx?mode=update&quono=" + li.quono + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.quono = Convert.ToDouble(e.CommandArgument);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                fqmclass.deletequoitemsdata(li);
                fqmclass.deletequomasterdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.quono + " Bifurcation Deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
        else if (e.CommandName == "revised")
        {
            li.quono = Convert.ToDouble(e.CommandArgument);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            Response.Redirect("~/Quotation.aspx?mode=revised&quono=" + li.quono + "");
        }
        else if (e.CommandName == "print")
        {
            li.quono = Convert.ToDouble(e.CommandArgument);
            SessionMgt.quoteno = li.quono;
            ModalPopupExtender2.Show();
        }
    }
    protected void gvso_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void btnquotation_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/Quotation.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }

    protected void btngo_Click(object sender, EventArgs e)
    {
        if (drpreporttype.SelectedItem.Text == "With Header")
        {
            rptdoc = new ReportDocument();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            string ftow = "";
            li.quono = SessionMgt.quoteno;
            DataSet1 imageDataSet = new DataSet1();
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
            SqlDataAdapter da11 = new SqlDataAdapter("select * from QuoMaster inner join QuoItems on QuoMaster.quono=QuoItems.quono where QuoMaster.quono='" + li.quono + "' and QuoMaster.cno='" + li.cno + "' order by QuoItems.srno", con);// order by QuoItems.srno1// inner join ItemMaster on ItemMaster.itemname=QuoItems.itemname
            DataTable dtc = new DataTable();
            da11.Fill(imageDataSet.Tables["DataTable29"]);
            li.acname = imageDataSet.Tables["DataTable29"].Rows[0]["acname"].ToString();
            li.ccode = Convert.ToInt64(imageDataSet.Tables["DataTable29"].Rows[0]["ccode"].ToString());
            con.Open();
            DataTable dtacname = new DataTable();
            SqlDataAdapter das = new SqlDataAdapter("select * from ACMaster where acname=@acname and cno=@cno", con);
            das.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            das.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            das.Fill(dtacname);
            con.Close();

            con.Open();
            DataTable dtclient = new DataTable();
            SqlDataAdapter das1 = new SqlDataAdapter("select * from ClientMaster where code=@ccode and cno=@cno", con);
            das1.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            das1.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            das1.Fill(dtclient);
            con.Close();
            string address = dtacname.Rows[0]["add1"].ToString() + " " + dtacname.Rows[0]["add2"].ToString() + " " + dtacname.Rows[0]["add3"].ToString();
            string rptname;
            rptname = Server.MapPath(@"~/Reports/bifurcation.rpt");
            rptdoc.Load(rptname);
            string ccode = dtclient.Rows[0]["code"].ToString() + " - " + dtclient.Rows[0]["name"].ToString();
            rptdoc.DataDefinition.FormulaFields["ccode"].Text = "'" + ccode + "'";
            rptdoc.SetDataSource(imageDataSet.Tables["DataTable29"]);
            ExportOptions exprtopt = default(ExportOptions);
            string fname = "bifurcation.pdf";
            //create instance for destination option - This one is used to set path of your pdf file save 
            DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
            destiopt.DiskFileName = Server.MapPath(@"~/cheque/" + fname);


            exprtopt = rptdoc.ExportOptions;
            exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

            //use PortableDocFormat for PDF data
            exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
            exprtopt.DestinationOptions = destiopt;

            //finally export your report document
            rptdoc.Export();
            rptdoc.Close();
            rptdoc.Clone();
            rptdoc.Dispose();
            string path = fname;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('RepBifurcation.aspx?path=" + path + "&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
        else if (drpreporttype.SelectedItem.Text == "Without Header")
        {
            rptdoc = new ReportDocument();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            string ftow = "";
            li.quono = SessionMgt.quoteno;
            DataSet1 imageDataSet = new DataSet1();
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
            SqlDataAdapter da11 = new SqlDataAdapter("select * from QuoMaster inner join QuoItems on QuoMaster.quono=QuoItems.quono where QuoMaster.quono='" + li.quono + "' and QuoMaster.cno='" + li.cno + "' order by QuoItems.srno1", con);// inner join ItemMaster on ItemMaster.itemname=QuoItems.itemname
            DataTable dtc = new DataTable();
            da11.Fill(imageDataSet.Tables["DataTable29"]);
            li.acname = imageDataSet.Tables["DataTable29"].Rows[0]["acname"].ToString();
            li.ccode = Convert.ToInt64(imageDataSet.Tables["DataTable29"].Rows[0]["ccode"].ToString());
            con.Open();
            DataTable dtacname = new DataTable();
            SqlDataAdapter das = new SqlDataAdapter("select * from ACMaster where acname=@acname and cno=@cno", con);
            das.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            das.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            das.Fill(dtacname);
            con.Close();

            con.Open();
            DataTable dtclient = new DataTable();
            SqlDataAdapter das1 = new SqlDataAdapter("select * from ClientMaster where code=@ccode and cno=@cno", con);
            das1.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            das1.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            das1.Fill(dtclient);
            con.Close();
            string address = dtacname.Rows[0]["add1"].ToString() + " " + dtacname.Rows[0]["add2"].ToString() + " " + dtacname.Rows[0]["add3"].ToString();
            string rptname;
            rptname = Server.MapPath(@"~/Reports/bifurcation_1.rpt");
            rptdoc.Load(rptname);
            string ccode = dtclient.Rows[0]["code"].ToString() + " - " + dtclient.Rows[0]["name"].ToString();
            rptdoc.DataDefinition.FormulaFields["ccode"].Text = "'" + ccode + "'";
            rptdoc.SetDataSource(imageDataSet.Tables["DataTable29"]);
            ExportOptions exprtopt = default(ExportOptions);
            string fname = "bifurcation.pdf";
            //create instance for destination option - This one is used to set path of your pdf file save 
            DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
            destiopt.DiskFileName = Server.MapPath(@"~/cheque/" + fname);


            exprtopt = rptdoc.ExportOptions;
            exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

            //use PortableDocFormat for PDF data
            exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
            exprtopt.DestinationOptions = destiopt;

            //finally export your report document
            rptdoc.Export();
            rptdoc.Close();
            rptdoc.Clone();
            rptdoc.Dispose();
            string path = fname;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('RepBifurcation.aspx?path=" + path + "&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
        else
        {
            rptdoc = new ReportDocument();
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            string ftow = "";
            li.quono = SessionMgt.quoteno;
            DataSet1 imageDataSet = new DataSet1();
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
            SqlDataAdapter da11 = new SqlDataAdapter("select * from QuoMaster inner join QuoItems on QuoMaster.quono=QuoItems.quono where QuoMaster.quono='" + li.quono + "' and QuoMaster.cno='" + li.cno + "' order by QuoItems.srno1", con);// inner join ItemMaster on ItemMaster.itemname=QuoItems.itemname
            DataTable dtc = new DataTable();
            da11.Fill(imageDataSet.Tables["DataTable29"]);
            li.acname = imageDataSet.Tables["DataTable29"].Rows[0]["acname"].ToString();
            li.ccode = Convert.ToInt64(imageDataSet.Tables["DataTable29"].Rows[0]["ccode"].ToString());
            con.Open();
            DataTable dtacname = new DataTable();
            SqlDataAdapter das = new SqlDataAdapter("select * from ACMaster where acname=@acname and cno=@cno", con);
            das.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            das.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            das.Fill(dtacname);
            con.Close();

            con.Open();
            DataTable dtclient = new DataTable();
            SqlDataAdapter das1 = new SqlDataAdapter("select * from ClientMaster where code=@ccode and cno=@cno", con);
            das1.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            das1.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            das1.Fill(dtclient);
            con.Close();
            string address = dtacname.Rows[0]["add1"].ToString() + " " + dtacname.Rows[0]["add2"].ToString() + " " + dtacname.Rows[0]["add3"].ToString();
            string rptname;
            rptname = Server.MapPath(@"~/Reports/bifurcation.rpt");
            rptdoc.Load(rptname);
            string ccode = dtclient.Rows[0]["code"].ToString() + " - " + dtclient.Rows[0]["name"].ToString();
            rptdoc.DataDefinition.FormulaFields["ccode"].Text = "'" + ccode + "'";
            rptdoc.SetDataSource(imageDataSet.Tables["DataTable29"]);
            ExportOptions exprtopt = default(ExportOptions);
            string fname = "bifurcation.pdf";
            //create instance for destination option - This one is used to set path of your pdf file save 
            DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
            destiopt.DiskFileName = Server.MapPath(@"~/cheque/" + fname);

            ExportFormatType formatType = ExportFormatType.NoFormat;
            exprtopt = rptdoc.ExportOptions;
            exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

            //use PortableDocFormat for PDF data
            formatType = ExportFormatType.Excel;
            //exprtopt.DestinationOptions = destiopt;

            //finally export your report document
            //rptdoc.Export();
            //rptdoc.Close();
            //rptdoc.Clone();
            //rptdoc.Dispose();
            //string path = fname;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('RepBifurcation.aspx?path=" + path + "&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);

            rptdoc.ExportToHttpResponse(formatType, Response, true, "Bifurcation");
            Response.End();
            ModalPopupExtender2.Hide();
        }
    }

}