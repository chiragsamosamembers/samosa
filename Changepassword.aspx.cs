﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Changepassword : System.Web.UI.Page
{
    ForRegister frclass = new ForRegister();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtcompanyid.Text = Request.Cookies["ForCompany"]["cno"];
            txtcompanyid.ReadOnly = true;
            txtuname.Text = Request.Cookies["ForLogin"]["username"];
            txtuname.ReadOnly = true;
        }
    }
    protected void btnsaves_Click(object sender, EventArgs e)
    {
        li.cno = Convert.ToInt64(txtcompanyid.Text);
        li.username = txtuname.Text;
        li.password = txtoldpassword.Text;
        DataTable dtcheck = new DataTable();
        dtcheck = frclass.checkuserforchangepassword(li);
        if (dtcheck.Rows.Count > 0)
        {
            li.password = txtnewpassword.Text;
            frclass.updatepassworddata(li);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = "Password changed for user - " + txtuname.Text;
            faclass.insertactivity(li);
            txtoldpassword.Text = string.Empty;
            txtnewpassword.Text = string.Empty;
            txtconfirmpassword.Text = string.Empty;
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Password Changed Successfully.');", true);
            return;
        }
        else
        {
            txtoldpassword.Text = string.Empty;
            txtnewpassword.Text = string.Empty;
            txtconfirmpassword.Text = string.Empty;
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Your Username or Old Password is different.Please try again.');", true);
            return;
        }
    }
}