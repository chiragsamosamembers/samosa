﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Net;
using System.IO;

public partial class CompanySelection : System.Web.UI.Page
{
    ForLogin flclass = new ForLogin();
    ForCompanyMaster fcmclass = new ForCompanyMaster();
    LogicLayer li = new LogicLayer();
    public HttpCookie cookie = new HttpCookie("ForCompany");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //StreamReader reader = new StreamReader(Server.MapPath("~/bulkmail.htm"));
            //string readFile = reader.ReadToEnd();
            //string myString = "";
            //myString = readFile;
            //myString = myString.Replace("$$code$$", "");
            //System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            //message.To.Add("alluresofttech@gmail.com");
            //message.From = new System.Net.Mail.MailAddress("alluresofttech@gmail.com");
            //message.Subject = "Airmax Database Back up of " + System.DateTime.Now.ToString();
            //string imageatt1 = "~\\alluredb\\AirmaxDb";
            //System.Net.Mail.Attachment attachment1 = new System.Net.Mail.Attachment(Server.MapPath(imageatt1));
            //message.Attachments.Add(attachment1);

            ////System.IO.MemoryStream stream = new System.IO.MemoryStream(new byte[64000]);
            ////System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(Server.MapPath(Session["fname"].ToString()));
            ////message.Attachments.Add(attachment);
            //message.Body = "";



            ////message.AlternateViews.Add(htmlMail1);
            //message.IsBodyHtml = true;
            //System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
            //smtp.Port = 587;
            //smtp.Credentials = new NetworkCredential("alluresofttech@gmail.com", "parth@chirag@123");
            //smtp.EnableSsl = true;
            ////smtp.UseDefaultCredentials = true;
            //bool bb = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            //if (bb == true)
            //{
            //    smtp.Send(message);
            //}
            lblacyear.Text = Request.Cookies["ForLogin"]["acyear"];
            if (SessionMgt.UserID == 1)
            {
                fillgrid();
                //lblcname.Text = Request.Cookies["ForLogIn"]["cname"];
                lblcname.Text = Request.Cookies["ForLogIn"]["username"];
                if (Request.Cookies["ForLogIn"]["username"] == "admin@airmax.so")
                {
                    btnnewcompany.Visible = true;
                }
                else
                {
                    btnnewcompany.Visible = false;
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }
        }
    }

    public void fillgrid()
    {
        string q = "";
        li.username = Request.Cookies["ForLogIn"]["username"];
        DataTable dtlogindata = new DataTable();
        dtlogindata = fcmclass.selectlogindatafromlogintable(li);
        for (int c = 0; c < dtlogindata.Rows.Count; c++)
        {
            q = q + dtlogindata.Rows[c]["cno"].ToString() + ",";
        }
        q = q.TrimEnd(',');
        li.strcno = q.ToString();
        DataTable dtdata = new DataTable();
        dtdata = fcmclass.selectallcompanyforselection(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvcompanylist.Visible = true;
            gvcompanylist.DataSource = dtdata;
            gvcompanylist.DataBind();
        }
        else
        {
            gvcompanylist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Company Data Found.";
        }
    }

    protected void gvcompanylist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            li.cno = Convert.ToInt64(e.CommandArgument);

            HttpCookie cookie1 = new HttpCookie("Forcon");
            string dbname = "AT" + HttpContext.Current.Request.Cookies["ForLogin"]["currentyear"];
            if (HttpContext.Current.Request.Cookies["Forcon"] != null)
            {
                cookie1.Expires = DateTime.Now.AddDays(-1); // --------> cookie.Expires is the property you can set timeout
                HttpContext.Current.Response.AppendCookie(cookie1);
            }
            string cc = "Data Source=CHIRAG-PC:Initial Catalog=" + dbname + ":Integrated Security=True";//AM18012018//AM05022018//AM14022018//AM16022018_1//AM19022018            
            cookie1.Values.Add("conc", cc);
            cookie1.Expires = DateTime.Now.AddDays(7); // --------> cookie.Expires is the property you can set timeout
            HttpContext.Current.Response.AppendCookie(cookie1);

            cookie.Values.Add("cno", li.cno.ToString());
            cookie.Expires = DateTime.Now.AddDays(7); // --------> cookie.Expires is the property you can set timeout
            HttpContext.Current.Response.AppendCookie(cookie);
            Response.Redirect("~/Dashboard.aspx?cno=" + li.cno + "");
            //}
        }
    }

}