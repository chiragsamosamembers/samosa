﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class VATTypeMaster : System.Web.UI.Page
{
    ForVatMaster fvmclass = new ForVatMaster();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "update")
            {
                filleditdata();
            }
            else
            {
                txtuser.Text = Request.Cookies["ForLogin"]["username"];
            }
        }
    }

    public void filleditdata()
    {
        li.id = Convert.ToInt64(Request["id"].ToString());
        DataTable dtdata = new DataTable();
        dtdata = fvmclass.selectVATdatafromid(li);
        if (dtdata.Rows.Count > 0)
        {
            txttype.Text = dtdata.Rows[0]["typename"].ToString();
            txttypename.Text = dtdata.Rows[0]["typename"].ToString();
            ViewState["name"] = dtdata.Rows[0]["typename"].ToString();
            txtvat.Text = dtdata.Rows[0]["vatp"].ToString();
            txtaddtax.Text = dtdata.Rows[0]["addtaxp"].ToString();
            txtcentralsalestax.Text = dtdata.Rows[0]["centralsalep"].ToString();
            txtsalestax.Text = dtdata.Rows[0]["selltaxp"].ToString();
            txtsurcharge.Text = dtdata.Rows[0]["surchargep"].ToString();
            txtservicetaxp.Text = dtdata.Rows[0]["servicetaxp"].ToString();
            txtvatdesc.Text = dtdata.Rows[0]["vatdesc"].ToString();
            txtaddtaxdesc.Text = dtdata.Rows[0]["addtaxdesc"].ToString();
            txtcentralsalestaxdesc.Text = dtdata.Rows[0]["cstdesc"].ToString();
            txtservicetaxdesc.Text = dtdata.Rows[0]["servicetaxdesc"].ToString();
            txtform.Text = dtdata.Rows[0]["form"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            ViewState["id"] = dtdata.Rows[0]["id"].ToString();
            btnsaves.Text = "Update";
        }
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        li.typename = txttypename.Text;
        if (txtvat.Text.Trim() != string.Empty)
        {
            li.vatp = Convert.ToDouble(txtvat.Text);
        }
        else
        {
            li.vatp = 0;
        }
        if (txtaddtax.Text.Trim() != string.Empty)
        {
            li.addtaxp = Convert.ToDouble(txtaddtax.Text);
        }
        else
        {
            li.addtaxp = 0;
        }
        if (txtcentralsalestax.Text.Trim() != string.Empty)
        {
            li.centralsalep = Convert.ToDouble(txtcentralsalestax.Text);
        }
        else
        {
            li.centralsalep = 0;
        }
        if (txtsalestax.Text.Trim() != string.Empty)
        {
            li.selltaxp = Convert.ToDouble(txtsalestax.Text);
        }
        else
        {
            li.selltaxp = 0;
        }
        if (txtsurcharge.Text.Trim() != string.Empty)
        {
            li.surchargep = Convert.ToDouble(txtsurcharge.Text);
        }
        else
        {
            li.surchargep = 0;
        }
        if (txtservicetaxp.Text.Trim() != string.Empty)
        {
            li.servicetaxp = Convert.ToDouble(txtservicetaxp.Text);
        }
        else
        {
            li.servicetaxp = 0;
        }
        li.vatdesc = txtvatdesc.Text;
        li.addtaxdesc = txtaddtaxdesc.Text;
        li.cstdesc = txtcentralsalestaxdesc.Text;
        li.servicetaxdesc = txtservicetaxdesc.Text;
        DataTable dtvatdesc = new DataTable();
        dtvatdesc = fvmclass.checkacnameexistornotvatdesc(li);
        if (dtvatdesc.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Vat desc not exist in ACMaster so Please first enter in ACMaster and then try again.');", true);
            return;
        }
        DataTable dtaddtaxdesc = new DataTable();
        dtaddtaxdesc = fvmclass.checkacnameexistornotaddtaxdesc(li);
        if (dtaddtaxdesc.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Add tax desc not exist in ACMaster so Please first enter in ACMaster and then try again.');", true);
            return;
        }
        DataTable dtcstdesc = new DataTable();
        dtcstdesc = fvmclass.checkacnameexistornotcstdesc(li);
        if (dtcstdesc.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Cst desc not exist in ACMaster so Please first enter in ACMaster and then try again.');", true);
            return;
        }
        DataTable dtservicetax = new DataTable();
        dtservicetax = fvmclass.checkacnameexistornotservicetaxdesc(li);
        if (dtservicetax.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Service tax desc not exist in ACMaster so Please first enter in ACMaster and then try again.');", true);
            return;
        }

        li.form = txtform.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        if (btnsaves.Text == "Save")
        {
            DataTable dtcheck = new DataTable();
            dtcheck = fvmclass.checkduplicate(li);
            if (dtcheck.Rows.Count == 0)
            {
                fvmclass.insertVATdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.typename + " VAT Type Master Inserted.";
                faclass.insertactivity(li);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Type Name already exists.Please try again with new Type Name.');", true);
                return;
            }
        }
        else
        {
            if (txttypename.Text.Trim() != ViewState["name"].ToString().Trim())
            {
                DataTable dtcheck = new DataTable();
                dtcheck = fvmclass.checkduplicate(li);
                if (dtcheck.Rows.Count == 0)
                {
                    li.id = Convert.ToInt64(ViewState["id"].ToString());
                    fvmclass.updateVATdata(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.id + " VAT Type Master Updated.";
                    faclass.insertactivity(li);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Type Name already exists.Please try again with new Type Name.');", true);
                    return;
                }
            }
            else
            {
                li.id = Convert.ToInt64(ViewState["id"].ToString());
                fvmclass.updateVATdata(li);
            }
        }
        Response.Redirect("~/VATTypeMasterList.aspx?pagename=VATTypeMasterList");
    }
}