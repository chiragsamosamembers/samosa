﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ClearNo.aspx.cs" Inherits="ClearNo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Clear No.</span><br />
        <div class="row" style="height:8px;"></div>
        <span style="color: white; background-color: Red; font-size:larger;">Once you enter Number and click on
            CLEAR DATA button then data of that Number will be deleted Permanently.So be Careful.</span><br />
            <div class="row" style="height:15px;"></div>
        <div class="row">
            <div class="col-md-1">
                <asp:DropDownList ID="drptype" runat="server" CssClass="form-control">
                    <asp:ListItem>BR</asp:ListItem>
                    <asp:ListItem>BP</asp:ListItem>
                    <asp:ListItem>CR</asp:ListItem>
                    <asp:ListItem>CP</asp:ListItem>
                    <asp:ListItem>JV</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtno" runat="server" CssClass="form-control" placeholder="No. to Clear"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter No."
                    ControlToValidate="txtno" ValidationGroup="valaccount" Text="*"></asp:RequiredFieldValidator>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnsave" runat="server" Text="Clear Data" class="btn btn-default forbutton"
                    ValidationGroup="valaccount" OnClick="btnsave_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="valaccount"
                    ShowMessageBox="true" ShowSummary="false" />
            </div>
        </div>
    </div>
</asp:Content>
