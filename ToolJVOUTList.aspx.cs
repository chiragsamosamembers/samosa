﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ToolJVOUTList : System.Web.UI.Page
{
    ForToolsJV ftjvclass = new ForToolsJV();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.inout = "OUT";
        DataTable dtdata = new DataTable();
        dtdata = ftjvclass.selectalldata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvlist.Visible = true;
            gvlist.DataSource = dtdata;
            gvlist.DataBind();
        }
        else
        {
            gvlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Tool JV Data Found.";
        }
    }

    protected void gvlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.strchallanno = e.CommandArgument.ToString();
                Response.Redirect("~/ToolJVOUT.aspx?challanno=" + li.strchallanno + "&mode=update");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.strchallanno = e.CommandArgument.ToString();
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.inout = "OUT";
                ftjvclass.deletedata(li);
                ftjvclass.deleteallitemdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.strchallanno + " Tool JVOUT Deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void btnaddnew_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/ToolJVOUT.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
}