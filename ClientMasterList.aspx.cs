﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;

public partial class ClientMasterList : System.Web.UI.Page
{
    ForClientMaster fcmclass = new ForClientMaster();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
            if (SessionMgt.UserRole == "JMP" || SessionMgt.UserRole == "RK" || SessionMgt.UserRole == "IRS" || SessionMgt.UserRole == "ST" || SessionMgt.UserRole == "CNP")
            {
                btncidwise.Visible = true;
            }
            else
            {
                btncidwise.Visible = false;
            }
            fillstatusdrop();
        }
    }

    public void fillstatusdrop()
    {
        DataTable dtstatus = new DataTable();
        dtstatus = fcmclass.selectallclientstatusdata();
        if (dtstatus.Rows.Count > 0)
        {
            drpstatus.DataSource = dtstatus;
            drpstatus.DataTextField = "name";
            drpstatus.DataBind();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpstatus.Items.Clear();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        if (Request.Cookies["ForLogin"]["username"] == "admin@airmax.so")
        {
            dtdata = fcmclass.selectallclientdata();
        }
        else
        {
            dtdata = fcmclass.selectallclientdatacnowise(li);
        }
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvaclient.Visible = true;
            gvaclient.DataSource = dtdata;
            gvaclient.DataBind();
            lblcount.Text = dtdata.Rows.Count.ToString();
        }
        else
        {
            gvaclient.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Record Found for Client Master.";
            lblcount.Text = "0";
        }
    }

    protected void gvaclient_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                string acyear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
                li.id = Convert.ToInt64(e.CommandArgument);
                DataTable dtcheck = fcmclass.checkacyearfordata(li);
                if (dtcheck.Rows.Count > 0)
                {
                    //if (dtcheck.Rows[0]["acyear"].ToString() == acyear)
                    //{
                    Response.Redirect("~/ClientMaster.aspx?mode=update&code=" + li.id + "");
                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Client Name is fetched from Old Year Client Master so you need to go to Last Year Database and update this data from there and then fetch Client data in new year again using Year End Process.');", true);
                    //}
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.id = Convert.ToInt64(e.CommandArgument);
                fcmclass.deleteclientdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.id + " Client Master Data deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvaclient_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void btnclient_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/ClientMaster.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (txtsearchclientname.Text != "" && txtsearchclientname.Text != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.name = txtsearchclientname.Text;
            DataTable dtitemname = new DataTable();
            dtitemname = fcmclass.selectsearchclientname11(li);
            if (dtitemname.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvaclient.Visible = true;
                gvaclient.DataSource = dtitemname;
                gvaclient.DataBind();
                lblcount.Text = dtitemname.Rows.Count.ToString();
            }
            else
            {
                gvaclient.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Record Found for Client Master.";
                lblcount.Text = "0";
            }

        }
        else if (txtsearchclientname.Text == "" && drpstatus.SelectedItem.Text != "--SELECT--")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.status = drpstatus.SelectedItem.Text;
            DataTable dtitemname = new DataTable();
            dtitemname = fcmclass.selectsearchclientname11status(li);
            if (dtitemname.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvaclient.Visible = true;
                gvaclient.DataSource = dtitemname;
                gvaclient.DataBind();
                lblcount.Text = dtitemname.Rows.Count.ToString();
            }
            else
            {
                gvaclient.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Record Found for Client Master.";
                lblcount.Text = "0";
            }
        }
        else
        {
            fillgrid();
        }
    }
    protected void btncidwise_Click(object sender, EventArgs e)
    {
        string Xrepname = "cidwise status report";
        string status = drpstatus.SelectedItem.Text;
        //Int64 vno = Convert.ToInt64(e.CommandArgument);
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?status=" + status + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }
}