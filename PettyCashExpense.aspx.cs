﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlClient;
using System.Data.SqlTypes;

public partial class PettyCashExpense : System.Web.UI.Page
{
    ForBankReceipt fbrclass = new ForBankReceipt();
    ForLedger flclass = new ForLedger();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "insert")
            {
                fillacnamedrop();
                getbrno();
                txtvdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemspc"] = CreateTemplate();
                txtvoucherno.ReadOnly = false;
            }
            else if (Request["mode"].ToString() == "ledger")
            {
                fillacnamedrop();
                filleditdata();
                txtvoucherno.ReadOnly = true;
                ViewState["RefUrl"] = Request.UrlReferrer.ToString();
            }
            else
            {
                fillacnamedrop();
                filleditdata();
                txtvoucherno.ReadOnly = true;
            }            
            Page.SetFocus(txtpettycashname);
            //fillbillcombo();
        }
    }

    public void fillacnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fbrclass.selectallacname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpacname.Items.Clear();
            drpacname.DataSource = dtdata;
            drpacname.DataTextField = "tt";
            drpacname.DataValueField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    public void getbrno()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.istype = "PC";
        //DataTable dtvno = new DataTable();
        //dtvno = fbrclass.selectunusedpettycashno(li);
        //if (dtvno.Rows.Count > 0)
        //{
        //    txtvoucherno.Text = dtvno.Rows[0]["vno"].ToString();
        //}
        //else
        //{
        //    txtvoucherno.Text = "";
        //}
        DataTable dtvno = new DataTable();
        dtvno = fbrclass.selectlastnostring(li);
        if (dtvno.Rows.Count > 0)
        {
            txtvoucherno.Text = (Convert.ToInt64(dtvno.Rows[0]["voucherno"].ToString()) + 1).ToString();
        }
        else
        {
            txtvoucherno.Text = "1";
        }
    }

    public void filleditdata()
    {
        li.istype = "PC";
        li.voucherno = Convert.ToInt64(Request["vno"].ToString());
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fbrclass.selectbr1datafromvno(li);
        if (dtdata.Rows.Count > 0)
        {
            txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
            txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtpettycashname.Text = dtdata.Rows[0]["name"].ToString();
            lbltotal.Text = dtdata.Rows[0]["total"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            DataTable dtitem = new DataTable();
            dtitem = fbrclass.selectbrdatafromvno(li);
            if (dtitem.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitem;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Data Found.";
            }
            counttotal();
            btnsaveall.Text = "Update";
        }
    }

    public void fillbillcombo()
    {
        //li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        //DataTable dtdata = new DataTable();
        //dtdata = fbrclass.selectallbillnoaa(li);
        //if (dtdata.Rows.Count > 0)
        //{
        //    combillno.DataSource = dtdata;
        //    combillno.DataTextField = "pino";
        //    combillno.DataBind();
        //}
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetBankname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallpettycashnamefromam(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetBillno(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallbillno(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    //protected void txtclientcode_TextChanged(object sender, EventArgs e)
    //{
    //    txtclientcode.Text=txtclientcode.Text.Split('-')[0];
    //}
    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("acname", typeof(string));
        dtpitems.Columns.Add("remarks", typeof(string));
        dtpitems.Columns.Add("ccode", typeof(Int64));
        dtpitems.Columns.Add("amount", typeof(double));
        return dtpitems;
    }
    protected void btnsaves_Click(object sender, EventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemspc"];
            DataRow dr = dt.NewRow();
            dr["id"] = 1;
            dr["acname"] = drpacname.SelectedValue;
            dr["remarks"] = txtremarks.Text;
            if (txtclientcode.Text.Trim() != string.Empty)
            {
                dr["ccode"] = txtclientcode.Text.Split('-')[0];
            }
            else
            {
                dr["ccode"] = 0;
            }
            dr["amount"] = txtamount.Text;
            dt.Rows.Add(dr);
            Session["dtpitemspc"] = dt;
            this.bindgrid();
        }
        else
        {
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            li.voucherdate = Convert.ToDateTime(txtvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.name = txtpettycashname.Text;
            li.acname = drpacname.SelectedValue;
            if (txtclientcode.Text.Trim() != string.Empty)
            {
                li.ccode = Convert.ToInt64(txtclientcode.Text.Split('-')[0]);
            }
            else
            {
                li.ccode = 0;
            }
            li.remarks = txtremarks.Text;
            li.amount = Convert.ToDouble(txtamount.Text);
            li.chequeno = "";
            li.agbill = "";
            li.istype = "PC";
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fbrclass.insertbrdata(li);
            li.activity = li.voucherno + " New Petty Cash Item Inserted.";
            faclass.insertactivity(li);
            li.strvoucherno = "PC" + (Convert.ToInt64(txtvoucherno.Text)).ToString();
            li.refid = SessionMgt.id;
            li.debitcode = txtpettycashname.Text;
            li.creditcode = drpacname.SelectedValue;
            li.description = li.remarks;
            li.istype1 = "C";
            //flclass.insertledgerdata(li);
            li.debitcode = drpacname.SelectedValue;
            li.creditcode = txtpettycashname.Text;
            li.istype1 = "D";
            //flclass.insertledgerdata(li);
            DataTable dtitem = new DataTable();
            dtitem = fbrclass.selectbrdatafromvno(li);
            if (dtitem.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitem;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Data Found.";
            }

        }
        counttotal();
        fillacnamedrop();
        txtremarks.Text = string.Empty;
        txtclientcode.Text = string.Empty;
        txtamount.Text = string.Empty;
        Page.SetFocus(drpacname);
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemspc"] as DataTable;
        rptlist.DataSource = dtsession;
        rptlist.DataBind();
    }

    protected void btnsaveall_Click(object sender, EventArgs e)
    {
        if (rptlist.Rows.Count > 0)
        {
            string xxyear = Request.Cookies["ForLogin"]["currentyear"];
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            li.voucherdate = Convert.ToDateTime(txtvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            //int zz = xxyear.IndexOf(yyyear);
            if (li.voucherdate <= yyyear1 && li.voucherdate >= yyyear)
            {

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
                return;
            }
            li.name = txtpettycashname.Text;
            li.istype = "PC";
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.totbasicamount = Convert.ToDouble(lbltotal.Text);
            if (btnsaveall.Text == "Save")
            {
                if (rptlist.Rows.Count > 0)
                {
                    DataTable dtcheck = new DataTable();
                    dtcheck = fbrclass.selectbr1datafromvno(li);
                    if (dtcheck.Rows.Count == 0)
                    {
                        fbrclass.insertbr1data(li);
                        li.activity = li.voucherno + " New Petty Cash Inserted.";
                        faclass.insertactivity(li);
                    }
                    else
                    {
                        li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                        fbrclass.updateisusedpc(li);
                        getbrno();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Voucher No. already exist.Try Again.');", true);
                        return;
                    }
                    for (int c = 0; c < rptlist.Rows.Count; c++)
                    {
                        li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                        TextBox lblacname = (TextBox)rptlist.Rows[c].FindControl("lblacname");
                        TextBox lblremarks = (TextBox)rptlist.Rows[c].FindControl("lblremarks");
                        TextBox lblclientcode = (TextBox)rptlist.Rows[c].FindControl("lblclientcode");
                        TextBox lblamount = (TextBox)rptlist.Rows[c].FindControl("lblamount");
                        li.acname = lblacname.Text;
                        li.remarks = lblremarks.Text;
                        li.ccode = Convert.ToInt64(lblclientcode.Text);
                        li.amount = Convert.ToDouble(lblamount.Text);
                        li.agbill = "";
                        li.chequeno = "";
                        fbrclass.insertbrdata(li);
                        li.strvoucherno = "PC" + (Convert.ToInt64(txtvoucherno.Text)).ToString();
                        li.refid = SessionMgt.id;
                        li.debitcode = txtpettycashname.Text;
                        li.creditcode = lblacname.Text;
                        li.description = li.remarks;
                        li.istype1 = "C";
                        //flclass.insertledgerdata(li);
                        li.debitcode = lblacname.Text;
                        li.creditcode = txtpettycashname.Text;
                        li.istype1 = "D";
                        //flclass.insertledgerdata(li);
                    }
                    fbrclass.updateisusedpc(li);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Petty Cash Data.');", true);
                    return;
                }
            }
            else
            {
                if (rptlist.Rows.Count > 0)
                {
                    li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                    li.type = "";
                    fbrclass.updatebr1data(li);
                    fbrclass.updatebrdatadateonly(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.voucherno + " Petty Cash data Updated.";
                    faclass.insertactivity(li);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Petty Cash Data.');", true);
                    return;
                }
            }
            counttotal();
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            fbrclass.deleteallledgerdatapc(li);
            for (int c = 0; c < rptlist.Rows.Count; c++)
            {
                Label lblid = (Label)rptlist.Rows[c].FindControl("lblid");
                TextBox lblacname = (TextBox)rptlist.Rows[c].FindControl("lblacname");
                TextBox lblremarks = (TextBox)rptlist.Rows[c].FindControl("lblremarks");
                TextBox lblclientcode = (TextBox)rptlist.Rows[c].FindControl("lblclientcode");
                TextBox lblamount = (TextBox)rptlist.Rows[c].FindControl("lblamount");
                li.id = Convert.ToInt64(lblid.Text);
                li.acname = lblacname.Text;
                li.remarks = lblremarks.Text;
                // li.ccode = Convert.ToInt64(lblclientcode.Text);
                var cc = lblclientcode.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.ccode = Convert.ToInt64(lblclientcode.Text.Split('-')[0]);
                }
                else
                {
                    if (lblclientcode.Text != "0")
                    {
                        li.ccode = Convert.ToInt64(lblclientcode.Text);
                    }
                    else
                    {
                        li.ccode = 0;
                    }
                }
                li.amount = Convert.ToDouble(lblamount.Text);
                li.agbill = "";
                li.chequeno = "";
                //fbrclass.insertbrdata(li);
                li.strvoucherno = "PC" + (Convert.ToInt64(txtvoucherno.Text)).ToString();
                li.refid = SessionMgt.id;
                li.debitcode = txtpettycashname.Text;
                li.creditcode = lblacname.Text;
                li.description = li.remarks;
                li.istype1 = "C";
                flclass.insertledgerdata(li);
                li.debitcode = lblacname.Text;
                li.creditcode = txtpettycashname.Text;
                li.istype1 = "D";
                flclass.insertledgerdata(li);
                if (btnsaveall.Text == "Update")
                {
                    fbrclass.updatebrdata(li);
                }
            }            
            if (Request["mode"].ToString() != "ledger")
            {
                Response.Redirect("~/PettyCashExpenseList.aspx?pagename=PettyCashExpenseList");
            }
            else
            {
                object refUrl = ViewState["RefUrl"];
                string ch = (string)refUrl;
                ch = ch.Split('?')[0] + "?acname=" + SessionMgt.acname + "&ccode=" + SessionMgt.ccode + "&fromdate=" + SessionMgt.fromdate + "&todate=" + SessionMgt.todate + "";
                SessionMgt.acname = "";
                SessionMgt.ccode = 0;
                SessionMgt.fromdate = "";
                SessionMgt.todate = "";
                if (refUrl != null)
                    Response.Redirect(ch);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Petty Cash Data.');", true);
            return;
        }
    }
    //protected void rptlist_ItemCommand(object source, RepeaterCommandEventArgs e)
    //{
    //    if (e.CommandName == "delete")
    //    {
    //        if (btnsaveall.Text == "Save")
    //        {
    //            DataTable dt = Session["dtpitems"] as DataTable;
    //            dt.Rows.Remove(dt.Rows[]);
    //            //Session["ddc"] = dt;
    //            Session["dtpitems"] = dt;
    //            this.bindgrid();
    //        }
    //        else
    //        {
    //        }
    //        //counttotal();
    //        //Label2.Text = gvitemlist.Rows.Count.ToString();
    //    }
    //}
    protected void rptlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = Session["dtpitemspc"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemspc"] = dt;
            this.bindgrid();
        }
        else
        {
            if (rptlist.Rows.Count > 1)
            {
                li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)rptlist.Rows[e.RowIndex].FindControl("lblid");
                li.id = Convert.ToInt64(lblid.Text);
                fbrclass.deletebrdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = "Petty Cash item deleted.";
                faclass.insertactivity(li);
                li.istype = "PC";
                //fbrclass.deletefromledgertable(li);
                li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                DataTable dtitem = new DataTable();
                dtitem = fbrclass.selectbrdatafromvno(li);
                if (dtitem.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    rptlist.Visible = true;
                    rptlist.DataSource = dtitem;
                    rptlist.DataBind();
                }
                else
                {
                    rptlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Data Found.";
                    rptlist.DataSource = null;
                    rptlist.DataBind();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
        counttotal();
        //counttotal();
        //Label2.Text = gvitemlist.Rows.Count.ToString();
    }

    public void counttotal()
    {
        double totamount = 0;
        for (int c = 0; c < rptlist.Rows.Count; c++)
        {
            TextBox lblamount = (TextBox)rptlist.Rows[c].FindControl("lblamount");
            totamount = totamount + Convert.ToDouble(lblamount.Text);
        }
        lbltotal.Text = totamount.ToString();
    }

    protected void btnfirst_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.istype = "PC";
        SqlDataAdapter da = new SqlDataAdapter("select * from BankACMaster1 where istype='PC' order by voucherno", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.voucherno = Convert.ToInt64(dtdataq.Rows[0]["voucherno"].ToString());
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            DataTable dtdata = new DataTable();
            dtdata = fbrclass.selectbr1datafromvno(li);
            if (dtdata.Rows.Count > 0)
            {
                txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
                txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtpettycashname.Text = dtdata.Rows[0]["name"].ToString();
                lbltotal.Text = dtdata.Rows[0]["total"].ToString();
                DataTable dtitem = new DataTable();
                dtitem = fbrclass.selectbrdatafromvno(li);
                if (dtitem.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    rptlist.Visible = true;
                    rptlist.DataSource = dtitem;
                    rptlist.DataBind();
                }
                else
                {
                    rptlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Data Found.";
                }
                counttotal();
                btnsaveall.Text = "Update";
            }
        }
    }
    protected void btnlast_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        li.istype = "PC";
        SqlDataAdapter da = new SqlDataAdapter("select * from BankACMaster1 where istype='PC' order by voucherno desc", con);
        DataTable dtdataq = new DataTable();
        da.Fill(dtdataq);
        if (dtdataq.Rows.Count > 0)
        {
            li.voucherno = Convert.ToInt64(dtdataq.Rows[0]["voucherno"].ToString());
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            DataTable dtdata = new DataTable();
            dtdata = fbrclass.selectbr1datafromvno(li);
            if (dtdata.Rows.Count > 0)
            {
                txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
                txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtpettycashname.Text = dtdata.Rows[0]["name"].ToString();
                lbltotal.Text = dtdata.Rows[0]["total"].ToString();
                DataTable dtitem = new DataTable();
                dtitem = fbrclass.selectbrdatafromvno(li);
                if (dtitem.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    rptlist.Visible = true;
                    rptlist.DataSource = dtitem;
                    rptlist.DataBind();
                }
                else
                {
                    rptlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Data Found.";
                }
                counttotal();
                btnsaveall.Text = "Update";
            }
        }
    }
    protected void btnnext_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter daa = new SqlDataAdapter("select * from BankACMaster1 where istype='PC' order by voucherno", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.voucherno = Convert.ToInt64(txtvoucherno.Text) + 1;
        for (Int64 i = li.voucherno; i <= Convert.ToInt64(dtdataqa.Rows[dtdataqa.Rows.Count - 1]["voucherno"].ToString()); i++)
        {
            li.voucherno = i;
            li.istype = "PC";
            SqlDataAdapter da = new SqlDataAdapter("select * from BankACMaster1 where istype='PC' and voucherno=" + li.voucherno + "", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtdata = new DataTable();
                dtdata = fbrclass.selectbr1datafromvno(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
                    txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtpettycashname.Text = dtdata.Rows[0]["name"].ToString();
                    lbltotal.Text = dtdata.Rows[0]["total"].ToString();
                    DataTable dtitem = new DataTable();
                    dtitem = fbrclass.selectbrdatafromvno(li);
                    if (dtitem.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        rptlist.Visible = true;
                        rptlist.DataSource = dtitem;
                        rptlist.DataBind();
                    }
                    else
                    {
                        rptlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Data Found.";
                    }
                    counttotal();
                    btnsaveall.Text = "Update";
                    return;
                }
            }
        }
    }
    protected void btnprevious_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter daa = new SqlDataAdapter("select * from BankACMaster1 where istype='PC' order by voucherno", con);
        DataTable dtdataqa = new DataTable();
        daa.Fill(dtdataqa);
        li.voucherno = Convert.ToInt64(txtvoucherno.Text) - 1;
        for (Int64 i = li.voucherno; i >= Convert.ToInt64(dtdataqa.Rows[0]["voucherno"].ToString()); i--)
        {
            li.voucherno = i;
            li.istype = "PC";
            //li.voucherno = Convert.ToInt64(txtvoucherno.Text) - 1;
            SqlDataAdapter da = new SqlDataAdapter("select * from BankACMaster1 where istype='PC' and voucherno=" + li.voucherno + "", con);
            DataTable dtdataq = new DataTable();
            da.Fill(dtdataq);
            if (dtdataq.Rows.Count > 0)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtdata = new DataTable();
                dtdata = fbrclass.selectbr1datafromvno(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
                    txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["voucherdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                    txtpettycashname.Text = dtdata.Rows[0]["name"].ToString();
                    lbltotal.Text = dtdata.Rows[0]["total"].ToString();
                    DataTable dtitem = new DataTable();
                    dtitem = fbrclass.selectbrdatafromvno(li);
                    if (dtitem.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        rptlist.Visible = true;
                        rptlist.DataSource = dtitem;
                        rptlist.DataBind();
                    }
                    else
                    {
                        rptlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Data Found.";
                    }
                    counttotal();
                    btnsaveall.Text = "Update";
                    return;
                }
            }
        }
    }

    //protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (DropDownList1.SelectedItem.Text == "Y")
    //    {
    //        txtbillno.Enabled = true;
    //    }
    //    else
    //    {
    //        txtbillno.Enabled = false;
    //    }
    //}

}