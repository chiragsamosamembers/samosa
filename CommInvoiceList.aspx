﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CommInvoiceList.aspx.cs" Inherits="CommInvoiceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Commercial Invoice List</span><br />
        <div class="row">
            <div class="col-md-2">
                <asp:Button ID="btnquotation" runat="server" Text="Add New Comm. Inv." class="btn btn-default forbutton"
                    OnClick="btnquotation_Click" /></div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkexcel" runat="server" Text="Export In $" /><asp:CheckBox ID="chkexcel1"
                    runat="server" Text="Export In €" /></div>
            <div class="col-md-2">
                <asp:RadioButtonList ID="rdotype" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True">PDF</asp:ListItem>
                    <asp:ListItem>Excel</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    Rupees Per Dollar</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtdtors" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvso" runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
                        AllowSorting="false" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered"
                        OnRowCommand="gvso_RowCommand" OnRowDeleting="gvso_RowDeleting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("invno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect121" CommandName="revised" runat="server" ImageUrl="~/images/buttons/revised1.jpg"
                                        ToolTip="Edit" Height="20px" Width="40px" CausesValidation="False" CommandArgument='<%# bind("invno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Comm. Inv. NO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblquono" ForeColor="Black" runat="server" Text='<%# bind("invno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText=" Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblquodate" ForeColor="Black" runat="server" Text='<%#Convert.ToDateTime(Eval("invdate")).ToString("dd-MM-yyyy") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Client Code" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AC Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblacname" ForeColor="Black" runat="server" Text='<%# bind("acname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dollar Rs." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldtors" ForeColor="Black" runat="server" Text='<%# bind("dtors") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Euro Rs." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbletors" ForeColor="Black" runat="server" Text='<%# bind("etors") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("invno") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="print" runat="server" ImageUrl="~/images/buttons/printer.gif"
                                        ToolTip="Print" CommandArgument='<%# bind("invno") %>' Height="20px" Width="20px"
                                        CausesValidation="False" /><%--OnClientClick="return confirmprintchq();"--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11ab" CommandName="directsc" runat="server" ImageUrl="~/images/buttons/viewer_ico_exit.png"
                                        ToolTip="Create SC" CommandArgument='<%# bind("invno") %>' Height="20px"
                                        Width="20px" CausesValidation="False" /><%--OnClientClick="return confirmprintchq();"--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
