﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Att.aspx.cs" Inherits="Att" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns="http://www.w3.org/1999/xhtml"> 
   <head id="Head1" runat="server">
   <title></title>
</head>
<body>
   <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>
   <script type="text/javascript">
       if (navigator.geolocation) {
           navigator.geolocation.getCurrentPosition(function (p) {
               var LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
               var mapOptions = {
                   center: LatLng,
                   zoom: 13,
                   mapTypeId: google.maps.MapTypeId.ROADMAP
               };
               var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
               var marker = new google.maps.Marker({
                   position: LatLng,
                   map: map,
                   title: "<div style = 'height:60px;width:200px'><b>Your location:</b><br />Latitude: " + p.coords.latitude + "<br />Longitude: " + p.coords.longitude
               });
               google.maps.event.addListener(marker, "click", function (e) {
                   long = e.latLng.lng();
                   lat = e.latLng.lat();
                   document.getElementById("lat").value = lat;
                   document.getElementById("lng").value = long;
                   alert("Latitude: " + lat + "\r\nLongitude: " + long);
               });
           });
       } else {
           alert('Geo Location feature is not supported in this browser.');
       }




      
   </script>

<form id="myForm" runat="server">
   <div id="dvMap" style="width: 70%; height: 600px">
   </div>
   
   <asp:HiddenField ID="lat" runat="server" />
   <asp:HiddenField ID="lng" runat="server" />
   <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />

</form>
</body>
</html>
