﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;
using System.IO;
using System.Net.Mail;
using System.Net;

public partial class MasterPage : System.Web.UI.MasterPage
{
    ForLogin flclass = new ForLogin();
    ForUserRight furclass = new ForUserRight();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (Request.Cookies["ForCompany"] != null)
            //{
            //    lblcname.Text = Request.Cookies["ForCompany"].ToString();
            //}
            //else
            //{
            //    Response.Redirect("~/Login.aspx");
            //}
            //if (!this.Page.User.Identity.IsAuthenticated)
            //{
            //    FormsAuthentication.RedirectToLoginPage();
            //}
            //lblcname.Text = Request.Cookies["ForLogIn"]["cname"];
            lblcname.Text = Request.Cookies["ForLogin"]["username"];
            lnkyear.Text = Request.Cookies["ForLogin"]["acyear"];
            showhidepages();
            //fillacyeardrop();
            //drpacyear.SelectedValue = Request.Cookies["ForLogin"]["acyear"];
        }
    }

    //public void fillacyeardrop()
    //{
    //    DataTable dtdata = new DataTable();
    //    dtdata = flclass.selectacyearfromshopid(li);
    //    if (dtdata.Rows.Count > 0)
    //    {
    //        drpacyear.DataSource = dtdata;
    //        drpacyear.DataTextField = "acyear";
    //        drpacyear.DataBind();
    //        //drpacyear.Items.Insert(0, "--Select--");
    //    }
    //    else
    //    {
    //        drpacyear.Items.Clear();
    //        drpacyear.Items.Insert(0, "--Select--");
    //    }
    //}

    public void showhidepages()
    {
        li.pagename = "AccountMasterList";
        DataTable dtr = new DataTable();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            if (dtr.Rows[0]["UVIEW"].ToString() == "Y")
            {
                acmaster.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                acmaster.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "ACGroupMaster";
        DataTable dtr1 = new DataTable();
        dtr1 = furclass.selectuserrights(li);
        if (dtr1.Rows.Count > 0)
        {
            if (dtr1.Rows[0]["UVIEW"].ToString() == "Y")
            {
                acgroupmaster.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                acgroupmaster.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "ItemMasterList";
        DataTable dtr3 = new DataTable();
        dtr3 = furclass.selectuserrights(li);
        if (dtr3.Rows.Count > 0)
        {
            if (dtr3.Rows[0]["UVIEW"].ToString() == "Y")
            {
                itemmaster.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                itemmaster.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "VATTypeMasterList";
        DataTable dtr4 = new DataTable();
        dtr4 = furclass.selectuserrights(li);
        if (dtr4.Rows.Count > 0)
        {
            if (dtr4.Rows[0]["UVIEW"].ToString() == "Y")
            {
                vattypemaster.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                vattypemaster.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "SalesmanMasterList";
        DataTable dtr5 = new DataTable();
        dtr5 = furclass.selectuserrights(li);
        if (dtr5.Rows.Count > 0)
        {
            if (dtr5.Rows[0]["UVIEW"].ToString() == "Y")
            {
                salesmanmaster.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                salesmanmaster.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "ClientMasterList";
        DataTable dtr6 = new DataTable();
        dtr6 = furclass.selectuserrights(li);
        if (dtr6.Rows.Count > 0)
        {
            if (dtr6.Rows[0]["UVIEW"].ToString() == "Y")
            {
                clientmaster.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                clientmaster.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "ToolsMasterList";
        DataTable dtr7 = new DataTable();
        dtr7 = furclass.selectuserrights(li);
        if (dtr7.Rows.Count > 0)
        {
            if (dtr7.Rows[0]["UVIEW"].ToString() == "Y")
            {
                toolsmaster.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                toolsmaster.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "Misc";
        DataTable dtr8 = new DataTable();
        dtr8 = furclass.selectuserrights(li);
        if (dtr8.Rows.Count > 0)
        {
            if (dtr8.Rows[0]["UVIEW"].ToString() == "Y")
            {
                Misc.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                Misc.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "Bifurcation";
        DataTable dtr8a = new DataTable();
        dtr8a = furclass.selectuserrights(li);
        if (dtr8a.Rows.Count > 0)
        {
            if (dtr8a.Rows[0]["UVIEW"].ToString() == "Y")
            {
                QuotationList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                QuotationList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "PerformaInvoiceList";
        DataTable dtr8b = new DataTable();
        dtr8b = furclass.selectuserrights(li);
        if (dtr8b.Rows.Count > 0)
        {
            if (dtr8b.Rows[0]["UVIEW"].ToString() == "Y")
            {
                PerformaInvoiceList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                PerformaInvoiceList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "SalesChallanList";
        DataTable dtr9 = new DataTable();
        dtr9 = furclass.selectuserrights(li);
        if (dtr9.Rows.Count > 0)
        {
            if (dtr9.Rows[0]["UVIEW"].ToString() == "Y")
            {
                SalesChallanList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                SalesChallanList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "SalesInvoiceList";
        DataTable dtr10 = new DataTable();
        dtr10 = furclass.selectuserrights(li);
        if (dtr10.Rows.Count > 0)
        {
            if (dtr10.Rows[0]["UVIEW"].ToString() == "Y")
            {
                SalesInvoiceList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                SalesInvoiceList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "PurchaseChallanList";
        DataTable dtr11 = new DataTable();
        dtr11 = furclass.selectuserrights(li);
        if (dtr11.Rows.Count > 0)
        {
            if (dtr11.Rows[0]["UVIEW"].ToString() == "Y")
            {
                PurchaseChallanList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                PurchaseChallanList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "PurchaseInvoiceList";
        DataTable dtr12 = new DataTable();
        dtr12 = furclass.selectuserrights(li);
        if (dtr12.Rows.Count > 0)
        {
            if (dtr12.Rows[0]["UVIEW"].ToString() == "Y")
            {
                PurchaseInvoiceList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                PurchaseInvoiceList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "BankReceiptList";
        DataTable dtr13 = new DataTable();
        dtr13 = furclass.selectuserrights(li);
        if (dtr13.Rows.Count > 0)
        {
            if (dtr13.Rows[0]["UVIEW"].ToString() == "Y")
            {
                BankReceiptList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                BankReceiptList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "BankPaymentList";
        DataTable dtr14 = new DataTable();
        dtr14 = furclass.selectuserrights(li);
        if (dtr14.Rows.Count > 0)
        {
            if (dtr14.Rows[0]["UVIEW"].ToString() == "Y")
            {
                BankPaymentList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                BankPaymentList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "CashReceiptList";
        DataTable dtr15 = new DataTable();
        dtr15 = furclass.selectuserrights(li);
        if (dtr15.Rows.Count > 0)
        {
            if (dtr15.Rows[0]["UVIEW"].ToString() == "Y")
            {
                CashReceiptList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                CashReceiptList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "CashPaymentList";
        DataTable dtr16 = new DataTable();
        dtr16 = furclass.selectuserrights(li);
        if (dtr16.Rows.Count > 0)
        {
            if (dtr16.Rows[0]["UVIEW"].ToString() == "Y")
            {
                CashPaymentList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                CashPaymentList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "PettyCashExpenseList";
        DataTable dtr17 = new DataTable();
        dtr17 = furclass.selectuserrights(li);
        if (dtr17.Rows.Count > 0)
        {
            if (dtr17.Rows[0]["UVIEW"].ToString() == "Y")
            {
                PettyCashExpenseList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                PettyCashExpenseList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "JournalVoucherList";
        DataTable dtr18 = new DataTable();
        dtr18 = furclass.selectuserrights(li);
        if (dtr18.Rows.Count > 0)
        {
            if (dtr18.Rows[0]["UVIEW"].ToString() == "Y")
            {
                JournalVoucherList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                JournalVoucherList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "PurchaseOrderList";
        DataTable dtr19 = new DataTable();
        dtr19 = furclass.selectuserrights(li);
        if (dtr19.Rows.Count > 0)
        {
            if (dtr19.Rows[0]["UVIEW"].ToString() == "Y")
            {
                PurchaseOrderList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                PurchaseOrderList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "SalesOrderList";
        DataTable dtr20 = new DataTable();
        dtr20 = furclass.selectuserrights(li);
        if (dtr20.Rows.Count > 0)
        {
            if (dtr20.Rows[0]["UVIEW"].ToString() == "Y")
            {
                SalesOrderList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                SalesOrderList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "ToolsDeliveryEntryList";
        DataTable dtr21 = new DataTable();
        dtr21 = furclass.selectuserrights(li);
        if (dtr21.Rows.Count > 0)
        {
            if (dtr21.Rows[0]["UVIEW"].ToString() == "Y")
            {
                ToolsDeliveryEntryList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                ToolsDeliveryEntryList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        //li.pagename = "ToolsReturnEntryList";
        //DataTable dtr22 = new DataTable();
        //dtr22 = furclass.selectuserrights(li);
        //if (dtr22.Rows.Count > 0)
        //{
        //    if (dtr22.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        ToolsReturnEntryList.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        ToolsReturnEntryList.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        li.pagename = "ReturnableReturnEntryList";
        DataTable dtr23 = new DataTable();
        dtr23 = furclass.selectuserrights(li);
        if (dtr23.Rows.Count > 0)
        {
            if (dtr23.Rows[0]["UVIEW"].ToString() == "Y")
            {
                ReturnableReturnEntryList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                ReturnableReturnEntryList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "PackingMaterialEntryMultipleList";
        DataTable dtr24 = new DataTable();
        dtr24 = furclass.selectuserrights(li);
        if (dtr24.Rows.Count > 0)
        {
            if (dtr24.Rows[0]["UVIEW"].ToString() == "Y")
            {
                PackingMaterialEntryMultipleList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                PackingMaterialEntryMultipleList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        //li.pagename = "RepPurchaseRegister201";
        //DataTable dtr241 = new DataTable();
        //dtr241 = furclass.selectuserrights(li);
        //if (dtr241.Rows.Count > 0)
        //{
        //    if (dtr241.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepPurchaseRegister.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepPurchaseRegister.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        li.pagename = "RepPurchaseRegister";
        DataTable dtr242 = new DataTable();
        dtr242 = furclass.selectuserrights(li);
        if (dtr242.Rows.Count > 0)
        {
            if (dtr242.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepPORegister.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepPORegister.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }
        
        li.pagename = "RepPendingChallanRegister";
        DataTable dtr244 = new DataTable();
        dtr244 = furclass.selectuserrights(li);
        if (dtr244.Rows.Count > 0)
        {
            if (dtr244.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepPCRegister.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepPCRegister.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepProjectExpense";
        DataTable dtr245 = new DataTable();
        dtr245 = furclass.selectuserrights(li);
        if (dtr245.Rows.Count > 0)
        {
            if (dtr245.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepProjectExpense.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepProjectExpense.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }
        li.pagename = "RepCoverPrinting";
        DataTable dtr246 = new DataTable();
        dtr246 = furclass.selectuserrights(li);
        if (dtr246.Rows.Count > 0)
        {
            if (dtr246.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepCover.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepCover.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }
        li.pagename = "RepOverall";
        DataTable dtr247 = new DataTable();
        dtr247 = furclass.selectuserrights(li);
        if (dtr247.Rows.Count > 0)
        {
            if (dtr247.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepOverall.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepOverall.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }
        li.pagename = "RepOutstanding";
        DataTable dtr248 = new DataTable();
        dtr248 = furclass.selectuserrights(li);
        if (dtr248.Rows.Count > 0)
        {
            if (dtr248.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepOutstanding.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepOutstanding.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }
        li.pagename = "RepBalanceSheet";
        DataTable dtr249 = new DataTable();
        dtr249 = furclass.selectuserrights(li);
        if (dtr249.Rows.Count > 0)
        {
            if (dtr249.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepBalanceSheet.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepBalanceSheet.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        //li.pagename = "RepPurchaseOrder";
        //DataTable dtr25 = new DataTable();
        //dtr25 = furclass.selectuserrights(li);
        //if (dtr25.Rows.Count > 0)
        //{
        //    if (dtr25.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepPurchaseOrder.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepPurchaseOrder.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        //li.pagename = "RepDeliveryChallan";
        //DataTable dtr26 = new DataTable();
        //dtr26 = furclass.selectuserrights(li);
        //if (dtr26.Rows.Count > 0)
        //{
        //    if (dtr26.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepDeliveryChallan.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepDeliveryChallan.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        //li.pagename = "RepSalesInvoice";
        //DataTable dtr27 = new DataTable();
        //dtr27 = furclass.selectuserrights(li);
        //if (dtr27.Rows.Count > 0)
        //{
        //    if (dtr27.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepSalesInvoice.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepSalesInvoice.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        //li.pagename = "RepBankPayment";
        //DataTable dtr28 = new DataTable();
        //dtr28 = furclass.selectuserrights(li);
        //if (dtr28.Rows.Count > 0)
        //{
        //    if (dtr28.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepBankPayment.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepBankPayment.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        //li.pagename = "RepCashPayment";
        //DataTable dtr29 = new DataTable();
        //dtr29 = furclass.selectuserrights(li);
        //if (dtr29.Rows.Count > 0)
        //{
        //    if (dtr29.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepCashPayment.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepCashPayment.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        //li.pagename = "RepBankReceipt";
        //DataTable dtr30 = new DataTable();
        //dtr30 = furclass.selectuserrights(li);
        //if (dtr30.Rows.Count > 0)
        //{
        //    if (dtr30.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepBankReceipt.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepBankReceipt.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        //li.pagename = "RepCashReceipt";
        //DataTable dtr31 = new DataTable();
        //dtr31 = furclass.selectuserrights(li);
        //if (dtr31.Rows.Count > 0)
        //{
        //    if (dtr31.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepCashReceipt.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepCashReceipt.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        //li.pagename = "RepJVVoucher";
        //DataTable dtr32 = new DataTable();
        //dtr32 = furclass.selectuserrights(li);
        //if (dtr32.Rows.Count > 0)
        //{
        //    if (dtr32.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepJVVoucher.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepJVVoucher.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        li.pagename = "RepJV";
        DataTable dtr33 = new DataTable();
        dtr33 = furclass.selectuserrights(li);
        if (dtr33.Rows.Count > 0)
        {
            if (dtr33.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepJV.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepJV.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepBankBook";
        DataTable dtr34 = new DataTable();
        dtr34 = furclass.selectuserrights(li);
        if (dtr34.Rows.Count > 0)
        {
            if (dtr34.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepBankBook.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepBankBook.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepCashBook";
        DataTable dtr35 = new DataTable();
        dtr35 = furclass.selectuserrights(li);
        if (dtr35.Rows.Count > 0)
        {
            if (dtr35.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepCashBook.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepCashBook.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepPettyCashBook";
        DataTable dtr36 = new DataTable();
        dtr36 = furclass.selectuserrights(li);
        if (dtr36.Rows.Count > 0)
        {
            if (dtr36.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepPettyCashBook.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepPettyCashBook.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepLedger";
        DataTable dtr37 = new DataTable();
        dtr37 = furclass.selectuserrights(li);
        if (dtr37.Rows.Count > 0)
        {
            if (dtr37.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepLedger.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepLedger.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepTrial";
        DataTable dtr38 = new DataTable();
        dtr38 = furclass.selectuserrights(li);
        if (dtr38.Rows.Count > 0)
        {
            if (dtr38.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepTrial.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepTrial.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepSalesTaxSummary";
        DataTable dtr39 = new DataTable();
        dtr39 = furclass.selectuserrights(li);
        if (dtr39.Rows.Count > 0)
        {
            if (dtr39.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepSalesTaxSummary.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepSalesTaxSummary.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepPurchaseTaxSummary";
        DataTable dtr40 = new DataTable();
        dtr40 = furclass.selectuserrights(li);
        if (dtr40.Rows.Count > 0)
        {
            if (dtr40.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepPurchaseTaxSummary.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepPurchaseTaxSummary.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepPartywiseItemRegister";
        DataTable dtr40a = new DataTable();
        dtr40a = furclass.selectuserrights(li);
        if (dtr40a.Rows.Count > 0)
        {
            if (dtr40a.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepPartywiseItemRegister.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepPartywiseItemRegister.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepSalesRegister";
        DataTable dtr41 = new DataTable();
        dtr41 = furclass.selectuserrights(li);
        if (dtr41.Rows.Count > 0)
        {
            if (dtr41.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepSalesRegister.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepSalesRegister.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        //////////li.pagename = "RepPORegisterAll";
        //////////DataTable dtr42a = new DataTable();
        //////////dtr42a = furclass.selectuserrights(li);
        //////////if (dtr42a.Rows.Count > 0)
        //////////{
        //////////    if (dtr42a.Rows[0]["UVIEW"].ToString() == "Y")
        //////////    {
        //////////        RepPORegisterAll.Visible = true;
        //////////        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //////////    }
        //////////    else
        //////////    {
        //////////        RepPORegisterAll.Visible = false;
        //////////        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //////////    }
        //////////}
        //////////else
        //////////{
        //////////    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //////////    Response.Redirect("~/CompanySelection.aspx");
        //////////}

        li.pagename = "RepPendingPO";
        DataTable dtr42b = new DataTable();
        dtr42b = furclass.selectuserrights(li);
        if (dtr42b.Rows.Count > 0)
        {
            if (dtr42b.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepPendingPO.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepPendingPO.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        //li.pagename = "RepPurchaseRegister";
        //DataTable dtr42 = new DataTable();
        //dtr42 = furclass.selectuserrights(li);
        //if (dtr42.Rows.Count > 0)
        //{
        //    if (dtr42.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepPurchaseRegister.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepPurchaseRegister.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        //li.pagename = "RepPORegister";
        //DataTable dtr42ab = new DataTable();
        //dtr42ab = furclass.selectuserrights(li);
        //if (dtr42ab.Rows.Count > 0)
        //{
        //    if (dtr42ab.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        RepPORegister.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        RepPORegister.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        li.pagename = "AddNewUser";
        DataTable dtr2 = new DataTable();
        dtr2 = furclass.selectuserrights(li);
        if (dtr2.Rows.Count > 0)
        {
            if (dtr2.Rows[0]["UVIEW"].ToString() == "Y")
            {
                UserList.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                UserList.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "Changepassword";
        DataTable dtr43 = new DataTable();
        dtr43 = furclass.selectuserrights(li);
        if (dtr43.Rows.Count > 0)
        {
            if (dtr43.Rows[0]["UVIEW"].ToString() == "Y")
            {
                Changepassword.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                Changepassword.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "RepStockSummary";
        DataTable dtr44 = new DataTable();
        dtr44 = furclass.selectuserrights(li);
        if (dtr44.Rows.Count > 0)
        {
            if (dtr44.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepStockSummary.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepStockSummary.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "UserRights";
        DataTable dtr45 = new DataTable();
        dtr45 = furclass.selectuserrights(li);
        if (dtr45.Rows.Count > 0)
        {
            if (dtr45.Rows[0]["UVIEW"].ToString() == "Y")
            {
                UserRights.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                UserRights.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "Stock";
        DataTable dtr46 = new DataTable();
        dtr46 = furclass.selectuserrights(li);
        if (dtr46.Rows.Count > 0)
        {
            if (dtr46.Rows[0]["UVIEW"].ToString() == "Y")
            {
                Stock.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                Stock.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "StockJVINList";
        DataTable dtr47 = new DataTable();
        dtr47 = furclass.selectuserrights(li);
        if (dtr47.Rows.Count > 0)
        {
            if (dtr47.Rows[0]["UVIEW"].ToString() == "Y")
            {
                StockJVIN.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                StockJVIN.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "StockJVOUTList";
        DataTable dtr48 = new DataTable();
        dtr48 = furclass.selectuserrights(li);
        if (dtr48.Rows.Count > 0)
        {
            if (dtr48.Rows[0]["UVIEW"].ToString() == "Y")
            {
                StockJVOUT.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                StockJVOUT.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        li.pagename = "StockAdjustment";
        DataTable dtr49 = new DataTable();
        dtr49 = furclass.selectuserrights(li);
        if (dtr49.Rows.Count > 0)
        {
            if (dtr49.Rows[0]["UVIEW"].ToString() == "Y")
            {
                StockAdjustment.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                StockAdjustment.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        //li.pagename = "AdjustPendingPO";
        //DataTable dtr491 = new DataTable();
        //dtr491 = furclass.selectuserrights(li);
        //if (dtr491.Rows.Count > 0)
        //{
        //    if (dtr491.Rows[0]["UVIEW"].ToString() == "Y")
        //    {
        //        AdjustPendingPO.Visible = true;
        //        //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
        //    }
        //    else
        //    {
        //        AdjustPendingPO.Visible = false;
        //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
        //    }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        //    Response.Redirect("~/CompanySelection.aspx");
        //}

        li.pagename = "RepOutstanding";
        DataTable dtr50 = new DataTable();
        dtr50 = furclass.selectuserrights(li);
        if (dtr50.Rows.Count > 0)
        {
            if (dtr50.Rows[0]["UVIEW"].ToString() == "Y")
            {
                RepOutstanding.Visible = true;
                //Response.Redirect("~/CompanyListNew.aspx?pagename=CompanyListNew");
            }
            else
            {
                RepOutstanding.Visible = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights For Company Master Page.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
            Response.Redirect("~/CompanySelection.aspx");
        }

        


    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        string destdir = Server.MapPath("~\\alluredb");

        //Check that directory already there otherwise create 
        if (!System.IO.Directory.Exists(destdir))
        {
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("~\\alluredb"));
            di.Delete(true);
        }

        string acyear = "AT" + Request.Cookies["ForLogin"]["currentyear"];
        //string dbnameee = drpacyear.SelectedItem.Text.Split('-')[0] + drpacyear.SelectedItem.Text.Split('-')[1];
        string dbname = acyear;
        SqlConnection sqlcon = new SqlConnection();
        SqlCommand sqlcmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataTable dt = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        sqlcon.ConnectionString = cz;

        //Enter destination directory where backup file stored
        //string destdir = Server.MapPath("~\\alluredb");

        //Check that directory already there otherwise create 
        if (!System.IO.Directory.Exists(destdir))
        {
            System.IO.Directory.CreateDirectory(Server.MapPath("~\\alluredb"));
        }
        try
        {
            //Open connection
            sqlcon.Close();
            sqlcon.Open();
            //query to take backup database
            sqlcmd = new SqlCommand("backup database " + dbname + " to disk='" + destdir + "\\AirmaxDb.Bak'", sqlcon);
            sqlcmd.ExecuteNonQuery();
            //Close connection
            sqlcon.Close();

            StreamReader reader = new StreamReader(Server.MapPath("~/bulkmail.htm"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$code$$", "");
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.To.Add("alluresofttech@gmail.com");
            message.From = new System.Net.Mail.MailAddress("alluresofttech@gmail.com");
            message.Subject = "Airmax Database Back up of " + System.DateTime.Now.ToString();
            string imageatt1 = "~\\alluredb\\AirmaxDb.Bak";
            System.Net.Mail.Attachment attachment1 = new System.Net.Mail.Attachment(Server.MapPath(imageatt1));
            message.Attachments.Add(attachment1);

            //System.IO.MemoryStream stream = new System.IO.MemoryStream(new byte[64000]);
            //System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(Server.MapPath(Session["fname"].ToString()));
            //message.Attachments.Add(attachment);
            message.Body = myString.ToString();



            //message.AlternateViews.Add(htmlMail1);
            message.IsBodyHtml = true;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
            smtp.Port = 587;
            smtp.Credentials = new NetworkCredential("alluresofttech@gmail.com", "parth@chirag@123");
            smtp.EnableSsl = true;
            bool bb = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            if (bb == true)
            {
                smtp.Send(message);
            }

            //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Backup of database has been taken successfully.');", true);
            //Response.Write("Backup database successfully");
        }
        catch (Exception ex)
        {
            //Response.Write("Error During backup database!");
        }
    }

    protected void lnkbackup_Click(object sender, EventArgs e)
    {
        string destdir = Server.MapPath("~\\alluredb");

        //Check that directory already there otherwise create 
        //if (System.IO.Directory.Exists(destdir))
        //{
        //    DirectoryInfo di = new DirectoryInfo(Server.MapPath("~\\alluredb"));
        //    di.Delete(true);
        //}

        string acyear = "AT" + Request.Cookies["ForLogin"]["currentyear"];
        //string dbnameee = drpacyear.SelectedItem.Text.Split('-')[0] + drpacyear.SelectedItem.Text.Split('-')[1];
        string dbname = acyear;
        SqlConnection sqlcon = new SqlConnection();
        SqlCommand sqlcmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataTable dt = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        sqlcon.ConnectionString = cz;

        //Enter destination directory where backup file stored
        //string destdir = Server.MapPath("~\\alluredb");

        //Check that directory already there otherwise create 
        if (!System.IO.Directory.Exists(destdir))
        {
            System.IO.Directory.CreateDirectory(Server.MapPath("~\\alluredb"));
        }
        try
        {
            //Open connection
            sqlcon.Close();
            sqlcon.Open();
            //query to take backup database
            sqlcmd = new SqlCommand("backup database " + dbname + " to disk='" + destdir + "\\AirmaxDb" + Request.Cookies["ForLogin"]["acyear"] + ".Bak'", sqlcon);
            sqlcmd.ExecuteNonQuery();
            //Close connection
            sqlcon.Close();

            //StreamReader reader = new StreamReader(Server.MapPath("~/bulkmail.htm"));
            //string readFile = reader.ReadToEnd();
            //string myString = "";
            //myString = readFile;
            //myString = myString.Replace("$$code$$", "");
            //System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            //message.To.Add("alluresofttech@gmail.com");
            //message.From = new System.Net.Mail.MailAddress("chirag4430@gmail.com");
            //message.Subject = "Airmax Database Back up of " + System.DateTime.Now.ToString();
            //string imageatt1 = "~\\alluredb\\AirmaxDb" + Request.Cookies["ForLogin"]["acyear"] + ".Bak";
            //System.Net.Mail.Attachment attachment1 = new System.Net.Mail.Attachment(Server.MapPath(imageatt1));
            //message.Attachments.Add(attachment1);

            ////System.IO.MemoryStream stream = new System.IO.MemoryStream(new byte[64000]);
            ////System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(Server.MapPath(Session["fname"].ToString()));
            ////message.Attachments.Add(attachment);
            //message.Body = myString.ToString();



            ////message.AlternateViews.Add(htmlMail1);
            //message.IsBodyHtml = true;
            //System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
            //smtp.Port = 465;
            //smtp.Credentials = new NetworkCredential("chirag4430@gmail.com", "zara@cheggy@4415");
            //smtp.EnableSsl = true;
            //bool bb = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            //if (bb == true)
            //{
            //    smtp.Send(message);
            //}

            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Backup of database has been taken successfully.');", true);
            //Response.Write("Backup database successfully");
        }
        catch (Exception ex)
        {
            //Response.Write("Error During backup database!");
        }
    }
    //protected void drpacyear_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    HttpCookie cookie1 = new HttpCookie("Forcon");
    //    string dbname = "AT" + drpacyear.SelectedItem.Text.Split('-')[0] + drpacyear.SelectedItem.Text.Split('-')[1];
    //    string cc = "Data Source=CHIRAG-PC:Initial Catalog=" + dbname + ":Integrated Security=True";//AM18012018//AM05022018//AM14022018//AM16022018_1//AM19022018
    //    cookie1.Values.Add("conc", cc);
    //    cookie1.Expires = DateTime.Now.AddDays(7); // --------> cookie.Expires is the property you can set timeout
    //    HttpContext.Current.Response.AppendCookie(cookie1);
    //    lnkyear.Text = drpacyear.SelectedItem.Text;
    //    HttpCookie cookie = new HttpCookie("ForLogin");
    //    cookie.Values.Add("acyear", drpacyear.SelectedItem.Text);
    //    cookie.Expires = DateTime.Now.AddDays(7); // --------> cookie.Expires is the property you can set timeout
    //    HttpContext.Current.Response.AppendCookie(cookie);
    //}
}
