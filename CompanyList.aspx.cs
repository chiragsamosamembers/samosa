﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CompanyList : System.Web.UI.Page
{
    ForCompanyMaster fcmclass = new ForCompanyMaster();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
        }
    }

    public void fillgrid()
    {
        DataTable dtdata = new DataTable();
        dtdata = fcmclass.selectallcompany();
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvcompanylist.Visible = true;
            gvcompanylist.DataSource = dtdata;
            gvcompanylist.DataBind();
        }
        else
        {
            gvcompanylist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Company Data Found.";
        }
    }

    protected void btnnewcompany_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/CompanyMaster.aspx?mode=insert");
    }
    protected void gvcompanylist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            li.cno = Convert.ToInt64(e.CommandArgument);
            //DataTable dtdata = new DataTable();
            //dtdata = fcmclass.selectcompanydatafromcno(li);
            //if (dtdata.Rows.Count > 0)
            //{            
            Response.Redirect("~/CompanyMaster.aspx?mode=update&cno=" + li.cno + "");
            //}
        }
        else if (e.CommandName == "delete")
        {
            li.cno = Convert.ToInt64(e.CommandArgument);
            fcmclass.deletecompanymasterdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.cno + " Company Data deleted.";
            faclass.insertactivity(li);
            fillgrid();
        }
    }
    protected void gvcompanylist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvcompanylist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvcompanylist.PageIndex = e.NewPageIndex;
        fillgrid();
    }
}