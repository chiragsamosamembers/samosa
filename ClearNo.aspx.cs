﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class ClearNo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Once you enter Number and click on CLEAR DATA button then data of that Number will be deleted Permanently.So be Careful.');", true);
            //return;
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlCommand cmd = new SqlCommand("delete from BankACMaster where istype='" + drptype.SelectedItem.Text + "' and voucherno=" + Convert.ToInt64(txtno.Text) + "", con);
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();
        SqlCommand cmd1 = new SqlCommand("delete from BankACMaster1 where istype='" + drptype.SelectedItem.Text + "' and voucherno=" + Convert.ToInt64(txtno.Text) + "", con);
        con.Open();
        cmd1.ExecuteNonQuery();
        con.Close();
        SqlCommand cmd2 = new SqlCommand("delete from BankACShadow where istype='" + drptype.SelectedItem.Text + "' and voucherno=" + Convert.ToInt64(txtno.Text) + "", con);
        con.Open();
        cmd2.ExecuteNonQuery();
        con.Close();
        SqlCommand cmd3 = new SqlCommand("delete from Ledger where istype='" + drptype.SelectedItem.Text + "' and voucherno=" + Convert.ToInt64(txtno.Text) + "", con);
        con.Open();
        cmd3.ExecuteNonQuery();
        con.Close();
        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No. " + Convert.ToInt64(txtno.Text) + " has been Cleared.');", true);
        return;
    }
}