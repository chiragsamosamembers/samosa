﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BankPayment.aspx.cs" Inherits="BankPayment" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Bank Payment</span><br />
        <div class="row">
            <div class="col-md-1">
                <asp:Button ID="btnfirst" runat="server" Text="First" OnClick="btnfirst_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnprevious" runat="server" Text="Previous" OnClick="btnprevious_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnnext" runat="server" Text="Next" OnClick="btnnext_Click" /></div>
            <div class="col-md-1">
                <asp:Button ID="btnlast" runat="server" Text="Last" OnClick="btnlast_Click" /></div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Voucher No.</label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtvoucherno" runat="server" CssClass="form-control" Width="150px"
                    onkeypress="return checkQuote();"></asp:TextBox></div>
            <div class="col-md-1">
                <label class="control-label">
                    Date</label>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtvdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Bank Name<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-6">
                <%--<asp:TextBox ID="txtbankname" runat="server" CssClass="form-control" Width="393px"
                    onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtbankname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetBankname">
                </asp:AutoCompleteExtender>--%>
                <asp:DropDownList ID="drpbankname" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    User</label>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" ReadOnly="true"></asp:TextBox></div>
        </div>
        .
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2 text-center">
                <label class="control-label">
                    Account Name<span style="color: Red;">*(<asp:Label ID="lblramount" runat="server"
                        ForeColor="Red" Font-Bold="true" Font-Size="Large"></asp:Label>)</span></label>
            </div>
            <div class="col-md-3 text-center">
                <label class="control-label">
                    Remarks</label>
            </div>
            <div class="col-md-2 text-center">
                <label class="control-label">
                    Client Code</label>
            </div>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    Amount<span style="color: Red; font-size: 20px;">*</span></label>
            </div>
            <div class="col-md-2 text-center">
                <label class="control-label">
                    Cheque No.</label>
            </div>
            <%--<div class="col-md-1 text-center">
                <label class="control-label">
                    Bill No.</label>
            </div>--%>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    A/g. Bill</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <%--<asp:TextBox ID="txtaccountname" runat="server" CssClass="form-control" placeholder="Account Name"
                    onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txtaccountname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetAccountname">
                </asp:AutoCompleteExtender>--%>
                <asp:DropDownList ID="drpacname" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnSelectedIndexChanged="drpacname_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-3">
                <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control" placeholder="Remarks"
                    onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" placeholder="Client Code"
                    onkeypress="return checkQuote();"></asp:TextBox><%-- AutoPostBack="True" 
                    ontextchanged="txtclientcode_TextChanged"--%>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtclientcode"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" placeholder="Amount"
                    onkeypress="javascript:return isNumber (event)" Width="80px"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtchequeno" runat="server" CssClass="form-control" placeholder="Cheque No."
                    MaxLength="6" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <%--<div class="col-md-1">
                <asp:TextBox ID="txtbillno" runat="server" CssClass="form-control" placeholder="Bill No."
                    onkeypress="javascript:return isNumber (event)" Enabled="false"></asp:TextBox>--%>
            <%--<asp:ComboBox ID="combillno" runat="server" AutoPostBack="true" DropDownStyle="DropDownList"
                    AutoCompleteMode="Suggest" CaseSensitive="False" CssClass="" ItemInsertLocation="Append">
                    <asp:ListItem>India</asp:ListItem>
                    <asp:ListItem>Lanka</asp:ListItem>
                    <asp:ListItem>Pak</asp:ListItem>
                    <asp:ListItem>Aus</asp:ListItem>
                    <asp:ListItem>Aps</asp:ListItem>
                </asp:ComboBox>
                <asp:ComboBox ID="ComboBox1" runat="server" AutoCompleteMode="Suggest" AutoPostBack="True"
                    DropDownStyle="Simple">
                    <asp:ListItem>India</asp:ListItem>
                    <asp:ListItem>Lanka</asp:ListItem>
                    <asp:ListItem>Pak</asp:ListItem>
                    <asp:ListItem>Aus</asp:ListItem>
                    <asp:ListItem>Aps</asp:ListItem>
                    </asp:ComboBox>--%>
            <%--<asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtbillno"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetBillno">
                </asp:AutoCompleteExtender>
            </div>--%>
            <div class="col-md-1">
                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                    <asp:ListItem>N</asp:ListItem>
                    <asp:ListItem>Y</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnsaves" runat="server" Text="Save" Height="30px" BackColor="#F05283"
                    ValidationGroup="valgvbankpayment" OnClick="btnsaves_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="valgvbankpayment" />
            </div>
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="rptlist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="Ridge" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered table-responsive"
                        OnRowDeleting="rptlist_RowDeleting" BackColor="White" BorderColor="White" BorderWidth="2px"
                        CellPadding="3" CellSpacing="1" GridLines="None">
                        <Columns>
                            <asp:TemplateField HeaderText="Account Name" SortExpression="Csnm" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# Eval("id") %>' />
                                    <asp:TextBox ID="lblacname" runat="server" Text='<%# Eval("acname") %>'></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender25" runat="server" TargetControlID="lblacname"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="GetAccountname">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblremarks" runat="server" Text='<%# Eval("remarks") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CCode" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblclientcode" runat="server" Text='<%# Eval("ccode") %>'></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="lblclientcode"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="GetClientname">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblamount" runat="server" Text='<%# Eval("amount") %>' onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cheque No" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblchequeno" runat="server" Text='<%# Eval("chequeno") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ag.Bill" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblagbill" runat="server" Text='<%# Eval("agbill") %>' />
                                    <asp:Label ID="lblgvsino" runat="server" Visible="false" Text='<%# Eval("sino") %>' />
                                    <asp:Label ID="lblgvpaidamount" runat="server" Visible="false" Text='<%# Eval("paidamount") %>' />
                                    <asp:Label ID="lblgvsipi" runat="server" Text='<%# Eval("issipi") %>' />
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                        <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="#DEDFDE" ForeColor="Black" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" CssClass="GridViewItemHeader" />
                        <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#594B9C" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#33276A" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row" style="text-align: center; color: Red">
            <label class="control-label">
                Total Amount :
            </label>
            &nbsp;&nbsp;&nbsp;<asp:Label ID="lbltotal" runat="server" CssClass="control-label"
                Font-Bold="true" Font-Size="15px"></asp:Label></div>
        <asp:Button ID="btnsaveall" runat="server" Text="Save" Height="30px" BackColor="#F05283"
            ValidationGroup="valbankpayment" OnClick="btnsaveall_Click" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
            ShowSummary="false" ValidationGroup="valbankpayment" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter date."
            Text="*" ValidationGroup="valbankpayment" ControlToValidate="txtvdate"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select bank name."
            Text="*" ValidationGroup="valbankpayment" ControlToValidate="drpbankname" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please select account name."
            Text="*" ValidationGroup="valgvbankpayment" ControlToValidate="drpacname" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter client code."
            Text="*" ValidationGroup="valgvbankpayment" ControlToValidate="txtclientcode"></asp:RequiredFieldValidator>--%>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter amount."
            Text="*" ValidationGroup="valgvbankpayment" ControlToValidate="txtamount"></asp:RequiredFieldValidator>
        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please enter cheque no."
            Text="*" ValidationGroup="valgvbankpayment" ControlToValidate="txtchequeno"></asp:RequiredFieldValidator>--%>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Text="DD-MM-YYYY"
            ErrorMessage="Voucher Date must be dd-MM-yyyy" ValidationGroup="valbankpayment"
            ForeColor="Red" ControlToValidate="txtvdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
        <div class="row">
            <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
            <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
                TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="390px" align="center"
                Width="95%" Style="display: none">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <span style="color: Red;"><b>Against Bill Entry</b></span></div>
                </div>
                <div class="row">
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-1 text-left">
                        <b>Name :</b></div>
                    <div class="col-md-7">
                        <asp:Label ID="lblpopupname" runat="server" Font-Bold="true"></asp:Label></div>
                </div>
                <div class="row">
                    <div class="col-md-1 text-left">
                        <b>Adj. Amount :</b></div>
                    <div class="col-md-2">
                        <asp:Label ID="lblpopupadjamount" runat="server" Font-Bold="true"></asp:Label></div>
                    <div class="col-md-2">
                        <asp:Button ID="btnok" runat="server" Text="Ok" AccessKey="c" BackColor="Red" ForeColor="White"
                            OnClick="btnok_Click" /></div>
                </div>
                <div class="row">
                    <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="270px" Width="98%">
                        <asp:Label ID="lblemptysi" runat="server" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="gvsalesinv" runat="server" AutoGenerateColumns="False" Width="100%"
                            BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                            CssClass="table table-bordered">
                            <Columns>
                                <asp:TemplateField HeaderText="Sales Inv No." SortExpression="Csnm" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblsino" ForeColor="Black" runat="server" Text='<%# bind("sino") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales Inv No." SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstrsino" ForeColor="Black" runat="server" Text='<%# bind("strsino") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                    <ItemTemplate>
                                        <asp:Label ID="lblsidate" runat="server" ForeColor="#505050" Text='<%# bind("sidate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bill Amount" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblbillamount" ForeColor="Black" runat="server" Text='<%# bind("billamount") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount Paid" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtsiamountpaid" runat="server"></asp:TextBox>
                                        <%--AutoPostBack="True" OnTextChanged="txtsiamountpaid_TextChanged"--%>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Recceived Amount" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblreceivedamount" ForeColor="Black" runat="server" Text='<%# bind("receivedamount") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Remain Amount" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblremainamount" ForeColor="Black" runat="server" Text='<%# bind("remainamount") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#4c4c4c" />
                            <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                            <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                        </asp:GridView>
                        <asp:Label ID="lblemptypi" runat="server" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="gvpurchaseinv" runat="server" AutoGenerateColumns="False" Width="100%"
                            BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                            CssClass="table table-bordered">
                            <Columns>
                                <asp:TemplateField HeaderText="Purchase Inv No." SortExpression="Csnm" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblpino" ForeColor="Black" runat="server" Text='<%# bind("pino") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Purchase Inv No." SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstrpino" ForeColor="Black" runat="server" Text='<%# bind("strpino") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                    <ItemTemplate>
                                        <asp:Label ID="lblpidate" runat="server" ForeColor="#505050" Text='<%# bind("pidate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bill Amount" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblbillamount" ForeColor="Black" runat="server" Text='<%# bind("billamount") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount Paid" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtpiamountpaid" runat="server"></asp:TextBox>
                                        <%--AutoPostBack="True" OnTextChanged="txtpiamountpaid_TextChanged"--%>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Recceived Amount" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblreceivedamount" ForeColor="Black" runat="server" Text='<%# bind("receivedamount") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Remain Amount" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblremainamount" ForeColor="Black" runat="server" Text='<%# bind("remainamount") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#4c4c4c" />
                            <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                            <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                        </asp:GridView>
                    </asp:Panel>
                    <asp:Button ID="btnClose" runat="server" Text="Close" AccessKey="c" BackColor="Red"
                        ForeColor="White" />
                </div>
            </asp:Panel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtvdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
