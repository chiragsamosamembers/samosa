﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ToolsMasterList : System.Web.UI.Page
{
    ForToolsMaster ftmclass = new ForToolsMaster();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        if (Request.Cookies["ForLogin"]["username"] == "admin@airmax.so")
        {
            dtdata = ftmclass.selectalltoolsdata();
        }
        else
        {
            dtdata = ftmclass.selectalltoolsdatacnowise(li);
        }
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvtools.Visible = true;
            gvtools.DataSource = dtdata;
            gvtools.DataBind();
        }
        else
        {
            gvtools.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Record Found for Salesman Master.";
        }
    }

    protected void btntools_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/ToolsMaster.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvtools_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                //string acyear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
                li.id = Convert.ToInt64(e.CommandArgument);
                //DataTable dtcheck = ftmclass.checkacyearfordata(li);
                //if (dtcheck.Rows.Count > 0)
                //{
                //    if (dtcheck.Rows[0]["acyear"].ToString() == acyear)
                //    {
                Response.Redirect("~/ToolsMaster.aspx?mode=update&id=" + li.id + "");
                //    }
                //    else
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Tool Name is fetched from Old Year Tools Master so you need to go to Last Year Database and update this data from there and then fetch Tool data in new year again using Year End Process.');", true);
                //    }
                //}
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.id = Convert.ToInt64(e.CommandArgument);
                DataTable dttool = new DataTable();
                dttool = ftmclass.selecttoolsdatafromid(li);
                if (dttool.Rows.Count > 0)
                {
                    li.name = dttool.Rows[0]["name"].ToString();
                    DataTable dtToolDelItem = new DataTable();
                    dtToolDelItem = ftmclass.checkToolDelItem(li);
                    if (dtToolDelItem.Rows.Count == 0)
                    {
                        ftmclass.deletetoolsdata(li);
                        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                        li.uname = Request.Cookies["ForLogin"]["username"];
                        li.udate = System.DateTime.Now;
                        li.activity = li.id + " Tools Master Deleted.";
                        faclass.insertactivity(li);
                        fillgrid();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Tool Name used in Tools Delivery Entry so you cant delete this Tool Name.');", true);
                        return;
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvtools_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void btntools_Click1(object sender, EventArgs e)
    {
        Response.Redirect("~/ToolsMaster.aspx?mode=insert");
    }
}