﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlClient;
using System.Data.SqlTypes;

public partial class RepStockRegister : System.Web.UI.Page
{
    ForReport frclass = new ForReport();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillitemdrop();
            fillitemdroppopup();
        }
    }

    public void fillitemdrop()
    {
        DataTable dtitem = new DataTable();
        dtitem = frclass.selectallitem();


        if (dtitem.Rows.Count > 0)
        {
            drpfromitem.DataSource = dtitem;
            drpfromitem.DataTextField = "itemname";
            drpfromitem.DataValueField = "srno";
            drpfromitem.DataBind();
            drpfromitem.Items.Insert(0, "--SELECT--");
            drptoitem.DataSource = dtitem;
            drptoitem.DataTextField = "itemname";
            drptoitem.DataValueField = "srno";
            drptoitem.DataBind();
            drptoitem.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpfromitem.Items.Clear();
            drpfromitem.Items.Insert(0, "--SELECT--");
            drptoitem.Items.Clear();
            drptoitem.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillitemdroppopup()
    {
        DataTable dtitem = new DataTable();
        dtitem = frclass.selectallitempopup();


        if (dtitem.Rows.Count > 0)
        {
            drpnewname.DataSource = dtitem;
            drpnewname.DataTextField = "sf";
            drpnewname.DataValueField = "itemname";
            //drpnewname.DataValueField = "srno";
            drpnewname.DataBind();
            drpnewname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpnewname.Items.Clear();
            drpnewname.Items.Insert(0, "--SELECT--");
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void btnpreview_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        if (drpfromitem.SelectedItem.Text != "--SELECT--")
        {
            string aa = "";
            DataTable dtitem = new DataTable();
            dtitem = frclass.selectallitem();
            if (drptoitem.SelectedItem.Text != "--SELECT--")
            {
                int a = Convert.ToInt16(drpfromitem.SelectedValue);
                int b = Convert.ToInt16(drptoitem.SelectedValue);
                for (int n = a; n <= b; n++)
                {
                    string expression;
                    expression = "srno=" + Convert.ToInt64(dtitem.Rows[n - 1]["srno"].ToString()) + "";
                    DataRow[] foundRows;

                    // Use the Select method to find all rows matching the filter.
                    foundRows = dtitem.Select(expression);

                    // Print column 0 of each returned row.

                    aa = aa + "'" + foundRows[0][1] + "',";
                }
                aa = aa.TrimEnd(',');
                li.fromitem = aa.ToString();
                SessionMgt.ItemNameList = aa;
            }
            else
            {
                li.fromitem = "'" + drpfromitem.SelectedItem.Text + "'";
                SessionMgt.ItemNameList = "'" + drpfromitem.SelectedItem.Text + "'";
            }

            string Xrepname = "Stock Register";
            //li.toitem = drptoitem.SelectedItem.Text;
            li.fromdate = txtfromdate.Text;
            li.todate = txttodate.Text;

            string ccodewise = "";
            //Int64 vno = Convert.ToInt64(txtvono.Text);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?fromdate=" + li.fromdate + "&todate=" + li.todate + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Select From Item.');", true);
            return;
        }
    }

    public void fillstockledger()
    {
        li.fromitem = "'" + drpfromitem.SelectedItem.Text + "'";
        SessionMgt.ItemNameList = "'" + drpfromitem.SelectedItem.Text + "'";
        Session["dtpitemsrsr"] = CreateTemplatecsr();
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromitem = SessionMgt.ItemNameList;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        string ftow = "";
        DataSet1 imageDataSet = new DataSet1();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        SqlDataAdapter da11 = new SqlDataAdapter("select itemname,description from itemmaster where itemname in (" + li.fromitem + ") group by itemname,description order by itemname", con);
        DataTable dtitem = new DataTable();
        da11.Fill(dtitem);
        DataTable dtall = new DataTable();
        DataTable dtq = new DataTable();
        dtq = (DataTable)Session["dtpitemsrsr"];
        double opbal = 0;
        double opbald = 0;
        for (int a = 0; a < dtitem.Rows.Count; a++)
        {
            double opbal1 = 0;
            li.itemname = dtitem.Rows[a]["itemname"].ToString();
            li.description = dtitem.Rows[a]["description"].ToString();
            SqlDataAdapter da11a = new SqlDataAdapter("select isnull(sum(opqty),0) as totop from itemmaster where itemname='" + li.itemname + "' group by itemname order by itemname", con);
            DataTable dtitema = new DataTable();
            da11a.Fill(dtitema);
            if (dtitema.Rows.Count > 0)
            {
                opbal1 = Convert.ToDouble(dtitema.Rows[0]["totop"].ToString());
            }
            string rptname;
            //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
            //{
            rptname = Server.MapPath(@"~/Reports/SrockRegister.rpt");
            li.fromdate1 = "01-04-" + (Convert.ToInt64(Request.Cookies["ForLogin"]["acyear"].Split('-')[0])).ToString();
            li.todate1 = "31-03-" + (Convert.ToInt64(Request.Cookies["ForLogin"]["acyear"].Split('-')[1])).ToString();
            li.fromdated1 = Convert.ToDateTime(li.fromdate1, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.todated1 = Convert.ToDateTime(li.todate1, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
           
            
            SqlDataAdapter dc00 = new SqlDataAdapter("select isnull(sum(PCItems.stockqty),0) as rqty from PCItems inner join PCMaster on PCMaster.pcno=PCItems.pcno where PCItems.itemname='" + li.itemname + "' and PCItems.cno=" + li.cno + " and (PCMaster.pcdate < @fromdate and PCMaster.pcdate between @fdate and @tdate)", con);// and PCMaster.pcdate between @fromdate and @todate
            dc00.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            //dc00.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            dc00.SelectCommand.Parameters.AddWithValue("@fdate", li.fromdated1);
            dc00.SelectCommand.Parameters.AddWithValue("@tdate", li.todated1);
            DataTable dtop00 = new DataTable();
            dc00.Fill(dtop00);

            SqlDataAdapter dc100 = new SqlDataAdapter("select isnull(sum(SCItems.stockqty),0) as iqty from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno where SCItems.itemname='" + li.itemname + "' and SCItems.cno=" + li.cno + " and (SCMaster.scdate < @fromdate and SCMaster.scdate between @fdate and @tdate)", con);// and SCMaster.scdate between @fromdate and @todate
            dc100.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            //dc100.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            dc100.SelectCommand.Parameters.AddWithValue("@fdate", li.fromdated1);
            dc100.SelectCommand.Parameters.AddWithValue("@tdate", li.todated1);
            DataTable dtop100 = new DataTable();
            dc100.Fill(dtop100);

            SqlDataAdapter dcaa = new SqlDataAdapter("select isnull(sum(StockJVItems.qty),0) as rqty from StockJVItems where StockJVItems.itemname='" + li.itemname + "' and StockJVItems.cno=" + li.cno + " and StockJVItems.inout='IN' and (StockJVItems.challandate < @fromdate and StockJVItems.challandate between @fdate and @tdate)", con);
            dcaa.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            dcaa.SelectCommand.Parameters.AddWithValue("@fdate", li.fromdated1);
            dcaa.SelectCommand.Parameters.AddWithValue("@tdate", li.todated1);
            DataTable dtopaa = new DataTable();
            dcaa.Fill(dtopaa);

            SqlDataAdapter dcbb = new SqlDataAdapter("select isnull(sum(StockJVItems.qty),0) as rqty from StockJVItems where StockJVItems.itemname='" + li.itemname + "' and StockJVItems.cno=" + li.cno + " and StockJVItems.inout='OUT' and (StockJVItems.challandate < @fromdate and StockJVItems.challandate between @fdate and @tdate)", con);
            dcbb.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            dcbb.SelectCommand.Parameters.AddWithValue("@fdate", li.fromdated1);
            dcbb.SelectCommand.Parameters.AddWithValue("@tdate", li.todated1);
            DataTable dtopbb = new DataTable();
            dcbb.Fill(dtopbb);

            double c1 = 0;
            double c2 = 0;
            double c3 = 0;
            double c4 = 0;
            if (dtop00.Rows.Count > 0)
            {
                c1 = Convert.ToDouble(dtop00.Rows[0]["rqty"].ToString());
            }
            if (dtop100.Rows.Count > 0)
            {
                c2 = Convert.ToDouble(dtop100.Rows[0]["iqty"].ToString());
            }
            if (dtopaa.Rows.Count > 0)
            {
                c3 = Convert.ToDouble(dtopaa.Rows[0]["rqty"].ToString());
            }
            if (dtopbb.Rows.Count > 0)
            {
                c4 = Convert.ToDouble(dtopbb.Rows[0]["rqty"].ToString());
            }
            opbal = 0 + opbal1 + c1 - c2 + c3 - c4;
            if (opbal > 0)
            {
                DataRow dr = dtq.NewRow();
                dr["strchno"] = "";
                dr["chdate"] = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr["billno"] = "";
                dr["billdate"] = "";
                dr["acname"] = "Opening Stock";

                dr["itemname"] = li.itemname;
                dr["descr"] = li.description;
                dr["descr1"] = "";
                dr["descr2"] = "";
                dr["descr3"] = "";
                dr["rqty"] = opbal;

                dr["iqty"] = "0";
                dr["clqty"] = "0";
                dr["type"] = "OP";
                dr["id"] = "0";
                dtq.Rows.Add(dr);
            }
            else if (opbal < 0)
            {
                DataRow dr = dtq.NewRow();
                dr["strchno"] = "";
                dr["chdate"] = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr["billno"] = "";
                dr["billdate"] = "";
                dr["acname"] = "Opening Stock";

                dr["itemname"] = li.itemname;
                dr["descr"] = li.description;
                dr["descr1"] = "";
                dr["descr2"] = "";
                dr["descr3"] = "";
                dr["rqty"] = "0";

                dr["iqty"] = opbal;
                dr["clqty"] = "0";
                dr["type"] = "OP";
                dr["id"] = "0";
                dtq.Rows.Add(dr);
            }
            //SqlDataAdapter dc = new SqlDataAdapter("select (CASE WHEN (type = 'D') THEN amount ELSE NULL END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE NULL END) as Creditamt from ledger where type='C'", con);
            SqlDataAdapter dc = new SqlDataAdapter("select * from PCItems inner join PCMaster on PCMaster.pcno=PCItems.pcno where PCItems.itemname='" + li.itemname + "' and PCItems.cno=" + li.cno + " and PCItems.stockqty>0 and PCMaster.pcdate between @fromdate and @todate", con);
            dc.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            dc.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtop = new DataTable();
            dc.Fill(dtop);
            for (int b = 0; b < dtop.Rows.Count; b++)
            {
                DataRow dr = dtq.NewRow();
                dr["id"] = Convert.ToInt64(dtop.Rows[b]["id"].ToString());
                dr["strchno"] = dtop.Rows[b]["pcno"].ToString();
                //dr["chdate"] = DateTime.ParseExact(dtop.Rows[b]["pcdate"].ToString(), "dd-MM-yyyy", null);
                dr["chdate"] = Convert.ToDateTime(dtop.Rows[b]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                Int64 pcno = 0;
                if (dtop.Rows[b]["pcno"].ToString() != string.Empty)
                {
                    pcno = Convert.ToInt64(dtop.Rows[b]["pcno"].ToString());
                }
                SqlDataAdapter dapbill = new SqlDataAdapter("select * from PIMaster inner join PIItems on PIMaster.strpino=PIItems.strpino where PIMaster.stringpcno like '%'+@pcno+'%' and PIItems.itemname='" + li.itemname + "'", con);
                dapbill.SelectCommand.Parameters.AddWithValue("@pcno", pcno.ToString());
                DataTable dtpbill = new DataTable();
                dapbill.Fill(dtpbill);
                if (dtpbill.Rows.Count > 0)
                {
                    dr["billno"] = dtpbill.Rows[0]["strpino"].ToString();
                    dr["billdate"] = Convert.ToDateTime(dtpbill.Rows[0]["pidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                else
                {
                    dr["billno"] = "";
                    dr["billdate"] = "";
                }
                dr["acname"] = dtop.Rows[b]["acname"].ToString();

                dr["itemname"] = dtop.Rows[b]["itemname"].ToString();
                dr["descr"] = li.description;
                dr["descr1"] = dtop.Rows[b]["descr1"].ToString();
                dr["descr2"] = dtop.Rows[b]["descr2"].ToString();
                dr["descr3"] = dtop.Rows[b]["descr3"].ToString();
                if (dtop.Rows[b]["stockqty"].ToString().Trim() != string.Empty)
                {
                    dr["rqty"] = dtop.Rows[b]["stockqty"].ToString();
                }
                else
                {
                    dr["rqty"] = "0";
                }
                dr["iqty"] = "0";
                if (dtop.Rows[b]["stockqty"].ToString().Trim() != string.Empty)
                {
                    opbal = opbal + Convert.ToDouble(dtop.Rows[b]["stockqty"].ToString());
                }
                dr["clqty"] = opbal;
                dr["type"] = "PCHN";
                dtq.Rows.Add(dr);
            }

            //Stock JV IN
            SqlDataAdapter dcsjvin = new SqlDataAdapter("select * from StockJVItems where itemname='" + li.itemname + "' and cno=" + li.cno + " and inout='IN' and challandate between @fromdate and @todate", con);
            dcsjvin.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            dcsjvin.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtopsjvin = new DataTable();
            dcsjvin.Fill(dtopsjvin);
            for (int b = 0; b < dtopsjvin.Rows.Count; b++)
            {
                DataRow dr = dtq.NewRow();
                dr["id"] = "0";
                dr["strchno"] = dtopsjvin.Rows[b]["challanno"].ToString();
                dr["chdate"] = Convert.ToDateTime(dtopsjvin.Rows[b]["challandate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr["billno"] = "";
                dr["billdate"] = "";
                dr["acname"] = dtopsjvin.Rows[b]["acname"].ToString();
                dr["itemname"] = dtopsjvin.Rows[b]["itemname"].ToString();
                dr["descr"] = li.description;
                dr["descr1"] = dtopsjvin.Rows[b]["descr1"].ToString();
                dr["descr2"] = dtopsjvin.Rows[b]["descr2"].ToString();
                dr["descr3"] = dtopsjvin.Rows[b]["descr3"].ToString();
                if (dtopsjvin.Rows[b]["qty"].ToString().Trim() != string.Empty)
                {
                    dr["rqty"] = dtopsjvin.Rows[b]["qty"].ToString();
                }
                else
                {
                    dr["rqty"] = "0";
                }
                dr["iqty"] = "0";
                if (dtopsjvin.Rows[b]["qty"].ToString().Trim() != string.Empty)
                {
                    opbal = opbal + Convert.ToDouble(dtopsjvin.Rows[b]["qty"].ToString());
                }
                dr["clqty"] = opbal;
                dr["type"] = "SJVIN";
                dtq.Rows.Add(dr);
            }
            //

            SqlDataAdapter dc1 = new SqlDataAdapter("select * from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno where SCItems.itemname='" + li.itemname + "' and SCItems.cno=" + li.cno + " and SCItems.stockqty>0 and SCMaster.scdate between @fromdate and @todate", con);
            dc1.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            dc1.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtop1 = new DataTable();
            dc1.Fill(dtop1);

            for (int b = 0; b < dtop1.Rows.Count; b++)
            {
                DataRow dr = dtq.NewRow();
                dr["id"] = Convert.ToInt64(dtop1.Rows[b]["id"].ToString());
                dr["strchno"] = dtop1.Rows[b]["strscno"].ToString();
                dr["chdate"] = Convert.ToDateTime(dtop1.Rows[b]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                string scno = "";
                if (dtop1.Rows[b]["strscno"].ToString() != string.Empty)
                {
                    scno = "'" + dtop1.Rows[b]["strscno"].ToString() + "'";
                }
                SqlDataAdapter dasbill = new SqlDataAdapter("select * from SIMaster inner join SIItems on SIMaster.strsino=SIItems.strsino where SIMaster.stringscno like '%'+@scno+'%' and SIItems.itemname='" + li.itemname + "'", con);
                dasbill.SelectCommand.Parameters.AddWithValue("@scno", scno);
                DataTable dtsbill = new DataTable();
                dasbill.Fill(dtsbill);
                if (dtsbill.Rows.Count > 0)
                {
                    dr["billno"] = dtsbill.Rows[0]["strsino"].ToString();
                    dr["billdate"] = Convert.ToDateTime(dtsbill.Rows[0]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                else
                {
                    dr["billno"] = "";
                    dr["billdate"] = "";
                }
                dr["acname"] = dtop1.Rows[b]["acname"].ToString();

                dr["itemname"] = dtop1.Rows[b]["itemname"].ToString();
                dr["descr"] = li.description;
                dr["descr1"] = dtop1.Rows[b]["descr1"].ToString();
                dr["descr2"] = dtop1.Rows[b]["descr2"].ToString();
                dr["descr3"] = dtop1.Rows[b]["descr3"].ToString();
                dr["rqty"] = "0";
                if (dtop1.Rows[b]["stockqty"].ToString().Trim() != string.Empty)
                {
                    dr["iqty"] = dtop1.Rows[b]["stockqty"].ToString();
                }
                else
                {
                    dr["iqty"] = "0";
                }
                if (dtop1.Rows[b]["stockqty"].ToString().Trim() != string.Empty)
                {
                    opbal = opbal - Convert.ToDouble(dtop1.Rows[b]["stockqty"].ToString());
                }
                dr["clqty"] = opbal;
                dr["type"] = "SCHN";
                dtq.Rows.Add(dr);
            }

            //Stock JV OUT
            SqlDataAdapter dcsjvout = new SqlDataAdapter("select * from StockJVItems where itemname='" + li.itemname + "' and cno=" + li.cno + " and inout='OUT' and challandate between @fromdate and @todate", con);
            dcsjvout.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
            dcsjvout.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
            DataTable dtopsjvout = new DataTable();
            dcsjvout.Fill(dtopsjvout);
            for (int b = 0; b < dtopsjvout.Rows.Count; b++)
            {
                DataRow dr = dtq.NewRow();
                dr["id"] = "0";
                dr["strchno"] = dtopsjvout.Rows[b]["challanno"].ToString();
                dr["chdate"] = Convert.ToDateTime(dtopsjvout.Rows[b]["challandate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                dr["billno"] = "";
                dr["billdate"] = "";
                dr["acname"] = dtopsjvout.Rows[b]["acname"].ToString();
                dr["itemname"] = dtopsjvout.Rows[b]["itemname"].ToString();
                dr["descr"] = li.description;
                dr["descr1"] = dtopsjvout.Rows[b]["descr1"].ToString();
                dr["descr2"] = dtopsjvout.Rows[b]["descr2"].ToString();
                dr["descr3"] = dtopsjvout.Rows[b]["descr3"].ToString();
                dr["rqty"] = "0";
                if (dtopsjvout.Rows[b]["qty"].ToString().Trim() != string.Empty)
                {
                    dr["iqty"] = dtopsjvout.Rows[b]["qty"].ToString();
                    opbal = opbal - Convert.ToDouble(dtopsjvout.Rows[b]["qty"].ToString());
                }
                else
                {
                    dr["iqty"] = "0";
                }
                dr["clqty"] = opbal;
                dr["type"] = "SJVOUT";
                dtq.Rows.Add(dr);
            }
            //

            //rptdoc.Load(rptname);
            double zero = 0;

            //sorting and creating final data for report
            Session["dtpitemsrsr"] = dtq;
            DataView dtdv = dtq.DefaultView;
            //dtdv.RowFilter = "rqty<>0 or iqty<>0";
            dtdv.Sort = "chdate asc";
            DataTable dtf = new DataTable();
            dtf = dtdv.ToTable();
            Session["dtpitemsrsr"] = CreateTemplatecsr();
            dtq = (DataTable)Session["dtpitemsrsr"];
            double fop = 0;
            for (int bb = 0; bb < dtf.Rows.Count; bb++)
            {
                DataRow dr = dtq.NewRow();
                dr["id"] = Convert.ToInt64(dtf.Rows[bb]["id"].ToString());
                dr["strchno"] = dtf.Rows[bb]["strchno"].ToString();
                dr["chdate"] = Convert.ToDateTime(dtf.Rows[bb]["chdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                //string scno = "";
                //if (dtf.Rows[bb]["strscno"].ToString() != string.Empty)
                //{
                //    scno = "'" + dtf.Rows[bb]["strscno"].ToString() + "'";
                //}

                dr["billno"] = dtf.Rows[bb]["billno"].ToString();
                if (dtf.Rows[bb]["billdate"].ToString() != "")
                {
                    dr["billdate"] = Convert.ToDateTime(dtf.Rows[bb]["billdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                else
                {
                    dr["billdate"] = "";
                }
                dr["acname"] = dtf.Rows[bb]["acname"].ToString();

                dr["itemname"] = dtf.Rows[bb]["itemname"].ToString();
                dr["descr"] = dtf.Rows[bb]["descr"].ToString();
                dr["descr1"] = dtf.Rows[bb]["descr1"].ToString();
                dr["descr2"] = dtf.Rows[bb]["descr2"].ToString();
                dr["descr3"] = dtf.Rows[bb]["descr3"].ToString();
                dr["rqty"] = dtf.Rows[bb]["rqty"].ToString();

                dr["iqty"] = dtf.Rows[bb]["iqty"].ToString();
                fop = fop + Convert.ToDouble(dtf.Rows[bb]["rqty"].ToString()) - Convert.ToDouble(dtf.Rows[bb]["iqty"].ToString());
                dr["clqty"] = Math.Round(fop, 2);
                dr["type"] = dtf.Rows[bb]["type"].ToString();
                //if (dtf.Rows[bb]["rqty"].ToString() == "0" && dtf.Rows[bb]["iqty"].ToString() == "0")
                //{
                //}
                //else
                //{
                dtq.Rows.Add(dr);
                //}
            }
            dtall.Merge(dtq);
            Session["dtpitemsrsr"] = CreateTemplatecsr();
            dtq = (DataTable)Session["dtpitemsrsr"];
            //
        }
        Session["dtpitemsrsr"] = dtall;
        Session["dtpitemsrsr"] = dtq;
        if (dtall.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvaclist.Visible = true;
            gvaclist.DataSource = dtall;
            gvaclist.DataBind();
        }
        else
        {
            gvaclist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Data Found.";
        }
        double r = 0;
        double i = 0;
        for (int c = 0; c < dtall.Rows.Count; c++)
        {
            Label lblrqty = (Label)gvaclist.Rows[c].FindControl("lblrqty");
            Label lbliqty = (Label)gvaclist.Rows[c].FindControl("lbliqty");
            r = r + Convert.ToDouble(dtall.Rows[c]["rqty"].ToString());
            i = i + Convert.ToDouble(dtall.Rows[c]["iqty"].ToString());
            if (dtall.Rows[c]["rqty"].ToString() == "0")
            {
                lblrqty.Visible = false;
            }
            if (dtall.Rows[c]["iqty"].ToString() == "0")
            {
                lbliqty.Visible = false;
            }
        }
        lbltotalreceived.Text = r.ToString();
        lbltotalissued.Text = i.ToString();
        if (dtall.Rows.Count > 0)
        {
            lblclosedqty.Text = dtall.Rows[dtall.Rows.Count - 1]["clqty"].ToString();
        }
        else
        {
            lblclosedqty.Text = "0";
        }
        //rptdoc.SetDataSource(dtall);
    }

    public DataTable CreateTemplatecsr()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("strchno", typeof(string));
        dtpitems.Columns.Add("chdate", typeof(DateTime));
        dtpitems.Columns.Add("billno", typeof(string));
        dtpitems.Columns.Add("billdate", typeof(string));
        dtpitems.Columns.Add("acname", typeof(string));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("descr", typeof(string));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("rqty", typeof(double));
        dtpitems.Columns.Add("iqty", typeof(double));
        dtpitems.Columns.Add("clqty", typeof(double));
        dtpitems.Columns.Add("type", typeof(string));
        dtpitems.Columns.Add("id", typeof(Int64));
        return dtpitems;
    }

    protected void btnfillledger_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        fillstockledger();
    }
    protected void gvaclist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblchno = (Label)currentRow.FindControl("lblchno");
            Label lbltype = (Label)currentRow.FindControl("lbltype");
            if (lbltype.Text == "OP")
            {

            }
            else if (lbltype.Text == "SCHN")
            {
                li.strscno = lblchno.Text;
                Response.Redirect("~/SalesChallan.aspx?mode=update&strscno=" + li.strscno + "");
            }
            else if (lbltype.Text == "PCHN")
            {
                li.pcno = Convert.ToInt64(lblchno.Text);
                Response.Redirect("~/PurchaseChallan.aspx?mode=update&pcno=" + li.pcno + "");
            }
            else if (lbltype.Text == "SJVIN")
            {
                li.strchallanno = lblchno.Text;
                Response.Redirect("~/StockJVIN.aspx?challanno=" + li.strchallanno + "&mode=update");
            }
            else
            {
                li.strchallanno = lblchno.Text;
                Response.Redirect("~/StockJVOUT.aspx?challanno=" + li.strchallanno + "&mode=update");
            }
        }
        else if (e.CommandName == "process")
        {
            if (drpfromitem.SelectedItem.Text != "--SELECT--")
            {
                lbloldname.Text = drpfromitem.SelectedItem.Text;
                GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                Label lblid = (Label)currentRow.FindControl("lblid");
                Label lbltype = (Label)currentRow.FindControl("lbltype");
                if (lblid.Text != "0")
                {
                    lblchallanno.Text = e.CommandArgument.ToString();
                    lblchid.Text = lblid.Text;
                    li.id = Convert.ToInt64(lblchid.Text);
                    string cz = Request.Cookies["Forcon"]["conc"];
                    cz = cz.Replace(":", ";");
                    SqlConnection con = new SqlConnection(cz);
                    lbltypec.Text = lbltype.Text;
                    if (lbltype.Text == "SCHN")
                    {
                        SqlDataAdapter da = new SqlDataAdapter("select * from SIItems where vid=" + li.id + "", con);
                        DataTable dtdata = new DataTable();
                        da.Fill(dtdata);
                        if (dtdata.Rows.Count > 0)
                        {
                            lblbillno.Text = dtdata.Rows[0]["strsino"].ToString();
                            lblbillid.Text = dtdata.Rows[0]["id"].ToString();
                        }
                        else
                        {
                            lblbillno.Text = "0";
                            lblbillid.Text = "0";
                        }
                    }
                    else if (lbltype.Text == "PCHN")
                    {
                        SqlDataAdapter da = new SqlDataAdapter("select * from PIItems where vid=" + li.id + "", con);
                        DataTable dtdata = new DataTable();
                        da.Fill(dtdata);
                        if (dtdata.Rows.Count > 0)
                        {
                            lblbillno.Text = dtdata.Rows[0]["strpino"].ToString();
                            lblbillid.Text = dtdata.Rows[0]["id"].ToString();
                        }
                        else
                        {
                            lblbillno.Text = "0";
                            lblbillid.Text = "0";
                        }
                    }
                    ModalPopupExtender2.Show();
                }
            }
        }
    }
    protected void btnpopupdate_Click(object sender, EventArgs e)
    {
        if (drpnewname.SelectedItem.Text != "--SELECT--")
        {
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            if (lbltypec.Text == "SCHN")
            {
                SqlCommand cmd = new SqlCommand("update SCItems set itemname=@itemname where id=@id", con);
                cmd.Parameters.AddWithValue("@itemname", drpnewname.SelectedValue);
                cmd.Parameters.AddWithValue("@id", Convert.ToInt64(lblchid.Text));
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                if (lblbillid.Text != "0")
                {
                    SqlCommand cmd1 = new SqlCommand("update SIItems set itemname=@itemname where id=@id", con);
                    cmd1.Parameters.AddWithValue("@itemname", drpnewname.SelectedValue);
                    cmd1.Parameters.AddWithValue("@id", Convert.ToInt64(lblbillid.Text));
                    con.Open();
                    cmd1.ExecuteNonQuery();
                    con.Close();
                }
            }
            else if (lbltypec.Text == "PCHN")
            {
                SqlCommand cmd = new SqlCommand("update PCItems set itemname=@itemname where id=@id", con);
                cmd.Parameters.AddWithValue("@itemname", drpnewname.SelectedValue);
                cmd.Parameters.AddWithValue("@id", Convert.ToInt64(lblchid.Text));
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                if (lblbillid.Text != "0")
                {
                    SqlCommand cmd1 = new SqlCommand("update PIItems set itemname=@itemname where id=@id", con);
                    cmd1.Parameters.AddWithValue("@itemname", drpnewname.SelectedValue);
                    cmd1.Parameters.AddWithValue("@id", Convert.ToInt64(lblbillid.Text));
                    con.Open();
                    cmd1.ExecuteNonQuery();
                    con.Close();
                }
            }
            ModalPopupExtender2.Hide();
            fillstockledger();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select New Name and Try Again.');", true);
            return;
        }
        lblchallanno.Text = string.Empty;
        lblchid.Text = string.Empty;
        lblbillno.Text = string.Empty;
        lblbillid.Text = string.Empty;
        lbloldname.Text = string.Empty;
        fillitemdroppopup();
    }
    protected void drpnewname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpnewname.SelectedItem.Text != "--SELECT--")
        {
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter da = new SqlDataAdapter("select * from ItemMaster where itemname='" + drpnewname.SelectedItem.Text + "'", con);
            DataTable dtdata = new DataTable();
            if (dtdata.Rows.Count > 0)
            {
                //lblunit.Text = dtdata.Rows[0]["unit"].ToString();
            }
            else
            {
                //lblunit.Text = "";
            }
        }
    }
    protected void drpfromitem_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpfromitem.SelectedItem.Text != "--SELECT--")
        {
            li.itemname = drpfromitem.SelectedItem.Text;
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            SqlDataAdapter da = new SqlDataAdapter("select * from ItemMaster where itemname='" + li.itemname + "'", con);
            DataTable dtdata = new DataTable();
            da.Fill(dtdata);
            if (dtdata.Rows.Count > 0)
            {
                lbldescr.Text = dtdata.Rows[0]["description"].ToString() + " / " + dtdata.Rows[0]["unit"].ToString();
            }
            else
            {
                lbldescr.Text = "";
            }
        }
        else
        {
            lbldescr.Text = "";
        }
    }
}