﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;

public partial class RepDeliveryChallan : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //string Xrepname = "sales challan";
            //Int64 scno = 50002;
            ////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?scno=" + scno + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getscno(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        li.acname = SessionMgt.acname;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallscnoacnamewisestring(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][16].ToString());
        }
        return CountryNames;
    }
    protected void txtacname_TextChanged(object sender, EventArgs e)
    {
        if (txtacname.Text != string.Empty)
        {
            SessionMgt.acname = txtacname.Text;
        }
    }
    protected void btnsaves_Click(object sender, EventArgs e)
    {
        string Xrepname = "sales challan";
        string strscno = txtsaleschallanno.Text;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?strscno=" + strscno + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }
}