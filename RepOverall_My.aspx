﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RepOverall_My.aspx.cs" Inherits="RepOverall" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Overall Report</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    CCODE</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtacname" runat="server" CssClass="form-control" Width="200px"></asp:TextBox><%--AutoPostBack="True" OnTextChanged="txtccode_TextChanged"--%>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtacname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter ccode."
                    Text="*" ControlToValidate="txtacname" ValidationGroup="val"></asp:RequiredFieldValidator>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    Type</label></div>
            <div class="col-md-2">
                <asp:DropDownList ID="drptype" runat="server" CssClass="form-control">
                    <asp:ListItem Value="Overall Report">Simple</asp:ListItem>
                    <asp:ListItem Value="Overall Report with no">With No.</asp:ListItem>
                    <asp:ListItem Value="Overall Tools Report">Tools</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-2">
                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                    <asp:ListItem Selected="True">PDF</asp:ListItem>
                    <asp:ListItem>Excel</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    From Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox>
                <%--<asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txtfromdate">
                </asp:MaskedEditExtender>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter from date."
                    Text="*" ControlToValidate="txtfromdate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="From Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txtfromdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    To Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
                <%-- <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txttodate">
                </asp:MaskedEditExtender>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter to date."
                    Text="*" ControlToValidate="txttodate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="To Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txttodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator></div>
        </div>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnprocessdata" runat="server" Text="Prepare Data" 
                    class="btn btn-default forbutton" onclick="btnprocessdata_Click" /> <asp:Button
                    ID="btnpreview" runat="server" Text="Preview" class="btn btn-default forbutton"
                    OnClick="btnpreview_Click" />
                <%--ValidationGroup="val"--%>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
            <script type="text/javascript">
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
                function getme() {
                    $("#<%= txtfromdate.ClientID %>").datepicker({
                        showmonth: true,
                        autoSize: true,
                        showAnim: 'slideDown',
                        duration: 'fast',
                        dateFormat: "dd-mm-yy"
                    });

                    $("#<%= txttodate.ClientID %>").datepicker({
                        showmonth: true,
                        autoSize: true,
                        showAnim: 'slideDown',
                        duration: 'fast',
                        dateFormat: "dd-mm-yy"
                    });


                }
            </script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#<%= txtfromdate.ClientID %>").click(function () {

                        $(this).datepicker({
                            showmonth: true,
                            autoSize: true,
                            showAnim: 'slideDown',
                            duration: 'fast',
                            dateFormat: "dd-mm-yy"
                        });
                    });
                    $("#<%= txtfromdate.ClientID %>").datepicker({
                        showmonth: true,
                        autoSize: true,
                        showAnim: 'slideDown',
                        duration: 'fast',
                        dateFormat: "dd-mm-yy"
                    });


                    $("#<%= txttodate.ClientID %>").click(function () {

                        $(this).datepicker({
                            showmonth: true,
                            autoSize: true,
                            showAnim: 'slideDown',
                            duration: 'fast',
                            dateFormat: "dd-mm-yy"
                        });
                    });
                    $("#<%= txttodate.ClientID %>").datepicker({
                        showmonth: true,
                        autoSize: true,
                        showAnim: 'slideDown',
                        duration: 'fast',
                        dateFormat: "dd-mm-yy"
                    });

                });
            </script>
        </div>
        <span style="color: white; background-color: Red">Overall Report (Challan/Bill wise)</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    CCODE</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtccode1" runat="server" CssClass="form-control" Width="200px"></asp:TextBox><%--AutoPostBack="True" OnTextChanged="txtccode_TextChanged"--%>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtccode1"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter ccode."
                    Text="*" ControlToValidate="txtacname" ValidationGroup="val"></asp:RequiredFieldValidator>
            </div>
            <div class="col-md-2">
                <asp:RadioButtonList ID="rdotype1" runat="server">
                    <asp:ListItem Selected="True">PDF</asp:ListItem>
                    <asp:ListItem>Excel</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnoverall1" runat="server" Text="Overall Challanwise" class="btn btn-default forbutton"
                    OnClick="btnoverall1_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
        </div>
    </div>
</asp:Content>
