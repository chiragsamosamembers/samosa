﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlTypes;
using Survey.Classes;

public partial class RepSalesRegister : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }

        if (txtsalesacname.Text != "" && (txtacname.Text != "" || txtclientcode.Text != ""))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This combination of filtering not possible.Please try again with only Sales ACName or ACName and CCode together.');", true);
            return;
        }

        string Xrepname = "Sales Register Report";
        //string Xrepname = "Trial Balance Report";
        //Int64 vno = Convert.ToInt64(txtvono.Text);
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        li.acname = txtacname.Text;
        li.salesac = txtsalesacname.Text;
        if (txtclientcode.Text != string.Empty)
        {
            li.ccode = Convert.ToInt64(txtclientcode.Text);
        }
        else
        {
            li.ccode = 0;
        }
        string tax = "No";
        if (chkshowtax.Checked)
        {
            tax = "Yes";
        }
        SessionMgt.acname = txtsalesacname.Text;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?fromdate=" + li.fromdate + "&todate=" + li.todate + "&tax=" + tax + "&salesac=" + li.salesac + "&acname=" + li.acname + "&ccode=" + li.ccode + "&rtype=" + RadioButtonList1.SelectedItem.Text + "&taxtype=" + RadioButtonList2.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //////////string Xrepname = "Ledger Report";
        //////////Int64 vno = Convert.ToInt64(txtvono.Text);
        ////////////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //////////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtclientcode.Text = string.Empty;
        }
        Page.SetFocus(txtfromdate);
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetSalesAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallsalesacname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

}