﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlTypes;

public partial class StockAdjustment : System.Web.UI.Page
{
    ForStockSettlement fssclass = new ForStockSettlement();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillitemdrop();
            TabPanel1.Enabled = true;
            TabPanel2.Enabled = false;
            TabPanel3.Enabled = false;
        }
    }

    public void fillitemdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fssclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    protected void btngetdata_Click(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            string xxyear = Request.Cookies["ForLogin"]["currentyear"];
            li.fromdated = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.todated = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.itemname = drpitemname.SelectedItem.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            DataTable dtsc = new DataTable();
            dtsc = fssclass.selectallscdatafromitemname(li);
            DataTable dtstockjvout = new DataTable();
            dtstockjvout = fssclass.selectallstockjvotdatafromitemname(li);
            dtsc.Merge(dtstockjvout);
            DataView dtdv = dtsc.DefaultView;
            dtdv.Sort = "scdate asc";
            DataTable dtf = new DataTable();
            dtf = dtdv.ToTable();
            if (dtf.Rows.Count > 0)
            {
                lblemptysc.Visible = false;
                gvsc.Visible = true;
                gvsc.DataSource = dtf;
                gvsc.DataBind();
            }
            else
            {
                gvsc.Visible = false;
                lblemptysc.Visible = true;
                lblemptysc.Text = "No Sales Challan Data Found.";
            }
            TabPanel1.Enabled = true;
            TabPanel2.Enabled = false;
            TabPanel3.Enabled = false;
        }
        else
        {
            lblemptysc.Visible = false;
            gvsc.Visible = false;
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select Item Name.');", true);
            return;
        }
    }

    protected void gvsc_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            string xxyear = Request.Cookies["ForLogin"]["currentyear"];
            li.fromdated = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.todated = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            Session["dtpitems"] = CreateTemplate();
            li.itemname = drpitemname.SelectedItem.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            //li.strscno = e.CommandArgument.ToString();
            li.scid = Convert.ToInt64(e.CommandArgument);
            hdnscid.Value = li.scid.ToString();
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblatype = (Label)currentRow.FindControl("lblatype");
            li.type = lblatype.Text;
            hdntype1.Value = li.type;
            if (li.type == "SCHN")
            {
                DataTable dtscdata = new DataTable();
                dtscdata = fssclass.selectalldatafromstrscno(li);

                if (dtscdata.Rows.Count > 0)
                {
                    txtscno.Text = dtscdata.Rows[0]["strscno"].ToString();
                    txtscdescr1.Text = dtscdata.Rows[0]["descr1"].ToString();
                    txtscdescr2.Text = dtscdata.Rows[0]["descr2"].ToString();
                    txtscdescr3.Text = dtscdata.Rows[0]["descr3"].ToString();
                    txtscqty.Text = dtscdata.Rows[0]["stockqty"].ToString();
                    txtscdate.Text = (Convert.ToDateTime(dtscdata.Rows[0]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)).ToString("dd-MM-yyyy");
                    txtscbalanceqty.Text = dtscdata.Rows[0]["balqty"].ToString();
                    hdnsc.Value = dtscdata.Rows[0]["adjqty"].ToString();

                    //Fill Opening Data From Item Master for Purchase
                    DataTable dt = (DataTable)Session["dtpitems"];
                    DataTable dtop = new DataTable();
                    dtop = fssclass.selectalldataopdatafromitemname(li);
                    for (int r = 0; r < dtop.Rows.Count; r++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["pcno"] = "0";
                        dr["id"] = Convert.ToInt64(dtop.Rows[r]["id"].ToString());
                        dr["acname"] = "Opening Balance";
                        dr["pcdate"] = Convert.ToDateTime(dtop.Rows[r]["opdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr["adjqty"] = Convert.ToDouble(dtop.Rows[r]["adjqty"].ToString());
                        dr["stockqty"] = Convert.ToDouble(dtop.Rows[r]["qty"].ToString());
                        dr["balqty"] = Convert.ToDouble(dtop.Rows[r]["balqty"].ToString());
                        dr["atype"] = "OP";
                        dt.Rows.Add(dr);
                    }
                    Session["dtpitems"] = dt;

                    DataTable dtpc = new DataTable();
                    dtpc = fssclass.selectallpcdatafromitemname(li);
                    if (dt.Rows.Count > 0)
                    {
                        dtpc.Merge(dt);
                    }
                    DataTable dtstockjvin = new DataTable();
                    dtstockjvin = fssclass.selectallstockjvindatafromitemname(li);
                    dtpc.Merge(dtstockjvin);
                    DataView dtdv = dtpc.DefaultView;
                    dtdv.Sort = "pcdate asc";
                    DataTable dtf = new DataTable();
                    dtf = dtdv.ToTable();
                    //if (dt.Rows.Count > 0)
                    //{
                    //    lblemptypc.Visible = false;
                    //    gvpc.Visible = true;
                    //    gvpc.DataSource = dt;
                    //    gvpc.DataBind();
                    //}
                    //else 
                    if (dtf.Rows.Count > 0)
                    {
                        lblemptypc.Visible = false;
                        gvpc.Visible = true;
                        gvpc.DataSource = dtf;
                        gvpc.DataBind();
                    }
                    else
                    {
                        gvpc.Visible = false;
                        lblemptypc.Visible = true;
                        lblemptypc.Text = "No Purchase Challan Data Found.";
                    }
                    TabPanel1.Enabled = false;
                    TabPanel2.Enabled = true;
                    TabPanel3.Enabled = false;
                }
            }
            else
            {
                DataTable dtscdata = new DataTable();
                dtscdata = fssclass.selectalldatafromsjvitemsid(li);

                if (dtscdata.Rows.Count > 0)
                {
                    txtscno.Text = dtscdata.Rows[0]["challanno"].ToString();
                    txtscdescr1.Text = dtscdata.Rows[0]["descr1"].ToString();
                    txtscdescr2.Text = dtscdata.Rows[0]["descr2"].ToString();
                    txtscdescr3.Text = dtscdata.Rows[0]["descr3"].ToString();
                    txtscqty.Text = dtscdata.Rows[0]["qty"].ToString();
                    txtscdate.Text = (Convert.ToDateTime(dtscdata.Rows[0]["challandate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)).ToString("dd-MM-yyyy");
                    txtscbalanceqty.Text = dtscdata.Rows[0]["balqty"].ToString();
                    hdnsc.Value = dtscdata.Rows[0]["adjqty"].ToString();

                    //Fill Opening Data From Item Master for Purchase
                    DataTable dt = (DataTable)Session["dtpitems"];
                    DataTable dtop = new DataTable();
                    dtop = fssclass.selectalldataopdatafromitemname(li);
                    for (int r = 0; r < dtop.Rows.Count; r++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["pcno"] = "0";
                        dr["id"] = Convert.ToInt64(dtop.Rows[r]["id"].ToString());
                        dr["acname"] = "Opening Balance";
                        dr["pcdate"] = Convert.ToDateTime(dtop.Rows[r]["opdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr["adjqty"] = Convert.ToDouble(dtop.Rows[r]["adjqty"].ToString());
                        dr["stockqty"] = Convert.ToDouble(dtop.Rows[r]["qty"].ToString());
                        dr["balqty"] = "0";
                        dr["atype"] = "OP";
                        dt.Rows.Add(dr);
                    }
                    Session["dtpitems"] = dt;

                    DataTable dtpc = new DataTable();
                    dtpc = fssclass.selectallpcdatafromitemname(li);
                    if (dt.Rows.Count > 0)
                    {
                        dtpc.Merge(dt);
                    }
                    DataTable dtstockjvin = new DataTable();
                    dtstockjvin = fssclass.selectallstockjvindatafromitemname(li);
                    dtpc.Merge(dtstockjvin);

                    DataView dtdv = dtpc.DefaultView;
                    dtdv.Sort = "pcdate asc";
                    DataTable dtf = new DataTable();
                    dtf = dtdv.ToTable();

                    //if (dtf.Rows.Count > 0)
                    //{
                    //    lblemptypc.Visible = false;
                    //    gvpc.Visible = true;
                    //    gvpc.DataSource = dtf;
                    //    gvpc.DataBind();
                    //}
                    //else 
                    if (dtf.Rows.Count > 0)
                    {
                        lblemptypc.Visible = false;
                        gvpc.Visible = true;
                        gvpc.DataSource = dtf;
                        gvpc.DataBind();
                    }
                    else
                    {
                        gvpc.Visible = false;
                        lblemptypc.Visible = true;
                        lblemptypc.Text = "No Purchase Challan Data Found.";
                    }
                    TabPanel1.Enabled = false;
                    TabPanel2.Enabled = true;
                    TabPanel3.Enabled = false;
                }
            }
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("pcno", typeof(Int64));
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("pcdate", typeof(DateTime));
        dtpitems.Columns.Add("acname", typeof(string));
        dtpitems.Columns.Add("adjqty", typeof(double));
        dtpitems.Columns.Add("stockqty", typeof(double));
        dtpitems.Columns.Add("balqty", typeof(double));
        dtpitems.Columns.Add("atype", typeof(string));
        return dtpitems;
    }

    protected void gvpc_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            li.itemname = drpitemname.SelectedItem.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pcid = Convert.ToInt64(e.CommandArgument);
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblpcno = (Label)currentRow.FindControl("lblstrscno");
            Label lblatype = (Label)currentRow.FindControl("lblatype");
            li.pcno = Convert.ToInt64(lblpcno.Text);
            hdnpcid.Value = li.pcid.ToString();
            li.type = lblatype.Text;
            hdntype2.Value = li.type;
            if (li.type == "PCHN")
            {
                DataTable dtpcdata = new DataTable();
                dtpcdata = fssclass.selectalldatafromstrpcno(li);
                if (dtpcdata.Rows.Count > 0)
                {
                    txtpcno.Text = dtpcdata.Rows[0]["pcno"].ToString();
                    txtpcdescr1.Text = dtpcdata.Rows[0]["descr1"].ToString();
                    txtpcdescr2.Text = dtpcdata.Rows[0]["descr2"].ToString();
                    txtpcdescr3.Text = dtpcdata.Rows[0]["descr3"].ToString();
                    txtpcqty.Text = dtpcdata.Rows[0]["balqty"].ToString();
                    if (txtscbalanceqty.Text.Trim() == string.Empty)
                    {
                        txtscbalanceqty.Text = "0";
                    }
                    if (Convert.ToDouble(dtpcdata.Rows[0]["balqty"].ToString()) < Convert.ToDouble(txtscbalanceqty.Text))
                    {
                        txtpcadjqty.Text = dtpcdata.Rows[0]["balqty"].ToString();
                    }
                    else
                    {
                        txtpcadjqty.Text = txtscbalanceqty.Text;
                    }
                    hdnpc.Value = dtpcdata.Rows[0]["adjqty"].ToString();
                    txtpcdate.Text = (Convert.ToDateTime(dtpcdata.Rows[0]["pcdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)).ToString("dd-MM-yyyy");
                    TabPanel1.Enabled = false;
                    TabPanel2.Enabled = false;
                    TabPanel3.Enabled = true;
                }
            }
            else if (li.type == "OP")
            {
                DataTable dtpcdata = new DataTable();
                dtpcdata = fssclass.selectalldatafromstrpcnofromitemmaster(li);
                if (dtpcdata.Rows.Count > 0)
                {
                    txtpcno.Text = "0";
                    txtpcdescr1.Text = "";
                    txtpcdescr2.Text = "";
                    txtpcdescr3.Text = "";
                    txtpcqty.Text = dtpcdata.Rows[0]["balqty"].ToString();
                    if (txtscbalanceqty.Text.Trim() == string.Empty)
                    {
                        txtscbalanceqty.Text = "0";
                    }
                    if (Convert.ToDouble(dtpcdata.Rows[0]["balqty"].ToString()) < Convert.ToDouble(txtscbalanceqty.Text))
                    {
                        txtpcadjqty.Text = dtpcdata.Rows[0]["balqty"].ToString();
                    }
                    else
                    {
                        txtpcadjqty.Text = txtscbalanceqty.Text;
                    }
                    hdnpc.Value = dtpcdata.Rows[0]["adjqty"].ToString();
                    txtpcdate.Text = (Convert.ToDateTime(System.DateTime.Now.ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)).ToString("dd-MM-yyyy");
                    TabPanel1.Enabled = false;
                    TabPanel2.Enabled = false;
                    TabPanel3.Enabled = true;
                }
            }
            else
            {
                DataTable dtpcdata = new DataTable();
                dtpcdata = fssclass.selectalldatafromchallannojvin(li);
                if (dtpcdata.Rows.Count > 0)
                {
                    txtpcno.Text = "0";
                    txtpcdescr1.Text = "";
                    txtpcdescr2.Text = "";
                    txtpcdescr3.Text = "";
                    txtpcqty.Text = dtpcdata.Rows[0]["balqty"].ToString();
                    if (txtscbalanceqty.Text.Trim() == string.Empty)
                    {
                        txtscbalanceqty.Text = "0";
                    }
                    if (Convert.ToDouble(dtpcdata.Rows[0]["balqty"].ToString()) < Convert.ToDouble(txtscbalanceqty.Text))
                    {
                        txtpcadjqty.Text = dtpcdata.Rows[0]["balqty"].ToString();
                    }
                    else
                    {
                        txtpcadjqty.Text = txtscbalanceqty.Text;
                    }
                    hdnpc.Value = dtpcdata.Rows[0]["adjqty"].ToString();
                    txtpcdate.Text = (Convert.ToDateTime(System.DateTime.Now.ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)).ToString("dd-MM-yyyy");
                    TabPanel1.Enabled = false;
                    TabPanel2.Enabled = false;
                    TabPanel3.Enabled = true;
                }
            }
        }
    }

    protected void btnok_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        li.fromdated = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        if (txtpcadjqty.Text != string.Empty)
        {
            if (Convert.ToDouble(txtpcadjqty.Text) > Convert.ToDouble(txtpcqty.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter quantity equal or less than purchase Challan quantity and try again.');", true);
                return;
            }
            if (Convert.ToDouble(txtpcadjqty.Text) > Convert.ToDouble(txtscbalanceqty.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter quantity equal or less than Sales Challan quantity and try again.');", true);
                return;
            }
            li.itemname = drpitemname.SelectedItem.Text;
            li.qtysc = Convert.ToDouble(hdnsc.Value) + Convert.ToDouble(txtpcadjqty.Text);
            li.qtypc = Convert.ToDouble(hdnpc.Value) + Convert.ToDouble(txtpcadjqty.Text);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.pcno = Convert.ToInt64(txtpcno.Text);
            li.strscno = txtscno.Text;
            li.scid = Convert.ToInt64(hdnscid.Value);
            li.pcid = Convert.ToInt64(hdnpcid.Value);
            li.type1 = hdntype2.Value;
            if (li.type1 == "PCHN")
            {
                fssclass.updateadjqtyinpcitems(li);
            }
            else if (li.type1 == "OP")
            {
                fssclass.updateadjqtyinitemmaster1(li);
            }
            else
            {
                fssclass.updateadjqtyinsjvitems(li);
            }
            li.type = hdntype1.Value;
            if (li.type == "SCHN")
            {
                fssclass.updateadjqtyinscitems(li);
            }
            else
            {
                fssclass.updateadjqtyinsjvitems1(li);
            }
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = "SCNO - " + li.strscno + " PCNO - " + li.pcno + " Stock Data Adjusted.";
            faclass.insertactivity(li);
            if (drpitemname.SelectedItem.Text != "--SELECT--")
            {
                li.itemname = drpitemname.SelectedItem.Text;
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtsc = new DataTable();
                dtsc = fssclass.selectallscdatafromitemname(li);
                DataTable dtstockjvout = new DataTable();
                dtstockjvout = fssclass.selectallstockjvotdatafromitemname(li);
                dtsc.Merge(dtstockjvout);
                DataView dtdv = dtsc.DefaultView;
                dtdv.Sort = "scdate asc";
                DataTable dtf = new DataTable();
                dtf = dtdv.ToTable();
                if (dtf.Rows.Count > 0)
                {
                    lblemptysc.Visible = false;
                    gvsc.Visible = true;
                    gvsc.DataSource = dtf;
                    gvsc.DataBind();
                }
                else
                {
                    gvsc.Visible = false;
                    lblemptysc.Visible = true;
                    lblemptysc.Text = "No Sales Challan Data Found.";
                }
                TabPanel1.Enabled = true;
                TabPanel2.Enabled = false;
                TabPanel3.Enabled = false;
            }
            else
            {
                lblemptysc.Visible = false;
                gvsc.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select Item Name.');", true);
                return;
            }
            //if (Convert.ToDouble(txtpcadjqty.Text) == Convert.ToDouble(txtscbalanceqty.Text))
            //{ 

            //}
            //else if (Convert.ToDouble(txtpcadjqty.Text) == Convert.ToDouble(txtpcqty.Text))
            //{ }
            //else if (Convert.ToDouble(txtpcadjqty.Text) < Convert.ToDouble(txtscbalanceqty.Text))
            //{ }
            //else if (Convert.ToDouble(txtpcadjqty.Text) < Convert.ToDouble(txtpcqty.Text))
            //{ }
            //Response.Redirect("~/StockAdjustment.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Adj Qty.');", true);
            return;
        }
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/StockAdjustment.aspx");
    }


    protected void btnrefresh_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        //if (drpitemname.SelectedItem.Text != "--SELECT--")
        //{
        //    li.itemname = drpitemname.SelectedItem.Text;
        //    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        //    fssclass.updateadjqtyinscitemsforrefresh(li);
        //    fssclass.updateadjqtyinpcitemsforrefresh(li);
        //    fssclass.updateadjqtyinsjvitemsforrefresh(li);
        //    fssclass.updateadjqtyinitemmasteropforrefresh(li);
        //    TabPanel1.Enabled = true;
        //    TabPanel2.Enabled = false;
        //    TabPanel3.Enabled = false;
        //    lblemptysc.Visible = false;
        //    gvsc.Visible = false;
        //    txtscno.Text = string.Empty;
        //    txtscdescr1.Text = string.Empty;
        //    txtscdescr2.Text = string.Empty;
        //    txtscdescr3.Text = string.Empty;
        //    txtscqty.Text = string.Empty;
        //    txtscdate.Text = string.Empty;
        //    txtscbalanceqty.Text = string.Empty;
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('All Stock adjustment Data has been refreshed for this item.');", true);
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select Item Name.');", true);
        //    return;
        //}
    }

    protected void btnyes_Click(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != "--SELECT--")
        {
            li.itemname = drpitemname.SelectedItem.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            fssclass.updateadjqtyinscitemsforrefresh(li);
            fssclass.updateadjqtyinpcitemsforrefresh(li);
            fssclass.updateadjqtyinsjvitemsforrefresh(li);
            fssclass.updateadjqtyinitemmasteropforrefresh(li);
            TabPanel1.Enabled = true;
            TabPanel2.Enabled = false;
            TabPanel3.Enabled = false;
            lblemptysc.Visible = false;
            gvsc.Visible = false;
            txtscno.Text = string.Empty;
            txtscdescr1.Text = string.Empty;
            txtscdescr2.Text = string.Empty;
            txtscdescr3.Text = string.Empty;
            txtscqty.Text = string.Empty;
            txtscdate.Text = string.Empty;
            txtscbalanceqty.Text = string.Empty;
            ModalPopupExtender2.Hide();
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('All Stock adjustment Data has been refreshed for this item.');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select Item Name.');", true);
            return;
        }
    }

}