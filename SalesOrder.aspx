﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SalesOrder.aspx.cs" Inherits="SalesOrder" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div class="container">
        <span style="color: white; background-color: Red">Sales Order</span><br />
        <div class="row">
            <div class="col-md-1">
                <asp:Button ID="btnfirst" runat="server" Text="First" OnClick="btnfirst_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnprevious" runat="server" Text="Previous" OnClick="btnprevious_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnnext" runat="server" Text="Next" OnClick="btnnext_Click" /></div>
            <div class="col-md-1">
                <asp:Button ID="btnlast" runat="server" Text="Last" OnClick="btnlast_Click" /></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            SO No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtsalesorderno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtsodate" runat="server" CssClass="form-control" Width="120px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Party PO No<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtpartypono" runat="server" CssClass="form-control" Width="120px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            PO Date<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtpodate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Client Code<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" AutoPostBack="true"
                            OnTextChanged="txtname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtclientcode"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetClientname">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Indent No<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtindentno" runat="server" CssClass="form-control" Width="120px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Indent Date<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtindentdate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-1">
                                <label class="control-label">
                                    Name<span style="color: Red;">*</span></label>
                            </div>
                            <div class="col-md-5">
                                <%--<asp:TextBox ID="txtname" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txtname"
                                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                    ServiceMethod="GetAccountname">
                                </asp:AutoCompleteExtender>--%>
                                <asp:DropDownList ID="drpacname" runat="server" CssClass="form-control" AutoPostBack="True"
                                    OnSelectedIndexChanged="drpacname_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-1">
                                <label class="control-label">
                                    Dispatch Through<span style="color: Red;">*</span></label>
                            </div>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtdispatchthrough" runat="server" CssClass="form-control" Width="120px"
                                    onkeypress="return checkQuote();"></asp:TextBox></div>
                            <div class="col-md-1">
                                <label class="control-label">
                                    User</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="100px" ReadOnly="true"></asp:TextBox></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Delivery At<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtdeliveryat" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <asp:Label ID="lblcount" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="22px"></asp:Label></div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Bifurcation No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="drpbifurcationno" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpbifurcationno_SelectedIndexChanged"
                            CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtdeliveryat2" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <%-- <div class="col-md-1">
                        <label class="control-label">
                            ITEM DESC
                        </label>
                    </div>
                    <div class="col-md-1" style="width: 3.9% !important">
                        <label class="control-label">
                            UOI
                        </label>
                    </div>
                    <div class="col-md-1" style="width: 4.7% !important">
                        <label class="control-label">
                            QTY
                        </label>
                    </div>
                    <div class="col-md-1" style="width: 6.9% !important">
                        <label class="control-label">
                            DELI.QTY
                        </label>
                    </div>
                    <div class="col-md-1" style="width: 9.1% !important">
                        <label class="control-label">
                            DELI.DATE
                        </label>
                    </div>
                    <div class="col-md-1" style="width: 8% !important">
                        <label class="control-label">
                            AMOUNT
                        </label>
                    </div>--%>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtdeliveryat3" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <%--   <div class="col-md-2" style="width: 15% !important">
                                <asp:TextBox ID="txtitemdescription" runat="server" CssClass="form-control" Width="200px"
                                    placeholder="ITEM DESCRIPTION"></asp:TextBox>
                            </div>
                            <div class="col-md-1" style="width: 4% !important">
                                <asp:TextBox ID="TextBox10" runat="server" CssClass="form-control" Width="50px" placeholder="UOM"></asp:TextBox>
                            </div>
                            <div class="col-md-1" style="width: 4.7% !important">
                                <asp:TextBox ID="TextBox11" runat="server" CssClass="form-control" Width="60px" placeholder="QTY"></asp:TextBox>
                            </div>
                            <div class="col-md-1" style="width: 6.9% !important">
                                <asp:TextBox ID="TextBox12" runat="server" CssClass="form-control" Width="90px" placeholder="DELI QTY"></asp:TextBox>
                            </div>
                            <div class="col-md-1" style="width: 9.1% !important">
                                <asp:TextBox ID="TextBox13" runat="server" CssClass="form-control" Width="120px"
                                    placeholder="DELI DATE"></asp:TextBox>
                            </div>
                            <div class="col-md-1" style="width: 8% !important">
                                <asp:TextBox ID="TextBox14" runat="server" CssClass="form-control" Width="120px"
                                    placeholder="AMOUNT"></asp:TextBox>
                            </div>--%>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Item Name</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Qty</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Unit</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Rate</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Basic Amount</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Tax Type
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    VAT/SGST %
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Ad.Vat/CGST%
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    CST/IGST %
                </label>
            </div>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    CST/IGST Amt</label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 1</label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 2</label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 3</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    VAT/SGST</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Add.Vat/CGST</label>
            </div>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    Amount</label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2" style="border-right: 1px solid;">
                <%--<asp:TextBox ID="txtitemname" runat="server" CssClass="form-control" placeholder="Item Name"
                    Width="200px" AutoPostBack="True" OnTextChanged="txtitemname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtitemname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getitemname">
                </asp:AutoCompleteExtender>--%>
                <asp:DropDownList ID="drpitemname" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnSelectedIndexChanged="drpitemname_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" placeholder="Qty"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtqty_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" placeholder="Unit"
                    Width="90px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtrate" runat="server" CssClass="form-control" placeholder="Rate"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtrate_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtbasicamt" runat="server" CssClass="form-control" placeholder="Basic Amt."
                    Width="90px" AutoPostBack="True" OnTextChanged="txtbasicamt_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txttaxtype" runat="server" CssClass="form-control" placeholder="Tax Type"
                    Width="90px" AutoPostBack="True" OnTextChanged="txttaxtype_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txttaxtype"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getvattype">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvvat" runat="server" CssClass="form-control" placeholder="VAT%"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtgvvat_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvadvat" runat="server" CssClass="form-control" placeholder="Ad.Vat%"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtgvadvat_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvcst" runat="server" CssClass="form-control" placeholder="CST%"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtgvcst_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtcstamount" runat="server" CssClass="form-control" placeholder="CST"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtcstamount_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription1" runat="server" CssClass="form-control" placeholder="Description 1"
                    Width="200px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription2" runat="server" CssClass="form-control" placeholder="Description 2"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription3" runat="server" CssClass="form-control" placeholder="Description 3"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtvatamt" runat="server" CssClass="form-control" placeholder="VAT"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtvatamt_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtaddtaxamt" runat="server" CssClass="form-control" placeholder="Add.Vat"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtaddtaxamt_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvamount" runat="server" CssClass="form-control" placeholder="Amount"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtgvamount_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:DropDownList ID="drpordnonord" runat="server" CssClass="form-control">
                    <asp:ListItem>Ordered</asp:ListItem>
                    <asp:ListItem>Returnable</asp:ListItem>
                    <asp:ListItem>Non Ordered</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnadd" runat="server" Text="Add" Height="30px" BackColor="#F05283"
                    ValidationGroup="valgvsalesorder" OnClick="btnadd_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="valgvsalesorder" />
            </div>
        </div>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server" />
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvsoitemlist" runat="server" AutoGenerateColumns="False" Width="1300px"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowDeleting="gvsoitemlist_RowDeleting" OnRowCommand="gvsoitemlist_RowCommand">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtgridqty" runat="server" Text='<%# bind("qty") %>' Width="80px"
                                        AutoPostBack="True" OnTextChanged="txtgridqty_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvunit" runat="server" Width="50px" Text='<%# bind("unit") %>'></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender222" runat="server" TargetControlID="txtgvunit"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="Getunit">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'></asp:Label><asp:TextBox
                                        ID="txtgvrate" runat="server" Text='<%# bind("rate") %>' Width="100px" AutoPostBack="True"
                                        OnTextChanged="txtgvrate_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="basic Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbasicamt" ForeColor="Black" runat="server" Text='<%# bind("basicamount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT Type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvattype" ForeColor="Black" runat="server" Text='<%# bind("taxtype") %>'></asp:Label><asp:TextBox
                                        ID="txtgridtaxtype" runat="server" Text='<%# bind("taxtype") %>' Width="120px"
                                        AutoPostBack="True" OnTextChanged="txtgridtaxtype_TextChanged"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtgridtaxtype"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="Getvattype">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvatp" ForeColor="Black" runat="server" Text='<%# bind("vatp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add VAT%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladdvatp" ForeColor="Black" runat="server" Text='<%# bind("addtaxp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CST%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcstp" ForeColor="Black" runat="server" Text='<%# bind("cstp") %>'></asp:Label><asp:TextBox
                                        ID="txtgridcstp" runat="server" Text='<%# bind("cstp") %>' Width="120px" AutoPostBack="True"
                                        OnTextChanged="txtgridcstp_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CST Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcstamt" ForeColor="Black" runat="server" Text='<%# bind("cstamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvatamt" ForeColor="Black" runat="server" Text='<%# bind("vatamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add VAT Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladdvatamt" ForeColor="Black" runat="server" Text='<%# bind("addtaxamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc1" ForeColor="Black" runat="server" Text='<%# bind("descr1") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtgvdesc1" runat="server" Text='<%# bind("descr1") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc2" ForeColor="Black" runat="server" Text='<%# bind("descr2") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtgvdesc2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc3" ForeColor="Black" runat="server" Text='<%# bind("descr3") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtgvdesc3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OType" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblotype" ForeColor="Black" runat="server" Text='<%# bind("otype") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">
                                    Basic</label>
                            </div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" Width="150px"
                                    AutoPostBack="True" OnTextChanged="txtamount_TextChanged" Text="0" ReadOnly="true"
                                    onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">
                                    CST</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtcst" runat="server" CssClass="form-control" Width="150px" AutoPostBack="True"
                                    OnTextChanged="txtcst_TextChanged" Text="0" ReadOnly="true" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">
                                    VAT</label>
                            </div>
                            <div class="col-md-4">
                                <asp:TextBox ID="txtvat" runat="server" CssClass="form-control" Width="150px" AutoPostBack="True"
                                    OnTextChanged="txtvat_TextChanged" Text="0" ReadOnly="true" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label">
                                    Total S.O.</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txttotalso" runat="server" CssClass="form-control" Width="150px"
                                    Text="0" onkeypress="return checkQuote();" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">
                                    AD. VAT</label>
                            </div>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtadvat" runat="server" CssClass="form-control" Width="150px" Text="0"
                                    onkeypress="return checkQuote();" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label">
                                    Status<span style="color: Red;">*</span></label>
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList ID="drpstatus" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5" style="border-left: 2px solid black">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Instructions</label>
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtinstructions1" runat="server" CssClass="form-control" Width="200px"
                                    placeholder="Transportation" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtinstructions2" runat="server" CssClass="form-control" Width="200px"
                                    placeholder="Insurance" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                </label>
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtinstructions3" runat="server" CssClass="form-control" Width="200px"
                                    placeholder="Payment" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtinstructions4" runat="server" CssClass="form-control" Width="200px"
                                    placeholder="Tax" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                </label>
                            </div>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtinstructions5" runat="server" CssClass="form-control" Width="200px"
                                    placeholder="Guarantee" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtinstructions6" runat="server" CssClass="form-control" Width="200px"
                                    placeholder="Modvate" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                </label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtinstructions7" runat="server" CssClass="form-control" Width="200px"
                                    placeholder="Delivery At" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <asp:Button ID="btnfinalsave" runat="server" Text="Save" class="btn btn-default forbutton"
                OnClick="btnfinalsave_Click" ValidationGroup="valsalesorder" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                ShowSummary="false" ValidationGroup="valsalesorder" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter so date."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtsodate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter party pono."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtpartypono"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter po date."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtpodate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter client code."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtclientcode"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter indent no."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtindentno"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please enter indent date."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtindentdate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter name."
                Text="*" ValidationGroup="valsalesorder" InitialValue="--SELECT--" ControlToValidate="drpacname"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please select dispatch through."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtdispatchthrough"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please enter delivery at."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtdeliveryat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="Please enter amount."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtamount"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Please enter cst."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtcst"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="Please enter vat."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="Please enter total so."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txttotalso"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="Please enter add vat."
                Text="*" ValidationGroup="valsalesorder" ControlToValidate="txtadvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Please select status."
                Text="*" ValidationGroup="valsalesorder" InitialValue="--SELECT--" ControlToValidate="drpstatus"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please select item name."
                Text="*" ValidationGroup="valgvsalesorder" InitialValue="--SELECT--" ControlToValidate="drpitemname"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please enter qty."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please enter unit."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtunit"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please enter rate."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtrate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Please enter basic amount."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtbasicamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Please enter tax type."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txttaxtype"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Please enter vat%."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtgvvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Please enter ad.vat%."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtgvadvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Please enter cst%."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtgvcst"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Please enter cst."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtcstamount"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Please enter description1."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtdescription1"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="Please enter description2."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtdescription2"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Please enter description3."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtdescription3"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="Please enter vat."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtvatamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Please enter add.vat."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtaddtaxamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="Please enter amount."
                Text="*" ValidationGroup="valgvsalesorder" ControlToValidate="txtgvamount"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="So Date must be dd-MM-yyyy" ValidationGroup="valsalesorder" ForeColor="Red"
                ControlToValidate="txtsodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Po Date must be dd-MM-yyyy" ValidationGroup="valsalesorder" ForeColor="Red"
                ControlToValidate="txtpodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Indent Date must be dd-MM-yyyy" ValidationGroup="valsalesorder"
                ForeColor="Red" ControlToValidate="txtindentdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
        </div>
    </div>
    <%--</ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="txtitemname" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtclientcode" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txttaxtype" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnadd" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="gvsoitemlist" EventName="RowDeleting" />
            <asp:AsyncPostBackTrigger ControlID="txtqty" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtrate" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtgvvat" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtgvadvat" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtgvcst" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtcstamount" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtvatamt" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtaddtaxamt" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtgvamount" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtamount" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtcst" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtvat" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtadvat" EventName="TextChanged" />
        </Triggers>
    </asp:UpdatePanel>--%>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtsodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtpodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtindentdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });



        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtsodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtsodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtpodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtpodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtindentdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtindentdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
