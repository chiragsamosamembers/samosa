﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CompanyMaster : System.Web.UI.Page
{
    ForCompanyMaster fcmclass = new ForCompanyMaster();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "update")
            {
                filleditdata();
            }
            else
            {
                txtcno.Text = "Auto";
                txtcno.ReadOnly = true;
            }
        }
    }

    public void filleditdata()
    {
        li.cno = Convert.ToInt64(Request["cno"].ToString());
        DataTable dtdata = new DataTable();
        dtdata = fcmclass.selectcompanydatafromcno(li);
        if (dtdata.Rows.Count > 0)
        {
            txtcno.Text = dtdata.Rows[0]["cno"].ToString();
            txtcompanyshrotname.Text = dtdata.Rows[0]["csnm"].ToString();
            txtcompanyname.Text = dtdata.Rows[0]["cname"].ToString();
            txtaddress.Text = dtdata.Rows[0]["address"].ToString();
            txtlocation.Text = dtdata.Rows[0]["location"].ToString();
            txtemailid.Text = dtdata.Rows[0]["emailid"].ToString();
            txtcontactno1.Text = dtdata.Rows[0]["contact1"].ToString();
            txtcontactno2.Text = dtdata.Rows[0]["contact2"].ToString();
            txtfaxno.Text = dtdata.Rows[0]["faxno"].ToString();
            txttanno.Text = dtdata.Rows[0]["tanno"].ToString();
            txtvatno.Text = dtdata.Rows[0]["vatno"].ToString();
            txtcsttin.Text = dtdata.Rows[0]["cstno"].ToString();
            txteccno.Text = dtdata.Rows[0]["eccno"].ToString();
            txtservicetaxno.Text = dtdata.Rows[0]["servicetaxno"].ToString();
            txtrocno.Text = dtdata.Rows[0]["rocno"].ToString();
            txtimportexportcode.Text = dtdata.Rows[0]["imexcode"].ToString();
            txtyearofestablishment.Text = dtdata.Rows[0]["yearoe"].ToString();
            txtbankaccountnumber1.Text = dtdata.Rows[0]["bank1"].ToString();
            txtbankaccountnumber2.Text = dtdata.Rows[0]["bank2"].ToString();
            txtcompanypt.Text = dtdata.Rows[0]["comppt"].ToString();
            txtemployeept.Text = dtdata.Rows[0]["emppt"].ToString();
            txtpfno.Text = dtdata.Rows[0]["pfno"].ToString();
            txtpanno.Text = dtdata.Rows[0]["panno"].ToString();
            txtgstno.Text = dtdata.Rows[0]["gstno"].ToString();
            btnsaves.Text = "Update";
        }
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        li.csnm = txtcompanyshrotname.Text;
        li.cname = txtcompanyname.Text;
        li.address = txtaddress.Text;
        li.location = txtlocation.Text;
        li.emailid = txtemailid.Text;
        li.contact1 = txtcontactno1.Text;
        li.contact2 = txtcontactno2.Text;
        li.faxno = txtfaxno.Text;
        li.tanno = txttanno.Text;
        li.vatno = txtvatno.Text;
        li.cstno = txtcsttin.Text;
        li.eccno = txteccno.Text;
        li.servicetaxno = txtservicetaxno.Text;
        li.rocno = txtrocno.Text;
        li.imexcode = txtimportexportcode.Text;
        li.yearoe = txtyearofestablishment.Text;
        li.bank1 = txtbankaccountnumber1.Text;
        li.bank2 = txtbankaccountnumber2.Text;
        li.emppt = txtemployeept.Text;
        li.comppt = txtcompanypt.Text;
        li.pfno = txtpfno.Text;
        li.panno = txtpanno.Text;
        li.gstno = txtgstno.Text;
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        if (btnsaves.Text == "Save")
        {
            fcmclass.insertcompanymasterdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.activity = li.cname + " New Company Inserted.";
            faclass.insertactivity(li);
        }
        else
        {
            li.cno = Convert.ToInt64(txtcno.Text);
            fcmclass.updatecompanymasterdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.activity = txtcno.Text + " Company Data updated.";
            faclass.insertactivity(li);
        }
        Response.Redirect("~/CompanyList.aspx?pagename=CompanyList");
    }
}