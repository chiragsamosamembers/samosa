﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Survey.Classes;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using System.Data;

public partial class RepCover : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    //[System.Web.Script.Services.ScriptMethod()]
    //[System.Web.Services.WebMethod]
    //public static List<string> GetAccountname(string prefixText)
    //{
    //    ForBankReceipt fbrclass = new ForBankReceipt();
    //    LogicLayer li = new LogicLayer();
    //    li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
    //    li.name = prefixText;
    //    DataTable dt = new DataTable();
    //    dt = fbrclass.selectallbankname(li);
    //    List<string> CountryNames = new List<string>();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        CountryNames.Add(dt.Rows[i][2].ToString());
    //    }
    //    return CountryNames;
    //}

    //[System.Web.Script.Services.ScriptMethod()]
    //[System.Web.Services.WebMethod]
    //public static List<string> Getvono(string prefixText)
    //{
    //    ForBankReceipt fbrclass = new ForBankReceipt();
    //    LogicLayer li = new LogicLayer();
    //    li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
    //    li.name = prefixText;
    //    li.acname = SessionMgt.acname;
    //    DataTable dt = new DataTable();
    //    dt = fbrclass.selectallvnofromjv1(li);
    //    List<string> CountryNames = new List<string>();
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        CountryNames.Add(dt.Rows[i][0].ToString());
    //    }
    //    return CountryNames;
    //}
    protected void btnsaves_Click(object sender, EventArgs e)
    {
        DataSet1 imageDataSet = new DataSet1();
        ReportDocument rptdoc = new ReportDocument();
        li.voucherno = SessionMgt.voucherno;
        string ftow = "";
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        SqlDataAdapter da11 = new SqlDataAdapter("select * from ACMaster where acname='" + txtacname.Text + "'", con);
        DataTable dtc = new DataTable();
        da11.Fill(imageDataSet.Tables["DataTable24"]);
        string rptname;
        rptname = Server.MapPath(@"~/Reports/cover.rpt");
        rptdoc.Load(rptname);
        if (txtname.Text.Trim() != string.Empty && txtname.Text.Trim() != "")
        {
            rptdoc.DataDefinition.FormulaFields["name"].Text = "'Kind Attn :- " + txtname.Text + "'";
        }
        rptdoc.DataDefinition.FormulaFields["no"].Text = "'" + txtcontactno.Text + "'";
        rptdoc.SetDataSource(imageDataSet.Tables["DataTable24"]);
        //CrystalReportViewer1.ReportSource = rptdoc;
        //CrystalReportViewer1.DataBind();
        ExportOptions exprtopt = default(ExportOptions);
        string fname = "cover.pdf";
        //create instance for destination option - This one is used to set path of your pdf file save 
        DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
        destiopt.DiskFileName = Server.MapPath(@"~/cheque/" + fname);


        exprtopt = rptdoc.ExportOptions;
        exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

        //use PortableDocFormat for PDF data
        exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
        exprtopt.DestinationOptions = destiopt;

        //finally export your report document
        rptdoc.Export();
        rptdoc.Close();
        rptdoc.Clone();
        rptdoc.Dispose();
        string path = fname;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('CoverPrinting.aspx?&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);

    }

    public String changeToWords(String numb)
    {
        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
        String endStr = ("Only.");
        try
        {
            int decimalPlace = numb.IndexOf(".");
            if (decimalPlace > 0)
            {
                wholeNo = numb.Substring(0, decimalPlace);
                points = numb.Substring(decimalPlace + 1);
                if (Convert.ToInt32(points) > 0)
                {
                    //int p = points.Length;
                    //char[] TPot = points.ToCharArray();
                    andStr = (" and ");// just to separate whole numbers from points/Rupees                  
                    //for(int i=0;i<p;i++)
                    //{
                    //    andStr += ones(Convert.ToString(TPot[i]))+" ";
                    //}
                    andStr += translateWholeNumber(points).Trim() + " Paisa";

                }
            }
            val = String.Format("{0} {1}{2} {3}", translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
        }
        catch
        {
            ;
        }
        return val;
    }

    private String translateWholeNumber(String number)
    {
        string word = "";
        try
        {
            bool beginsZero = false;//tests for 0XX
            bool isDone = false;//test if already translated
            double dblAmt = (Convert.ToDouble(number));
            //if ((dblAmt > 0) && number.StartsWith("0"))

            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric
                beginsZero = number.StartsWith("0");
                int numDigits = number.Length;
                int pos = 0;//store digit grouping
                String place = "";//digit grouping name:hundres,thousand,etc...
                switch (numDigits)
                {
                    case 1://ones' range
                        word = ones(number);
                        isDone = true;
                        break;
                    case 2://tens' range
                        word = tens(number);
                        isDone = true;
                        break;
                    case 3://hundreds' range
                        pos = (numDigits % 3) + 1;
                        place = " Hundred ";
                        break;
                    case 4://thousands' range
                    case 5:
                        pos = (numDigits % 4) + 1;
                        place = " Thousand ";
                        break;
                    case 6:

                    case 7://millions' range
                        pos = (numDigits % 6) + 1;
                        // place = " Million ";
                        place = " Lakh ";
                        break;
                    case 8:
                    case 9:

                    case 10://Billions's range
                        pos = (numDigits % 8) + 1;
                        place = " Core ";
                        break;
                    //add extra case options for anything above Billion...
                    default:
                        isDone = true;
                        break;
                }
                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)
                    if (beginsZero) place = "";
                    word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));
                    //check for trailing zeros
                    if (beginsZero) word = " and " + word.Trim();
                }
                //ignore digit grouping names
                if (word.Trim().Equals(place.Trim())) word = "";
            }
        }
        catch
        {
            ;
        }
        return word.Trim();
    }

    private String tens(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = null;
        switch (digt)
        {
            case 10:
                name = "Ten";
                break;
            case 11:
                name = "Eleven";
                break;
            case 12:
                name = "Twelve";
                break;
            case 13:
                name = "Thirteen";
                break;
            case 14:
                name = "Fourteen";
                break;
            case 15:
                name = "Fifteen";
                break;
            case 16:
                name = "Sixteen";
                break;
            case 17:
                name = "Seventeen";
                break;
            case 18:
                name = "Eighteen";
                break;
            case 19:
                name = "Nineteen";
                break;
            case 20:
                name = "Twenty";
                break;
            case 30:
                name = "Thirty";
                break;
            case 40:
                name = "Fourty";
                break;
            case 50:
                name = "Fifty";
                break;
            case 60:
                name = "Sixty";
                break;
            case 70:
                name = "Seventy";
                break;
            case 80:
                name = "Eighty";
                break;
            case 90:
                name = "Ninety";
                break;
            default:
                if (digt > 0)
                {
                    name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));
                }
                break;
        }
        return name;
    }

    private String ones(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = "";
        switch (digt)
        {
            case 1:
                name = "One";
                break;
            case 2:
                name = "Two";
                break;
            case 3:
                name = "Three";
                break;
            case 4:
                name = "Four";
                break;
            case 5:
                name = "Five";
                break;
            case 6:
                name = "Six";
                break;
            case 7:
                name = "Seven";
                break;
            case 8:
                name = "Eight";
                break;
            case 9:
                name = "Nine";
                break;
        }
        return name;
    }

}