﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CompanyMaster.aspx.cs" Inherits="CompanyMaster" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Company Master</span><br />
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            CNO</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtcno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Company Short Name</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtcompanyshrotname" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Company Name</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtcompanyname" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Address</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtaddress" runat="server" CssClass="form-control" Width="200px"
                            TextMode="MultiLine" Height="40px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Location</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtlocation" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Email ID</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtemailid" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Contact No 1</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtcontactno1" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Contact No 2</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtcontactno2" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Fax No.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtfaxno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            TAN No.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txttanno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            VAT NO.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtvatno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            CST TIN</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtcsttin" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Ecc No.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txteccno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Service Tax No.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtservicetaxno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Roc No.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtrocno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Import Export Code</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtimportexportcode" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Year Of Establishment</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtyearofestablishment" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Bank Account Number1</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtbankaccountnumber1" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Bank Account Number2</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtbankaccountnumber2" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Employee PT</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtemployeept" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Company PT</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtcompanypt" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            PF No.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtpfno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            PAN No.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtpanno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                </div>
                <div class="row" style="height: 10px">
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            GST No.</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtgstno" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row" style="height: 10px">
                </div>
                <div class="row">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                        <asp:Button ID="btnsaves" runat="server" Text="Save" class="btn btn-default forbutton"
                            ValidationGroup="val" OnClick="btnsaves_Click" />
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
