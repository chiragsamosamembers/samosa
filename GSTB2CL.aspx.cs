﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlTypes;

public partial class GSTB2CL : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();
    public ReportDocument rptdoc;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    //protected void btngstb2cl_Click(object sender, EventArgs e)
    //{
    //    if (txtfromdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
    //    {
    //        fillgridb2b();
    //        ExportGridToExcel();
    //    }
    //}

    //public void fillgridb2b()
    //{
    //    li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
    //    li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
    //    Session["dtpitems"] = CreateTemplate();
    //    DataTable dtq = new DataTable();
    //    string cz = Request.Cookies["Forcon"]["conc"];
    //    cz = cz.Replace(":", ";");
    //    SqlConnection con = new SqlConnection(cz);
    //    SqlDataAdapter da = new SqlDataAdapter("select SIItems.strsino,SIItems.sidate,SUM(SIItems.basicamount) as totbasic,(sum(SIItems.basicamount)+sum(SIItems.vatamt)+sum(SIItems.addtaxamt)+SUM((CASE WHEN (SIMaster.salesac != 'Sales A/c. IGST - Export') THEN SIItems.cstamt ELSE 0 END))) as totbillamt,SIItems.taxtype from SIItems inner join simaster on simaster.strsino=SIItems.strsino where simaster.sidate between @fromdate and @todate group by SIItems.taxtype,SIItems.strsino,SIItems.sidate,SIItems.sino order by SIItems.sino", con);
    //    da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
    //    da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
    //    DataTable dtdata = new DataTable();
    //    da.Fill(dtdata);
    //    if (dtdata.Rows.Count > 0)
    //    {
    //        for (int c = 0; c < dtdata.Rows.Count; c++)
    //        {
    //            dtq = (DataTable)Session["dtpitems"];
    //            DataRow dr1 = dtq.NewRow();
    //            dr1["strsino"] = dtdata.Rows[c]["strsino"].ToString();
    //            dr1["sidate"] = Convert.ToDateTime(dtdata.Rows[c]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MMM-yy");
    //            dr1["totbasicamount"] = dtdata.Rows[c]["totbasic"].ToString();

    //            var cc = dtdata.Rows[c]["taxtype"].ToString().IndexOf("-");
    //            if (cc != -1)
    //            {
    //                double vatp = Convert.ToDouble(dtdata.Rows[c]["taxtype"].ToString().Split('-')[0]);
    //                double addvatp = Convert.ToDouble(dtdata.Rows[c]["taxtype"].ToString().Split('-')[1]);
    //                dr1["taxdesc"] = (vatp + addvatp).ToString();
    //            }
    //            else
    //            {
    //                dr1["taxdesc"] = dtdata.Rows[c]["taxtype"].ToString();
    //            }

    //            li.strsino = dtdata.Rows[c]["strsino"].ToString();
    //            SqlDataAdapter dasi = new SqlDataAdapter("select * from simaster where strsino='" + li.strsino + "'", con);
    //            DataTable dtsi = new DataTable();
    //            dasi.Fill(dtsi);
    //            dr1["billamount"] = dtsi.Rows[0]["billamount"].ToString();
    //            SqlDataAdapter daa = new SqlDataAdapter("select * from SIMaster inner join ACMaster on SIMaster.acname=ACMaster.acname where SIMaster.strsino='" + li.strsino + "' and city not like '%'+@fullname+'%' and SIMaster.billamount>250000 and ACMaster.gstno!=''", con);
    //            daa.SelectCommand.Parameters.AddWithValue("@fullname", "gujarat");
    //            DataTable dta = new DataTable();
    //            daa.Fill(dta);
    //            if (dta.Rows.Count > 0)
    //            {
    //                dr1["gstno"] = dta.Rows[0]["gstno"].ToString();
    //                if (dta.Rows[0]["gstno"].ToString() != "")
    //                {
    //                    dr1["rcm"] = "N";
    //                }
    //                else
    //                {
    //                    dr1["rcm"] = "Y";
    //                }
    //                dr1["city"] = dta.Rows[0]["city"].ToString();
    //            }
    //            else
    //            {
    //                dr1["gstno"] = "";
    //                dr1["city"] = "";
    //                dr1["rcm"] = "";
    //            }
    //            if (dta.Rows.Count > 0)
    //            {
    //                dr1["cess"] = "0";
    //                dr1["gsttin"] = "";
    //                dtq.Rows.Add(dr1);
    //            }
    //        }
    //        Session["dtpitems"] = dtq;
    //        gvaclist.DataSource = dtq;
    //        gvaclist.DataBind();



    //        rptdoc = new ReportDocument();
    //        string rptname = "";
    //        rptname = Server.MapPath(@"~/Reports/GSTB2CL.rpt");

    //        //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
    //        //}
    //        //else
    //        //{
    //        //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
    //        //    {
    //        //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
    //        //    }
    //        //    else
    //        //    {
    //        //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
    //        //    }
    //        //}            
    //        rptdoc.Load(rptname);
    //        rptdoc.SetDataSource(dtq);
    //        //rptdoc.PrintOptions.PrinterName = "Canon LBP6000/LBP6018";
    //        //rptdoc.PrintToPrinter(1, false, 0, 0);
    //        //CrystalReportViewer1.ReportSource = rptdoc;
    //        //CrystalReportViewer1.DataBind();
    //        ExportOptions exprtopt = default(ExportOptions);
    //        string fname = "GSTB2CL.xls";
    //        //create instance for destination option - This one is used to set path of your pdf file save 
    //        DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
    //        destiopt.DiskFileName = Server.MapPath(@"~/cheque/" + fname);


    //        exprtopt = rptdoc.ExportOptions;
    //        exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

    //        //use PortableDocFormat for PDF data
    //        exprtopt.ExportFormatType = ExportFormatType.ExcelRecord;
    //        exprtopt.DestinationOptions = destiopt;

    //        //finally export your report document
    //        rptdoc.Export();
    //        rptdoc.Close();
    //        rptdoc.Clone();
    //        rptdoc.Dispose();
    //        string path = fname;
    //        fbarcode.Attributes.Add("src", "cheque/GSTB2CL.xls");


    //    }
    //}



    //private void ExportGridToExcel()
    //{
    //    DataTable dtq = (DataTable)Session["dtpitems"];
    //    if (dtq.Rows.Count > 0)
    //    {
    //        //Response.Clear();
    //        //Response.Buffer = true;
    //        //Response.ClearContent();
    //        //Response.ClearHeaders();
    //        //Response.Charset = "";
    //        //string FileName = "GSTB2CL" + DateTime.Now + ".xls";
    //        //StringWriter strwritter = new StringWriter();
    //        //HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
    //        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //        //Response.ContentType = "application/vnd.ms-excel";
    //        //Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
    //        //gvaclist.GridLines = GridLines.Both;
    //        //gvaclist.HeaderStyle.Font.Bold = true;
    //        //gvaclist.RenderControl(htmltextwrtter);
    //        //Response.Write(strwritter.ToString());
    //        //Response.End();

    //        //GridView GridView1 = new GridView();
    //        //GridView1.AllowPaging = false;
    //        //GridView1.DataSource = dtdata;
    //        //GridView1.DataBind();
    //        //Response.Clear();
    //        //Response.Buffer = true;
    //        //Response.AddHeader("content-disposition", "attachment;filename=ClientData.xls");
    //        //Response.Charset = "";
    //        //Response.ContentType = "application/vnd.ms-excel";
    //        //StringWriter sw = new StringWriter();
    //        //HtmlTextWriter hw = new HtmlTextWriter(sw);
    //        //for (int i = 0; i < GridView1.Rows.Count; i++)
    //        //{
    //        //    //Apply text style to each Row
    //        //    GridView1.Rows[i].Attributes.Add("class", "textmode");
    //        //}
    //        //GridView1.RenderControl(hw);
    //        ////style to format numbers to string
    //        //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
    //        //Response.Write(style);
    //        //Response.Output.Write(sw.ToString());
    //        //Response.Flush();
    //        //Response.End();

    //    }
    //    else
    //    {
    //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No B2CL data available for export.');", true);
    //        return;
    //    }
    //}



    //public DataTable CreateTemplate()
    //{
    //    DataTable dtpitems = new DataTable();
    //    dtpitems.Columns.Add("gstno", typeof(string));
    //    dtpitems.Columns.Add("strsino", typeof(string));
    //    dtpitems.Columns.Add("sidate", typeof(string));
    //    dtpitems.Columns.Add("billamount", typeof(double));
    //    dtpitems.Columns.Add("city", typeof(string));
    //    dtpitems.Columns.Add("taxdesc", typeof(string));
    //    dtpitems.Columns.Add("totbasicamount", typeof(double));
    //    dtpitems.Columns.Add("rcm", typeof(string));
    //    dtpitems.Columns.Add("cess", typeof(string));
    //    dtpitems.Columns.Add("gsttin", typeof(string));

    //    //dtpitems.Columns.Add("gstno", typeof(string));
    //    //dtpitems.Columns.Add("strsino", typeof(DateTime));
    //    //dtpitems.Columns.Add("sidate", typeof(string));
    //    //dtpitems.Columns.Add("billamount", typeof(double));
    //    //dtpitems.Columns.Add("city", typeof(double));
    //    //dtpitems.Columns.Add("taxdesc", typeof(double));
    //    //dtpitems.Columns.Add("totbasicamount", typeof(string));
    //    return dtpitems;
    //}

    //public override void VerifyRenderingInServerForm(Control control)
    //{
    //    //
    //}

    protected void btngstb2cl_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        //if (txtfromdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
        //{
            fillgridb2b();
            ExportGridToExcel();
        //}
    }

    public void fillgridb2b()
    {
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        Session["dtpitems"] = CreateTemplate();
        DataTable dtq = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select strsino,sidate,SUM(basicamount) as totbasic,(sum(SIItems.basicamount)+sum(SIItems.vatamt)+sum(SIItems.addtaxamt)+sum(SIItems.cstamt)) as totbillamt,taxdesc from SIItems where sidate between @fromdate and @todate group by taxdesc,strsino,sidate,sino order by sino", con);
        da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtdata = new DataTable();
        da.Fill(dtdata);
        if (dtdata.Rows.Count > 0)
        {
            for (int c = 0; c < dtdata.Rows.Count; c++)
            {
                dtq = (DataTable)Session["dtpitems"];
                DataRow dr1 = dtq.NewRow();
                dr1["strsino"] = dtdata.Rows[c]["strsino"].ToString();
                dr1["sidate"] = Convert.ToDateTime(dtdata.Rows[c]["sidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MMM-yy");
                dr1["totbasicamount"] = dtdata.Rows[c]["totbasic"].ToString();

                var cc = dtdata.Rows[c]["taxdesc"].ToString().IndexOf("-");
                if (cc != -1)
                {
                    double vatp = Convert.ToDouble(dtdata.Rows[c]["taxdesc"].ToString().Split('-')[0]);
                    double addvatp = Convert.ToDouble(dtdata.Rows[c]["taxdesc"].ToString().Split('-')[1]);
                    dr1["taxdesc"] = (vatp + addvatp).ToString();
                }
                else
                {
                    dr1["taxdesc"] = dtdata.Rows[c]["taxdesc"].ToString();
                }

                li.strsino = dtdata.Rows[c]["strsino"].ToString();
                SqlDataAdapter dasi = new SqlDataAdapter("select * from simaster where strsino='" + li.strsino + "'", con);
                DataTable dtsi = new DataTable();
                dasi.Fill(dtsi);
                dr1["billamount"] = dtsi.Rows[0]["billamount"].ToString();
                SqlDataAdapter daa = new SqlDataAdapter("select * from SIMaster inner join ACMaster on SIMaster.acname=ACMaster.acname where SIMaster.strsino='" + li.strsino + "' and ACMaster.state<>@fullname and SIMaster.billamount>250000 and ACMaster.gstno!=''", con);//city not like '%'+@fullname+'%'
                daa.SelectCommand.Parameters.AddWithValue("@fullname", "Gujarat");
                DataTable dta = new DataTable();
                daa.Fill(dta);
                if (dta.Rows.Count > 0)
                {
                    dr1["gstno"] = dta.Rows[0]["gstno"].ToString();
                    if (dta.Rows[0]["gstno"].ToString() != "")
                    {
                        dr1["rcm"] = "N";
                    }
                    else
                    {
                        dr1["rcm"] = "Y";
                    }
                    dr1["city"] = dta.Rows[0]["city"].ToString();
                }
                else
                {
                    dr1["gstno"] = "";
                    dr1["city"] = "";
                    dr1["rcm"] = "";
                }
                if (dta.Rows.Count > 0)
                {
                    dtq.Rows.Add(dr1);
                }
            }
            Session["dtpitems"] = dtq;
            //DataView dtv = dtq.DefaultView;
            //dtv. = "billamount>250000";
            //dtq = dtv.ToTable();
            gvaclist.DataSource = dtq;
            gvaclist.DataBind();
        }
    }



    private void ExportGridToExcel()
    {
        DataTable dtq = (DataTable)Session["dtpitems"];
        if (dtq.Rows.Count > 0)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "GSTB2CL" + DateTime.Now + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            gvaclist.GridLines = GridLines.Both;
            gvaclist.HeaderStyle.Font.Bold = true;
            gvaclist.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No B2CL data available for export.');", true);
            return;
        }
    }



    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("gstno", typeof(string));
        dtpitems.Columns.Add("strsino", typeof(string));
        dtpitems.Columns.Add("sidate", typeof(string));
        dtpitems.Columns.Add("billamount", typeof(double));
        dtpitems.Columns.Add("city", typeof(string));
        dtpitems.Columns.Add("taxdesc", typeof(string));
        dtpitems.Columns.Add("totbasicamount", typeof(double));
        dtpitems.Columns.Add("rcm", typeof(string));

        //dtpitems.Columns.Add("gstno", typeof(string));
        //dtpitems.Columns.Add("strsino", typeof(DateTime));
        //dtpitems.Columns.Add("sidate", typeof(string));
        //dtpitems.Columns.Add("billamount", typeof(double));
        //dtpitems.Columns.Add("city", typeof(double));
        //dtpitems.Columns.Add("taxdesc", typeof(double));
        //dtpitems.Columns.Add("totbasicamount", typeof(string));
        return dtpitems;
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //
    }

}