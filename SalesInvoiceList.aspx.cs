﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

public partial class SalesInvoiceList : System.Web.UI.Page
{
    public ReportDocument rptdoc;
    ForSalesInvoice fsiclass = new ForSalesInvoice();
    ForUserRight furclass = new ForUserRight();
    //ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtall = new DataTable();
        DataTable dtinvtype = new DataTable();
        dtinvtype = fsiclass.selectallinvtype(li);
        for (int c = 0; c < dtinvtype.Rows.Count; c++)
        {
            li.invtype = dtinvtype.Rows[c]["name"].ToString();
            DataTable dtdt = new DataTable();
            dtdt = fsiclass.selectallsimasterdatafrominvtype(li);
            dtall.Merge(dtdt);
        }
        //DataTable dtdata = new DataTable();
        //dtdata = fsiclass.selectallsimasterdata(li);
        if (dtall.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvsalesinvoicelist.Visible = true;
            gvsalesinvoicelist.DataSource = dtall;
            gvsalesinvoicelist.DataBind();
        }
        else
        {
            gvsalesinvoicelist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Sales Invoice Data Found.";
        }
    }

    protected void gvsalesinvoicelist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.strsino = e.CommandArgument.ToString();
                Response.Redirect("~/SalesInvoice.aspx?mode=update&strsino=" + li.strsino + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "directsi")
        {
            li.strsino = e.CommandArgument.ToString();
            Response.Redirect("~/PerformaInvoice.aspx?mode=directsi&strsino=" + li.strsino + "");
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.strsino = e.CommandArgument.ToString();
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);

                DataTable dtdata1 = new DataTable();
                dtdata1 = fsiclass.selectallsimasterdatafromsinostring(li);
                li.strscno = "0";
                li.strscno1 = "0";
                if (dtdata1.Rows[0]["scno"].ToString() != "0" && dtdata1.Rows[0]["scno"].ToString() != "")
                {
                    li.strscno = dtdata1.Rows[0]["scno"].ToString();
                }
                if (dtdata1.Rows[0]["scno1"].ToString() != "0" && dtdata1.Rows[0]["scno1"].ToString() != "")
                {
                    li.strscno1 = dtdata1.Rows[0]["scno1"].ToString();
                }
                //li.scno = Convert.ToInt64(dtdata1.Rows[0]["scno"].ToString());
                //li.vnono=
                DataTable dtdata = new DataTable();
                dtdata = fsiclass.selectallsiitemsfromsinostring(li);
                for (int c = 0; c < dtdata.Rows.Count; c++)
                {
                    li.vid = Convert.ToInt64(dtdata.Rows[c]["vid"].ToString());
                    li.id = Convert.ToInt64(dtdata.Rows[c]["id"].ToString());
                    li.itemname = dtdata.Rows[c]["itemname"].ToString();
                    li.qty = Convert.ToDouble(dtdata.Rows[c]["qty"].ToString());
                    if (li.strscno != "0" && li.strscno != null && li.strscno != string.Empty)
                    {
                        if (li.vid != 0)
                        {
                            DataTable dtqty = new DataTable();
                            dtqty = fsiclass.selectqtyremainusedfromscno1(li);
                            if (dtqty.Rows.Count > 0)
                            {
                                li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + li.qty;
                                li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - li.qty;
                                li.vnono = 0;
                                fsiclass.updateqtyduringdelete(li);
                            }
                        }
                    }
                    if (li.strscno1 != "0" && li.strscno1 != null && li.strscno1 != string.Empty)
                    {
                        if (li.vid != 0)
                        {
                            //li.scno = li.scno1;
                            DataTable dtqty = new DataTable();
                            dtqty = fsiclass.selectqtyremainusedfromscno11(li);
                            if (dtqty.Rows.Count > 0)
                            {
                                li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + li.qty;
                                li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - li.qty;
                                li.vnono = 0;
                                fsiclass.updateqtyduringdelete(li);
                            }
                        }
                    }
                }
                if (dtdata1.Rows.Count > 0)
                {
                    li.stringscno = dtdata1.Rows[0]["stringscno"].ToString();
                    //li.strsino = "";
                    string[] words = li.stringscno.Split(',');
                    foreach (string word in words)
                    {
                        li.strscno11 = word.Replace("'", "");
                        fsiclass.updatestrsinoinscmaster(li);
                        fsiclass.updatestrsinoinscmastera(li);
                        li.strscno = word.Replace("'", "");
                        fsiclass.updatescstatusforallchnoa1(li);
                        //Console.WriteLine(word);
                    }
                }
                fsiclass.deletesimasterdatafromsinostring(li);
                fsiclass.deletesiitemsdatafromsinostring(li);
                fsiclass.deletesalesinvoiceledgeraa(li);
                li.stringscno = dtdata1.Rows[0]["stringscno"].ToString();
                //fsiclass.updatescstatusforallchnoopen(li);
                fsiclass.updateisusedn(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                //li.activity = li.strsino + " Sales Invoice Deleted.";
                //faclass.insertactivity(li);
                fillgrid();
                if (dtdata1.Rows[0]["scno"].ToString() != "0" && dtdata1.Rows[0]["scno"].ToString() != "")
                {
                    li.strscno = dtdata1.Rows[0]["scno"].ToString();
                    fsiclass.updatescstatusopen(li);
                }
                if (dtdata1.Rows[0]["scno1"].ToString() != "0" && dtdata1.Rows[0]["scno1"].ToString() != "")
                {
                    li.strscno = dtdata1.Rows[0]["scno1"].ToString();
                    fsiclass.updatescstatusopen(li);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
        else if (e.CommandName == "print")
        {
            if (chkexcel.Checked == false && chkexcel1.Checked == false)
            {
                rptdoc = new ReportDocument();
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                string ftow = "";
                li.strsino = e.CommandArgument.ToString();
                DataSet1 imageDataSet = new DataSet1();
                string cz = Request.Cookies["Forcon"]["conc"];
                cz = cz.Replace(":", ";");
                SqlConnection con = new SqlConnection(cz);
                //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
                SqlDataAdapter da11 = new SqlDataAdapter("select * from SIMaster inner join SIItems on SIMaster.strsino=SIItems.strsino inner join ACMaster on ACMaster.acname=SIMaster.acname inner join ItemMaster on ItemMaster.itemname=SIItems.itemname where SIMaster.strsino='" + li.strsino + "' and SIMaster.cno='" + li.cno + "' order by SIItems.vid", con);
                DataTable dtc = new DataTable();
                da11.Fill(imageDataSet.Tables["DataTable4"]);
                for (int z = 0; z < imageDataSet.Tables["DataTable4"].Rows.Count; z++)
                {
                    if (imageDataSet.Tables["DataTable4"].Rows[z]["hsncode"].ToString() == "" || imageDataSet.Tables["DataTable4"].Rows[z]["hsncode"].ToString() == string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter HSN Code For " + imageDataSet.Tables["DataTable4"].Rows[z]["itemname"].ToString() + " And Try Again.');", true);
                        return;
                    }
                }
                if (imageDataSet.Tables["DataTable4"].Rows.Count > 0)
                {
                    //li.acname = imageDataSet.Tables["DataTable4"].Rows[0]["acname"].ToString();
                    //DataTable dtata = fsiclass.selectallacdatefromacname(li);
                    //ViewState["excludetax"] = dtata.Rows[0]["excludetax"].ToString();
                    if (imageDataSet.Tables["DataTable4"].Rows[0]["excludetax"].ToString() == "Yes")//if (imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. IGST - Export" || imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. - EOU")
                    {
                        //string amount = imageDataSet.Tables["DataTable4"].Rows[0]["totbasicamount"].ToString();
                        //ftow = changeToWords(amount);
                        string amount = Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["totbasicamount"].ToString()).ToString("0.00");
                        Int64 a = Convert.ToInt64(amount.Split('.')[0]);
                        int b = Convert.ToInt16(amount.Split('.')[1]);
                        ftow = NumberToText(a) + " Rupees ";
                        if (b == 0)
                        {
                            ftow = ftow + " Only.";
                        }
                        if (b != 0)
                        {
                            ftow = ftow + " and " + NumberToText1(b);
                        }
                    }
                    else
                    {
                        //string amount = imageDataSet.Tables["DataTable4"].Rows[0]["billamount"].ToString();
                        //ftow = changeToWords(amount);
                        string amount = Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["billamount"].ToString()).ToString("0.00");
                        Int64 a = Convert.ToInt64(amount.Split('.')[0]);
                        int b = Convert.ToInt16(amount.Split('.')[1]);
                        ftow = NumberToText(a) + " Rupees ";
                        if (b == 0)
                        {
                            ftow = ftow + " Only.";
                        }
                        if (b != 0)
                        {
                            ftow = ftow + " and " + NumberToText1(b);
                        }
                    }
                }
                //if (imageDataSet.Tables["DataTable4"].Rows[0]["excludetax"].ToString() == "Yes")//if (imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. IGST - Export" || imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. - EOU")
                //{
                //    if ((imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString().Trim() == string.Empty || imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString().Trim() == "0") && (imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString().Trim() == string.Empty || imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString().Trim() == "0"))
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This is export bill so please enter $ or € rate.Try Again.');", true);
                //        return;
                //    }
                //}
                li.acname = imageDataSet.Tables["DataTable4"].Rows[0]["acname"].ToString();
                li.ccode = Convert.ToInt64(imageDataSet.Tables["DataTable4"].Rows[0]["ccode"].ToString());
                con.Open();
                DataTable dtacname = new DataTable();
                SqlDataAdapter das = new SqlDataAdapter("select * from ACMaster where acname=@acname and cno=@cno", con);
                das.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
                das.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                das.Fill(dtacname);
                con.Close();

                con.Open();
                DataTable dtclient = new DataTable();
                SqlDataAdapter das1 = new SqlDataAdapter("select * from ClientMaster where code=@ccode and cno=@cno", con);
                das1.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
                das1.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                das1.Fill(dtclient);
                con.Close();
                string address = dtacname.Rows[0]["add1"].ToString() + " " + dtacname.Rows[0]["add2"].ToString() + " " + dtacname.Rows[0]["add3"].ToString();
                string rptname;
                //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
                //{
                if ((imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString().Trim() != string.Empty && imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString().Trim() != "0") || (imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString().Trim() != string.Empty && imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString().Trim() != "0"))
                {
                    rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport_CSTrs.rpt");
                }
                else
                {
                    if (Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["cstamt"].ToString()) > 0)
                    {
                        rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport_CST.rpt");
                    }
                    else if (Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["vatamt"].ToString()) > 0)
                    {
                        if (imageDataSet.Tables["DataTable4"].Rows[0]["excludetax"].ToString() == "Yes")//if (imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. IGST - Export" || imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. - EOU")
                        {
                            rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport_EOU.rpt");
                        }
                        else
                        {
                            rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport.rpt");
                        }
                    }
                    else
                    {
                        if (imageDataSet.Tables["DataTable4"].Rows[0]["excludetax"].ToString() == "Yes")//if (imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. IGST - Export" || imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. - EOU")
                        {
                            rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport_EOU.rpt");
                        }
                        else
                        {
                            rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport.rpt");
                        }
                    }
                }
                //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
                //}
                //else
                //{
                //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
                //    {
                //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
                //    }
                //    else
                //    {
                //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
                //    }
                //}
                rptdoc.Load(rptname);
                if (dtacname.Rows.Count > 0)
                {

                }
                //string title = "";
                //if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
                //{
                //    title = "Tax Invoice";
                //    rptdoc.DataDefinition.FormulaFields["rtitle"].Text = "'" + title + "'";
                //}
                //else
                //{
                //    title = "Retail Invoice";
                //    rptdoc.DataDefinition.FormulaFields["rtitle"].Text = "'" + title + "'";
                //}
                string gst = dtacname.Rows[0]["gsttinno"].ToString() + "    " + dtacname.Rows[0]["date1"].ToString();
                string cst = dtacname.Rows[0]["csttinno"].ToString() + "    " + dtacname.Rows[0]["date2"].ToString();
                string ccode = dtclient.Rows[0]["name"].ToString();//dtclient.Rows[0]["code"].ToString() + "  " +
                string nn = imageDataSet.Tables["DataTable4"].Rows[0]["sino"].ToString();
                if (nn.Length == 1)
                {
                    nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + "00" + nn;
                }
                else if (nn.Length == 2)
                {
                    nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + "0" + nn;
                }
                else
                {
                    nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + nn;
                }
                rptdoc.DataDefinition.FormulaFields["gsttin"].Text = "'" + gst + "'";
                rptdoc.DataDefinition.FormulaFields["csttin"].Text = "'" + cst + "'";
                rptdoc.DataDefinition.FormulaFields["gstno"].Text = "'" + dtacname.Rows[0]["gstno"].ToString() + "'";
                //rptdoc.DataDefinition.FormulaFields["email"].Text = "'" + dtacname.Rows[0]["emailid"].ToString() + "'";
                rptdoc.DataDefinition.FormulaFields["clientname"].Text = "'" + ccode + "'";
                rptdoc.DataDefinition.FormulaFields["ftow"].Text = "'" + ftow + "'";
                //rptdoc.DataDefinition.FormulaFields["cramount"].Text = "'" + dtcc.Rows[0]["cramount"].ToString() + "'";
                rptdoc.DataDefinition.FormulaFields["invoicenostring"].Text = "'" + nn + "'";
                rptdoc.DataDefinition.FormulaFields["add1"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add1"].ToString().Trim().Replace("'", "") + "'";
                rptdoc.DataDefinition.FormulaFields["add2"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add2"].ToString().Trim().Replace("'", "") + "'";
                rptdoc.DataDefinition.FormulaFields["add3"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add3"].ToString().Trim().Replace("'", "") + "'";
                rptdoc.DataDefinition.FormulaFields["city"].Text = "'" + dtacname.Rows[0]["city"].ToString().Trim().Replace("'", "") + "'";
                rptdoc.DataDefinition.FormulaFields["cadd1"].Text = "'" + dtclient.Rows[0]["add1"].ToString().Trim().Replace("'", "") + "'";
                rptdoc.DataDefinition.FormulaFields["cadd2"].Text = "'" + dtclient.Rows[0]["add2"].ToString().Trim().Replace("'", "") + "'";
                rptdoc.DataDefinition.FormulaFields["cadd3"].Text = "'" + dtclient.Rows[0]["add3"].ToString().Trim().Replace("'", "") + "'";
                rptdoc.DataDefinition.FormulaFields["ABCCCCC"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["stringscno"].ToString().Replace("'", "") + "'";

                //if (imageDataSet.Tables["DataTable4"].Rows[0]["excludetax"].ToString() == "Yes")
                //{
                //    double bamt = Math.Round((Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["totbasicamount"].ToString()), 2) / Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString()), 2)), 2);
                //    double round1 = Math.Round((bamt - Math.Round(bamt)), 2);
                //    if (Math.Round(bamt) > bamt)
                //    {
                //        if (round1 > 0)
                //        {
                //            if (round1 != 0.5)
                //            {
                //                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + round1 + "'";
                //            }
                //            else
                //            {
                //                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + (round1 * (-1)) + "'";
                //            }
                //        }
                //        else
                //        {
                //            rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + (round1 * (-1)) + "'";
                //        }
                //    }
                //    else
                //    {
                //        if (round1 > 0)
                //        {
                //            if (round1 != 0.5)
                //            {
                //                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + (round1 * (-1)) + "'";
                //            }
                //            else
                //            {
                //                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + round1 + "'";
                //            }
                //        }
                //        else
                //        {
                //            rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + round1 + "'";
                //        }
                //    }
                //}


                if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "RIT")
                {
                    rptdoc.DataDefinition.FormulaFields["repname"].Text = "'RETAIL INVOICE'";
                    rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'RIT'";
                }
                else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "TIT")
                {
                    rptdoc.DataDefinition.FormulaFields["repname"].Text = "'TAX INVOICE'";
                    rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'TIT'";
                }
                else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "S")
                {
                    rptdoc.DataDefinition.FormulaFields["repname"].Text = "'SERVICE INVOICE'";
                    rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'S'";
                }
                else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "L")
                {
                    rptdoc.DataDefinition.FormulaFields["repname"].Text = "'LABOUR INVOICE'";
                    rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'L'";
                }
                else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "GST")
                {
                    rptdoc.DataDefinition.FormulaFields["repname"].Text = "'GST INVOICE'";
                    rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'GST'";
                }
                rptdoc.SetDataSource(imageDataSet.Tables["DataTable4"]);
                //////////rptdoc.PrintOptions.PaperSize = PaperSize.PaperA4;
                //////////rptdoc.PrintOptions.PaperSource = PaperSource.Auto;
                //////////rptdoc.PrintOptions.PaperOrientation = PaperOrientation.Portrait;
                ////////////rptdoc.PrintOptions.
                //////////rptdoc.PrintOptions.PrinterName = "Canon LBP6000/LBP6018";
                //////////rptdoc.PrintToPrinter(1, false, 0, 0);
                //CrystalReportViewer1.ReportSource = rptdoc;
                //rptdoc.PrintOptions.PrinterName = "Canon LBP2900 on nitin";
                //rptdoc.PrintToPrinter(1, false, 0, 0);
                //Export Report
                ExportOptions exprtopt = default(ExportOptions);
                string fname = "salesinvoice_" + li.strsino + ".pdf";
                //create instance for destination option - This one is used to set path of your pdf file save 
                DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
                destiopt.DiskFileName = Server.MapPath(@"~/salesinvoice/" + fname);


                exprtopt = rptdoc.ExportOptions;
                exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

                //use PortableDocFormat for PDF data
                //exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
                //exprtopt.DestinationOptions = destiopt;

                ////finally export your report document
                //rptdoc.Export();
                //rptdoc.Close();
                //rptdoc.Clone();
                //rptdoc.Dispose();
                //string path = fname;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('RepSalesInvoiceView.aspx?path=" + path + "&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
                if (rdotype.SelectedItem.Text == "PDF")
                {
                    exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
                    //exprtopt.ExportFormatType = ExportFormatType.Excel;
                    exprtopt.DestinationOptions = destiopt;
                    rptdoc.Export();
                    rptdoc.Close();
                    rptdoc.Clone();
                    rptdoc.Dispose();
                    string path = fname;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('RepSalesInvoiceView.aspx?path=" + path + "&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
                }
                else
                {
                    string fnameexcel = "salesinvoice_" + li.strsino;
                    rptdoc.ExportToHttpResponse(ExportFormatType.Excel, Response, true, fnameexcel);
                    Response.End();
                    //finally export your report document
                    rptdoc.Export();
                    rptdoc.Close();
                    rptdoc.Clone();
                    rptdoc.Dispose();
                }
            }
            else if (chkexcel.Checked == false && chkexcel1.Checked == true)
            {
                if (txtdtors.Text.Trim() != string.Empty)
                {
                    double dtors = Convert.ToDouble(txtdtors.Text);
                    rptdoc = new ReportDocument();
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    string ftow = "";
                    li.strsino = e.CommandArgument.ToString();
                    DataSet1 imageDataSet = new DataSet1();
                    string cz = Request.Cookies["Forcon"]["conc"];
                    cz = cz.Replace(":", ";");
                    SqlConnection con = new SqlConnection(cz);
                    SqlCommand cmdd = new SqlCommand("update SIMaster set etors=" + dtors + " where strsino='" + li.strsino + "'", con);
                    con.Open();
                    cmdd.ExecuteNonQuery();
                    con.Close();
                    //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
                    SqlDataAdapter da11 = new SqlDataAdapter("select * from SIMaster inner join SIItems on SIMaster.strsino=SIItems.strsino inner join ACMaster on ACMaster.acname=SIMaster.acname inner join ItemMaster on ItemMaster.itemname=SIItems.itemname where SIMaster.strsino='" + li.strsino + "' and SIMaster.cno='" + li.cno + "' order by SIItems.vid", con);
                    DataTable dtc = new DataTable();
                    da11.Fill(imageDataSet.Tables["DataTable4"]);
                    if (imageDataSet.Tables["DataTable4"].Rows.Count > 0)
                    {
                        //string amount = Math.Round((Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["totbasicamount"].ToString()), 2) / Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString()), 2))).ToString();
                        //ftow = changeToWordseuro(amount);
                        string amount = Math.Round((Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["totbasicamount"].ToString()), 2) / Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString()), 2)), 2).ToString();
                        var cc = amount.IndexOf(".");
                        if (cc != -1)
                        {
                            Int64 a = Convert.ToInt64(amount.Split('.')[0]);
                            int b = Convert.ToInt16(amount.Split('.')[1]);
                            ftow = NumberToText(a) + " Euro ";
                            if (b == 0)
                            {
                                ftow = ftow + " ";
                            }
                            if (b != 0)
                            {
                                ftow = ftow + " and " + NumberToText1(b);
                            }
                            ftow = ftow + " Only.";
                        }
                        else
                        {
                            Int64 a = Convert.ToInt64(amount);
                            ftow = NumberToText(a) + " Euro Only.";
                        }
                    }
                    //if (imageDataSet.Tables["DataTable4"].Rows[0]["excludetax"].ToString() == "Yes")//if (imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. IGST - Export" || imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. - EOU")
                    //{
                    //    if ((imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString().Trim() == string.Empty || imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString().Trim() == "0") && (imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString().Trim() == string.Empty || imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString().Trim() == "0"))
                    //    {
                    //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This is export bill so please enter $ or € rate.Try Again.');", true);
                    //        return;
                    //    }
                    //}
                    li.acname = imageDataSet.Tables["DataTable4"].Rows[0]["acname"].ToString();
                    li.ccode = Convert.ToInt64(imageDataSet.Tables["DataTable4"].Rows[0]["ccode"].ToString());
                    con.Open();
                    DataTable dtacname = new DataTable();
                    SqlDataAdapter das = new SqlDataAdapter("select * from ACMaster where acname=@acname and cno=@cno", con);
                    das.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
                    das.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                    das.Fill(dtacname);
                    con.Close();

                    con.Open();
                    DataTable dtclient = new DataTable();
                    SqlDataAdapter das1 = new SqlDataAdapter("select * from ClientMaster where code=@ccode and cno=@cno", con);
                    das1.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
                    das1.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                    das1.Fill(dtclient);
                    con.Close();
                    string address = dtacname.Rows[0]["add1"].ToString() + " " + dtacname.Rows[0]["add2"].ToString() + " " + dtacname.Rows[0]["add3"].ToString();
                    string rptname;
                    //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
                    //{
                    //if (Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["cstamt"].ToString()) > 0)
                    //{
                    //    rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport_CST.rpt");
                    //}
                    //else if (Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["vatamt"].ToString()) > 0)
                    //{
                    //    rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport.rpt");
                    //}
                    //else
                    //{
                    rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport_CSTExport1.rpt");
                    //}
                    //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
                    //}
                    //else
                    //{
                    //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
                    //    {
                    //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
                    //    }
                    //    else
                    //    {
                    //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
                    //    }
                    //}
                    rptdoc.Load(rptname);
                    if (dtacname.Rows.Count > 0)
                    {

                    }
                    //string title = "";
                    //if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
                    //{
                    //    title = "Tax Invoice";
                    //    rptdoc.DataDefinition.FormulaFields["rtitle"].Text = "'" + title + "'";
                    //}
                    //else
                    //{
                    //    title = "Retail Invoice";
                    //    rptdoc.DataDefinition.FormulaFields["rtitle"].Text = "'" + title + "'";
                    //}
                    string gst = dtacname.Rows[0]["gsttinno"].ToString() + "    " + dtacname.Rows[0]["date1"].ToString();
                    string cst = dtacname.Rows[0]["csttinno"].ToString() + "    " + dtacname.Rows[0]["date2"].ToString();
                    string ccode = dtclient.Rows[0]["name"].ToString();//dtclient.Rows[0]["code"].ToString() + "  " +
                    string nn = imageDataSet.Tables["DataTable4"].Rows[0]["sino"].ToString();
                    if (nn.Length == 1)
                    {
                        nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + "00" + nn;
                    }
                    else if (nn.Length == 2)
                    {
                        nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + "0" + nn;
                    }
                    else
                    {
                        nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + nn;
                    }
                    rptdoc.DataDefinition.FormulaFields["gsttin"].Text = "'" + gst + "'";
                    rptdoc.DataDefinition.FormulaFields["dtors"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString() + "'";
                    rptdoc.DataDefinition.FormulaFields["csttin"].Text = "'" + cst + "'";
                    rptdoc.DataDefinition.FormulaFields["gstno"].Text = "'" + dtacname.Rows[0]["gstno"].ToString() + "'";
                    //rptdoc.DataDefinition.FormulaFields["email"].Text = "'" + dtacname.Rows[0]["emailid"].ToString() + "'";
                    rptdoc.DataDefinition.FormulaFields["clientname"].Text = "'" + ccode + "'";
                    rptdoc.DataDefinition.FormulaFields["ftow"].Text = "'" + ftow + "'";
                    //rptdoc.DataDefinition.FormulaFields["cramount"].Text = "'" + dtcc.Rows[0]["cramount"].ToString() + "'";
                    rptdoc.DataDefinition.FormulaFields["invoicenostring"].Text = "'" + nn + "'";
                    rptdoc.DataDefinition.FormulaFields["add1"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add1"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["add2"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add2"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["add3"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add3"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["city"].Text = "'" + dtacname.Rows[0]["city"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["cadd1"].Text = "'" + dtclient.Rows[0]["add1"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["cadd2"].Text = "'" + dtclient.Rows[0]["add2"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["cadd3"].Text = "'" + dtclient.Rows[0]["add3"].ToString().Trim().Replace("'", "") + "'";
                    double bamt = Math.Round((Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["totbasicamount"].ToString()), 2) / Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString()), 2)), 2);
                    double round1 = Math.Round((bamt - Math.Round(bamt)), 2);
                    if (Math.Round(bamt) > bamt)
                    {
                        if (round1 > 0)
                        {
                            if (round1 != 0.5)
                            {
                                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + round1 + "'";
                            }
                            else
                            {
                                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + (round1 * (-1)) + "'";
                            }
                        }
                        else
                        {
                            rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + (round1 * (-1)) + "'";
                        }
                    }
                    else
                    {
                        if (round1 > 0)
                        {
                            if (round1 != 0.5)
                            {
                                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + (round1 * (-1)) + "'";
                            }
                            else
                            {
                                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + round1 + "'";
                            }
                        }
                        else
                        {
                            rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + round1 + "'";
                        }
                    }

                    if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "RIT")
                    {
                        rptdoc.DataDefinition.FormulaFields["repname"].Text = "'RETAIL INVOICE'";
                        rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'RIT'";
                    }
                    else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "TIT")
                    {
                        rptdoc.DataDefinition.FormulaFields["repname"].Text = "'TAX INVOICE'";
                        rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'TIT'";
                    }
                    else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "S")
                    {
                        rptdoc.DataDefinition.FormulaFields["repname"].Text = "'SERVICE INVOICE'";
                        rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'S'";
                    }
                    else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "L")
                    {
                        rptdoc.DataDefinition.FormulaFields["repname"].Text = "'LABOUR INVOICE'";
                        rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'L'";
                    }
                    else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "GST")
                    {
                        rptdoc.DataDefinition.FormulaFields["repname"].Text = "'GST INVOICE'";
                        rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'GST'";
                    }
                    rptdoc.SetDataSource(imageDataSet.Tables["DataTable4"]);

                    //Export Report
                    //ExportOptions exprtopt = default(ExportOptions);
                    //string fname = "salesinvoice_" + li.strsino + ".xlsx";
                    ////create instance for destination option - This one is used to set path of your pdf file save 
                    //DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
                    //destiopt.DiskFileName = Server.MapPath(@"~/salesinvoice/" + fname);

                    //Response.Buffer = false;
                    //Response.ClearContent();
                    //Response.ClearHeaders();
                    //rptdoc.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, fname);
                    //Response.End();
                    ExportOptions exprtopt = default(ExportOptions);
                    string fname = "salesinvoice_" + li.strsino + ".pdf";
                    //create instance for destination option - This one is used to set path of your pdf file save 
                    DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
                    destiopt.DiskFileName = Server.MapPath(@"~/salesinvoice/" + fname);


                    exprtopt = rptdoc.ExportOptions;
                    exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

                    //use PortableDocFormat for PDF data
                    //exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
                    //exprtopt.DestinationOptions = destiopt;

                    ////finally export your report document
                    //rptdoc.Export();
                    //rptdoc.Close();
                    //rptdoc.Clone();
                    //rptdoc.Dispose();
                    //string path = fname;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('RepSalesInvoiceView.aspx?path=" + path + "&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
                    if (rdotype.SelectedItem.Text == "PDF")
                    {
                        exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
                        //exprtopt.ExportFormatType = ExportFormatType.Excel;
                        exprtopt.DestinationOptions = destiopt;
                        rptdoc.Export();
                        rptdoc.Close();
                        rptdoc.Clone();
                        rptdoc.Dispose();
                        string path = fname;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('RepSalesInvoiceView.aspx?path=" + path + "&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
                    }
                    else
                    {
                        string fnameexcel = "salesinvoice_" + li.strsino;
                        rptdoc.ExportToHttpResponse(ExportFormatType.Excel, Response, true, fnameexcel);
                        Response.End();
                        //finally export your report document
                        rptdoc.Export();
                        rptdoc.Close();
                        rptdoc.Clone();
                        rptdoc.Dispose();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Enter Rupees Value for 1 Euro.');", true);
                }
            }
            else
            {
                if (txtdtors.Text.Trim() != string.Empty)
                {
                    double dtors = Convert.ToDouble(txtdtors.Text);
                    rptdoc = new ReportDocument();
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    string ftow = "";
                    li.strsino = e.CommandArgument.ToString();
                    DataSet1 imageDataSet = new DataSet1();
                    string cz = Request.Cookies["Forcon"]["conc"];
                    cz = cz.Replace(":", ";");
                    SqlConnection con = new SqlConnection(cz);
                    SqlCommand cmdd = new SqlCommand("update SIMaster set dtors=" + dtors + " where strsino='" + li.strsino + "'", con);
                    con.Open();
                    cmdd.ExecuteNonQuery();
                    con.Close();
                    //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
                    SqlDataAdapter da11 = new SqlDataAdapter("select * from SIMaster inner join SIItems on SIMaster.strsino=SIItems.strsino inner join ACMaster on ACMaster.acname=SIMaster.acname inner join ItemMaster on ItemMaster.itemname=SIItems.itemname where SIMaster.strsino='" + li.strsino + "' and SIMaster.cno='" + li.cno + "' order by SIItems.vid", con);
                    DataTable dtc = new DataTable();
                    da11.Fill(imageDataSet.Tables["DataTable4"]);
                    if (imageDataSet.Tables["DataTable4"].Rows.Count > 0)
                    {
                        //string amount = Math.Round((Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["totbasicamount"].ToString()), 2) / Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString()), 2))).ToString();
                        //ftow = changeToWordsdollar(amount);
                        string amount = Math.Round((Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["totbasicamount"].ToString()), 2) / Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString()), 2)), 2).ToString();
                        var cc = amount.IndexOf(".");
                        if (cc != -1)
                        {
                            Int64 a = Convert.ToInt64(amount.Split('.')[0]);
                            int b = Convert.ToInt16(amount.Split('.')[1]);
                            ftow = NumberToText(a) + " Dollar ";
                            if (b == 0)
                            {
                                ftow = ftow + " ";
                            }
                            if (b != 0)
                            {
                                ftow = ftow + " and " + NumberToText1(b);
                            }
                            ftow = ftow + " Only.";
                        }
                        else
                        {
                            Int64 a = Convert.ToInt64(amount);
                            ftow = NumberToText(a) + " Dollar Only.";
                        }
                    }
                    //if (imageDataSet.Tables["DataTable4"].Rows[0]["excludetax"].ToString() == "Yes")//if (imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. IGST - Export" || imageDataSet.Tables["DataTable4"].Rows[0]["salesac"].ToString() == "Sales A/c. - EOU")
                    //{
                    //    if ((imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString().Trim() == string.Empty || imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString().Trim() == "0") && (imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString().Trim() == string.Empty || imageDataSet.Tables["DataTable4"].Rows[0]["etors"].ToString().Trim() == "0"))
                    //    {
                    //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This is export bill so please enter $ or € rate.Try Again.');", true);
                    //        return;
                    //    }
                    //}
                    li.acname = imageDataSet.Tables["DataTable4"].Rows[0]["acname"].ToString();
                    li.ccode = Convert.ToInt64(imageDataSet.Tables["DataTable4"].Rows[0]["ccode"].ToString());
                    con.Open();
                    DataTable dtacname = new DataTable();
                    SqlDataAdapter das = new SqlDataAdapter("select * from ACMaster where acname=@acname and cno=@cno", con);
                    das.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
                    das.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                    das.Fill(dtacname);
                    con.Close();

                    con.Open();
                    DataTable dtclient = new DataTable();
                    SqlDataAdapter das1 = new SqlDataAdapter("select * from ClientMaster where code=@ccode and cno=@cno", con);
                    das1.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
                    das1.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                    das1.Fill(dtclient);
                    con.Close();
                    string address = dtacname.Rows[0]["add1"].ToString() + " " + dtacname.Rows[0]["add2"].ToString() + " " + dtacname.Rows[0]["add3"].ToString();
                    string rptname;
                    //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
                    //{
                    //if (Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["cstamt"].ToString()) > 0)
                    //{
                    //    rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport_CST.rpt");
                    //}
                    //else if (Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["vatamt"].ToString()) > 0)
                    //{
                    //    rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport.rpt");
                    //}
                    //else
                    //{
                    rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport_CSTExport.rpt");
                    //}
                    //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
                    //}
                    //else
                    //{
                    //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
                    //    {
                    //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
                    //    }
                    //    else
                    //    {
                    //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
                    //    }
                    //}
                    rptdoc.Load(rptname);
                    if (dtacname.Rows.Count > 0)
                    {

                    }
                    //string title = "";
                    //if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
                    //{
                    //    title = "Tax Invoice";
                    //    rptdoc.DataDefinition.FormulaFields["rtitle"].Text = "'" + title + "'";
                    //}
                    //else
                    //{
                    //    title = "Retail Invoice";
                    //    rptdoc.DataDefinition.FormulaFields["rtitle"].Text = "'" + title + "'";
                    //}
                    string gst = dtacname.Rows[0]["gsttinno"].ToString() + "    " + dtacname.Rows[0]["date1"].ToString();
                    string cst = dtacname.Rows[0]["csttinno"].ToString() + "    " + dtacname.Rows[0]["date2"].ToString();
                    string ccode = dtclient.Rows[0]["name"].ToString();//dtclient.Rows[0]["code"].ToString() + "  " +
                    string nn = imageDataSet.Tables["DataTable4"].Rows[0]["sino"].ToString();
                    if (nn.Length == 1)
                    {
                        nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + "00" + nn;
                    }
                    else if (nn.Length == 2)
                    {
                        nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + "0" + nn;
                    }
                    else
                    {
                        nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + nn;
                    }
                    rptdoc.DataDefinition.FormulaFields["gsttin"].Text = "'" + gst + "'";
                    rptdoc.DataDefinition.FormulaFields["dtors"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString() + "'";
                    rptdoc.DataDefinition.FormulaFields["csttin"].Text = "'" + cst + "'";
                    rptdoc.DataDefinition.FormulaFields["gstno"].Text = "'" + dtacname.Rows[0]["gstno"].ToString() + "'";
                    //rptdoc.DataDefinition.FormulaFields["email"].Text = "'" + dtacname.Rows[0]["emailid"].ToString() + "'";
                    rptdoc.DataDefinition.FormulaFields["clientname"].Text = "'" + ccode + "'";
                    rptdoc.DataDefinition.FormulaFields["ftow"].Text = "'" + ftow + "'";
                    //rptdoc.DataDefinition.FormulaFields["cramount"].Text = "'" + dtcc.Rows[0]["cramount"].ToString() + "'";
                    rptdoc.DataDefinition.FormulaFields["invoicenostring"].Text = "'" + nn + "'";
                    rptdoc.DataDefinition.FormulaFields["add1"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add1"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["add2"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add2"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["add3"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add3"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["city"].Text = "'" + dtacname.Rows[0]["city"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["cadd1"].Text = "'" + dtclient.Rows[0]["add1"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["cadd2"].Text = "'" + dtclient.Rows[0]["add2"].ToString().Trim().Replace("'", "") + "'";
                    rptdoc.DataDefinition.FormulaFields["cadd3"].Text = "'" + dtclient.Rows[0]["add3"].ToString().Trim().Replace("'", "") + "'";
                    double bamt = Math.Round((Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["totbasicamount"].ToString()), 2) / Math.Round(Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["dtors"].ToString()), 2)), 2);
                    double round1 = Math.Round((bamt - Math.Round(bamt)), 2);
                    if (Math.Round(bamt) > bamt)
                    {
                        if (round1 > 0)
                        {
                            if (round1 != 0.5)
                            {
                                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + round1 + "'";
                            }
                            else
                            {
                                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + (round1 * (-1)) + "'";
                            }
                        }
                        else
                        {
                            rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + (round1 * (-1)) + "'";
                        }
                    }
                    else
                    {
                        if (round1 > 0)
                        {
                            if (round1 != 0.5)
                            {
                                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + (round1 * (-1)) + "'";
                            }
                            else
                            {
                                rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + round1 + "'";
                            }
                        }
                        else
                        {
                            rptdoc.DataDefinition.FormulaFields["roundoff"].Text = "'" + round1 + "'";
                        }
                    }

                    if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "RIT")
                    {
                        rptdoc.DataDefinition.FormulaFields["repname"].Text = "'RETAIL INVOICE'";
                        rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'RIT'";
                    }
                    else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "TIT")
                    {
                        rptdoc.DataDefinition.FormulaFields["repname"].Text = "'TAX INVOICE'";
                        rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'TIT'";
                    }
                    else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "S")
                    {
                        rptdoc.DataDefinition.FormulaFields["repname"].Text = "'SERVICE INVOICE'";
                        rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'S'";
                    }
                    else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "L")
                    {
                        rptdoc.DataDefinition.FormulaFields["repname"].Text = "'LABOUR INVOICE'";
                        rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'L'";
                    }
                    else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "GST")
                    {
                        rptdoc.DataDefinition.FormulaFields["repname"].Text = "'GST INVOICE'";
                        rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'GST'";
                    }
                    rptdoc.SetDataSource(imageDataSet.Tables["DataTable4"]);

                    //Export Report
                    //ExportOptions exprtopt = default(ExportOptions);
                    //string fname = "salesinvoice_" + li.strsino + ".xlsx";
                    ////create instance for destination option - This one is used to set path of your pdf file save 
                    //DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
                    //destiopt.DiskFileName = Server.MapPath(@"~/salesinvoice/" + fname);

                    //Response.Buffer = false;
                    //Response.ClearContent();
                    //Response.ClearHeaders();
                    //rptdoc.ExportToHttpResponse(ExportFormatType.WordForWindows, Response, true, fname);
                    //Response.End();
                    ExportOptions exprtopt = default(ExportOptions);
                    string fname = "salesinvoice_" + li.strsino + ".pdf";
                    //create instance for destination option - This one is used to set path of your pdf file save 
                    DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
                    destiopt.DiskFileName = Server.MapPath(@"~/salesinvoice/" + fname);


                    exprtopt = rptdoc.ExportOptions;
                    exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

                    //use PortableDocFormat for PDF data
                    //exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
                    //exprtopt.DestinationOptions = destiopt;

                    ////finally export your report document
                    //rptdoc.Export();
                    //rptdoc.Close();
                    //rptdoc.Clone();
                    //rptdoc.Dispose();
                    //string path = fname;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('RepSalesInvoiceView.aspx?path=" + path + "&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
                    if (rdotype.SelectedItem.Text == "PDF")
                    {
                        exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
                        //exprtopt.ExportFormatType = ExportFormatType.Excel;
                        exprtopt.DestinationOptions = destiopt;
                        rptdoc.Export();
                        rptdoc.Close();
                        rptdoc.Clone();
                        rptdoc.Dispose();
                        string path = fname;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('RepSalesInvoiceView.aspx?path=" + path + "&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
                    }
                    else
                    {
                        string fnameexcel = "salesinvoice_" + li.strsino;
                        rptdoc.ExportToHttpResponse(ExportFormatType.Excel, Response, true, fnameexcel);
                        Response.End();
                        //finally export your report document
                        rptdoc.Export();
                        rptdoc.Close();
                        rptdoc.Clone();
                        rptdoc.Dispose();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Enter Rupees Value for 1 Dollar.');", true);
                }
            }
        }
    }
    protected void gvsalesinvoicelist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void btnsalesinvoice_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/SalesInvoice.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }

    public string NumberToText(Int64 number)
    {
        if (number == 0) return "Zero";
        Int64[] num = new Int64[4];
        int first = 0;
        Int64 u, h, t;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (number < 0)
        {
            sb.Append("Minus ");
            number = -number;
        }
        string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lakh ", "Crore " };
        num[0] = number % 1000; // units
        num[1] = number / 1000;
        num[2] = number / 100000;
        num[1] = num[1] - 100 * num[2]; // thousands
        num[3] = number / 10000000; // crores
        num[2] = num[2] - 100 * num[3]; // lakhs
        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10; // ones
            t = num[i] / 10;
            h = num[i] / 100; // hundreds
            t = t - 10 * h; // tens
            if (h > 0) sb.Append(words0[h] + "Hundred ");
            if (u > 0 || t > 0)
            {
                //if (h > 0 || i == 0) sb.Append("and ");
                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);
            }
            if (i != 0) sb.Append(words3[i - 1]);
        }


        // TextBox2.Text = "Rupees " + sb.ToString().TrimEnd() + " Only";
        return sb.ToString().TrimEnd();
    }
    public string NumberToText1(int number)
    {
        if (number != 0)
        {
            if (number == 0) return "Zero";
            int[] num = new int[4];
            int first = 0;
            int u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (number < 0)
            {
                sb.Append("Minus ");
                number = -number;
            }
            string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
            string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
            string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };
            num[0] = number % 1000; // units
            num[1] = number / 1000;
            num[2] = number / 100000;
            num[1] = num[1] - 100 * num[2]; // thousands
            num[3] = number / 10000000; // crores
            num[2] = num[2] - 100 * num[3]; // lakhs
            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (num[i] == 0) continue;
                u = num[i] % 10; // ones
                t = num[i] / 10;
                h = num[i] / 100; // hundreds
                t = t - 10 * h; // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    //if (h > 0 || i == 0) sb.Append("and ");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            // TextBox2.Text = "Rupees " + sb.ToString().TrimEnd() + " Only";
            if (chkexcel.Checked == true && txtdtors.Text != string.Empty)
            {
                return sb.ToString().TrimEnd() + " Cents";
            }
            else if (chkexcel1.Checked == true && txtdtors.Text != string.Empty)
            {
                return sb.ToString().TrimEnd() + " Cents";
            }
            else
            {
                return sb.ToString().TrimEnd() + " Paisa.";
            }
        }
        else
        {
            return null;
        }
    }

    public String changeToWords(String numb)
    {
        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
        String endStr = ("Only.");
        try
        {
            int decimalPlace = numb.IndexOf(".");
            if (decimalPlace > 0)
            {
                wholeNo = numb.Substring(0, decimalPlace);
                points = numb.Substring(decimalPlace + 1);
                if (Convert.ToInt32(points) > 0)
                {
                    //int p = points.Length;
                    //char[] TPot = points.ToCharArray();
                    andStr = (" and ");// just to separate whole numbers from points/Rupees                  
                    //for(int i=0;i<p;i++)
                    //{
                    //    andStr += ones(Convert.ToString(TPot[i]))+" ";
                    //}
                    andStr += translateWholeNumber(points).Trim() + " Rupees";

                }
            }
            val = String.Format("{0} {1}{2} {3}", translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
        }
        catch
        {
            ;
        }
        return val;
    }

    public String changeToWordsdollar(String numb)
    {
        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
        String endStr = ("Dollar Only.");
        try
        {
            int decimalPlace = numb.IndexOf(".");
            if (decimalPlace > 0)
            {
                wholeNo = numb.Substring(0, decimalPlace);
                points = numb.Substring(decimalPlace + 1);
                if (Convert.ToInt32(points) > 0)
                {
                    //int p = points.Length;
                    //char[] TPot = points.ToCharArray();
                    andStr = (" and ");// just to separate whole numbers from points/Rupees                  
                    //for(int i=0;i<p;i++)
                    //{
                    //    andStr += ones(Convert.ToString(TPot[i]))+" ";
                    //}
                    andStr += translateWholeNumberd(points).Trim();

                }
            }
            val = String.Format("{0} {1}{2} {3}", translateWholeNumberd(wholeNo).Trim(), andStr, pointStr, endStr);
        }
        catch
        {
            ;
        }
        return val;
    }

    public String changeToWordseuro(String numb)
    {
        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
        String endStr = ("Euro Only.");
        try
        {
            int decimalPlace = numb.IndexOf(".");
            if (decimalPlace > 0)
            {
                wholeNo = numb.Substring(0, decimalPlace);
                points = numb.Substring(decimalPlace + 1);
                if (Convert.ToInt32(points) > 0)
                {
                    //int p = points.Length;
                    //char[] TPot = points.ToCharArray();
                    andStr = (" and ");// just to separate whole numbers from points/Rupees                  
                    //for(int i=0;i<p;i++)
                    //{
                    //    andStr += ones(Convert.ToString(TPot[i]))+" ";
                    //}
                    andStr += translateWholeNumberd(points).Trim();

                }
            }
            val = String.Format("{0} {1}{2} {3}", translateWholeNumberd(wholeNo).Trim(), andStr, pointStr, endStr);
        }
        catch
        {
            ;
        }
        return val;
    }

    private String translateWholeNumberd(String number)
    {
        string word = "";
        try
        {
            bool beginsZero = false;//tests for 0XX
            bool isDone = false;//test if already translated
            double dblAmt = (Convert.ToDouble(number));
            //if ((dblAmt > 0) && number.StartsWith("0"))

            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric
                beginsZero = number.StartsWith("0");
                int numDigits = number.Length;
                int pos = 0;//store digit grouping
                String place = "";//digit grouping name:hundres,thousand,etc...
                switch (numDigits)
                {
                    case 1://ones' range
                        word = ones(number);
                        isDone = true;
                        break;
                    case 2://tens' range
                        word = tens(number);
                        isDone = true;
                        break;
                    case 3://hundreds' range
                        pos = (numDigits % 3) + 1;
                        place = " Hundred ";
                        break;
                    case 4://thousands' range
                    case 5:
                        pos = (numDigits % 4) + 1;
                        place = " Thousand ";
                        break;
                    case 6:

                    case 7://millions' range
                        pos = (numDigits % 6) + 1;
                        // place = " Million ";
                        place = " Hundred ";
                        break;
                    case 8:
                    case 9:

                    case 10://Billions's range
                        pos = (numDigits % 8) + 1;
                        place = " Crore ";
                        break;
                    //add extra case options for anything above Billion...
                    default:
                        isDone = true;
                        break;
                }
                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)
                    if (beginsZero) place = "";
                    word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));
                    //check for trailing zeros
                    if (beginsZero) word = " and " + word.Trim();
                }
                //ignore digit grouping names
                if (word.Trim().Equals(place.Trim())) word = "";
            }
        }
        catch
        {
            ;
        }
        return word.Trim();
    }

    private String translateWholeNumber(String number)
    {
        string word = "";
        try
        {
            bool beginsZero = false;//tests for 0XX
            bool isDone = false;//test if already translated
            double dblAmt = (Convert.ToDouble(number));
            //if ((dblAmt > 0) && number.StartsWith("0"))

            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric
                beginsZero = number.StartsWith("0");
                int numDigits = number.Length;
                int pos = 0;//store digit grouping
                String place = "";//digit grouping name:hundres,thousand,etc...
                switch (numDigits)
                {
                    case 1://ones' range
                        word = ones(number);
                        isDone = true;
                        break;
                    case 2://tens' range
                        word = tens(number);
                        isDone = true;
                        break;
                    case 3://hundreds' range
                        pos = (numDigits % 3) + 1;
                        place = " Hundred ";
                        break;
                    case 4://thousands' range
                    case 5:
                        pos = (numDigits % 4) + 1;
                        place = " Thousand ";
                        break;
                    case 6:

                    case 7://millions' range
                        pos = (numDigits % 6) + 1;
                        // place = " Million ";
                        place = " Lakh ";
                        break;
                    case 8:
                    case 9:

                    case 10://Billions's range
                        pos = (numDigits % 8) + 1;
                        place = " Crore ";
                        break;
                    //add extra case options for anything above Billion...
                    default:
                        isDone = true;
                        break;
                }
                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)
                    if (beginsZero) place = "";
                    word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));
                    //check for trailing zeros
                    if (beginsZero) word = " and " + word.Trim();
                }
                //ignore digit grouping names
                if (word.Trim().Equals(place.Trim())) word = "";
            }
        }
        catch
        {
            ;
        }
        return word.Trim();
    }

    private String tens(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = null;
        switch (digt)
        {
            case 10:
                name = "Ten";
                break;
            case 11:
                name = "Eleven";
                break;
            case 12:
                name = "Twelve";
                break;
            case 13:
                name = "Thirteen";
                break;
            case 14:
                name = "Fourteen";
                break;
            case 15:
                name = "Fifteen";
                break;
            case 16:
                name = "Sixteen";
                break;
            case 17:
                name = "Seventeen";
                break;
            case 18:
                name = "Eighteen";
                break;
            case 19:
                name = "Nineteen";
                break;
            case 20:
                name = "Twenty";
                break;
            case 30:
                name = "Thirty";
                break;
            case 40:
                name = "Fourty";
                break;
            case 50:
                name = "Fifty";
                break;
            case 60:
                name = "Sixty";
                break;
            case 70:
                name = "Seventy";
                break;
            case 80:
                name = "Eighty";
                break;
            case 90:
                name = "Ninety";
                break;
            default:
                if (digt > 0)
                {
                    name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));
                }
                break;
        }
        return name;
    }

    private String ones(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = "";
        switch (digt)
        {
            case 1:
                name = "One";
                break;
            case 2:
                name = "Two";
                break;
            case 3:
                name = "Three";
                break;
            case 4:
                name = "Four";
                break;
            case 5:
                name = "Five";
                break;
            case 6:
                name = "Six";
                break;
            case 7:
                name = "Seven";
                break;
            case 8:
                name = "Eight";
                break;
            case 9:
                name = "Nine";
                break;
        }
        return name;
    }

    protected void btnsalesinvoiceexport_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/SalesInvoiceExport.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
}