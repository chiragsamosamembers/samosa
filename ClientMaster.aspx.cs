﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlTypes;

public partial class ClientMaster : System.Web.UI.Page
{
    ForClientMaster fcmclass = new ForClientMaster();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "update")
            {
                fillstatusdrop();
                filleditgrid();
                if (SessionMgt.UserRole == "RK")
                {
                    txtcode.ReadOnly = true;
                    txtname.ReadOnly = true;
                    txtaddress1.ReadOnly = true;
                    txtaddress2.ReadOnly = true;
                    txtaddress3.ReadOnly = true;
                    txtcity.ReadOnly = true;
                    txtpincode.ReadOnly = true;
                    txtemailid.ReadOnly = true;
                    txtmobileno.ReadOnly = true;
                    txtopbalance.ReadOnly = true;
                    txtacname.ReadOnly = true;
                }
            }
            else
            {
                fillstatusdrop();
                getcid();
                txtuser.Text = Request.Cookies["ForLogin"]["username"];
            }
        }
    }

    public void getcid()
    {
        DataTable dtdata = new DataTable();
        dtdata = fcmclass.selectallclientdata();
        if (dtdata.Rows.Count > 0)
        {
            txtcode.Text = (Convert.ToInt64(dtdata.Rows[0]["code"].ToString()) + 1).ToString();
        }
        else
        {
            txtcode.Text = "1";
        }
    }

    public void fillstatusdrop()
    {
        DataTable dtstatus = new DataTable();
        dtstatus = fcmclass.selectallclientstatusdata();
        if (dtstatus.Rows.Count > 0)
        {
            drpstatus.DataSource = dtstatus;
            drpstatus.DataTextField = "name";
            drpstatus.DataBind();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpstatus.Items.Clear();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
    }

    public void filleditgrid()
    {
        li.id = Convert.ToInt64(Request["code"].ToString());
        DataTable dtdata = new DataTable();
        dtdata = fcmclass.selectclientdatafromid(li);
        if (dtdata.Rows.Count > 0)
        {
            txtcode.Text = dtdata.Rows[0]["code"].ToString();
            txtname.Text = dtdata.Rows[0]["name"].ToString();
            ViewState["name"] = dtdata.Rows[0]["name"].ToString();
            txtaddress1.Text = dtdata.Rows[0]["add1"].ToString();
            txtaddress2.Text = dtdata.Rows[0]["add2"].ToString();
            txtaddress3.Text = dtdata.Rows[0]["add3"].ToString();
            txtcity.Text = dtdata.Rows[0]["city"].ToString();
            txtpincode.Text = dtdata.Rows[0]["pincode"].ToString();
            txtmobileno.Text = dtdata.Rows[0]["mobileno"].ToString();
            txtemailid.Text = dtdata.Rows[0]["emailid"].ToString();
            txtopbalance.Text = dtdata.Rows[0]["opbalance"].ToString();
            txtacname.Text = dtdata.Rows[0]["acname"].ToString();
            txtreference.Text = dtdata.Rows[0]["acname"].ToString();
            txthandledby.Text = dtdata.Rows[0]["acname"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
            if (dtdata.Rows[0]["pdate"].ToString().Trim() != string.Empty)
            {
                txtdate.Text = Convert.ToDateTime(dtdata.Rows[0]["pdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            txtremarks.Text = dtdata.Rows[0]["remarks"].ToString();
            ViewState["code"] = dtdata.Rows[0]["id"].ToString();
            //txtcode.ReadOnly = true;
            btnsaves.Text = "Update";

        }
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        SqlDateTime sqldatenull = SqlDateTime.Null;
        li.ccode = Convert.ToInt64(txtcode.Text);
        li.name = txtname.Text;
        li.add1 = txtaddress1.Text;
        li.add2 = txtaddress2.Text;
        li.add3 = txtaddress3.Text;
        li.city = txtcity.Text;
        li.pincode = txtpincode.Text;
        li.mobileno = txtmobileno.Text;
        li.emailid = txtemailid.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        if (txtopbalance.Text.Trim() != string.Empty)
        {
            li.opbalance = Convert.ToDouble(txtopbalance.Text);
        }
        else
        {
            li.opbalance = 0;
        }
        li.acname = txtacname.Text;
        li.referencename = txtreference.Text;
        li.handledby = txthandledby.Text;
        li.status = drpstatus.SelectedItem.Text;
        li.remarks = txtremarks.Text;
        if (txtdate.Text.Trim() != string.Empty)
        {
            li.pdate = Convert.ToDateTime(txtdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.pdate = sqldatenull;
        }
        li.acyear = Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
        if (btnsaves.Text == "Save")
        {
            DataTable dtcheck = new DataTable();
            dtcheck = fcmclass.checkduplicate11(li);
            if (dtcheck.Rows.Count == 0)
            {
                fcmclass.insertclientdata(li);
                li.activity = li.ccode + " New Client Inserted.";
                faclass.insertactivity(li);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Client Code already exists.Please try again with new Client Code.');", true);
                return;
            }
        }
        else
        {
            //if (txtcode.Text.Trim() != ViewState["code"].ToString().Trim())
            //{
            //DataTable dtcheck = new DataTable();
            //dtcheck = fcmclass.checkduplicate11(li);
            //if (dtcheck.Rows.Count == 0)
            //{
            li.id = Convert.ToInt64(ViewState["code"].ToString());
            fcmclass.updateclientdata(li);
            li.activity = li.id + " Client Data Updated.";
            faclass.insertactivity(li);
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Client Code already exists.Please try again with new Client Code.');", true);
            //    return;
            //}
            //}
            //else
            //{
            //    li.ccode = Convert.ToInt64(ViewState["code"].ToString());
            //    fcmclass.updateclientdata(li);
            //}
        }
        Response.Redirect("~/ClientMasterList.aspx?pagename=ClientMasterList");
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

}