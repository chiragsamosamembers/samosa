﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ToolsMaster.aspx.cs" Inherits="ToolsMaster" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Tools Master</span><br />
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Tool Name<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txttoolname" runat="server" CssClass="form-control" Width="250px" onkeypress="return checkQuote();"></asp:TextBox></div>
                        <div class="col-md-2">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" ReadOnly="true"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Unit<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" Width="150px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Rate<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtrate" runat="server" CssClass="form-control" Width="150px" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Opening Stock</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtopeningstock" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Inward</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtinward" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Outward</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtoutward" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Closing Stock</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtclosingstock" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-3">
                        <asp:Button ID="btnsaves" runat="server" Text="Save" class="btn btn-default forbutton"
                            ValidationGroup="valtool" OnClick="btnsaves_Click" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                            ShowSummary="false" ValidationGroup="valtool" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter tool name."
                            Text="*" ValidationGroup="valtool" ControlToValidate="txttoolname"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter unit."
                            Text="*" ValidationGroup="valtool" ControlToValidate="txtunit"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter rate."
                            Text="*" ValidationGroup="valtool" ControlToValidate="txtrate"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
