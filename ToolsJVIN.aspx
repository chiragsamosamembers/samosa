﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ToolsJVIN.aspx.cs" Inherits="ToolsJVIN" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Tool JV->Tool IN</span><br />
        <div class="row">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Challan No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtchallanno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();" AutoPostBack="True" OnTextChanged="txtchallanno_TextChanged"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtjvdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            User</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" ReadOnly="True"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Name<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="432px" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetAccountname">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-4 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Tool Name<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Tool Qty<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Rate<span style="color: Red;">*</span></label>
            </div>
            <div class="col-md-3 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-4" style="border-right: 1px solid;">
                <asp:TextBox ID="txttoolname" runat="server" CssClass="form-control" placeholder="Tool Name"
                    Width="350px" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txttoolname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetToolname">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txttoolqty" runat="server" CssClass="form-control" placeholder="Tool Qty"
                    Width="90px" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtrate" runat="server" CssClass="form-control" placeholder="Rate"
                    Width="90px" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-3" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription" runat="server" CssClass="form-control" placeholder="Description"
                    Width="300px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:Button ID="btnsaves" runat="server" Text="ADD" Height="30px" BackColor="#F05283"
                    ValidationGroup="gvtooljvin" OnClick="btnsaves_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="gvtooljvin" />
            </div>
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="250px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvtool" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowDeleting="gvtool_RowDeleting">
                        <Columns>
                            <%-- <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvdescr" runat="server" CssClass="form-control" Text='<%# bind("descr") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <asp:Button ID="btnsaveall" runat="server" Text="Save" Height="30px" BackColor="#F05283"
            ValidationGroup="tooljvin" OnClick="btnsaveall_Click" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
            ShowSummary="false" ValidationGroup="tooljvin" />
        <div class="row">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter challan no."
                Text="*" ValidationGroup="tooljvin" ControlToValidate="txtchallanno"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter date."
                Text="*" ValidationGroup="tooljvin" ControlToValidate="txtjvdate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter name."
                Text="*" ValidationGroup="tooljvin" ControlToValidate="txtname"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter Tool name."
                Text="*" ValidationGroup="gvtooljvin" ControlToValidate="txttoolname"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter Tool qty."
                Text="*" ValidationGroup="gvtooljvin" ControlToValidate="txttoolqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter rate."
                Text="*" ValidationGroup="gvtooljvin" ControlToValidate="txtrate"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Jv Date must be dd-MM-yyyy" ValidationGroup="tooljvin" ForeColor="Red"
                ControlToValidate="txtjvdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtjvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtjvdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtjvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });
        });
    </script>
</asp:Content>
