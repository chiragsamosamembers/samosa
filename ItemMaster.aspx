﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ItemMaster.aspx.cs" Inherits="ItemMaster" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Item Master</span><br />
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Item Name<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtitemname" runat="server" CssClass="form-control" Width="250px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" ReadOnly="true"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Description</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtdescription" runat="server" CssClass="form-control" Width="250px"
                            Height="40px" TextMode="MultiLine" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Unit<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" Width="150px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Unit (eWay Bill)<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="drpunitewaybill" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            HSN Code</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txthsncode" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Sales Rate</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtsalesrate" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Purchase Rate</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtpurchaserate" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Valuation Rate</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtvaluationrate" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Vat Type<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtvattyppe" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtvattyppe"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getvattype">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            GST Type</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtgsttype" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtvattyppe"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getvattype">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Vat Desc</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtvatdesc" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtvattyppe"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getvattype">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            GST Desc</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtgstdesc" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txtvattyppe"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getvattype">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Reason</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtrreason" runat="server" CssClass="form-control" Width="250px"
                            Height="40px" TextMode="MultiLine" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Block</label></div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkblock" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-3">
                        <asp:Button ID="btnsaves" runat="server" Text="Save" class="btn btn-default forbutton"
                            ValidationGroup="valitem" OnClick="btnsaves_Click" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                            ShowSummary="false" ValidationGroup="valitem" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter item name."
                            Text="*" ValidationGroup="valitem" ControlToValidate="txtitemname"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter unit."
                            Text="*" ValidationGroup="valitem" ControlToValidate="txtunit"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please select Unit (eWay Bill)."
                            Text="*" ValidationGroup="valitem" ControlToValidate="drpunitewaybill" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter sales rate."
                            Text="*" ValidationGroup="valitem" ControlToValidate="txtsalesrate"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter purchase rate."
                            Text="*" ValidationGroup="valitem" ControlToValidate="txtpurchaserate"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter valuation rate."
                            Text="*" ValidationGroup="valitem" ControlToValidate="txtvaluationrate"></asp:RequiredFieldValidator>--%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter vat type."
                            Text="*" ValidationGroup="valitem" ControlToValidate="txtvattyppe"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="border: 2px solid red !important">
                <span style="color: white; background-color: Red">Current Stock Status</span><br />
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">
                            Opening Stock</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtopeningstock" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">
                            OP. Stock Value</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtopstockvalue" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">
                            Inward</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtinward" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">
                            Outward</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtoutward" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">
                            Closing Stock</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtclosingstock" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">
                            CL. Stock Value</label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtclstockvalue" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
            </div>
        </div>
        <span style="color: white; background-color: Red">Opening Stock Updation</span><br />
        <div class="row">
            <div class="col-md-1">
                Op. Date</div>
            <div class="col-md-2">
                <asp:TextBox ID="txtopdate" runat="server" CssClass="form-control"></asp:TextBox></div>
            <div class="col-md-1">
                Op. Qty.</div>
            <div class="col-md-1">
                <asp:TextBox ID="txtopqty" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnTextChanged="txtopqty_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
            <div class="col-md-1">
                Op. Rate</div>
            <div class="col-md-1">
                <asp:TextBox ID="txtoprate" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnTextChanged="txtoprate_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
            <div class="col-md-1">
                Op. Amount</div>
            <div class="col-md-1">
                <asp:TextBox ID="txtopamount" runat="server" CssClass="form-control" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
            <div class="col-md-1">
                <asp:Button ID="btnopitem" runat="server" Text="Add" class="btn btn-default forbutton"
                    OnClick="btnopitem_Click" ValidationGroup="itemmaster" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="itemmaster" />
            </div>
            <div class="row">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter op.date."
                    Text="*" ValidationGroup="itemmaster" ControlToValidate="txtopdate"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="OP Date must be dd-MM-yyyy" ValidationGroup="itemmaster" ForeColor="Red"
                    ControlToValidate="txtopdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter op.qty."
                    Text="*" ValidationGroup="itemmaster" ControlToValidate="txtopqty"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter op.rate."
                    Text="*" ValidationGroup="itemmaster" ControlToValidate="txtoprate"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please enter op.amount."
                    Text="*" ValidationGroup="itemmaster" ControlToValidate="txtopamount"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="150px" Width="98%">
                    <asp:Label ID="lblemptyop" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvitemop" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvitemop_RowCommand" OnRowDeleting="gvitemop_RowDeleting">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbloprate" ForeColor="Black" runat="server" Text='<%# bind("opdate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-5">
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lbltotopqty" runat="server" Text="Label" Font-Bold="true" Font-Size="Larger"></asp:Label>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lbltotopamount" runat="server" Text="Label" Font-Bold="true" Font-Size="Larger"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtopdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtopdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtopdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
