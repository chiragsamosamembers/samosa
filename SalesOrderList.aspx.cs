﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class SalesOrderList : System.Web.UI.Page
{
    ForSalesOrder fsoclass = new ForSalesOrder();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fsoclass.selectallsalesorderdata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvso.Visible = true;
            gvso.DataSource = dtdata;
            gvso.DataBind();
        }
        else
        {
            gvso.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Sales Order Found.";
        }
    }

    protected void btnso_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/SalesOrder.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvso_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.sono = Convert.ToInt64(e.CommandArgument);
                Response.Redirect("~/SalesOrder.aspx?mode=update&sono=" + li.sono + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "print")
        {
            //if (ViewState["uedit"].ToString() == "Y")
            //{
            string Xrepname = "sales order";
            string sono = e.CommandArgument.ToString();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?pono=" + sono + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            //Response.Redirect("~/SalesOrder.aspx?mode=update&sono=" + li.sono + "");
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            //}
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.sono = Convert.ToInt64(e.CommandArgument);

                DataTable dtcheck1 = new DataTable();
                dtcheck1 = fsoclass.selectpodatafromsono(li);
                if (dtcheck1.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Sales Order Used in Purchse Order No. " + dtcheck1.Rows[0]["strscno"].ToString() + " So You Cant delete this Sales Order.First delete that Challan then try to delete this Sales Order.');", true);
                    return;
                }

                DataTable dtcheck = new DataTable();
                dtcheck = fsoclass.selectchallandatafromsono(li);
                if (dtcheck.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Sales Order Used in Sales Challan No. " + dtcheck.Rows[0]["strscno"].ToString() + " So You Cant delete this Sales Order.First delete that Challan then try to delete this Sales Order.');", true);
                    return;
                }
                else
                {
                    DataTable dtso = new DataTable();
                    dtso = fsoclass.selectallsalesorderdatafromsono(li);
                    if (dtso.Rows.Count > 0)
                    {
                        if (dtso.Rows[0]["quono"].ToString() != "0")
                        {
                            li.quono = Convert.ToDouble(dtso.Rows[0]["quono"].ToString());
                            fsoclass.updateissodatainbifurcation1(li);
                        }
                    }
                    fsoclass.deletesomasterdata(li);
                    fsoclass.deletesoitemsdatafromsono(li);
                    fsoclass.updateisusedn(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.sono + " Sales Order Deleted.";
                    faclass.insertactivity(li);
                    fillgrid();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvso_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvso.PageIndex = e.NewPageIndex;
        fillgrid();
    }
}