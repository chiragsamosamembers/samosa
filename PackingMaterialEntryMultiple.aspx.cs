﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlTypes;


public partial class PackingMaterialEntryMultiple : System.Web.UI.Page
{
    ForPackingMaterial fpmclass = new ForPackingMaterial();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            if (Request["mode"].ToString() == "insert")
            {
                getbrno();
                txtvdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemspme"] = CreateTemplate();
                filltransportname();
            }
            else
            {
                filltransportname();
                filleditdata();
            }

        }
    }

    public void filltransportname()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpmclass.selectalltransportname(li);
        if (dtdata.Rows.Count > 0)
        {
            drptransportname.DataSource = dtdata;
            drptransportname.DataTextField = "name";
            drptransportname.DataBind();
            drptransportname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drptransportname.Items.Clear();
            drptransportname.Items.Insert(0,"--SELECT--");
        }
    }

    public void getbrno()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtvno = new DataTable();
        dtvno = fpmclass.selectlastpmvno(li);//selectunusedpmvno(li);
        if (dtvno.Rows.Count > 0)
        {
            txtvoucherno.Text = (Convert.ToInt64(dtvno.Rows[0]["voucherno"].ToString())+1).ToString();
        }
        else
        {
            txtvoucherno.Text = "4001";
        }
    }

    public void filleditdata()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.voucherno = Convert.ToInt64(Request["vno"].ToString());
        DataTable dtdata = new DataTable();
        dtdata = fpmclass.selectallpmmasterdatafromvoucherno(li);
        if (dtdata.Rows.Count > 0)
        {
            txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
            txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["vdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtname.Text = dtdata.Rows[0]["name"].ToString();
            txtclientcode.Text = dtdata.Rows[0]["clientcode"].ToString();
            //txtdcno.Text = dtdata.Rows[0]["dcno"].ToString();
            txtreturnablematerialchallanno.Text = dtdata.Rows[0]["rmcno"].ToString();
            txttoolsdeliverychallanno.Text = dtdata.Rows[0]["tdcno"].ToString();
            drptransportname.SelectedValue = dtdata.Rows[0]["transportname"].ToString();
            txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
            if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty && dtdata.Rows[0]["lrdate"].ToString() != "" && dtdata.Rows[0]["lrdate"].ToString() != null)
            {
                txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            }
            txtvehicleno.Text = dtdata.Rows[0]["vehicleno"].ToString();
            txtdrivername.Text = dtdata.Rows[0]["drivername"].ToString();
            txtdriverlicenseno.Text = dtdata.Rows[0]["dlicenseno"].ToString();
            txtdrivermobileno.Text = dtdata.Rows[0]["mobileno"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            txtmateriall.Text = dtdata.Rows[0]["materiall"].ToString();
            txtmaterialr.Text = dtdata.Rows[0]["materialr"].ToString();
            //txtremarks.Text = dtdata.Rows[0]["remarks"].ToString();
            DataTable dtitems = new DataTable();
            dtitems = fpmclass.selectallpmitemsdata(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitems;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Packing Material Data Found.";
            }
            btnsaveall.Text = "Update";
        }
        else
        {
            dtdata = fpmclass.selectallpmmasterdatafromvoucherno2(li);
            if (dtdata.Rows.Count > 0)
            {
                txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
                txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["vdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                txtname.Text = dtdata.Rows[0]["name"].ToString();
                txtclientcode.Text = dtdata.Rows[0]["clientcode"].ToString();
                //txtdcno.Text = dtdata.Rows[0]["dcno"].ToString();
                txtreturnablematerialchallanno.Text = dtdata.Rows[0]["rmcno"].ToString();
                txttoolsdeliverychallanno.Text = dtdata.Rows[0]["tdcno"].ToString();
                drptransportname.SelectedValue = dtdata.Rows[0]["transportname"].ToString();
                txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
                if (dtdata.Rows[0]["lrdate"].ToString() != string.Empty && dtdata.Rows[0]["lrdate"].ToString() != "" && dtdata.Rows[0]["lrdate"].ToString() != null)
                {
                    txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
                }
                txtvehicleno.Text = dtdata.Rows[0]["vehicleno"].ToString();
                txtdrivername.Text = dtdata.Rows[0]["drivername"].ToString();
                txtdriverlicenseno.Text = dtdata.Rows[0]["dlicenseno"].ToString();
                txtdrivermobileno.Text = dtdata.Rows[0]["mobileno"].ToString();
                txtmateriall.Text = dtdata.Rows[0]["materiall"].ToString();
                txtmaterialr.Text = dtdata.Rows[0]["materialr"].ToString();
                DataTable dtitems = new DataTable();
                dtitems = fpmclass.selectallpmitemsdata(li);
                if (dtitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    rptlist.Visible = true;
                    rptlist.DataSource = dtitems;
                    rptlist.DataBind();
                }
                else
                {
                    rptlist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Packing Material Data Found.";
                }
                btnsaveall.Text = "Update";
            }
        }
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemspme"] as DataTable;
        rptlist.DataSource = dtsession;
        rptlist.DataBind();
    }

    protected void rptlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = Session["dtpitemspme"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemspme"] = dt;
            this.bindgrid();
        }
        else
        {
            if (rptlist.Rows.Count > 1)
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)rptlist.Rows[e.RowIndex].FindControl("lblid");
                Label lbldcno = (Label)rptlist.Rows[e.RowIndex].FindControl("lbldcno");
                li.id = Convert.ToInt64(lblid.Text);

                if (rptlist.Rows.Count == 1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Enter Other Delivery Challan No. and Try Again.');", true);
                    return;
                }
                else
                {

                    fpmclass.deletepmitemsedatafromid(li);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.id + " Packing Material Item Deleted.";
                    faclass.insertactivity(li);
                    li.strscno = lbldcno.Text;
                    fpmclass.updateispackinginscmastersetnull(li);
                    li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                    DataTable dtitem = new DataTable();
                    dtitem = fpmclass.selectallpmmasterdatafromvoucherno(li);
                    if (dtitem.Rows.Count > 0)
                    {
                        lblempty.Visible = false;
                        rptlist.Visible = true;
                        rptlist.DataSource = dtitem;
                        rptlist.DataBind();
                    }
                    else
                    {
                        rptlist.Visible = false;
                        lblempty.Visible = true;
                        lblempty.Text = "No Item Data Found.";
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("dcno", typeof(string));
        return dtpitems;
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemspme"];
            DataRow dr = dt.NewRow();
            dr["id"] = 1;
            dr["dcno"] = txtdcno.Text;
            dt.Rows.Add(dr);
            Session["dtpitemspme"] = dt;
            this.bindgrid();
            txtdcno.Text = string.Empty;
        }
        else
        {
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            li.voucherdate = Convert.ToDateTime(txtvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.dcno1 = txtdcno.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.istype = "TD";
            fpmclass.insertpmitemserdeldata(li);
            li.activity = li.voucherno + " Packig Material Item Inserted.";
            faclass.insertactivity(li);
            DataTable dtitem = new DataTable();
            dtitem = fpmclass.selectallpmitemsdata(li);
            if (dtitem.Rows.Count > 0)
            {
                lblempty.Visible = false;
                rptlist.Visible = true;
                rptlist.DataSource = dtitem;
                rptlist.DataBind();
            }
            else
            {
                rptlist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Item Data Found.";
            }
        }
        txtdcno.Text = string.Empty;
    }
    protected void btnsaveall_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime sqldatenull = SqlDateTime.Null;
        li.voucherno = Convert.ToInt64(txtvoucherno.Text);
        li.voucherdate = Convert.ToDateTime(txtvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.voucherdate <= yyyear1 && li.voucherdate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        li.name = txtname.Text;
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            li.ccode = Convert.ToInt64(txtclientcode.Text);
        }
        else
        {
            li.ccode = 0;
        }
        //li.dcno = Convert.ToInt64(txtdcno.Text);
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            li.strrmcno = txtreturnablematerialchallanno.Text;
        }
        else
        {
            li.strrmcno = "0";
        }
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            li.strtdcno = txttoolsdeliverychallanno.Text;
        }
        else
        {
            li.strtdcno = "0";
        }
        li.transportname = drptransportname.SelectedItem.Text;
        li.lrno = txtlrno.Text;
        if (txtlrdate.Text.Trim() != string.Empty)
        {
            li.lrdate = Convert.ToDateTime(txtlrdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }
        else
        {
            li.lrdate = sqldatenull;
        }
        li.vehicleno = txtvehicleno.Text;
        li.drivername = txtdrivername.Text;
        li.licenseno = txtdriverlicenseno.Text;
        li.mobileno = txtdrivermobileno.Text;
        li.materiall = txtmateriall.Text;
        li.materialr = txtmaterialr.Text;
        li.istype = "M";
        //li.remarks = txtremarks.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        if (btnsaveall.Text == "Save")
        {
            //if (rptlist.Rows.Count > 0)
            //{
            DataTable dtcheck = new DataTable();
            dtcheck = fpmclass.selectallpmmasterdatafromvoucherno(li);
            if (dtcheck.Rows.Count == 0)
            {
                fpmclass.insertpmmasterdeldata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.voucherno + " Packing Material Data Inserted.";
                faclass.insertactivity(li);
                li.ispacking = "Y";
                li.strscno = txtreturnablematerialchallanno.Text;
                fpmclass.updateispackinginscmaster(li);
                if (txttoolsdeliverychallanno.Text.Trim() != string.Empty)
                {
                    li.vnono1 = Convert.ToInt64(txttoolsdeliverychallanno.Text);
                }
                else
                {
                    li.vnono1 = 0;
                }
                fpmclass.updateispackingintoodelmaster(li);
            }
            else
            {
                li.voucherno = Convert.ToInt64(txtvoucherno.Text);
                fpmclass.updateisused(li);
                getbrno();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Voucher No. already exist.Try Again.');", true);
                return;
            }
            li.voucherno = Convert.ToInt64(txtvoucherno.Text);
            for (int c = 0; c < rptlist.Rows.Count; c++)
            {
                Label lbldcno = (Label)rptlist.Rows[c].FindControl("lbldcno");
                li.dcno1 = lbldcno.Text;
                li.strscno = li.dcno1.ToString();
                fpmclass.insertpmitemserdeldata(li);
                li.ispacking = "Y";
                fpmclass.updateispackinginscmaster(li);
            }
            fpmclass.updateisused(li);
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Delivery Challan No.');", true);
            //    return;
            //}
        }
        else
        {
            li.voucherno = Convert.ToInt64(Request["vno"].ToString());
            fpmclass.updatepmmasterdeldata(li);
            fpmclass.updatepmitemsdatedeldata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.voucherno + " Packing Material Data Updated.";
            faclass.insertactivity(li);
            for (int c = 0; c < rptlist.Rows.Count; c++)
            {
                Label lbldcno = (Label)rptlist.Rows[c].FindControl("lbldcno");
                li.dcno1 = lbldcno.Text;
                li.strscno = li.dcno1.ToString();
                //fpmclass.insertpmitemserdeldata(li);
                li.ispacking = "Y";
                fpmclass.updateispackinginscmaster(li);
            }
            // fpmclass.updatepmitemsdeldata(li);
        }
        Response.Redirect("~/PackingMaterialEntryMultipleList.aspx?pagename=PackingMaterialEntryMultipleList");
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getacname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallacnamegeneral(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getrrcno(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallrrcno(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getdelcno(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectalldelcno(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtclientcode.Text = string.Empty;
        }
    }

    protected void lnkselectionpopup_Click(object sender, EventArgs e)
    {
        if (txtname.Text.Trim() != string.Empty)
        {
            lblpopupacname.Text = txtname.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = txtname.Text;
            ////DataTable dtso = new DataTable();
            ////dtso = fsiclass.selectallsalesorderforpopup(li);
            ////if (dtso.Rows.Count > 0)
            ////{
            ////    lblemptyso.Visible = false;
            ////    gvsalesorders.Visible = true;
            ////    gvsalesorders.DataSource = dtso;
            ////    gvsalesorders.DataBind();
            ////}
            ////else
            ////{
            ////    gvsalesorders.Visible = false;
            ////    lblemptyso.Visible = true;
            ////    lblemptyso.Text = "No Sales Order Found.";
            ////}

            DataTable dtsc = new DataTable();
            dtsc = fpmclass.selectallsaleschallanforpopupfort(li);
            if (dtsc.Rows.Count > 0)
            {
                lblemptysc.Visible = false;
                gvsaleschallans.Visible = true;
                gvsaleschallans.DataSource = dtsc;
                gvsaleschallans.DataBind();
            }
            else
            {
                gvsaleschallans.Visible = false;
                lblemptysc.Visible = true;
                lblemptysc.Text = "No Sales Challan Found.";
            }
            ModalPopupExtender2.Show();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Name and Try Again.');", true);
            return;
        }
    }

    protected void gvsaleschallans_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            txtdcno.Text = e.CommandArgument.ToString();
            ModalPopupExtender2.Hide();
        }
    }

    protected void lnkselectionpopupr_Click(object sender, EventArgs e)
    {
        if (txtname.Text.Trim() != string.Empty)
        {
            lblpopupacnamer.Text = txtname.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = txtname.Text;
            ////DataTable dtso = new DataTable();
            ////dtso = fsiclass.selectallsalesorderforpopup(li);
            ////if (dtso.Rows.Count > 0)
            ////{
            ////    lblemptyso.Visible = false;
            ////    gvsalesorders.Visible = true;
            ////    gvsalesorders.DataSource = dtso;
            ////    gvsalesorders.DataBind();
            ////}
            ////else
            ////{
            ////    gvsalesorders.Visible = false;
            ////    lblemptyso.Visible = true;
            ////    lblemptyso.Text = "No Sales Order Found.";
            ////}

            DataTable dtsc = new DataTable();
            dtsc = fpmclass.selectallsaleschallanforpopupforrm(li);
            if (dtsc.Rows.Count > 0)
            {
                lblemptyscr.Visible = false;
                gvsaleschallansr.Visible = true;
                gvsaleschallansr.DataSource = dtsc;
                gvsaleschallansr.DataBind();
            }
            else
            {
                gvsaleschallansr.Visible = false;
                lblemptyscr.Visible = true;
                lblemptyscr.Text = "No Returnable Material Challan Found.";
            }
            ModalPopupExtender1.Show();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Name and Try Again.');", true);
            return;
        }
    }

    protected void gvsaleschallansr_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            txtreturnablematerialchallanno.Text = e.CommandArgument.ToString();
            ModalPopupExtender1.Hide();
        }
    }

    protected void lnkselectionpopupt_Click(object sender, EventArgs e)
    {
        if (txtname.Text.Trim() != string.Empty)
        {
            lblpopupacnamet.Text = txtname.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.acname = txtname.Text;
            ////DataTable dtso = new DataTable();
            ////dtso = fsiclass.selectallsalesorderforpopup(li);
            ////if (dtso.Rows.Count > 0)
            ////{
            ////    lblemptyso.Visible = false;
            ////    gvsalesorders.Visible = true;
            ////    gvsalesorders.DataSource = dtso;
            ////    gvsalesorders.DataBind();
            ////}
            ////else
            ////{
            ////    gvsalesorders.Visible = false;
            ////    lblemptyso.Visible = true;
            ////    lblemptyso.Text = "No Sales Order Found.";
            ////}

            DataTable dtsc = new DataTable();
            dtsc = fpmclass.selectallsaleschallanforpopupfortagain(li);
            if (dtsc.Rows.Count > 0)
            {
                lblemptysct.Visible = false;
                gvsaleschallanst.Visible = true;
                gvsaleschallanst.DataSource = dtsc;
                gvsaleschallanst.DataBind();
            }
            else
            {
                gvsaleschallanst.Visible = false;
                lblemptysct.Visible = true;
                lblemptysct.Text = "No Tools Delivery Challan Found.";
            }
            ModalPopupExtender3.Show();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Name and Try Again.');", true);
            return;
        }
    }

    protected void gvsaleschallanst_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            txttoolsdeliverychallanno.Text = e.CommandArgument.ToString();
            ModalPopupExtender3.Hide();
        }
    }

    //s , a , r

}