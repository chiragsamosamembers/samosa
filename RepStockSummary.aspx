﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RepStockSummary.aspx.cs" Inherits="RepStockSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Stock Summary Report</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    From Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox>
                <%--<asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txtfromdate">
                </asp:MaskedEditExtender>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter from date."
                    Text="*" ControlToValidate="txtfromdate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="From Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txtfromdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    To Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
                <%-- <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txttodate">
                </asp:MaskedEditExtender>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter to date."
                    Text="*" ControlToValidate="txttodate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="To Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txttodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator></div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnsaves" runat="server" Text="Preview" class="btn btn-default forbutton"
                    OnClick="btnsaves_Click" ValidationGroup="val" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <%-- <div class="row">
            <div class="col-md-12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter account name."
                    Text="*" ValidationGroup="val" ControlToValidate="txtacname"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter voucher no."
                    Text="*" ValidationGroup="val" ControlToValidate="txtvono"></asp:RequiredFieldValidator>
            </div>
        </div>--%>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtfromdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


            $("#<%= txttodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
