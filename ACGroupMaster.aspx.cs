﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class ACGroupMaster : System.Web.UI.Page
{
    ForACGroupMaster fagclass = new ForACGroupMaster();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
            //DataTable dt = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 order by id");
            //this.PopulateTreeView(dt, 0, null);
            DataTable dt = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 where type='Debit' and id between 99 and 300 order by id");
            this.PopulateTreeView(dt, 0, null);

            DataTable dt1 = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 where type='Credit' and id between 99 and 300 order by id");
            this.PopulateTreeView1(dt1, 0, null);
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.type = RadioButtonList1.SelectedItem.Text;
        DataTable dtdata = new DataTable();
        dtdata = fagclass.selectaccountgroupdatafromtype(li);
        DataTable dtdata1 = new DataTable();
        dtdata1 = fagclass.selectaccountgroupdatafromtype1(li);
        dtdata.Merge(dtdata1);
        DataView dtdv = dtdata.DefaultView;
        dtdv.Sort = "id asc";
        //DataTable dtdt = new DataTable();
        //dtdt = dtdv.ToTable();
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            rptlist.Visible = true;
            rptlist.DataSource = dtdv;
            rptlist.DataBind();
        }
        else
        {
            rptlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No AC Group Master Data Found.";
        }
    }

    private DataTable GetData(string query)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        DataTable dt = new DataTable();
        string constr = cz;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand(query))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    sda.Fill(dt);
                }
            }
            return dt;
        }
    }

    private void PopulateTreeView(DataTable dtParent, int parentId, TreeNode treeNode)
    {
        if (dtParent.Rows.Count > 0)
        {
            foreach (DataRow row in dtParent.Rows)
            {
                TreeNode child = new TreeNode
                {
                    Text = "- " + row["groupcode"].ToString() + " " + row["groupname"].ToString(),
                    Value = row["groupcode"].ToString()
                };
                if (parentId == 0)
                {
                    TreeView1.Nodes.Add(child);
                    DataTable dtChild = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster WHERE mastercode= " + child.Value + " and type='Debit' order by id");
                    PopulateTreeView(dtChild, int.Parse(child.Value), child);
                }
                else
                {
                    treeNode.ChildNodes.Add(child);
                }
            }
        }
        else
        {
            TreeView1.Nodes.Clear();
        }
        if (rdoexco.SelectedItem.Text == "Full Expand")
        {
            TreeView1.ExpandAll();
        }
        else
        {
            TreeView1.CollapseAll();
        }
    }

    private void PopulateTreeView1(DataTable dtParent, int parentId, TreeNode treeNode)
    {
        TreeView2.Nodes.Clear();
        if (dtParent.Rows.Count > 0)
        {
            foreach (DataRow row in dtParent.Rows)
            {
                TreeNode child = new TreeNode
                {
                    Text = "- " + row["groupcode"].ToString() + " " + row["groupname"].ToString(),
                    Value = row["groupcode"].ToString()
                };
                if (parentId == 0)
                {
                    TreeView2.Nodes.Add(child);
                    DataTable dtChild = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster WHERE mastercode= " + child.Value + " and type='Credit' order by id");
                    PopulateTreeView(dtChild, int.Parse(child.Value), child);
                }
                else
                {
                    treeNode.ChildNodes.Add(child);
                }
            }
        }
        else
        {
            TreeView2.Nodes.Clear();
        }
        if (rdoexco.SelectedItem.Text == "Full Expand")
        {
            TreeView2.ExpandAll();
        }
        else
        {
            TreeView2.CollapseAll();
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            li.id = Convert.ToInt64(txtgroupcode.Text);
            li.groupcode = txtgroupcode.Text;
            li.groupname = txtgroupname.Text;
            li.mastercode = txtgroupcode.Text.Substring(0, 2) + "0";
            li.type = RadioButtonList1.SelectedItem.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            if (li.groupcode == li.mastercode)
            {
                fagclass.insertaccountdata1(li);
            }
            else
            {
                fagclass.insertaccountdata(li);
            }
            li.activity = li.groupcode + "-" + li.groupname + "-" + li.mastercode + "-" + " New Account Group Name Inserted.";
            faclass.insertactivity(li);
            fillgrid();
            //TreeView1.Nodes.Clear();
            //TreeView2.Nodes.Clear();
            //DataTable dt = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 order by id");
            //this.PopulateTreeView(dt, 0, null);
            DataTable dt = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 where type='Debit' and id between 99 and 300 order by id");
            this.PopulateTreeView(dt, 0, null);
            DataTable dt1 = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 where type='Credit' and id between 99 and 300 order by id");
            this.PopulateTreeView1(dt1, 0, null);
            txtgroupcode.Text = string.Empty;
            txtgroupname.Text = string.Empty;
            Page.SetFocus(txtgroupcode);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void btntrading_Click(object sender, EventArgs e)
    {
        TreeView1.Nodes.Clear();
        TreeView2.Nodes.Clear();
        DataTable dt = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 where type='Debit' and id between 100 and 299 order by id");
        this.PopulateTreeView(dt, 0, null);
        DataTable dt1 = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 where type='Credit' and id between 100 and 299 order by id");
        this.PopulateTreeView1(dt1, 0, null);
    }
    protected void btnprofit_Click(object sender, EventArgs e)
    {
        TreeView1.Nodes.Clear();
        TreeView2.Nodes.Clear();
        DataTable dt = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 where type='Debit' and id between 300 and 499 order by id");
        this.PopulateTreeView(dt, 0, null);
        DataTable dt1 = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 where type='Credit' and id between 300 and 499 order by id");
        this.PopulateTreeView1(dt1, 0, null);
    }
    protected void btnbalance_Click(object sender, EventArgs e)
    {
        TreeView1.Nodes.Clear();
        TreeView2.Nodes.Clear();
        DataTable dt = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 where type='Debit' and id>=500 order by id");
        this.PopulateTreeView(dt, 0, null);
        DataTable dt1 = this.GetData("SELECT groupcode,groupname FROM ACGroupMaster1 where type='Credit' and id>=500 order by id");
        this.PopulateTreeView1(dt1, 0, null);
    }
    protected void rdoexco_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdoexco.SelectedItem.Text == "Full Expand")
        {
            TreeView1.ExpandAll();
            TreeView2.ExpandAll();
        }
        else
        {
            TreeView1.CollapseAll();
            TreeView2.CollapseAll();
        }
    }
}