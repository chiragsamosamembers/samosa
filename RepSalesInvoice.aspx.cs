﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class RepSalesInvoice : System.Web.UI.Page
{
    public ReportDocument rptdoc;
    LogicLayer li = new LogicLayer();
    ForReport frclass = new ForReport();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getsino(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        li.acname = SessionMgt.acname;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallsinoacnamewisestring(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][24].ToString());
        }
        return CountryNames;
    }
    protected void txtacname_TextChanged(object sender, EventArgs e)
    {
        if (txtacname.Text != string.Empty)
        {
            SessionMgt.acname = txtacname.Text;
        }
    }
    protected void btnsaves_Click(object sender, EventArgs e)
    {
        ////string Xrepname = "sales invoice";
        ////Int64 sino = Convert.ToInt64(txtsalesinvoiceno.Text);
        //////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ////ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?sino=" + sino + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        rptdoc = new ReportDocument();
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        string ftow = "";
        li.strsino = txtsalesinvoiceno.Text;
        DataSet1 imageDataSet = new DataSet1();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        SqlDataAdapter da11 = new SqlDataAdapter("select * from SIMaster inner join SIItems on SIMaster.strsino=SIItems.strsino inner join ACMaster on ACMaster.acname=SIMaster.acname inner join ItemMaster on ItemMaster.itemname=SIItems.itemname where SIMaster.strsino='" + li.strsino + "' and SIMaster.cno='" + li.cno + "'", con);
        DataTable dtc = new DataTable();
        da11.Fill(imageDataSet.Tables["DataTable4"]);
        if (imageDataSet.Tables["DataTable4"].Rows.Count > 0)
        {
            string amount = imageDataSet.Tables["DataTable4"].Rows[0]["billamount"].ToString();
            ftow = changeToWords(amount);
        }
        li.acname = imageDataSet.Tables["DataTable4"].Rows[0]["acname"].ToString();
        li.ccode = Convert.ToInt64(imageDataSet.Tables["DataTable4"].Rows[0]["ccode"].ToString());
        con.Open();
        DataTable dtacname = new DataTable();
        SqlDataAdapter das = new SqlDataAdapter("select * from ACMaster where acname=@acname and cno=@cno", con);
        das.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
        das.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
        das.Fill(dtacname);
        con.Close();

        con.Open();
        DataTable dtclient = new DataTable();
        SqlDataAdapter das1 = new SqlDataAdapter("select * from ClientMaster where code=@ccode and cno=@cno", con);
        das1.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
        das1.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
        das1.Fill(dtclient);
        con.Close();
        string address = dtacname.Rows[0]["add1"].ToString() + " " + dtacname.Rows[0]["add2"].ToString() + " " + dtacname.Rows[0]["add3"].ToString();
        string rptname;
        //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
        //{
        if (Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["cstamt"].ToString()) > 0)
        {
            rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport_CST.rpt");
        }
        else if (Convert.ToDouble(imageDataSet.Tables["DataTable4"].Rows[0]["vatamt"].ToString()) > 0)
        {
            rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport.rpt");
        }
        else
        {
            rptname = Server.MapPath(@"~/Reports/SalesInvoiceReport.rpt");
        }
        //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
        //}
        //else
        //{
        //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
        //    {
        //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
        //    }
        //    else
        //    {
        //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
        //    }
        //}
        rptdoc.Load(rptname);
        if (dtacname.Rows.Count > 0)
        {

        }
        //string title = "";
        //if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
        //{
        //    title = "Tax Invoice";
        //    rptdoc.DataDefinition.FormulaFields["rtitle"].Text = "'" + title + "'";
        //}
        //else
        //{
        //    title = "Retail Invoice";
        //    rptdoc.DataDefinition.FormulaFields["rtitle"].Text = "'" + title + "'";
        //}
        string gst = dtacname.Rows[0]["gsttinno"].ToString() + "    " + dtacname.Rows[0]["date1"].ToString();
        string cst = dtacname.Rows[0]["csttinno"].ToString() + "    " + dtacname.Rows[0]["date2"].ToString();
        string ccode = dtclient.Rows[0]["name"].ToString();//dtclient.Rows[0]["code"].ToString() + "  " +
        string nn = imageDataSet.Tables["DataTable4"].Rows[0]["sino"].ToString();
        if (nn.Length == 1)
        {
            nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + "00" + nn;
        }
        else if (nn.Length == 2)
        {
            nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + "0" + nn;
        }
        else
        {
            nn = imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() + nn;
        }
        rptdoc.DataDefinition.FormulaFields["gsttin"].Text = "'" + gst + "'";
        rptdoc.DataDefinition.FormulaFields["csttin"].Text = "'" + cst + "'";
        rptdoc.DataDefinition.FormulaFields["gstno"].Text = "'" + dtacname.Rows[0]["gstno"].ToString() + "'";
        //rptdoc.DataDefinition.FormulaFields["email"].Text = "'" + dtacname.Rows[0]["emailid"].ToString() + "'";
        rptdoc.DataDefinition.FormulaFields["clientname"].Text = "'" + ccode + "'";
        rptdoc.DataDefinition.FormulaFields["ftow"].Text = "'" + ftow + "'";
        //rptdoc.DataDefinition.FormulaFields["cramount"].Text = "'" + dtcc.Rows[0]["cramount"].ToString() + "'";
        rptdoc.DataDefinition.FormulaFields["invoicenostring"].Text = "'" + nn + "'";
        rptdoc.DataDefinition.FormulaFields["add1"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add1"].ToString().Trim().Replace("'", "") + "'";
        rptdoc.DataDefinition.FormulaFields["add2"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add2"].ToString().Trim().Replace("'", "") + "'";
        rptdoc.DataDefinition.FormulaFields["add3"].Text = "'" + imageDataSet.Tables["DataTable4"].Rows[0]["add3"].ToString().Trim().Replace("'", "") + "'";
        rptdoc.DataDefinition.FormulaFields["city"].Text = "'" + dtacname.Rows[0]["city"].ToString().Trim().Replace("'", "") + "'";
        rptdoc.DataDefinition.FormulaFields["cadd1"].Text = "'" + dtclient.Rows[0]["add1"].ToString().Trim().Replace("'", "") + "'";
        rptdoc.DataDefinition.FormulaFields["cadd2"].Text = "'" + dtclient.Rows[0]["add2"].ToString().Trim().Replace("'", "") + "'";
        rptdoc.DataDefinition.FormulaFields["cadd3"].Text = "'" + dtclient.Rows[0]["add3"].ToString().Trim().Replace("'", "") + "'";
        if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "RIT")
        {
            rptdoc.DataDefinition.FormulaFields["repname"].Text = "'RETAIL INVOICE'";
            rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'RIT'";
        }
        else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "TIT")
        {
            rptdoc.DataDefinition.FormulaFields["repname"].Text = "'TAX INVOICE'";
            rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'TIT'";
        }
        else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "S")
        {
            rptdoc.DataDefinition.FormulaFields["repname"].Text = "'SERVICE INVOICE'";
            rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'S'";
        }
        else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "L")
        {
            rptdoc.DataDefinition.FormulaFields["repname"].Text = "'LABOUR INVOICE'";
            rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'L'";
        }
        else if (imageDataSet.Tables["DataTable4"].Rows[0]["invtype"].ToString() == "GST")
        {
            rptdoc.DataDefinition.FormulaFields["repname"].Text = "'GST INVOICE'";
            rptdoc.DataDefinition.FormulaFields["prefix"].Text = "'GST'";
        }
        rptdoc.SetDataSource(imageDataSet.Tables["DataTable4"]);
        //////////rptdoc.PrintOptions.PaperSize = PaperSize.PaperA4;
        //////////rptdoc.PrintOptions.PaperSource = PaperSource.Auto;
        //////////rptdoc.PrintOptions.PaperOrientation = PaperOrientation.Portrait;
        ////////////rptdoc.PrintOptions.
        //////////rptdoc.PrintOptions.PrinterName = "Canon LBP6000/LBP6018";
        //////////rptdoc.PrintToPrinter(1, false, 0, 0);
        //CrystalReportViewer1.ReportSource = rptdoc;
        //rptdoc.PrintOptions.PrinterName = "Canon LBP2900 on nitin";
        //rptdoc.PrintToPrinter(1, false, 0, 0);
        //Export Report
        ExportOptions exprtopt = default(ExportOptions);
        string fname = "salesinvoice_" + li.strsino + ".pdf";
        //create instance for destination option - This one is used to set path of your pdf file save 
        DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
        destiopt.DiskFileName = Server.MapPath(@"~/salesinvoice/" + fname);


        exprtopt = rptdoc.ExportOptions;
        exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

        //use PortableDocFormat for PDF data
        exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
        exprtopt.DestinationOptions = destiopt;

        //finally export your report document
        rptdoc.Export();
        rptdoc.Close();
        rptdoc.Clone();
        rptdoc.Dispose();
        string path = fname;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('RepSalesInvoiceView.aspx?path=" + path + "&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);

    }

    public string NumberToText(Int64 number)
    {
        if (number == 0) return "Zero";
        Int64[] num = new Int64[4];
        int first = 0;
        Int64 u, h, t;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (number < 0)
        {
            sb.Append("Minus ");
            number = -number;
        }
        string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lakh ", "Crore " };
        num[0] = number % 1000; // units
        num[1] = number / 1000;
        num[2] = number / 100000;
        num[1] = num[1] - 100 * num[2]; // thousands
        num[3] = number / 10000000; // crores
        num[2] = num[2] - 100 * num[3]; // lakhs
        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10; // ones
            t = num[i] / 10;
            h = num[i] / 100; // hundreds
            t = t - 10 * h; // tens
            if (h > 0) sb.Append(words0[h] + "Hundred ");
            if (u > 0 || t > 0)
            {
                //if (h > 0 || i == 0) sb.Append("and ");
                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);
            }
            if (i != 0) sb.Append(words3[i - 1]);
        }


        // TextBox2.Text = "Rupees " + sb.ToString().TrimEnd() + " Only";
        return sb.ToString().TrimEnd() + " Rupees Only.";
    }

    public String changeToWords(String numb)
    {
        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
        String endStr = ("Only.");
        try
        {
            int decimalPlace = numb.IndexOf(".");
            if (decimalPlace > 0)
            {
                wholeNo = numb.Substring(0, decimalPlace);
                points = numb.Substring(decimalPlace + 1);
                if (Convert.ToInt32(points) > 0)
                {
                    //int p = points.Length;
                    //char[] TPot = points.ToCharArray();
                    andStr = (" and ");// just to separate whole numbers from points/Rupees                  
                    //for(int i=0;i<p;i++)
                    //{
                    //    andStr += ones(Convert.ToString(TPot[i]))+" ";
                    //}
                    andStr += translateWholeNumber(points).Trim() + " Rupees";

                }
            }
            val = String.Format("{0} {1}{2} {3}", translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
        }
        catch
        {
            ;
        }
        return val;
    }

    private String translateWholeNumber(String number)
    {
        string word = "";
        try
        {
            bool beginsZero = false;//tests for 0XX
            bool isDone = false;//test if already translated
            double dblAmt = (Convert.ToDouble(number));
            //if ((dblAmt > 0) && number.StartsWith("0"))

            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric
                beginsZero = number.StartsWith("0");
                int numDigits = number.Length;
                int pos = 0;//store digit grouping
                String place = "";//digit grouping name:hundres,thousand,etc...
                switch (numDigits)
                {
                    case 1://ones' range
                        word = ones(number);
                        isDone = true;
                        break;
                    case 2://tens' range
                        word = tens(number);
                        isDone = true;
                        break;
                    case 3://hundreds' range
                        pos = (numDigits % 3) + 1;
                        place = " Hundred ";
                        break;
                    case 4://thousands' range
                    case 5:
                        pos = (numDigits % 4) + 1;
                        place = " Thousand ";
                        break;
                    case 6:

                    case 7://millions' range
                        pos = (numDigits % 6) + 1;
                        // place = " Million ";
                        place = " Lakh ";
                        break;
                    case 8:
                    case 9:

                    case 10://Billions's range
                        pos = (numDigits % 8) + 1;
                        place = " Core ";
                        break;
                    //add extra case options for anything above Billion...
                    default:
                        isDone = true;
                        break;
                }
                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)
                    if (beginsZero) place = "";
                    word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));
                    //check for trailing zeros
                    if (beginsZero) word = " and " + word.Trim();
                }
                //ignore digit grouping names
                if (word.Trim().Equals(place.Trim())) word = "";
            }
        }
        catch
        {
            ;
        }
        return word.Trim();
    }

    private String tens(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = null;
        switch (digt)
        {
            case 10:
                name = "Ten";
                break;
            case 11:
                name = "Eleven";
                break;
            case 12:
                name = "Twelve";
                break;
            case 13:
                name = "Thirteen";
                break;
            case 14:
                name = "Fourteen";
                break;
            case 15:
                name = "Fifteen";
                break;
            case 16:
                name = "Sixteen";
                break;
            case 17:
                name = "Seventeen";
                break;
            case 18:
                name = "Eighteen";
                break;
            case 19:
                name = "Nineteen";
                break;
            case 20:
                name = "Twenty";
                break;
            case 30:
                name = "Thirty";
                break;
            case 40:
                name = "Fourty";
                break;
            case 50:
                name = "Fifty";
                break;
            case 60:
                name = "Sixty";
                break;
            case 70:
                name = "Seventy";
                break;
            case 80:
                name = "Eighty";
                break;
            case 90:
                name = "Ninety";
                break;
            default:
                if (digt > 0)
                {
                    name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));
                }
                break;
        }
        return name;
    }

    private String ones(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = "";
        switch (digt)
        {
            case 1:
                name = "One";
                break;
            case 2:
                name = "Two";
                break;
            case 3:
                name = "Three";
                break;
            case 4:
                name = "Four";
                break;
            case 5:
                name = "Five";
                break;
            case 6:
                name = "Six";
                break;
            case 7:
                name = "Seven";
                break;
            case 8:
                name = "Eight";
                break;
            case 9:
                name = "Nine";
                break;
        }
        return name;
    }

}