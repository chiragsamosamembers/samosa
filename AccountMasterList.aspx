﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AccountMasterList.aspx.cs" Inherits="AccountMasterList" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Account Master List</span><br />
        <div class="row">
            <div class="col-md-2">
                <asp:Button ID="btnaccount" runat="server" Text="Add New Account" class="btn btn-default forbutton"
                    OnClick="btnaccount_Click" /></div>
            <div class="col-md-3">
                <asp:TextBox ID="txtsearchaccountname" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtsearchaccountname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetAccountname">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnsearch" runat="server" Text="Search" class="btn btn-default forbutton"
                    OnClick="btnsearch_Click" />
            </div>
            <div class="col-md-4">
                <asp:Button ID="btnprint" runat="server" Text="Print" class="btn btn-default forbutton"
                    OnClick="btnprint_Click" />
            </div>
            <div class="col-md-2">
                <asp:Label ID="Label1" runat="server" Text="No. GST NO." BackColor="Red"></asp:Label><br />
                <asp:Label ID="Label2" runat="server" Text="Wrong GST NO." BackColor="Orange"></asp:Label>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvaclist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvaclist_RowCommand" OnRowDeleting="gvaclist_RowDeleting"
                        OnRowDataBound="gvaclist_RowDataBound" AllowPaging="true" PageSize="10">
                        <%--onrowdatabound="gvaclist_RowDataBound"--%>
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Company Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("acname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="GST No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblgstno" ForeColor="Black" runat="server" Text='<%# bind("gstno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PAN No." SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblpan" ForeColor="Black" runat="server" Text='<%# bind("pan") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Opening" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblopening" ForeColor="Black" runat="server" Text='<%# bind("opbalance1") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Debit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldebit" ForeColor="Black" runat="server" Text='<%# bind("drbalance") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Credit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcredit" ForeColor="Black" runat="server" Text='<%# bind("crbalance") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Closing" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblclosing" ForeColor="Black" runat="server" Text='<%# bind("clbalance") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dr.Code" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldebitgroupcode" ForeColor="Black" runat="server" Text='<%# bind("debitgroupcode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cr.Code" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcreditgroupcode" ForeColor="Black" runat="server" Text='<%# bind("creditgroupcode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="A/C Type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblactype" ForeColor="Black" runat="server" Text='<%# bind("actype") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact Person" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbranch" ForeColor="Black" runat="server" Text='<%# bind("contactperson") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phone No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblphoneno" ForeColor="Black" runat="server" Text='<%# bind("phone1") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mobile No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblmobileno" ForeColor="Black" runat="server" Text='<%# bind("mobileno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladd1" ForeColor="Black" runat="server" Text='<%# bind("add1") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladd2" ForeColor="Black" runat="server" Text='<%# bind("add2") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladd3" ForeColor="Black" runat="server" Text='<%# bind("add3") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcity" ForeColor="Black" runat="server" Text='<%# bind("city") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
