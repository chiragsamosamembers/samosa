﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ToolsReturnEntryList : System.Web.UI.Page
{
    ForToolDelMaster ftdmclass = new ForToolDelMaster();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = ftdmclass.selectalltooldelreturndata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvtoolsreturnlist.Visible = true;
            gvtoolsreturnlist.DataSource = dtdata;
            gvtoolsreturnlist.DataBind();
        }
        else
        {
            gvtoolsreturnlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Tool Deliery Data Found.";
        }
    }

    protected void btntooslreturn_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("ToolsReturnEntry.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvtoolsreturnlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                Response.Redirect("ToolsReturnEntry.aspx?mode=update&vno=" + li.voucherno + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                ftdmclass.deletetooldeldata(li);
                ftdmclass.deletetooldelitemdatafromvno(li);
                ftdmclass.updateisusedntr(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.voucherno + " Tools Return Deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvtoolsreturnlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
}