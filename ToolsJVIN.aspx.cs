﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlTypes;

public partial class ToolsJVIN : System.Web.UI.Page
{

    ForToolsJV ftjvclass = new ForToolsJV();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "insert")
            {
                getbrno();
                txtjvdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                txtuser.Text = Request.Cookies["ForLogIn"]["username"];
                Session["dtpitemssjvin"] = CreateTemplate();
            }
            else
            {
                filleditdata();
            }
            //fillbillcombo();
            Page.SetFocus(txtname);
        }
    }

    public void getbrno()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.inout = "IN";
        DataTable dtvno = new DataTable();
        dtvno = ftjvclass.selectchallanno(li);
        if (dtvno.Rows.Count > 0)
        {
            txtchallanno.Text = (Convert.ToInt64(dtvno.Rows[0]["challanno"].ToString()) + 1).ToString();
        }
        else
        {
            txtchallanno.Text = "1";
        }
    }

    public void filleditdata()
    {
        li.strchallanno = Request["challanno"].ToString();
        li.inout = "IN";
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        DataTable dtd = new DataTable();
        dtd = ftjvclass.selectalldatafromchallannomaster(li);
        if (dtd.Rows.Count > 0)
        {
            txtchallanno.Text = dtd.Rows[0]["challanno"].ToString();
            txtjvdate.Text = Convert.ToDateTime(dtd.Rows[0]["challandate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtuser.Text = dtd.Rows[0]["uname"].ToString();
            txtname.Text = dtd.Rows[0]["acname"].ToString();
            btnsaveall.Text = "Update";
            DataTable dtdata = new DataTable();
            dtdata = ftjvclass.selectalldatafromchallannoitems(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvtool.Visible = true;
                gvtool.DataSource = dtdata;
                gvtool.DataBind();
            }
            else
            {
                gvtool.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Stock JV IN Data Found.";
            }
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("descr", typeof(string));
        return dtpitems;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetAccountname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallaccountname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetToolname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectalltoolname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemssjvin"];
            DataRow dr = dt.NewRow();
            dr["id"] = 1;
            dr["itemname"] = txttoolname.Text;
            dr["qty"] = txttoolqty.Text;
            dr["rate"] = txtrate.Text;
            dr["descr"] = txtdescription.Text;
            dt.Rows.Add(dr);
            Session["dtpitemssjvin"] = dt;
            this.bindgrid();
        }
        else
        {
            li.strchallanno = txtchallanno.Text;
            li.cdate = Convert.ToDateTime(txtjvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.acname = txtname.Text;
            li.itemname = txttoolname.Text;
            li.qty = Convert.ToDouble(txttoolqty.Text);
            li.rate = Convert.ToDouble(txtrate.Text);
            li.descr = txtdescription.Text;
            li.inout = "IN";
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            ftjvclass.insertitemdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strchallanno + " Tool JVIN Item inserted.";
            faclass.insertactivity(li);
            DataTable dtdata = new DataTable();
            dtdata = ftjvclass.selectalldatafromchallannoitems(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvtool.Visible = true;
                gvtool.DataSource = dtdata;
                gvtool.DataBind();
            }
            else
            {
                gvtool.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Tool JV IN Data Found.";
            }
        }
        txttoolname.Text = string.Empty;
        txttoolqty.Text = string.Empty;
        txtrate.Text = string.Empty;
        txtdescription.Text = string.Empty;
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemssjvin"] as DataTable;
        gvtool.DataSource = dtsession;
        gvtool.DataBind();
    }

    protected void btnsaveall_Click(object sender, EventArgs e)
    {
        li.cdate = Convert.ToDateTime(txtjvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.cdate <= yyyear1 && li.cdate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        if (btnsaveall.Text == "Save")
        {
            if (gvtool.Rows.Count > 0)
            {
                li.strchallanno = txtchallanno.Text;
                li.cdate = Convert.ToDateTime(txtjvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                li.acname = txtname.Text;
                li.inout = "IN";
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                ftjvclass.insertdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.strchallanno + " Tool JVIN inserted.";
                faclass.insertactivity(li);
                for (int c = 0; c < gvtool.Rows.Count; c++)
                {
                    Label lblitemname = (Label)gvtool.Rows[c].FindControl("lblitemname");
                    Label lblqty = (Label)gvtool.Rows[c].FindControl("lblqty");
                    Label lblrate = (Label)gvtool.Rows[c].FindControl("lblrate");
                    TextBox txtgvdescr = (TextBox)gvtool.Rows[c].FindControl("txtgvdescr");
                    li.itemname = lblitemname.Text;
                    li.qty = Convert.ToDouble(lblqty.Text);
                    li.rate = Convert.ToDouble(lblrate.Text);
                    li.descr = txtgvdescr.Text;
                    ftjvclass.insertitemdata(li);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter Tool JV IN Data.');", true);
                return;
            }
        }
        else
        {
            li.strchallanno = txtchallanno.Text;
            li.cdate = Convert.ToDateTime(txtjvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.acname = txtname.Text;
            li.inout = "IN";
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            ftjvclass.updatedata(li);
            ftjvclass.updateitemsdatedata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strchallanno + " Tool JVIN Updated.";
            faclass.insertactivity(li);
        }
        Response.Redirect("~/ToolsJVINList.aspx?pagename=StockJVINList");
    }

    protected void gvtool_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnsaveall.Text == "Save")
        {
            DataTable dt = Session["dtpitemssjvin"] as DataTable;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);

            //DataTable dtamount = new DataTable();
            //dtamount = fbrclass.selectamounttodebitfromacfromaccountname(li);
            //if (dtamount.Rows.Count > 0)
            //{            
            ////////li.amount = Convert.ToDouble(dt.Rows[0]["amount"].ToString());
            ////////li.invoiceno = SessionMgt.invoiceno;
            ////////li.acname = dt.Rows[0]["acname"].ToString();
            ////////li.issipi = SessionMgt.issipi;
            ////////DataTable dtsi = new DataTable();
            ////////dtsi = fbrclass.selectamountforupdation(li);
            ////////li.receivedamount = Convert.ToDouble(dtsi.Rows[0]["receivedamount"].ToString()) - li.amount;
            ////////li.remainamount = Convert.ToDouble(dtsi.Rows[0]["remainamount"].ToString()) + li.amount;
            ////////fbrclass.updatesidata11(li);
            //}

            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            ////////li.voucherno = Convert.ToInt64(txtvoucherno.Text);

            ////////fbrclass.deletefromshadowtable(li);
            Session["dtpitemssjvin"] = dt;
            this.bindgrid();

        }
        else
        {
            if (gvtool.Rows.Count > 1)
            {
                Label lblid = (Label)gvtool.Rows[e.RowIndex].FindControl("lblid");
                li.id = Convert.ToInt64(lblid.Text);
                ftjvclass.deleteitemdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.id + " Tool JVIN Item Deleted.";
                faclass.insertactivity(li);
                li.strchallanno = txtchallanno.Text;
                li.inout = "IN";
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                //fsjvclass.insertitemdata(li);
                DataTable dtdata = new DataTable();
                dtdata = ftjvclass.selectalldatafromchallannoitems(li);
                if (dtdata.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvtool.Visible = true;
                    gvtool.DataSource = dtdata;
                    gvtool.DataBind();
                }
                else
                {
                    gvtool.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Tool JV IN Data Found.";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
    }
    protected void txtchallanno_TextChanged(object sender, EventArgs e)
    {
        if (txtchallanno.Text.Trim() != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.inout = "IN";
            li.challanno = Convert.ToInt64(txtchallanno.Text);
            DataTable dtvno = new DataTable();
            dtvno = ftjvclass.checkchallanno(li);
            if (dtvno.Rows.Count > 0)
            {
                getbrno();
                Page.SetFocus(txtjvdate);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Challan NO Already Exist.');", true);
                return;
            }
        }
    }
}