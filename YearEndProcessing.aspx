﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="YearEndProcessing.aspx.cs" Inherits="YearEndProcessing" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Year End Process</span><br />
        <div class="row">
            <div class="col-md-2">
                <asp:CheckBox ID="chkbalancesheet" runat="server" Text="Transfer Balance Sheet A/C." />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkitemstock" runat="server" Text="Transfer Items Opening Stock" />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chktoolstock" runat="server" Text="Transfer Tools Opening Stock" Enabled="false" />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkitemstockpricewise" runat="server" Text="Transfer Pricewise OP Stock" />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkclientmaster" runat="server" Text="Transfer Client Master" />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkpendingquo" runat="server" Text="Transfer Pending Bifur." />
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <asp:CheckBox ID="chkpendingpo" runat="server" Text="Transfer Pending P.O." />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkpendingsc" runat="server" Text="Transfer Pending S.C." />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkpendingsignsc" runat="server" Text="Transfer Pending Sign S.C." Enabled="false" />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkpendingpc" runat="server" Text="Transfer Pending P.C." />
            </div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkpendingso" runat="server" Text="Transfer Pending S.O." Enabled="false" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <asp:CheckBox ID="chkreconcile" runat="server" Text="Transfer Pending Cheque" />
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:Button ID="btnyearend" runat="server" Text="Process Year End" class="btn btn-default forbutton"
                    ValidationGroup="val" OnClick="btnyearend_Click" /></div>
        </div>
    </div>
</asp:Content>
