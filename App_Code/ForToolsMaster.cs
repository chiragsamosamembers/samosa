﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForToolsMaster
/// </summary>
public class ForToolsMaster : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForToolsMaster()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectalltoolsdata()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsMaster order by id desc", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalltoolsdatacnowise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsMaster where cno=@cno order by id desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkduplicate(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsMaster where cno=@cno and name=@name", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@name", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selecttoolsdatafromid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsMaster where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selecttoolsstockdatafromtoolname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select name as toolname,(select isnull(sum(opstock),0) from toolsmaster where name=@itemname) as opqty,(select isnull(SUM(qty),0) from ToolDelItem where toolname=@itemname) as tots,(select isnull(SUM(rfs),0) from ToolDelItem where toolname=@itemname) as totp,(((select isnull(SUM(rfs),0) from ToolDelItem where toolname=@itemname)-(select isnull(SUM(qty),0) from ToolDelItem where toolname=@itemname))+(select isnull(sum(opstock),0) from ToolsMaster where name=@itemname)) as totalqty from ToolsMaster where name=@itemname", con);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void inserttoolsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ToolsMaster (name,unit,rate,opstock,inward,outward,clstock,cno,uname,udate,acyear) values (@name,@unit,@rate,@opstock,@inward,@outward,@clstock,@cno,@uname,@udate,@acyear)", con);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@opstock", li.opstock);
            cmd.Parameters.AddWithValue("@inward", li.inward);
            cmd.Parameters.AddWithValue("@outward", li.outward);
            cmd.Parameters.AddWithValue("@clstock", li.clstock);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@acyear", li.acyear);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatetoolsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolsMaster set name=@name,unit=@unit,rate=@rate,opstock=@opstock,inward=@inward,outward=@outward,clstock=@clstock,cno=@cno,uname=@uname,udate=@udate,acyear=@acyear where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@opstock", li.opstock);
            cmd.Parameters.AddWithValue("@inward", li.inward);
            cmd.Parameters.AddWithValue("@outward", li.outward);
            cmd.Parameters.AddWithValue("@clstock", li.clstock);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@acyear", li.acyear);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletetoolsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ToolsMaster where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectalltoolname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsMaster where name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where itemname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemnamenotblocked(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where isblock='N' and itemname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallclientname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ClientMaster where name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallacnamegeneral(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname like '%'+@fullname+'%' and actype='General'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallrrcno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from rrmaster where challanno like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldelcno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolDelMaster where voucherno like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from salesorder where sono like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsonoforrr(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where scno like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            //da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    //update toolname in whole system
    public void updateToolDelMaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolDelItem set toolname=@name1 where toolname=@name and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@name1", li.name1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateToolJVMaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolsJVItems set itemname=@name1 where itemname=@name and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@name1", li.name1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    //check used or not
    public DataTable checkToolDelItem(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from ToolDelItem where toolname=@name and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@name", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkacyearfordata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ToolsMaster where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}