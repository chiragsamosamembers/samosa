﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForSalesOrder
/// </summary>
public class ForSalesOrder : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

	public ForSalesOrder()
	{
        con = new SqlConnection(GetConnectionString());
	}

    public DataTable selectallsalesorderdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrder where cno=@cno order by sono desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderdatafromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrder where sono=@sono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderitemdatafromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderitemdatafromid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrderItems where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemdatafromitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where cno=@cno and itemname=@itemname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalltaxdatafromtaxname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from TaxMaster where typename=@typename", con);            
            da.SelectCommand.Parameters.AddWithValue("@typename", li.typename);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertsoitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into SalesOrderItems (sono,sodate,itemname,qty,unit,rate,basicamount,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,descr1,descr2,descr3,cno,uname,udate,qtyremain,qtyused,vno,qtyremain1,qtyused1,vid,otype) values (@sono,@sodate,@itemname,@qty,@unit,@rate,@basicamt,@vattype,@vatp,@addtaxp,@cstp,@vatamt,@addtaxamt,@cstamt,@amount,@descr1,@descr2,@descr3,@cno,@uname,@udate,@qtyremain,@qtyused,@vno,@qtyremain1,@qtyused1,@vid,@otype) SELECT SCOPE_IDENTITY()", con);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused1", li.qtyused);
            cmd.Parameters.AddWithValue("@qtyremain1", li.qtyremain);
            //cmd.Parameters.AddWithValue("@qtyused1", li.qtyused1);
            //cmd.Parameters.AddWithValue("@qtyremain1", li.qtyremain1);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamt", li.basicamount);
            cmd.Parameters.AddWithValue("@vattype", li.taxtype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@vno", li.vnono);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@otype", li.otype);
            con.Open();
            var cc = cmd.ExecuteScalar();
            SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesoitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrderItems set sodate=@sodate,itemname=@itemname,qty=@qty,unit=@unit,rate=@rate,basicamount=@basicamt,taxtype=@vattype,vatp=@vatp,addtaxp=@addtaxp,cstp=@cstp,vatamt=@vatamt,addtaxamt=@addtaxamt,cstamt=@cstamt,amount=@amount,descr1=@descr1,descr2=@descr2,descr3=@descr3,uname=@uname,udate=@udate,qtyremain=@qtyremain,qtyused=@qtyused,vno=@vno,qtyremain1=@qtyremain1,qtyused1=@qtyused1,otype=@otype where id=@id", con);
            //cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused1", li.qtyused);
            cmd.Parameters.AddWithValue("@qtyremain1", li.qtyremain);
            //cmd.Parameters.AddWithValue("@qtyused1", li.qtyused1);
            //cmd.Parameters.AddWithValue("@qtyremain1", li.qtyremain1);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamt", li.basicamount);
            cmd.Parameters.AddWithValue("@vattype", li.taxtype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@vno", li.vnono);
            cmd.Parameters.AddWithValue("@otype", li.otype);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertsomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into SalesOrder (sono,sodate,pono,podate,ccode,indentno,indentdate,acname,dispatchthrough,delat,delat1,delat2,totbasic,totvat,totaddvat,totcst,totsoamt,inst1,inst2,inst3,inst4,inst5,inst6,inst7,status,cno,uname,udate,quono) values (@sono,@sodate,@pono,@podate,@ccode,@indentno,@indentdate,@acname,@dispatchthrough,@delat,@delat1,@delat2,@totbasic,@totvat,@totaddvat,@totcst,@totsoamt,@inst1,@inst2,@inst3,@inst4,@inst5,@inst6,@inst7,@status,@cno,@uname,@udate,@quono)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@podate", li.podate);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@indentno", li.indentno);
            cmd.Parameters.AddWithValue("@indentdate", li.indentdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@dispatchthrough", li.dispatchedby);
            cmd.Parameters.AddWithValue("@delat", li.delat);
            cmd.Parameters.AddWithValue("@delat1", li.delat1);
            cmd.Parameters.AddWithValue("@delat2", li.delat2);
            cmd.Parameters.AddWithValue("@totbasic", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddvat", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@totsoamt", li.totsoamt);
            cmd.Parameters.AddWithValue("@inst1", li.inst1);
            cmd.Parameters.AddWithValue("@inst2", li.inst2);
            cmd.Parameters.AddWithValue("@inst3", li.inst3);
            cmd.Parameters.AddWithValue("@inst4", li.inst4);
            cmd.Parameters.AddWithValue("@inst5", li.inst5);
            cmd.Parameters.AddWithValue("@inst6", li.inst6);
            cmd.Parameters.AddWithValue("@inst7", li.inst7);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@quono", li.quono);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrder set sodate=@sodate,pono=@pono,podate=@podate,ccode=@ccode,indentno=@indentno,indentdate=@indentdate,acname=@acname,dispatchthrough=@dispatchthrough,delat=@delat,delat1=@delat1,delat2=@delat2,totbasic=@totbasic,totvat=@totvat,totaddvat=@totaddvat,totcst=@totcst,totsoamt=@totsoamt,inst1=@inst1,inst2=@inst2,inst3=@inst3,inst4=@inst4,inst5=@inst5,inst6=@inst6,inst7=@inst7,status=@status,uname=@uname,udate=@udate,quono=@quono where sono=@sono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@podate", li.podate);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@indentno", li.indentno);
            cmd.Parameters.AddWithValue("@indentdate", li.indentdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@dispatchthrough", li.dispatchedby);
            cmd.Parameters.AddWithValue("@delat", li.delat);
            cmd.Parameters.AddWithValue("@delat1", li.delat1);
            cmd.Parameters.AddWithValue("@delat2", li.delat2);
            cmd.Parameters.AddWithValue("@totbasic", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddvat", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@totsoamt", li.totsoamt);
            cmd.Parameters.AddWithValue("@inst1", li.inst1);
            cmd.Parameters.AddWithValue("@inst2", li.inst2);
            cmd.Parameters.AddWithValue("@inst3", li.inst3);
            cmd.Parameters.AddWithValue("@inst4", li.inst4);
            cmd.Parameters.AddWithValue("@inst5", li.inst5);
            cmd.Parameters.AddWithValue("@inst6", li.inst6);
            cmd.Parameters.AddWithValue("@inst7", li.inst7);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@quono", li.quono);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesomasterdataupdateso(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrder set sono=@sono1 where sono=@sono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sono1", li.sono1);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesoitemsdata11(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrderItems set sono=@sono1 where sono=@sono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sono1", li.sono1);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesoitemsdatedata11(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrderItems set sodate=@sodate where sono=@sono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletesomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SalesOrder where sono=@sono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletesoitemsdatafromsono(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SalesOrderItems where sono=@sono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletesoitemsdatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SalesOrderItems where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectunusedsalesordernolast(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select sono from SalesOrder where cno=@cno order by sono desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedsalesorderno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and issoused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisused(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set issoused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.sono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallstatus(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Status' order by name", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisusedn(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set issoused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.sono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatevidinsoitems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrderItems set vid=@vid where id=@id", con);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where isblock='N' order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemnameall(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallbifurcationo(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from QuoMaster where cno=@cno and isso is null order by quono desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallbifurcationofromacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from QuoMaster where cno=@cno and acname=@acname and isso is null order by quono desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallbifurcationoedit(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from QuoMaster where cno=@cno order by quono desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalldatafrombifurcationo(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select QuoItems.basicamt as basicamount,* from QuoItems inner join QuoMaster on QuoMaster.quono=QuoItems.quono where QuoMaster.cno=@cno and QuoMaster.quono=@quono", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@quono", li.quono);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateissodatainbifurcation(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update QuoMaster set isso=@isso where quono=@quono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@quono", li.quono);
            cmd.Parameters.AddWithValue("@isso", li.issipi);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateissodatainbifurcation1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update QuoMaster set isso=null where quono=@quono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@quono", li.quono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectchallandatafromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where sono1=@sono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectpodatafromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrder where sono=@sono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectchallandatafromsoid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCItems where vid=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectpodatafromsoid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems where vid=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectsodatafromccode(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();

            da = new SqlDataAdapter("select convert(varchar(15),firstquotation,105) as firstquotation,convert(varchar(15),technicaldrawing,105) as technicaldrawing,convert(varchar(15),rewisequotation,105) as rewisequotation,convert(varchar(15),finaltech,105) as finaltech,convert(varchar(15),lastfollowupdate,105) as lastfollowupdate,convert(varchar(15),pendingorderlying,105) as pendingorderlying,convert(varchar(15),okorderdate,105) as okorderdate,convert(varchar(15),tentativedeliverydate,105) as tentativedeliverydate,convert(varchar(15),deliverydate,105) as deliverydate,(SalesOrderItems.itemname+'-'+SalesOrderItems.descr1+','+SalesOrderItems.descr2+','+SalesOrderItems.descr3) as dd,* from SalesOrderItems inner join salesorder on salesorder.sono=SalesOrderItems.sono where salesorder.ccode=@ccode", con);
            da.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpendingorderlyingdata()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Pending Order Lying'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatesomasterdataduringdelete(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrder set totbasic=@totbasic,totvat=@totvat,totaddvat=@totaddvat,totcst=@totcst,totsoamt=@totsoamt,uname=@uname,udate=@udate where sono=@sono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@totbasic", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddvat", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@totsoamt", li.totsoamt);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}