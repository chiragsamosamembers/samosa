﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForBankReceipt
/// </summary>
public class ForBankReceipt : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForBankReceipt()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallbr1data(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from BankACMaster1 inner join BankACMaster on BankACMaster.istype+convert(varchar(50),BankACMaster.voucherno)=BankACMaster1.istype+convert(varchar(50),BankACMaster1.voucherno) where BankACMaster1.cno=@cno and BankACMaster1.istype=@istype and BankACMaster.istype=@istype order by BankACMaster1.voucherdate desc,BankACMaster1.voucherno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@istype", li.istype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallbrdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from BankACMaster where voucherno=@voucherno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectbr1datafromvno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from BankACMaster1 where voucherno=@voucherno and cno=@cno and istype=@istype", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            da.SelectCommand.Parameters.AddWithValue("@istype", li.istype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectbrdatafromvno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from BankACMaster where voucherno=@voucherno and cno=@cno and istype=@istype", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            da.SelectCommand.Parameters.AddWithValue("@istype", li.istype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertbr1dataforjv(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into BankACMaster1 (voucherno,voucherdate,name,istype,cno,uname,udate,total,type,billno,vtype) values (@voucherno,@voucherdate,@name,@istype,@cno,@uname,@udate,@total,@type,@billno,@vtype)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@total", li.totbasicamount);
            cmd.Parameters.AddWithValue("@type", li.type);
            cmd.Parameters.AddWithValue("@billno", li.sbillno);
            cmd.Parameters.AddWithValue("@vtype", li.vtype);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertbr1dataforcp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into BankACMaster1 (voucherno,voucherdate,name,istype,cno,uname,udate,total,billno,vtype) values (@voucherno,@voucherdate,@name,@istype,@cno,@uname,@udate,@total,@billno,@vtype)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@total", li.totbasicamount);
            cmd.Parameters.AddWithValue("@billno", li.sbillno);
            cmd.Parameters.AddWithValue("@vtype", li.vtype);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertbr1data(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into BankACMaster1 (voucherno,voucherdate,name,istype,cno,uname,udate,total) values (@voucherno,@voucherdate,@name,@istype,@cno,@uname,@udate,@total)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@total", li.totbasicamount);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertbrdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into BankACMaster (voucherno,voucherdate,name,acname,ccode,remarks,amount,chequeno,agbill,istype,cno,uname,udate) values (@voucherno,@voucherdate,@name,@acname,@ccode,@remarks,@amount,@chequeno,@agbill,@istype,@cno,@uname,@udate) SELECT SCOPE_IDENTITY()", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@chequeno", li.chequeno);
            cmd.Parameters.AddWithValue("@agbill", li.agbill);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            var cc = cmd.ExecuteScalar();
            SessionMgt.id = Convert.ToInt64(cc.ToString());
            //cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertbrdataforcp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into BankACMaster (voucherno,voucherdate,name,acname,ccode,remarks,amount,chequeno,agbill,istype,cno,uname,udate,hsncode) values (@voucherno,@voucherdate,@name,@acname,@ccode,@remarks,@amount,@chequeno,@agbill,@istype,@cno,@uname,@udate,@hsncode) SELECT SCOPE_IDENTITY()", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@chequeno", li.chequeno);
            cmd.Parameters.AddWithValue("@agbill", li.agbill);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
            con.Open();
            var cc = cmd.ExecuteScalar();
            SessionMgt.id = Convert.ToInt64(cc.ToString());
            //cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertbrdataForBank(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into BankACMaster (voucherno,voucherdate,name,acname,ccode,remarks,amount,chequeno,agbill,istype,cno,uname,udate,cldate) values (@voucherno,@voucherdate,@name,@acname,@ccode,@remarks,@amount,@chequeno,@agbill,@istype,@cno,@uname,@udate,@cldate) SELECT SCOPE_IDENTITY()", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@chequeno", li.chequeno);
            cmd.Parameters.AddWithValue("@agbill", li.agbill);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@cldate", li.cldate);
            con.Open();
            var cc = cmd.ExecuteScalar();
            SessionMgt.id = Convert.ToInt64(cc.ToString());
            //cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertbrdatajv(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into BankACMaster (voucherno,voucherdate,name,acname,ccode,remarks,amount,chequeno,agbill,istype,cno,uname,udate,iscd,hsncode) values (@voucherno,@voucherdate,@name,@acname,@ccode,@remarks,@amount,@chequeno,@agbill,@istype,@cno,@uname,@udate,@iscd,@hsncode) SELECT SCOPE_IDENTITY()", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@chequeno", li.chequeno);
            cmd.Parameters.AddWithValue("@agbill", li.agbill);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@iscd", li.iscd);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
            con.Open();
            var cc = cmd.ExecuteScalar();
            SessionMgt.id = Convert.ToInt64(cc.ToString());
            //cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebr1data(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACMaster1 set voucherdate=@voucherdate,name=@name,uname=@uname,udate=@udate,total=@total,type=@type where voucherno=@voucherno and cno=@cno and istype=@istype", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@total", li.totbasicamount);
            cmd.Parameters.AddWithValue("@type", li.type);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebr1dataforcp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACMaster1 set voucherdate=@voucherdate,name=@name,uname=@uname,udate=@udate,total=@total,type=@type,billno=@billno,vtype=@vtype where voucherno=@voucherno and cno=@cno and istype=@istype", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@total", li.totbasicamount);
            cmd.Parameters.AddWithValue("@type", li.type);
            cmd.Parameters.AddWithValue("@billno", li.sbillno);
            cmd.Parameters.AddWithValue("@vtype", li.vtype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebr1dataforjv(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACMaster1 set voucherdate=@voucherdate,name=@name,uname=@uname,udate=@udate,total=@total,type=@type,billno=@billno,vtype=@vtype where voucherno=@voucherno and cno=@cno and istype=@istype", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@total", li.totbasicamount);
            cmd.Parameters.AddWithValue("@type", li.type);
            cmd.Parameters.AddWithValue("@billno", li.sbillno);
            cmd.Parameters.AddWithValue("@vtype", li.vtype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebrdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACMaster set voucherno=@voucherno,voucherdate=@voucherdate,name=@name,acname=@acname,ccode=@ccode,remarks=@remarks,amount=@amount,chequeno=@chequeno,agbill=@agbill,uname=@uname,udate=@udate where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@chequeno", li.chequeno);
            cmd.Parameters.AddWithValue("@agbill", li.agbill);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebrdataforcp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACMaster set voucherno=@voucherno,voucherdate=@voucherdate,name=@name,acname=@acname,ccode=@ccode,remarks=@remarks,amount=@amount,chequeno=@chequeno,agbill=@agbill,uname=@uname,udate=@udate,hsncode=@hsncode where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@chequeno", li.chequeno);
            cmd.Parameters.AddWithValue("@agbill", li.agbill);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebrdataforjv(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACMaster set voucherno=@voucherno,voucherdate=@voucherdate,name=@name,acname=@acname,ccode=@ccode,iscd=@iscd,remarks=@remarks,amount=@amount,chequeno=@chequeno,agbill=@agbill,uname=@uname,udate=@udate,hsncode=@hsncode where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@iscd", li.iscd);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@chequeno", li.chequeno);
            cmd.Parameters.AddWithValue("@agbill", li.agbill);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebrdatadateonly(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACMaster set voucherdate=@voucherdate,name=@name where voucherno=@voucherno and istype=@istype and cno=@cno", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebankacshadowdatadateonly(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACShadow set voucherdate=@voucherdate,acname=@name where voucherno=@voucherno and istype=@istype and cno=@cno", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletebr1data(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from BankACMaster1 where voucherno=@voucherno and cno=@cno and istype=@istype", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletebrdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from BankACMaster where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletebrdatafromvno(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from BankACMaster where voucherno=@voucherno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletefromshadowtable(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("delete from BankACShadow where voucherno=@voucherno and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd = new SqlCommand("delete from BankACShadow where refid=@id and cno=@cno and istype=@istype", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteallfromshadowtable(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("delete from BankACShadow where voucherno=@voucherno and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd = new SqlCommand("delete from BankACShadow where voucherno=@voucherno and cno=@cno and istype=@istype", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletefromledgertable(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("delete from Ledger where voucherno=@voucherno and cno=@cno and istype=@istype", con);
            //cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            //cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd = new SqlCommand("delete from Ledger where refid=@id and cno=@cno and istype=@istype", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallaccountname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where cno=@cno and acname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallaccountnamebankch(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where cno=@cno and actype='Bank' and acname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallaccountnamecashch(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where cno=@cno and actype='Cash' and acname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallaccountnamepettycashch(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where cno=@cno and actype='Petty Cash' and acname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallbankname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from miscmaster where type='Bank Name' and name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallcashname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from miscmaster where type='Cash Name' and name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallcashnamefromacm(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from acmaster where actype='Cash' and acname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpettycashname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from miscmaster where type='Petty Cash Name' and name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpettycashnamefromam(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where actype='Petty Cash' and acname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallclientname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ClientMaster where cno=@cno and name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallbillno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseInvoice where cno=@cno and pino like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallbillnoaa(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseInvoice where cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallvattype(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from TaxMaster where typename like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalltaxacdescfrommisc()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Tax Type'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalltaxacdescfrommiscautocomp(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Tax Type' and name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where isblock='N' and itemname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallunit(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select unit from ItemMaster where unit like '%'+@fullname+'%' group by unit", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrder where sono like '%'+@fullname+'%' and status!='Closed'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesman(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesmanMaster where name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrder where pono like '%'+@fullname+'%' and status!='Closed'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallvnofrombp(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select BankACMaster1.voucherno from BankACMaster1 inner join BankACMaster on BankACMaster.voucherno=BankACMaster1.voucherno where BankACMaster1.voucherno like '%'+@fullname+'%' and BankACMaster1.istype='BP' and BankACMaster.acname=@acname group by BankACMaster1.voucherno", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallvnofrombp1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select BankACMaster1.voucherno from BankACMaster1 inner join BankACMaster on BankACMaster.voucherno=BankACMaster1.voucherno where BankACMaster1.voucherno like '%'+@fullname+'%' and BankACMaster1.istype='BP' and BankACMaster1.name=@acname group by BankACMaster1.voucherno", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallvnofromcp(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select BankACMaster1.voucherno from BankACMaster1 inner join BankACMaster on BankACMaster.voucherno=BankACMaster1.voucherno where BankACMaster1.voucherno like '%'+@fullname+'%' and BankACMaster1.istype='CP' and BankACMaster.acname=@acname group by BankACMaster1.voucherno", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallvnofromcp1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select BankACMaster1.voucherno from BankACMaster1 inner join BankACMaster on BankACMaster.voucherno=BankACMaster1.voucherno where BankACMaster1.voucherno like '%'+@fullname+'%' and BankACMaster1.istype='CP' and BankACMaster1.name=@acname group by BankACMaster1.voucherno", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallvnofrombr(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select BankACMaster1.voucherno from BankACMaster1 inner join BankACMaster on BankACMaster.voucherno=BankACMaster1.voucherno where BankACMaster1.voucherno like '%'+@fullname+'%' and BankACMaster1.istype='BR' and BankACMaster.acname=@acname group by BankACMaster1.voucherno", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallvnofrombr1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select BankACMaster1.voucherno from BankACMaster1 inner join BankACMaster on BankACMaster.voucherno=BankACMaster1.voucherno where BankACMaster1.voucherno like '%'+@fullname+'%' and BankACMaster1.istype='BR' and BankACMaster1.name=@acname group by BankACMaster1.voucherno", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallvnofrompackingslip(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from pmmaster where voucherno like '%'+@fullname+'%' and name=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallvnofromcr(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select BankACMaster1.voucherno from BankACMaster1 inner join BankACMaster on BankACMaster.voucherno=BankACMaster1.voucherno where BankACMaster1.voucherno like '%'+@fullname+'%' and BankACMaster1.istype='CR' and BankACMaster.acname=@acname group by BankACMaster1.voucherno", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallvnofromcr1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select BankACMaster1.voucherno from BankACMaster1 inner join BankACMaster on BankACMaster.voucherno=BankACMaster1.voucherno where BankACMaster1.voucherno like '%'+@fullname+'%' and BankACMaster1.istype='CR' and BankACMaster1.name=@acname group by BankACMaster1.voucherno", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallponoacnamewise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrder where pono like '%'+@fullname+'%' and status!='Closed' and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpcno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCMaster where pcno like '%'+@fullname+'%' and status!='Closed'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpurchaseac(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Purchase AC' and name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesac(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Sales AC' and name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where scno like '%'+@fullname+'%' and status!='Closed'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallscnoacnamewise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where scno like '%'+@fullname+'%' and status!='Closed' and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallscnoacnamewisestring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where strscno like '%'+@fullname+'%' and status!='Closed' and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsinoacnamewise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where sino like '%'+@fullname+'%' and status!='Closed' and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsinoacnamewisestring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where strsino like '%'+@fullname+'%' and status!='Closed' and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallunpaidsalesinvoice(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where remainamount>0 and acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallunpaidpurchaseinvoice(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where remainamount>0 and acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatesidata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set receivedamount=@receivedamount,remainamount=@remainamount where sino=@sino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@receivedamount", li.receivedamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesidatastring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set receivedamount=@receivedamount,remainamount=@remainamount where strsino=@strsino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@receivedamount", li.receivedamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepidata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIMaster set receivedamount=@receivedamount,remainamount=@remainamount where pino=@pino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@receivedamount", li.receivedamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepidatastring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIMaster set receivedamount=@receivedamount,remainamount=@remainamount where strpino=@strpino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@receivedamount", li.receivedamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectunusedbankreceiptno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and isbrused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectlastnostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from BankACMaster1 where cno=@cno and istype=@istype order by voucherno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@istype", li.istype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedcashreceiptno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and isbr2used='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedbankpaymentno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and isbr1used='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedjvno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and isjvused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedcashpaymentno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and isbr3used='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedpettycashno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and isbr4used='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisused(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isbrused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedbp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isbr1used='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedcr(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isbr2used='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedcp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isbr3used='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedpc(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isbr4used='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedn(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isbrused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusednbp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isbr1used='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusednjv(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isjvused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedyjv(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isjvused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedncr(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isbr2used='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedncp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isbr3used='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusednpc(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isbr4used='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesidata11(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set receivedamount=@receivedamount,remainamount=@remainamount where sino=@sino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino", li.invoiceno);
            cmd.Parameters.AddWithValue("@receivedamount", li.receivedamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesidata11string(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set receivedamount=@receivedamount,remainamount=@remainamount where strsino=@strsino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@receivedamount", li.receivedamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepidata11(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIMaster set receivedamount=@receivedamount,remainamount=@remainamount where pino=@pino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino", li.invoiceno);
            cmd.Parameters.AddWithValue("@receivedamount", li.receivedamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepidata11string(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIMaster set receivedamount=@receivedamount,remainamount=@remainamount where strpino=@strpino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@receivedamount", li.receivedamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectamountforupdation(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where sino=@sino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sino", li.invoiceno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectamountforupdationstring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where strsino=@strsino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@strsino", li.strsino);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectamountforupdationpi(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where pino=@pino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pino", li.invoiceno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectamountforupdationpistring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where strpino=@strpino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@strpino", li.strpino);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertbankacshadowdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into BankACShadow (voucherno,ccode,invoiceno,amount,istype,cno,uname,udate,issipi,refid,acname,voucherdate) values (@voucherno,@ccode,@invoiceno,@amount,@istype,@cno,@uname,@udate,@issipi,@refid,@acname,@voucherdate)", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            //cmd.Parameters.AddWithValue("@invoiceno", li.sino);
            cmd.Parameters.AddWithValue("@invoiceno", li.strsino);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@issipi", li.issipi);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@refid", li.refid);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertbankacshadowdatatemp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into TempBankACShadow (voucherno,ccode,invoiceno,amount,istype,cno,uname,udate,issipi,refid,acname,voucherdate) values (@voucherno,@ccode,@invoiceno,@amount,@istype,@cno,@uname,@udate,@issipi,@refid,@acname,@voucherdate)", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            //cmd.Parameters.AddWithValue("@invoiceno", li.sino);
            cmd.Parameters.AddWithValue("@invoiceno", li.strsino);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@issipi", li.issipi);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@refid", li.refid);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteshadowtabledata(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("delete from BankACShadow where voucherno=@voucherno and istype=@istype and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            //cmd.Parameters.AddWithValue("@istype", li.istype);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd = new SqlCommand("delete from BankACShadow where refid=@id and istype=@istype and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletetepshadowtabledatafroidnvno(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("delete from BankACShadow where voucherno=@voucherno and istype=@istype and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            //cmd.Parameters.AddWithValue("@istype", li.istype);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd = new SqlCommand("delete from TempBankACShadow where refid=@id and istype=@istype and voucherno=@voucherno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectamounttodebitfromacfromaccountname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from BankACMaster inner join BankACShadow on BankACMaster.voucherno=BankACShadow.voucherno where BankACMaster.cno=@cno and BankACMaster.voucherno=@voucherno", con);
            da = new SqlDataAdapter("select * from BankACMaster inner join BankACShadow on BankACMaster.id=BankACShadow.refid where BankACMaster.cno=@cno and BankACMaster.id=@id and BankACMaster.istype=@istype", con);
            //da.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@istype", li.istype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectdatatfromsino(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where cno=@cno and sino=@sino", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@sino", li.sino);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selecttempbankacshadowdatatfromrefid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from TempBankACShadow where cno=@cno and refid=@refid", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@refid", li.refid);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectdatatfromsinostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where cno=@cno and strsino=@strsino", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@strsino", li.strsino);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectdatatfrompino(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where cno=@cno and pino=@pino", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pino", li.pino);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectdatatfrompinostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where cno=@cno and strpino=@strpino", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@strpino", li.strpino);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void deletebr1datajv(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where voucherno=@voucherno and cno=@cno and istype='JV'", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteallledgerdatabr(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where voucherno=@voucherno and cno=@cno and istype='BR'", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletefromtempbankacshadow(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from TempBankACShadow where refid=@refid and cno=@cno and istype=@istype", con);
            cmd.Parameters.AddWithValue("@refid", li.refid1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteallledgerdatabp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where voucherno=@voucherno and cno=@cno and istype='BP'", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    //crcp
    public void deleteallledgerdatacr(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where voucherno=@voucherno and cno=@cno and istype='CR'", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteallledgerdatacp(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where voucherno=@voucherno and cno=@cno and istype='CP'", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteallledgerdatapc(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where voucherno=@voucherno and cno=@cno and istype='PC'", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallvnofromjv1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select BankACMaster1.voucherno from BankACMaster1 inner join BankACMaster on BankACMaster.voucherno=BankACMaster1.voucherno where BankACMaster1.voucherno like '%'+@fullname+'%' and BankACMaster1.istype='JV' and BankACMaster1.name=@acname group by BankACMaster1.voucherno", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select (acname+'-'+city) as tt,* from ACMaster where actype='General' order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallacnameforjv(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select (acname+'-'+city) as tt,* from ACMaster order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallbankname11(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where actype='Bank' order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemname22(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalljvdatagroupby(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from Ledger where voucherno=@voucherno and istype='JV' and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void deleteledger_edata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from Ledger_e where strvoucherno=@strvoucherno", con);
            cmd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectgstnofromacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname='" + li.acname + "'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where actype='Sales' and acname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpurchaseacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where actype='Purchase' and acname like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallcodename(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACGroupMaster where groupcode like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}