﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForItemMaster
/// </summary>
public class ForItemMaster : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForItemMaster()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallitemdata()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkduplicate(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where cno=@cno and itemname=@itemname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectopbalancedata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster1 where cno=@cno and itemname=@itemname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemdatacnowise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where cno=@cno order by itemname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectitemdatafromid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ItemMaster (itemname,description,unit,unitewaybill,hsncode,salesrate,purchaserate,valuationrate,vattype,oprate,qty,rate,amount,cno,uname,udate,gsttype,vatdesc,gstdesc,isblock,reason,opqty,acyear,unitewaybill1) values (@itemname,@description,@unit,@unitewaybill,@hsncode,@salesrate,@purchaserate,@valuationrate,@vattype,@oprate,@qty,@rate,@amount,@cno,@uname,@udate,@gsttype,@vatdesc,@gstdesc,@isblock,@reason,@opqty,@acyear,@unitewaybill1)", con);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@description", li.description);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@unitewaybill", li.unitewaybill);
            cmd.Parameters.AddWithValue("@unitewaybill1", li.unitewaybill1);
            cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
            cmd.Parameters.AddWithValue("@salesrate", li.salesrate);
            cmd.Parameters.AddWithValue("@purchaserate", li.purchaserate);
            cmd.Parameters.AddWithValue("@valuationrate", li.valuationrate);
            cmd.Parameters.AddWithValue("@vattype", li.vattype);
            cmd.Parameters.AddWithValue("@oprate", li.oprate);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@opqty", li.opqty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@gsttype", li.gsttype);
            cmd.Parameters.AddWithValue("@vatdesc", li.vatdesc);
            cmd.Parameters.AddWithValue("@gstdesc", li.gstdesc);
            cmd.Parameters.AddWithValue("@isblock", li.isblock);
            cmd.Parameters.AddWithValue("@reason", li.reason);
            cmd.Parameters.AddWithValue("@acyear", li.acyear);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ItemMaster set itemname=@itemname,description=@description,unit=@unit,unitewaybill=@unitewaybill,hsncode=@hsncode,salesrate=@salesrate,purchaserate=@purchaserate,valuationrate=@valuationrate,vattype=@vattype,oprate=@oprate,qty=@qty,rate=@rate,amount=@amount,cno=@cno,uname=@uname,udate=@udate,gsttype=@gsttype,vatdesc=@vatdesc,gstdesc=@gstdesc,isblock=@isblock,reason=@reason,opqty=@opqty,unitewaybill1=@unitewaybill1 where id=@id", con);//,acyear=@acyear
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@description", li.description);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@unitewaybill", li.unitewaybill);
            cmd.Parameters.AddWithValue("@unitewaybill1", li.unitewaybill1);
            cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
            cmd.Parameters.AddWithValue("@salesrate", li.salesrate);
            cmd.Parameters.AddWithValue("@purchaserate", li.purchaserate);
            cmd.Parameters.AddWithValue("@valuationrate", li.valuationrate);
            cmd.Parameters.AddWithValue("@vattype", li.vattype);
            cmd.Parameters.AddWithValue("@oprate", li.oprate);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@opqty", li.opqty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@gsttype", li.gsttype);
            cmd.Parameters.AddWithValue("@vatdesc", li.vatdesc);
            cmd.Parameters.AddWithValue("@gstdesc", li.gstdesc);
            cmd.Parameters.AddWithValue("@isblock", li.isblock);
            cmd.Parameters.AddWithValue("@reason", li.reason);
            //cmd.Parameters.AddWithValue("@acyear", li.acyear);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertopitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ItemMaster1 (itemname,opdate,qty,rate,amount,cno,uname,udate,adjqty,acyear) values (@itemname,@opdate,@qty,@rate,@amount,@cno,@uname,@udate,@adjqty,@acyear)", con);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@opdate", li.opdate);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@rate", li.oprate);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@adjqty", li.adjqty);
            cmd.Parameters.AddWithValue("@acyear", li.acyear);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ItemMaster where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteopitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ItemMaster1 where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteopitemdata1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ItemMaster1 where itemname=@itemname", con);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectitemdatafromitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from ItemMaster where itemname like '%'+@itemname+'%' and cno=@cno order by itemname", con);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectopitembalancedata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select itemname,(select isnull(sum(qty),0) from ItemMaster1 where itemname=@itemname) as opqty,(select isnull(sum(amount),0) from ItemMaster1 where itemname=@itemname) as opamount,(select isnull(SUM(qty),0) from SCItems where itemname=@itemname)as tots,(select isnull(SUM(qty),0) from PCItems where itemname=@itemname)as totp,((select isnull(SUM(qty),0) from SCItems where itemname=@itemname)-(select isnull(SUM(qty),0) from PCItems where itemname=@itemname)) as stockqty,(((select isnull(SUM(qty),0) from SCItems where itemname=@itemname)-(select isnull(SUM(qty),0) from PCItems where itemname=@itemname))+(select isnull(sum(qty),0) as totopqty from ItemMaster1 where itemname=@itemname)) as clqty from ItemMaster where itemname=@itemname and cno=@cno", con);without stockjv
            da = new SqlDataAdapter("select itemname,(select isnull(sum(opqty),0) from ItemMaster where itemname=@itemname) as opqty,(select isnull(sum(amount),0) from ItemMaster1 where itemname=@itemname) as opamount,((select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname))as tots,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)+(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname))as totp,((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname)) as stockqty,(((select isnull(SUM(stockqty),0) from PCItems where itemname=@itemname)-(select isnull(SUM(stockqty),0) from SCItems where itemname=@itemname))+(select isnull(sum(opqty),0) as totopqty from ItemMaster where itemname=@itemname)++(select isnull(SUM(qty),0) from StockJVItems where inout='IN' and itemname=@itemname)-(select isnull(SUM(qty),0) from StockJVItems where inout='OUT' and itemname=@itemname)) as clqty from ItemMaster where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    //update itemname everywhere
    public void updateItemMaster1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ItemMaster1 set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatePCItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCItems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatePerInvItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PerInvItems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateCommInvItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update CommInvItems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatePIItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIItems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatePurchaseOrderItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PurchaseOrderItems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateQuoItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update QuoItems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updaterritems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update rritems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateSalesOrderItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrderItems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateSCItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCItems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateSIItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIItems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    //public void updatehsncodeinledger_e(LogicLayer li)
    //{
    //    try
    //    {
    //        cmd = new SqlCommand("update Ledger_e set hsncode=@hsncode1 where hsncode=@hsncode", con);
    //        cmd.Parameters.AddWithValue("@hsncode1", li.hsncode1);
    //        cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
    //        con.Open();
    //        cmd.ExecuteNonQuery();
    //        con.Close();
    //    }
    //    catch (Exception ex)
    //    { }
    //    finally
    //    {
    //        if (con.State == ConnectionState.Open)
    //        {
    //            con.Close();
    //        }
    //    }
    //}

    public void updateStockJVItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update StockJVItems set itemname=@itemname1 where itemname=@itemname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@itemname1", li.itemname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    //select item used or not

    public DataTable checkItemMaster1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from ItemMaster1 where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkPCItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from PCItems where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkPerInvItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from PerInvItems where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkPIItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from PIItems where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkPurchaseOrderItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from PurchaseOrderItems where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkQuoItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from QuoItems where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkrritems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from rritems where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkSalesOrderItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from SalesOrderItems where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkSCItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from SCItems where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkSIItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from SIItems where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkStockJVItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from StockJVItems where itemname=@itemname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkacyearfordata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectewaybillunit(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Eway Bill Unit'", con);
            //da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatehsncodeinitemdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ItemMaster set hsncode=@hsncode where id=@id", con);//,acyear=@acyear
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}