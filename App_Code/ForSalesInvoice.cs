﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForSalesInvoice
/// </summary>
public class ForSalesInvoice : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForSalesInvoice()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallsimasterdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where cno=@cno order by sino desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsimasterdatafrominvtype(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where cno=@cno and invtype=@invtype order by sino desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@invtype", li.invtype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsimasterdatafromsino(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where sino=@sino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@sino", li.sino);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsimasterdatafromsinostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where strsino=@strsino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@strsino", li.strsino);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsimasterdatafromsinoag(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where sino=@sino and invtype=@invtype and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@sino", li.sino);
            da.SelectCommand.Parameters.AddWithValue("@invtype", li.invtype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsiitemsfromsino(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIItems where sino=@sino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sino", li.sino);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsiitemsfromsinostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIItems where strsino=@strsino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@strsino", li.strsino);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertsiitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into SIItems (sino,sidate,itemname,qty,stockqty,rate,basicamount,taxtype,vatp,vatamt,addtaxp,addtaxamt,cstp,cstamt,unit,amount,ccode,descr1,descr2,descr3,cno,uname,udate,qtyremain,qtyused,vno,vid,taxdesc,monyr,strsino) values (@sino,@sidate,@itemname,@qty,@stockqty,@rate,@basicamount,@taxtype,@vatp,@vatamt,@addtaxp,@addtaxamt,@cstp,@cstamt,@unit,@amount,@ccode,@descr1,@descr2,@descr3,@cno,@uname,@udate,@qtyremain,@qtyused,@vno,@vid,@taxdesc,@monyr,@strsino)", con);
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@sidate", li.sidate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@stockqty", li.stockqty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@taxtype", li.taxtype);
            cmd.Parameters.AddWithValue("@taxdesc", li.taxdesc);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@vno", li.vnono);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesiitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIItems set sidate=@sidate,itemname=@itemname,qty=@qty,stockqty=@stockqty,rate=@rate,basicamount=@basicamount,taxtype=@taxtype,vatp=@vatp,vatamt=@vatamt,addtaxp=@addtaxp,addtaxamt=@addtaxamt,cstp=@cstp,cstamt=@cstamt,unit=@unit,amount=@amount,ccode=@ccode,descr1=@descr1,descr2=@descr2,descr3=@descr3,uname=@uname,udate=@udate,qtyremain=@qtyremain,qtyused=@qtyused,taxdesc=@taxdesc,strsino=@strsino where id=@id", con);
            //cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@sidate", li.sidate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@stockqty", li.stockqty);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@taxtype", li.taxtype);
            cmd.Parameters.AddWithValue("@taxdesc", li.taxdesc);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertsimasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into SIMaster (sino,sidate,invtype,sono,sodate,scno,scdate,scno1,scdate1,acname,salesac,totbasicamount,totvat,totaddtax,totcst,servicetaxp,servicetaxamount,cartage,roundoff,billamount,type,remainamount,strsino,status,cno,uname,udate,remarks,form,transport,salesman,lrno,lrdate,duedays,monyr,stringscno,portcode,sbillno,sbilldate) values (@sino,@sidate,@invtype,@sono,@sodate,@scno,@scdate,@scno1,@scdate1,@acname,@salesac,@totbasicamount,@totvat,@totaddtax,@totcst,@servicetaxp,@servicetaxamount,@cartage,@roundoff,@billamount,@type,@remainamount,@strsino,@status,@cno,@uname,@udate,@remarks,@form,@transport,@salesman,@lrno,@lrdate,@duedays,@monyr,@stringscno,@portcode,@sbillno,@sbilldate)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@sidate", li.sidate);
            cmd.Parameters.AddWithValue("@invtype", li.invtype);
            cmd.Parameters.AddWithValue("@sono", li.strsono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@scno", li.strscno);
            cmd.Parameters.AddWithValue("@scdate", li.scdate);
            cmd.Parameters.AddWithValue("@scno1", li.strscno1);
            cmd.Parameters.AddWithValue("@scdate1", li.scdate1);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@salesac", li.salesac);
            cmd.Parameters.AddWithValue("@totbasicamount", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddtax", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@servicetaxp", li.servicetaxp);
            cmd.Parameters.AddWithValue("@servicetaxamount", li.servicetaxamount);
            cmd.Parameters.AddWithValue("@cartage", li.cartage);
            cmd.Parameters.AddWithValue("@roundoff", li.roundoff);
            cmd.Parameters.AddWithValue("@billamount", li.billamount);
            cmd.Parameters.AddWithValue("@type", li.type);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@form", li.form);
            cmd.Parameters.AddWithValue("@transport", li.transportname);
            cmd.Parameters.AddWithValue("@salesman", li.salesmanname);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@duedays", li.duedays);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@stringscno", li.stringscno);
            cmd.Parameters.AddWithValue("@portcode", li.portcode);
            cmd.Parameters.AddWithValue("@sbilldate", li.sbilldate);
            cmd.Parameters.AddWithValue("@sbillno", li.sbillno);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesimasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set sidate=@sidate,invtype=@invtype,sono=@sono,sodate=@sodate,scno=@scno,scdate=@scdate,scno1=@scno1,scdate1=@scdate1,acname=@acname,salesac=@salesac,totbasicamount=@totbasicamount,totvat=@totvat,totaddtax=@totaddtax,totcst=@totcst,servicetaxp=@servicetaxp,servicetaxamount=@servicetaxamount,cartage=@cartage,roundoff=@roundoff,billamount=@billamount,remainamount=@remainamount,strsino=@strsino,status=@status,uname=@uname,udate=@udate,remarks=@remarks,form=@form,transport=@transport,salesman=@salesman,lrno=@lrno,lrdate=@lrdate,duedays=@duedays,monyr=@monyr where sino=@sino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@sidate", li.sidate);
            cmd.Parameters.AddWithValue("@invtype", li.invtype);
            cmd.Parameters.AddWithValue("@sono", li.strsono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@scno", li.strscno);
            cmd.Parameters.AddWithValue("@scdate", li.scdate);
            cmd.Parameters.AddWithValue("@scno1", li.strscno1);
            cmd.Parameters.AddWithValue("@scdate1", li.scdate1);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@salesac", li.salesac);
            cmd.Parameters.AddWithValue("@totbasicamount", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddtax", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@servicetaxp", li.servicetaxp);
            cmd.Parameters.AddWithValue("@servicetaxamount", li.servicetaxamount);
            cmd.Parameters.AddWithValue("@cartage", li.cartage);
            cmd.Parameters.AddWithValue("@roundoff", li.roundoff);
            cmd.Parameters.AddWithValue("@billamount", li.billamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@form", li.form);
            cmd.Parameters.AddWithValue("@transport", li.transportname);
            cmd.Parameters.AddWithValue("@salesman", li.salesmanname);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@duedays", li.duedays);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            //cmd.Parameters.AddWithValue("@portcode", li.portcode);
            //cmd.Parameters.AddWithValue("@sbilldate", li.sbilldate);
            //cmd.Parameters.AddWithValue("@sbillno", li.sbillno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesimasterdatastring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set sino=@sino,sidate=@sidate,invtype=@invtype,sono=@sono,sodate=@sodate,scno=@scno,scdate=@scdate,scno1=@scno1,scdate1=@scdate1,acname=@acname,salesac=@salesac,totbasicamount=@totbasicamount,totvat=@totvat,totaddtax=@totaddtax,totcst=@totcst,servicetaxp=@servicetaxp,servicetaxamount=@servicetaxamount,cartage=@cartage,roundoff=@roundoff,billamount=@billamount,remainamount=@remainamount,strsino=@strsino,status=@status,uname=@uname,udate=@udate,remarks=@remarks,form=@form,transport=@transport,salesman=@salesman,lrno=@lrno,lrdate=@lrdate,duedays=@duedays,monyr=@monyr,stringscno=@stringscno,dtors=@dtors,portcode=@portcode,sbillno=@sbillno,sbilldate=@sbilldate where strsino=@strsino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@sidate", li.sidate);
            cmd.Parameters.AddWithValue("@invtype", li.invtype);
            cmd.Parameters.AddWithValue("@sono", li.strsono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@scno", li.strscno);
            cmd.Parameters.AddWithValue("@scdate", li.scdate);
            cmd.Parameters.AddWithValue("@scno1", li.strscno1);
            cmd.Parameters.AddWithValue("@scdate1", li.scdate1);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@salesac", li.salesac);
            cmd.Parameters.AddWithValue("@totbasicamount", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddtax", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@servicetaxp", li.servicetaxp);
            cmd.Parameters.AddWithValue("@servicetaxamount", li.servicetaxamount);
            cmd.Parameters.AddWithValue("@cartage", li.cartage);
            cmd.Parameters.AddWithValue("@roundoff", li.roundoff);
            cmd.Parameters.AddWithValue("@billamount", li.billamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@form", li.form);
            cmd.Parameters.AddWithValue("@transport", li.transportname);
            cmd.Parameters.AddWithValue("@salesman", li.salesmanname);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@duedays", li.duedays);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@stringscno", li.stringscno);
            cmd.Parameters.AddWithValue("@dtors", li.dtors);
            cmd.Parameters.AddWithValue("@portcode", li.portcode);
            cmd.Parameters.AddWithValue("@sbilldate", li.sbilldate);
            cmd.Parameters.AddWithValue("@sbillno", li.sbillno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatemonyrinsiitems(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SCItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SIItems set monyr=@monyr where sino=@sino and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@vno", li.vnono1);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatemonyrinsiitemsstring(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SCItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SIItems set monyr=@monyr,sidate=@sidate where strsino=@sino and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@vno", li.vnono1);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@sino", li.strsino);
            cmd.Parameters.AddWithValue("@sidate", li.sidate);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateremainqtyinsaleschallan(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SCItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SCItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@vno", li.vnono1);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletesimasterdatafromsino(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SIMaster where sino=@sino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletesimasterdatafromsinostring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SIMaster where strsino=@strsino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletesiitemsdatafromsino(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SIItems where sino=@sino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletesiitemsdatafromsinostring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SIItems where strsino=@strsino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletesiitemsdatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SIItems where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallsalesorderitemdatafromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrderItems inner join SalesOrder on SalesOrder.sono=SalesOrderItems.sono where SalesOrder.sono=@sono and SalesOrder.cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemdatafromscnotexhchange(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select * from SCItems inner join SCMaster on SCMaster.scno=SCItems.scno where SCMaster.scno in (" + li.remarks + ") and SCMaster.cno=@cno and SCItems.qtyremain>0", con);
            da = new SqlDataAdapter("select * from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno inner join ItemMaster on ItemMaster.itemname=SCItems.itemname where SCMaster.scno in (" + li.remarks + ") and SCMaster.cno=@cno and SCItems.qtyremain>0", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@scno", li.scno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemdatafromscnotexhchangestring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select * from SCItems inner join SCMaster on SCMaster.scno=SCItems.scno where SCMaster.scno in (" + li.remarks + ") and SCMaster.cno=@cno and SCItems.qtyremain>0", con);
            //da = new SqlDataAdapter("select * from SCItems inner join SCMaster on SCMaster.scno=SCItems.scno inner join ItemMaster on ItemMaster.itemname=SCItems.itemname where SCMaster.strscno in (" + li.remarks + ") and SCMaster.cno=@cno and SCItems.qtyremain>0", con);
            da = new SqlDataAdapter("select * from SCItems inner join SCMaster on SCMaster.strscno=SCItems.strscno inner join ItemMaster on ItemMaster.itemname=SCItems.itemname where SCMaster.strscno in (" + li.remarks + ") and SCMaster.cno=@cno order by SCItems.id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@scno", li.scno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallscmasterdatafromscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where scno=@scno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@scno", li.scno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }


    public DataTable selectallscmasterdatafromscnostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where strscno=@strscno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@strscno", li.strscno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertledgerdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ledger (voucherno,strvoucherno,voucherdate,refid,debitcode,creditcode,description,projectcode,amount,type,istype,cno,uname,udate) values (@voucherno,@strvoucherno,@voucherdate,@refid,@debitcode,@creditcode,@description,@projectcode,@amount,@type,@istype,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@refid", li.refid);
            cmd.Parameters.AddWithValue("@debitcode", li.debitcode);
            cmd.Parameters.AddWithValue("@creditcode", li.creditcode);
            cmd.Parameters.AddWithValue("@description", li.description);
            cmd.Parameters.AddWithValue("@projectcode", li.ccode);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@type", li.istype1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateledgerdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ledger set voucherdate=@voucherdate,debitcode=@debitcode,creditcode=@creditcode,projectcode=@projectcode,amount=@amount,uname=@uname,udate=@udate where voucherno=@voucherno and istype='SI' and description=@description", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@debitcode", li.debitcode);
            cmd.Parameters.AddWithValue("@creditcode", li.creditcode);
            cmd.Parameters.AddWithValue("@projectcode", li.ccode);
            cmd.Parameters.AddWithValue("@description", li.description);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectunusedsalesinvoiceno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and issiused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedsalesinvoicenolast(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select sino from SIMaster where cno=@cno order by sino desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisused(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set issiused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.sino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescstatus(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set status='Closed' where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescstatusforallchno(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set status='Closed' where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescstatusforallchnoa1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set status='Open' where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescstatusforallchnoopen(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set status='Open' where strscno in (" + li.stringscno + ") and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescstatusopen(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set status='Open' where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallsaleschallanforpopup(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where status!='Closed' and cno=@cno and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderforpopup(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrder where status!='Closed' and cno=@cno and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallstatus(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Status' order by name", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallinvtype(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Inv Type' order by name", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectscdatefromscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select convert(varchar(50),scdate,105) as cdate from SCMaster where scno=@scno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@scno", li.scno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectscdatefromscnostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select convert(varchar(50),scdate,105) as cdate from SCMaster where strscno=@strscno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@strscno", li.strscno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectscdatefromscno1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select convert(varchar(50),scdate,105) as cdate from SCMaster where scno=@scno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@scno", li.scno1);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectscdatefromscno1string(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select convert(varchar(50),scdate,105) as cdate from SCMaster where strscno=@strscno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@strscno", li.strscno1);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateqtyduringdelete(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SCItems set qtyremain=@qtyremain,qtyused=@qtyused where itemname=@itemname and scno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SCItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@vid", con);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            //cmd.Parameters.AddWithValue("@vno", li.vnono);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectqtyremainusedfromscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SCItems where cno=@cno and scno=@scno and itemname=@itemname", con);
            da = new SqlDataAdapter("select * from SCItems where id=@vid", con);
            da.SelectCommand.Parameters.AddWithValue("@vid", li.vid);
            //da.SelectCommand.Parameters.AddWithValue("@scno", li.vnono);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectqtyremainusedfromscno1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SCItems where cno=@cno and scno=@vno and itemname=@itemname", con);
            da = new SqlDataAdapter("select * from SCItems where id=@vid", con);
            da.SelectCommand.Parameters.AddWithValue("@vid", li.vid);
            //da.SelectCommand.Parameters.AddWithValue("@vno", li.scno);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectqtyremainusedfromscno11(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCItems where id=@vid", con);
            da.SelectCommand.Parameters.AddWithValue("@vid", li.vid);
            //da.SelectCommand.Parameters.AddWithValue("@vno", li.scno1);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisusedn(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set issiused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.sino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesimasterdataupdatesi(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set sino=@sino1 where sino=@sino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino1", li.sino1);
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesimasterdataupdatesistring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set sino=@sino1,invtype=@invtype1,strsino=@strsino1 where strsino=@strsino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino1", li.sino1);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@strsino1", li.strsino1);
            cmd.Parameters.AddWithValue("@invtype1", li.invtype1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesiitemsdataupdatesi(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIItems set sino=@sino1 where sino=@sino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino1", li.sino1);
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesiitemsdataupdatesistring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIItems set sino=@sino1,strsino=@strsino1 where strsino=@strsino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino1", li.sino1);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@strsino1", li.strsino1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatesiitemsdatedataupdatesistring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIItems set sidate=@sidate where strsino=@strsino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sidate", li.sidate);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateledgerdataupdatestrvouchernostring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ledger set voucherno=@sino1,strvoucherno=@strsino1 where strvoucherno=@strsino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@sino1", li.sino1);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@strsino1", li.strsino1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectalltaxacdesc()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from TaxMaster where vatdesc!='' and addtaxdesc!='' and cstdesc!='' and servicetaxdesc!='' order by id desc", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalltaxacdescfrommisc()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Tax Type'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void deletesalesinvoiceledger(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where voucherno=@sino and cno=@cno and istype='SI'", con);
            cmd.Parameters.AddWithValue("@sino", li.sino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletesalesinvoiceledgeraa(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where strvoucherno=@strvoucherno and cno=@cno and istype='SI'", con);
            cmd.Parameters.AddWithValue("@strvoucherno", li.strsino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where actype='Sales' order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where isblock='N' order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatestrsinoinscitems(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SCItems set strsino=@strsino where id=@id", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            //cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrsinoinscitems1(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SCItems set strsino=@strsino where id=@vid", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            //cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrsinoinscmaster(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SCMaster set strsino=@strsino where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@strscno", li.strscno11);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrsinoinscmastera(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SCItems set strsino=@strsino where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@strscno", li.strscno11);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateewaybilldetailfromstrsino(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set ewaybillno=@ewaybillno,ewaybilldate=@ewaybilldate,cewaybillno=@cewaybillno,cewaybilldate=@cewaybilldate,supplytype=@supplytype,subtype=@subtype,doctype=@doctype,tramode=@tramode,distanceinkm=@distanceinkm,transportername=@transportername,vehicleno=@vehicleno,vehicletype=@vehicletype,mainhsncode=@mainhsncode,trano=@trano,tradate=@tradate,traid=@traid,consignor=@consignor,consignoradd1=@consignoradd1,consignoradd2=@consignoradd2,consignorplace=@consignorplace,consignorpincode=@consignorpincode,consignorstate=@consignorstate,actualfromstatecode=@actualfromstatecode,consignorgst=@consignorgst,consignee=@consignee,consigneeadd1=@consigneeadd1,consigneeadd2=@consigneeadd2,consigneeplace=@consigneeplace,consigneepincode=@consigneepincode,consigneestate=@consigneestate,actualtostatecode=@actualtostatecode,consigneegst=@consigneegst where strsino=@strsino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);

            cmd.Parameters.AddWithValue("@ewaybillno", li.ewaybillno);
            cmd.Parameters.AddWithValue("@ewaybilldate", li.ewaybilldate);
            cmd.Parameters.AddWithValue("@cewaybillno", li.cewaybillno);
            cmd.Parameters.AddWithValue("@cewaybilldate", li.cewaybilldate);
            cmd.Parameters.AddWithValue("@supplytype", li.supplytype);
            cmd.Parameters.AddWithValue("@subtype", li.subtype);
            cmd.Parameters.AddWithValue("@doctype", li.doctype);

            cmd.Parameters.AddWithValue("@tramode", li.tramode);
            cmd.Parameters.AddWithValue("@distanceinkm", li.distanceinkm);
            cmd.Parameters.AddWithValue("@transportername", li.transportername);
            cmd.Parameters.AddWithValue("@vehicleno", li.vehiclenumber);
            cmd.Parameters.AddWithValue("@vehicletype", li.vehicletype);
            cmd.Parameters.AddWithValue("@mainhsncode", li.mainhsncode);
            cmd.Parameters.AddWithValue("@trano", li.trano);
            cmd.Parameters.AddWithValue("@tradate", li.tradate);
            cmd.Parameters.AddWithValue("@traid", li.traid);

            cmd.Parameters.AddWithValue("@consignor", li.consignor);
            cmd.Parameters.AddWithValue("@consignoradd1", li.consignoradd1);
            cmd.Parameters.AddWithValue("@consignoradd2", li.consignoradd2);
            cmd.Parameters.AddWithValue("@consignorplace", li.consignorplace);
            cmd.Parameters.AddWithValue("@consignorpincode", li.consignorpincode);
            cmd.Parameters.AddWithValue("@consignorstate", li.consignorstate);
            cmd.Parameters.AddWithValue("@actualfromstatecode", li.actualfromstate);
            cmd.Parameters.AddWithValue("@consignorgst", li.consignorgst);

            cmd.Parameters.AddWithValue("@consignee", li.consignee);
            cmd.Parameters.AddWithValue("@consigneeadd1", li.consigneeadd1);
            cmd.Parameters.AddWithValue("@consigneeadd2", li.consigneeadd2);
            cmd.Parameters.AddWithValue("@consigneeplace", li.consigneeplace);
            cmd.Parameters.AddWithValue("@consigneepincode", li.consigneepincode);
            cmd.Parameters.AddWithValue("@consigneestate", li.consigneestate);
            cmd.Parameters.AddWithValue("@actualtostatecode", li.actualtostate);
            cmd.Parameters.AddWithValue("@consigneegst", li.consigneegst);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallacdatefromacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname='" + li.acname + "'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectmainhsncode(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIItems inner join itemmaster on siitems.itemname=itemmaster.itemname where SIItems.strsino='" + li.strsino + "'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectewaybilldatafromstrsino(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select *,convert(varchar(20),ewaybilldate,103) as date1,convert(varchar(20),cewaybilldate,103) as date2,convert(varchar(20),tradate,103) as date3 from SIMaster where strsino='" + li.strsino + "'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatesimasterdataduringdelete(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set totbasicamount=@totbasicamount,totvat=@totvat,totaddtax=@totaddtax,totcst=@totcst,servicetaxp=@servicetaxp,servicetaxamount=@servicetaxamount,cartage=@cartage,roundoff=@roundoff,billamount=@billamount,remainamount=@remainamount,uname=@uname,udate=@udate where strsino=@strsino and cno=@cno", con);                        
            cmd.Parameters.AddWithValue("@totbasicamount", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddtax", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@servicetaxp", li.servicetaxp);
            cmd.Parameters.AddWithValue("@servicetaxamount", li.servicetaxamount);
            cmd.Parameters.AddWithValue("@cartage", li.cartage);
            cmd.Parameters.AddWithValue("@roundoff", li.roundoff);
            cmd.Parameters.AddWithValue("@billamount", li.billamount);
            cmd.Parameters.AddWithValue("@remainamount", li.billamount);
            cmd.Parameters.AddWithValue("@strsino", li.strsino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}