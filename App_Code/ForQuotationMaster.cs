﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForQuotationMaster
/// </summary>
public class ForQuotationMaster : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForQuotationMaster()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallitemdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where id in (" + li.remarks + ") order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectmiscdataitemwise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type=@type", con);
            da.SelectCommand.Parameters.AddWithValue("@type", li.type);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectlastquono()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select FLOOR(quono) as quono1,* from QuoMaster order by quono desc", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectquodatafromquono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from QuoMaster where quono=@quono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@quono", li.quono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectquoitemsdatafromquono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from QuoItems where quono=@quono and cno=@cno order by srno1", con);
            da = new SqlDataAdapter("select * from QuoItems where quono=@quono and cno=@cno order by srno1", con);
            da.SelectCommand.Parameters.AddWithValue("@quono", li.quono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallacnamedata()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertquomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into QuoMaster (quono,quodate,ccode,acname,basicamount,vat,advat,cst,amount,status,cno,uname,udate,annexure,isigst) values (@quono,@quodate,@ccode,@acname,@basicamount,@vat,@advat,@cst,@amount,@status,@cno,@uname,@udate,@annexure,@isigst)", con);
            cmd.Parameters.AddWithValue("@quono", li.quono);
            cmd.Parameters.AddWithValue("@quodate", li.quodate);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@vat", li.vatamt);
            cmd.Parameters.AddWithValue("@advat", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cst", li.cstamt);
            cmd.Parameters.AddWithValue("@amount", li.grandtotal);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@annexure", li.annexure);
            cmd.Parameters.AddWithValue("@isigst", li.isigst);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatequomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update QuoMaster set quodate=@quodate,ccode=@ccode,acname=@acname,basicamount=@basicamount,vat=@vat,advat=@advat,cst=@cst,amount=@amount,cno=@cno,uname=@uname,udate=@udate,status=@status,annexure=@annexure,isigst=@isigst where quono=@quono", con);
            cmd.Parameters.AddWithValue("@quono", li.quono);
            cmd.Parameters.AddWithValue("@quodate", li.quodate);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@vat", li.vatamt);
            cmd.Parameters.AddWithValue("@advat", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cst", li.cstamt);
            cmd.Parameters.AddWithValue("@amount", li.grandtotal);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@annexure", li.annexure);
            cmd.Parameters.AddWithValue("@isigst", li.isigst);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertquoitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into QuoItems (srno,quono,quodate,ccode,acname,itemname,qty,rate,basicamt,taxtype,vatp,vatamt,addtaxp,addtaxamt,cstp,cstamt,unit,amount,descr1,descr2,descr3,cno,uname,udate,make,hsncode,srno1) values (@srno,@quono,@quodate,@ccode,@acname,@itemname,@qty,@rate,@basicamt,@vattype,@vatp,@vatamt,@addtaxp,@addtaxamt,@cstp,@cstamt,@unit,@amount,@descr1,@descr2,@descr3,@cno,@uname,@udate,@make,@hsncode,@srno1)", con);
            cmd.Parameters.AddWithValue("@srno", li.srno);
            cmd.Parameters.AddWithValue("@srno1", li.srno1);
            cmd.Parameters.AddWithValue("@quono", li.quono);
            cmd.Parameters.AddWithValue("@quodate", li.quodate);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamt", li.basicamount);
            cmd.Parameters.AddWithValue("@vattype", li.taxtype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@make", li.make);
            cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatequoitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update QuoItems set srno=@srno,quono=@quono,quodate=@quodate,ccode=@ccode,acname=@acname,itemname=@itemname,qty=@qty,rate=@rate,basicamt=@basicamt,taxtype=@vattype,vatp=@vatp,vatamt=@vatamt,addtaxp=@addtaxp,addtaxamt=@addtaxamt,cstp=@cstp,cstamt=@cstamt,unit=@unit,amount=@amount,descr1=@descr1,descr2=@descr2,descr3=@descr3,cno=@cno,uname=@uname,udate=@udate,make=@make,hsncode=@hsncode,srno1=@srno1 where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@srno", li.srno);
            cmd.Parameters.AddWithValue("@srno1", li.srno1);
            cmd.Parameters.AddWithValue("@quono", li.quono);
            cmd.Parameters.AddWithValue("@quodate", li.quodate);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamt", li.basicamount);
            cmd.Parameters.AddWithValue("@vattype", li.taxtype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@make", li.make);
            cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletequoitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from QuoItems  where quono=@quono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@quono", li.quono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletequoitemsdatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from QuoItems where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletequomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from QuoMaster  where quono=@quono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@quono", li.quono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}