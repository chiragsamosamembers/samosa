﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForCommInvoice
/// </summary>
public class ForCommInvoice : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForCommInvoice()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallitemdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where id in (" + li.remarks + ") order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectmiscdataitemwise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type=@type", con);
            da.SelectCommand.Parameters.AddWithValue("@type", li.type);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectlastquono()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select FLOOR(invno) as invno1,* from CommInvMaster order by invno desc", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectquodatafromquono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from CommInvMaster where invno=@invno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@invno", li.invoiceno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectquoitemsdatafromquono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from CommInvItems where invno=@invno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@invno", li.invoiceno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallacnamedata()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertquomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into CommInvMaster (invno,invdate,ccode,acname,basicamount,vat,advat,cst,amount,status,cno,uname,udate,pono,podate,annexure,isigst,sono) values (@invno,@invdate,@ccode,@acname,@basicamount,@vat,@advat,@cst,@amount,@status,@cno,@uname,@udate,@pono,@podate,@annexure,@isigst,@sono)", con);
            cmd.Parameters.AddWithValue("@invno", li.invoiceno);
            cmd.Parameters.AddWithValue("@invdate", li.quodate);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@vat", li.vatamt);
            cmd.Parameters.AddWithValue("@advat", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cst", li.cstamt);
            cmd.Parameters.AddWithValue("@amount", li.grandtotal);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@podate", li.strpodate);
            cmd.Parameters.AddWithValue("@annexure", li.annexure);
            cmd.Parameters.AddWithValue("@isigst", li.isigst);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatequomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update CommInvMaster set invdate=@invdate,ccode=@ccode,acname=@acname,basicamount=@basicamount,vat=@vat,advat=@advat,cst=@cst,amount=@amount,cno=@cno,uname=@uname,udate=@udate,status=@status,pono=@pono,podate=@podate,annexure=@annexure,isigst=@isigst,sono=@sono where invno=@invno", con);
            cmd.Parameters.AddWithValue("@invno", li.invoiceno);
            cmd.Parameters.AddWithValue("@invdate", li.quodate);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@vat", li.vatamt);
            cmd.Parameters.AddWithValue("@advat", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cst", li.cstamt);
            cmd.Parameters.AddWithValue("@amount", li.grandtotal);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@podate", li.strpodate);
            cmd.Parameters.AddWithValue("@annexure", li.annexure);
            cmd.Parameters.AddWithValue("@isigst", li.isigst);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertquoitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into CommInvItems (invno,invdate,ccode,acname,itemname,qty,rate,basicamt,taxtype,vatp,vatamt,addtaxp,addtaxamt,cstp,cstamt,unit,amount,descr1,descr2,descr3,cno,uname,udate,vid) values (@invno,@invdate,@ccode,@acname,@itemname,@qty,@rate,@basicamt,@vattype,@vatp,@vatamt,@addtaxp,@addtaxamt,@cstp,@cstamt,@unit,@amount,@descr1,@descr2,@descr3,@cno,@uname,@udate,@vid)", con);
            cmd.Parameters.AddWithValue("@invno", li.invoiceno);
            cmd.Parameters.AddWithValue("@invdate", li.quodate);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamt", li.basicamount);
            cmd.Parameters.AddWithValue("@vattype", li.taxtype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatequoitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update CommInvItems set invno=@invno,invdate=@invdate,ccode=@ccode,acname=@acname,itemname=@itemname,qty=@qty,rate=@rate,basicamt=@basicamt,taxtype=@vattype,vatp=@vatp,vatamt=@vatamt,addtaxp=@addtaxp,addtaxamt=@addtaxamt,cstp=@cstp,cstamt=@cstamt,unit=@unit,amount=@amount,descr1=@descr1,descr2=@descr2,descr3=@descr3,cno=@cno,uname=@uname,udate=@udate where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@invno", li.invoiceno);
            cmd.Parameters.AddWithValue("@invdate", li.quodate);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamt", li.basicamount);
            cmd.Parameters.AddWithValue("@vattype", li.taxtype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletequoitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from CommInvItems  where invno=@invno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@invno", li.invoiceno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletequoitemsdatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from CommInvItems where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletequomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from CommInvMaster  where invno=@invno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@invno", li.invoiceno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallciitemdatafordirectsc(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno and qtyremain>0", con);
            da = new SqlDataAdapter("select * from CommInvItems where invno=@invno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@invno", li.invoiceno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallcimasterdatafordirectsc(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno and qtyremain>0", con);
            da = new SqlDataAdapter("select * from CommInvMaster where invno=@invno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@invno", li.invoiceno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallciitemdatafordirectpisc(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno and qtyremain>0", con);
            da = new SqlDataAdapter("select * from PerInvItems where id in (" + li.remarks + ") and cno=@cno", con);
            //da.SelectCommand.Parameters.AddWithValue("@invno", li.invoiceno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallcimasterdatafordirectpisc(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno and qtyremain>0", con);
            da = new SqlDataAdapter("select * from PerInvMaster where invno=@invno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@invno", li.invoiceno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}