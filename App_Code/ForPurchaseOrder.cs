﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForPurchaseOrder
/// </summary>
public class ForPurchaseOrder : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForPurchaseOrder()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallPurchaseorderdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrder where cno=@cno order by podate desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallPurchaseorderdatafrompono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrder where pono=@pono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.pono);
            //da.SelectCommand.Parameters.AddWithValue("@pono", li.strpono);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallPurchaseorderdatafromponostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrder where pono=@pono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.strpono);
            //da.SelectCommand.Parameters.AddWithValue("@pono", li.strpono);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallPurchaseorderitemdatafrompono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems where pono=@pono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.pono);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallPurchaseorderitemdatafromponostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems where pono=@pono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.strpono);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemdatafromitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where cno=@cno and itemname=@itemname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectalltaxdatafromtaxname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from TaxMaster where typename=@typename", con);
            da.SelectCommand.Parameters.AddWithValue("@typename", li.typename);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertpoitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into PurchaseOrderItems (pono,podate,itemname,qty,unit,rate,basicamount,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,descr1,descr2,descr3,cno,uname,udate,qtyremain,qtyused,vno,vid,adjqty) values (@pono,@podate,@itemname,@qty,@unit,@rate,@basicamt,@taxtype,@vatp,@addtaxp,@cstp,@vatamt,@addtaxamt,@cstamt,@amount,@descr1,@descr2,@descr3,@cno,@uname,@udate,@qtyremain,@qtyused,@vno,@vid,@adjqty)", con);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            //cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@podate", li.podate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamt", li.basicamount);
            cmd.Parameters.AddWithValue("@taxtype", li.vattype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@vno", li.vnono);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain1);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused1);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@adjqty", li.adjqty);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepoitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PurchaseOrderItems set podate=@podate,itemname=@itemname,qty=@qty,unit=@unit,rate=@rate,basicamount=@basicamount,taxtype=@taxtype,vatp=@vatp,addtaxp=@addtaxp,cstp=@cstp,vatamt=@vatamt,addtaxamt=@addtaxamt,cstamt=@cstamt,amount=@amount,descr1=@descr1,descr2=@descr2,descr3=@descr3,uname=@uname,udate=@udate,qtyremain=@qtyremain,qtyused=@qtyused where id=@id", con);
            //cmd.Parameters.AddWithValue("@pono", li.pono);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@podate", li.podate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@taxtype", li.vattype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertpomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into PurchaseOrder (pono,podate,sono,deldays,ccode,followupdate,acname,followupdetails,followupdetails1,totbasic,excisep,exciseamt,cstp,cstamt,vatp,vatamt,grandtotal,delivery,payment,deliveryat,taxes,octroi,excise,form,transportation,insurance,packing,descr1,descr2,descr3,descr4,descr5,descr6,status,cno,uname,udate,type) values (@pono,@podate,@sono,@deldays,@ccode,@followupdate,@acname,@followupdetails,@followupdetails1,@totbasic,@excisep,@exciseamt,@cstp,@cstamt,@vatp,@vatamt,@grandtotal,@delivery,@payment,@deliveryat,@taxes,@octroi,@excise,@form,@transportation,@insurance,@packing,@descr1,@descr2,@descr3,@descr4,@descr5,@descr6,@status,@cno,@uname,@udate,@type)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            //cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@podate", li.podate);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@deldays", li.deldays);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@followupdate", li.followupdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@followupdetails", li.followupdetails);
            cmd.Parameters.AddWithValue("@followupdetails1", li.followupdetails1);
            //cmd.Parameters.AddWithValue("@totbasic", li.totbasicamount);
            //cmd.Parameters.AddWithValue("@delat2", li.delat2);
            cmd.Parameters.AddWithValue("@totbasic", li.totbasicamount);
            cmd.Parameters.AddWithValue("@excisep", li.excisep);
            cmd.Parameters.AddWithValue("@exciseamt", li.exciseamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@grandtotal", li.grandtotal);
            cmd.Parameters.AddWithValue("@delivery", li.deliverysch);
            cmd.Parameters.AddWithValue("@payment", li.payment);
            cmd.Parameters.AddWithValue("@deliveryat", li.deliveryat);
            cmd.Parameters.AddWithValue("@taxes", li.taxes);
            cmd.Parameters.AddWithValue("@octroi", li.octroi);
            cmd.Parameters.AddWithValue("@excise", li.excise);
            cmd.Parameters.AddWithValue("@form", li.form);
            cmd.Parameters.AddWithValue("@transportation", li.transportation);
            cmd.Parameters.AddWithValue("@insurance", li.insurance);
            cmd.Parameters.AddWithValue("@packing", li.packing);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@descr4", li.descr4);
            cmd.Parameters.AddWithValue("@descr5", li.descr5);
            cmd.Parameters.AddWithValue("@descr6", li.descr6);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@type", li.type);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PurchaseOrder set podate=@podate,sono=@sono,deldays=@deldays,ccode=@ccode,followupdate=@followupdate,acname=@acname,followupdetails=@followupdetails,followupdetails1=@followupdetails1,totbasic=@totbasic,excisep=@excisep,exciseamt=@exciseamt,cstp=@cstp,cstamt=@cstamt,vatp=@vatp,vatamt=@vatamt,grandtotal=@grandtotal,delivery=@delivery,payment=@payment,deliveryat=@deliveryat,taxes=@taxes,octroi=@octroi,excise=@excise,form=@form,transportation=@transportation,insurance=@insurance,packing=@packing,descr1=@descr1,descr2=@descr2,descr3=@descr3,descr4=@descr4,descr5=@descr5,descr6=@descr6,status=@status,uname=@uname,udate=@udate,type=@type where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@podate", li.podate);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@deldays", li.deldays);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@followupdate", li.followupdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@followupdetails", li.followupdetails);
            cmd.Parameters.AddWithValue("@followupdetails1", li.followupdetails1);
            cmd.Parameters.AddWithValue("@totbasic", li.totbasicamount);
            cmd.Parameters.AddWithValue("@excisep", li.excisep);
            cmd.Parameters.AddWithValue("@exciseamt", li.exciseamt);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@grandtotal", li.grandtotal);
            cmd.Parameters.AddWithValue("@delivery", li.deliverysch);
            cmd.Parameters.AddWithValue("@payment", li.payment);
            cmd.Parameters.AddWithValue("@deliveryat", li.deliveryat);
            cmd.Parameters.AddWithValue("@taxes", li.taxes);
            cmd.Parameters.AddWithValue("@octroi", li.octroi);
            cmd.Parameters.AddWithValue("@excise", li.excise);
            cmd.Parameters.AddWithValue("@form", li.form);
            cmd.Parameters.AddWithValue("@transportation", li.transportation);
            cmd.Parameters.AddWithValue("@insurance", li.insurance);
            cmd.Parameters.AddWithValue("@packing", li.packing);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@descr4", li.descr4);
            cmd.Parameters.AddWithValue("@descr5", li.descr5);
            cmd.Parameters.AddWithValue("@descr6", li.descr6);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@type", li.type);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepomasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PurchaseOrder where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pono", li.pono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepomasterdatastring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PurchaseOrder where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepoitemsdatafrompono(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PurchaseOrderItems where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pono", li.pono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepoitemsdatafromponostring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PurchaseOrderItems where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepoitemsdatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PurchaseOrderItems where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallsalesorderitemdatafromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno and qtyremain1>0", con);
            da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderitemdatafromsonoforupdate(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno and qtyremain1>0", con);
            da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and id not in (" + li.remarks.TrimEnd(',') + ")", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderitemdatafromsonojoinitemmaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrderItems inner join ItemMaster on ItemMaster.itemname=SalesOrderItems.itemname where SalesOrderItems.sono=@sono and SalesOrderItems.cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderitemdatafromsonousingin(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrderItems where id in (" + li.remarks + ") and cno=@cno and qtyremain1>0", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems inner join ItemMaster on ItemMaster.itemname=SalesOrderItems.itemname where SalesOrderItems.id in (" + li.remarks + ") and SalesOrderItems.cno=@cno and SalesOrderItems.qtyremain1>0", con);
            //da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderitemdatafromsonousinginforiteminnerjoin(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from SalesOrderItems inner join ItemMaster on ItemMaster.itemname=SalesOrderItems.itemname where SalesOrderItems.id in (" + li.remarks + ") and SalesOrderItems.cno=@cno", con);
            //da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesordermasterdatafromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrder where sono=@sono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedpurchaseorderno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and ispoused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedpurchaseordernolast(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select pono from PurchaseOrder where cno=@cno order by pono desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedpurchaseordernolaststring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("SELECT LEFT(subsrt, PATINDEX('%[^0-9]%', subsrt + 't') - 1)  as ponum FROM (SELECT subsrt = SUBSTRING(pono, pos, LEN(pono)) FROM (SELECT pono, pos = PATINDEX('%[0-9]%', pono)FROM PurchaseOrder) d) t order by ponum desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisused(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set ispoused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.pono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateiscame(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PurchaseOrderItems set iscame=@iscame where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@iscame", li.iscame);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallstatus(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Status' order by name", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateremainqtyinsalesorder(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrderItems set qtyremain1=@qtyremain,qtyused1=@qtyused where id=@id", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@vno", li.vnono1);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectqtyremainusedfromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where cno=@cno and sono=@sono and itemname=@itemname", con);
            da = new SqlDataAdapter("select * from SalesOrderItems where id=@vid", con);
            //da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@vid", li.vid);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateqtyduringdelete(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrderItems set qtyremain1=@qtyremain,qtyused1=@qtyused where id=@vid", con);
            //cmd.Parameters.AddWithValue("@itemname", li.itemname);
            //cmd.Parameters.AddWithValue("@sono", li.sono);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedn(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set ispoused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.pono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallsalesorderforpopup(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrder where status!='Closed' and cno=@cno and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderforpopupfromccode(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrder where status!='Closed' and cno=@cno and ccode=@ccode", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderforpopupfromccodeforupdate(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrder where sono=@sono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatepomasterdataupdatepo(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PurchaseOrder set pono=@pono1 where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pono1", li.strpono1);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepoitemsdataupdatepo(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PurchaseOrderItems set pono=@pono1 where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pono1", li.strpono1);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepoitemsdatedataupdatepo(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PurchaseOrderItems set podate=@podate where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@podate", li.podate);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where isblock='N' order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectbillinvoicedatafrompcno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCMaster where ((stringpono like '%'+@strpono+'%') or pono=@strpono or pono1=@strpono) and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@strpono", li.strpono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectbillinvoicedatafrompcid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIItems where vid=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectbillinvoicedatafrompcidag(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCItems where vid=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatepomasterdataduringdelete(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PurchaseOrder set totbasic=@totbasic,excisep=@excisep,exciseamt=@exciseamt,cstp=@cstp,cstamt=@cstamt,vatp=@vatp,vatamt=@vatamt,grandtotal=@grandtotal,uname=@uname,udate=@udate where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@totbasic", li.totbasicamount);
            cmd.Parameters.AddWithValue("@excisep", li.excisep);
            cmd.Parameters.AddWithValue("@exciseamt", li.exciseamt);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            cmd.Parameters.AddWithValue("@grandtotal", li.grandtotal);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            //cmd.Parameters.AddWithValue("@type", li.type);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}