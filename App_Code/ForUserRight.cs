﻿using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System;
using System.IO;
using Survey.Classes;

/// <summary>
/// Summary description for ForUserRight
/// </summary>
public class ForUserRight : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;
    public ForUserRight()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallrole()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from MiscMaster where type='role'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }
    public DataTable getuserRole(LogicLayer li)
    {
        DataTable dtr = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from userrights where role='" + li.role + "' order by PAGE asc", con);
            da.Fill(dtr);
            con.Close();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dtr;
    }

    public DataTable selectuserrights(LogicLayer li)
    {
        DataTable dtr = new DataTable();
        //dtr = null;
        try
        {
            con.Open();
            //HttpContext.Current.Request.Cookies["ForLogin"]["UserRole"]
            da = new SqlDataAdapter("select * from userrights where role='" + SessionMgt.UserRole + "' and page='" + li.pagename + "'", con);
            da.Fill(dtr);
            con.Close();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dtr;
    }

    public DataTable getuserRoleNull()
    {
        DataTable dtr = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from pagename order by page asc", con);
            da.Fill(dtr);
            con.Close();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dtr;
    }

    public void insertrightsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into userrights (PAGE,pagename,UADD,UEDIT,UDELETE,UVIEW,ROLE) values (@PAGE,@pagename,@UADD,@UEDIT,@UDELETE,@UVIEW,@ROLE)", con);
            cmd.Parameters.AddWithValue("@PAGE", li.pagename);
            cmd.Parameters.AddWithValue("@pagename", li.pagename1);
            cmd.Parameters.AddWithValue("@UADD", li.UADD);
            cmd.Parameters.AddWithValue("@UEDIT", li.UEDIT);
            cmd.Parameters.AddWithValue("@UDELETE", li.UDELETE);
            //cmd.Parameters.AddWithValue("@UPRINT", li.UPRINT);
            cmd.Parameters.AddWithValue("@UVIEW", li.UVIEW);
            cmd.Parameters.AddWithValue("@ROLE", li.Role);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updaterightsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update userrights set PAGE=@PAGE,pagename=@pagename,UADD=@UADD,UEDIT=@UEDIT,UDELETE=@UDELETE,UVIEW=@UVIEW where ID=@ID and ROLE=@ROLE", con);
            cmd.Parameters.AddWithValue("@ID", li.id);
            cmd.Parameters.AddWithValue("@PAGE", li.pagename);
            cmd.Parameters.AddWithValue("@pagename", li.pagename1);
            cmd.Parameters.AddWithValue("@UADD", li.UADD);
            cmd.Parameters.AddWithValue("@UEDIT", li.UEDIT);
            cmd.Parameters.AddWithValue("@UDELETE", li.UDELETE);
            //cmd.Parameters.AddWithValue("@UPRINT", li.UPRINT);
            cmd.Parameters.AddWithValue("@UVIEW", li.UVIEW);
            cmd.Parameters.AddWithValue("@ROLE", li.Role);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

public DataTable selectuserdatafromid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from login where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}