﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for ForLogin
/// </summary>
public class ForLogin 
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForLogin()
    {
        con = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
    }

    public bool userlogin(LogicLayer li)
    {
        DataSet ds = new DataSet();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select * from login where username='" + li.username + "' and password='" + li.password + "' COLLATE SQL_Latin1_General_CP1_CS_AS", con);
            da = new SqlDataAdapter("SELECT * FROM login WHERE username='" + li.username + "' COLLATE SQL_Latin1_General_CP1_CS_AS and password='" + li.password + "' COLLATE SQL_Latin1_General_CP1_CS_AS", con);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        { 
        
        }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        if (ds.Tables[0].Rows.Count != 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public DataTable selectlogindatafromlogintable(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from login where username=@username and password=@password", con);
            da.SelectCommand.Parameters.AddWithValue("@username", li.username);
            da.SelectCommand.Parameters.AddWithValue("@password", li.password);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectcmastdatafromcno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from cmast where cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }  

    public DataTable selectacyearfromshopid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from acyear", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertaddnewuserdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into Login (username,password,cno,isactive,role) values (@username,@password,@cno,@isactive,@role)", con);
            cmd.Parameters.AddWithValue("@username", li.username);
            cmd.Parameters.AddWithValue("@password", li.password);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@isactive", li.isactive);
            cmd.Parameters.AddWithValue("@role", li.role);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable checkexpiry()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from expire", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}