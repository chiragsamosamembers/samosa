﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForMiscMaster
/// </summary>
public class ForMiscMaster : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

	public ForMiscMaster()
	{
        con = new SqlConnection(GetConnectionString());
	}

    public DataTable selectmiscdatafromtype(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type=@type", con);
            da.SelectCommand.Parameters.AddWithValue("@type", li.type);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertmiscdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into MiscMaster (type,name,uname,udate) values (@type,@name,@uname,@udate)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@type", li.type);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletemiscdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from MiscMaster where id=@id", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}