﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForAccountMaster
/// </summary>
public class ForAccountMaster : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForAccountMaster()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallaccountdata()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallaccounttypefrommiscdata()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Account Type' order by name", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkduplicate(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkduplicate1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname1);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallaccountdatacnowise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where cno=@cno order by acname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectaccountdatafromid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectaccountdataopfromacnaame(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from Ledger where debitcode=@acname and istype='OP'", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }


    public DataTable selectaccountdatadebitfromacnaame(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select isnull(sum(amount),0) as totd from Ledger where debitcode=@acname and type='D' and istype<>'OP'", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectaccountdatacreditfromacnaame(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select isnull(sum(amount),0) as totc from Ledger where debitcode=@acname and type='C' and istype<>'OP'", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }


    public void insertaccountdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ACMaster (acname,contactperson,add1,add2,add3,city,pincode,phone1,phone2,phoner1,phoner2,mobileno,fax,emailid,actype,gsttinno,date1,csttinno,date2,duedays,discp,servicetaxno,tan,pan,debitgroupcode,creditgroupcode,opbalance,cno,uname,udate,gstno,gstnodate,inexdebit,acyear,excludetax,inexport,state) values (@acname,@contactperson,@add1,@add2,@add3,@city,@pincode,@phone1,@phone2,@phoner1,@phoner2,@mobileno,@fax,@emailid,@actype,@gsttinno,@date1,@csttinno,@date2,@duedays,@discp,@servicetaxno,@tan,@pan,@debitgroupcode,@creditgroupcode,@opbalance,@cno,@uname,@udate,@gstno,@gstnodate,@inexdebit,@acyear,@excludetax,@inexport,@state)", con);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@contactperson", li.contactperson);
            cmd.Parameters.AddWithValue("@add1", li.add1);
            cmd.Parameters.AddWithValue("@add2", li.add2);
            cmd.Parameters.AddWithValue("@add3", li.add3);
            cmd.Parameters.AddWithValue("@city", li.city);
            cmd.Parameters.AddWithValue("@pincode", li.pincode);
            cmd.Parameters.AddWithValue("@phone1", li.phone1);
            cmd.Parameters.AddWithValue("@phone2", li.phone2);
            cmd.Parameters.AddWithValue("@phoner1", li.phoner1);
            cmd.Parameters.AddWithValue("@phoner2", li.phoner2);
            cmd.Parameters.AddWithValue("@mobileno", li.mobileno);
            cmd.Parameters.AddWithValue("@fax", li.fax);
            cmd.Parameters.AddWithValue("@emailid", li.emailid);
            cmd.Parameters.AddWithValue("@actype", li.actype);
            cmd.Parameters.AddWithValue("@gsttinno", li.gsttinno);
            cmd.Parameters.AddWithValue("@date1", li.date1);
            cmd.Parameters.AddWithValue("@csttinno", li.csttinno);
            cmd.Parameters.AddWithValue("@date2", li.date2);
            cmd.Parameters.AddWithValue("@duedays", li.duedays);
            cmd.Parameters.AddWithValue("@discp", li.discp);
            cmd.Parameters.AddWithValue("@servicetaxno", li.servicetaxno);
            cmd.Parameters.AddWithValue("@tan", li.tan);
            cmd.Parameters.AddWithValue("@pan", li.pan);
            cmd.Parameters.AddWithValue("@debitgroupcode", li.debitgroupcode);
            cmd.Parameters.AddWithValue("@creditgroupcode", li.creditgroupcode);
            cmd.Parameters.AddWithValue("@opbalance", li.opbalance);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@gstno", li.gstno);
            cmd.Parameters.AddWithValue("@gstnodate", li.gstnodate);
            cmd.Parameters.AddWithValue("@inexdebit", li.inexdebit);
            cmd.Parameters.AddWithValue("@acyear", li.acyear);
            cmd.Parameters.AddWithValue("@excludetax", li.excludetax);
            cmd.Parameters.AddWithValue("@inexport", li.inexport);
            cmd.Parameters.AddWithValue("@state", li.state);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateaccountdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ACMaster set acname=@acname,contactperson=@contactperson,add1=@add1,add2=@add2,add3=@add3,city=@city,pincode=@pincode,phone1=@phone1,phone2=@phone2,phoner1=@phoner1,phoner2=@phoner2,mobileno=@mobileno,fax=@fax,emailid=@emailid,actype=@actype,gsttinno=@gsttinno,date1=@date1,csttinno=@csttinno,date2=@date2,duedays=@duedays,discp=@discp,servicetaxno=@servicetaxno,tan=@tan,pan=@pan,debitgroupcode=@debitgroupcode,creditgroupcode=@creditgroupcode,opbalance=@opbalance,cno=@cno,uname=@uname,udate=@udate,gstno=@gstno,gstnodate=@gstnodate,inexdebit=@inexdebit,excludetax=@excludetax,inexport=@inexport,state=@state where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@acname", li.acname1);
            cmd.Parameters.AddWithValue("@contactperson", li.contactperson);
            cmd.Parameters.AddWithValue("@add1", li.add1);
            cmd.Parameters.AddWithValue("@add2", li.add2);
            cmd.Parameters.AddWithValue("@add3", li.add3);
            cmd.Parameters.AddWithValue("@city", li.city);
            cmd.Parameters.AddWithValue("@pincode", li.pincode);
            cmd.Parameters.AddWithValue("@phone1", li.phone1);
            cmd.Parameters.AddWithValue("@phone2", li.phone2);
            cmd.Parameters.AddWithValue("@phoner1", li.phoner1);
            cmd.Parameters.AddWithValue("@phoner2", li.phoner2);
            cmd.Parameters.AddWithValue("@mobileno", li.mobileno);
            cmd.Parameters.AddWithValue("@fax", li.fax);
            cmd.Parameters.AddWithValue("@emailid", li.emailid);
            cmd.Parameters.AddWithValue("@actype", li.actype);
            cmd.Parameters.AddWithValue("@gsttinno", li.gsttinno);
            cmd.Parameters.AddWithValue("@date1", li.date1);
            cmd.Parameters.AddWithValue("@csttinno", li.csttinno);
            cmd.Parameters.AddWithValue("@date2", li.date2);
            cmd.Parameters.AddWithValue("@duedays", li.duedays);
            cmd.Parameters.AddWithValue("@discp", li.discp);
            cmd.Parameters.AddWithValue("@servicetaxno", li.servicetaxno);
            cmd.Parameters.AddWithValue("@tan", li.tan);
            cmd.Parameters.AddWithValue("@pan", li.pan);
            cmd.Parameters.AddWithValue("@debitgroupcode", li.debitgroupcode);
            cmd.Parameters.AddWithValue("@creditgroupcode", li.creditgroupcode);
            cmd.Parameters.AddWithValue("@opbalance", li.opbalance);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@gstno", li.gstno);
            cmd.Parameters.AddWithValue("@gstnodate", li.gstnodate);
            cmd.Parameters.AddWithValue("@inexdebit", li.inexdebit);
            cmd.Parameters.AddWithValue("@excludetax", li.excludetax);
            cmd.Parameters.AddWithValue("@inexport", li.inexport);
            cmd.Parameters.AddWithValue("@state", li.state);
            //cmd.Parameters.AddWithValue("@acyear", li.acyear);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updategstnoinledger_e(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update Ledger_e set gstno=@gstno where acname=@acname", con);
            cmd.Parameters.AddWithValue("@gstno", li.gstno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteaccountdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ACMaster where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteaccountdatafromledger(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from Ledger where debitcode=@acname", con);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectitemdatafromaccountname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from ACMaster where acname like '%'+@acname+'%' and cno=@cno order by acname", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatetaxmaster1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update TaxMaster set vatdesc=@acname1 where vatdesc=@acname", con);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatetaxmaster2(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update TaxMaster set addtaxdesc=@acname1 where addtaxdesc=@acname", con);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatetaxmaster3(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update TaxMaster set cstdesc=@acname1 where cstdesc=@acname", con);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatetaxmaster4(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update TaxMaster set servicetaxdesc=@acname1 where servicetaxdesc=@acname", con);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebankacmaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateacgroupmastermaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ACGroupMaster set groupname=@acname1 where groupname=@acname", con);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateacgroupmaster1master(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ACGroupMaster1 set groupname=@acname1 where groupname=@acname", con);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebankacmaster2(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACMaster set name=@acname1 where name=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebankacmaster1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACMaster1 set name=@acname1 where name=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatebankacshadow(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update BankACShadow set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateledgercreditcode(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update Ledger set creditcode=@acname1 where creditcode=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateledgerdebitcode(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update Ledger set debitcode=@acname1 where debitcode=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
    public void updateledger_eacname(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update Ledger_e set acname=@acname1 where acname=@acname", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateledger_egstno(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update Ledger_e set gstno=@gstno where acname=@acname", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@gstno", li.gstno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcmaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateperinvitems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PerInvItems set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateperinvmaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PerInvMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatecomminvitems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update CommInvItems set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatecomminvmaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update CommInvMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepimaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepmmaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update pmmaster set name=@acname1 where name=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
    public void updatePurchaseOrder(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PurchaseOrder set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateQuoItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update QuoItems set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateQuoMaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update QuoMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updaterrmaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update rrmaster set name=@acname1 where name=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateSalesOrder(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SalesOrder set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateSCMaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateSIMaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SIMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateStockJVItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update StockJVItems set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateStockJVMaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update StockJVMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateToolsJVItems(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolsJVItems set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateToolsJVMaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolsJVMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateToolDelMaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ToolDelMaster set name=@acname1 where name=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateClientMaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ClientMaster set acname=@acname1 where acname=@acname and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@acname1", li.acname1);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    //check used or not
    public DataTable checktaxmaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from TaxMaster where (vatdesc=@acname or addtaxdesc=@acname or cstdesc=@acname or servicetaxdesc=@acname)", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }
    public DataTable checkbankacmaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from BankACMaster where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkbankacmaster1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from BankACMaster1 where name=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkbankacshadow(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from BankACShadow where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkledgercreditcode(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from Ledger where creditcode=@acname and cno=@cno and istype!='OP'", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkledgerdebitcode(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from Ledger where debitcode=@acname and cno=@cno and istype!='OP'", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkpcmaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from PCMaster where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkperinvitems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from PerInvItems where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkcomminvitems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from CommInvItems where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkperinvmaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from PerInvMaster where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkcomminvmaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from CommInvMaster where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkpimaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from PIMaster where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkpmmaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from pmmaster where name=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }
    public DataTable checkPurchaseOrder(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from PurchaseOrder where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkQuoItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from QuoItems where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkQuoMaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from QuoMaster where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkrrmaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from rrmaster where name=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkSalesOrder(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from SalesOrder where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkSCMaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from SCMaster where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkSIMaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from SIMaster where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkStockJVItems(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from StockJVItems where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkStockJVMaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from StockJVMaster where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkToolDelMaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from ToolDelMaster where name=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkClientMaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select * from ClientMaster where acname=@acname and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkacyearfordata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id",li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectopcldrcrdataacnamewise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select *,(select amount from Ledger where debitcode=@acname and istype='OP') as opbalance,(select isnull(sum(amount),0) as drbal from Ledger where debitcode=@acname and type='D' and istype<>'OP') as drbalance,(select isnull(sum(amount),0) from Ledger where debitcode=@acname and type='C' and istype<>'OP') as crbalance,((select amount from Ledger where debitcode=@acname and istype='OP')+(select isnull(sum(amount),0) as drbal from Ledger where debitcode=@acname and type='D' and istype<>'OP')-(select isnull(sum(amount),0) from Ledger where debitcode=@acname and type='C' and istype<>'OP')) as clbalance from acmaster where acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}