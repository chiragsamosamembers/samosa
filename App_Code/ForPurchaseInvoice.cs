﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForPurchaseInvoice
/// </summary>
public class ForPurchaseInvoice : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForPurchaseInvoice()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallpimasterdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where cno=@cno order by pino desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpimasterdatafrompino(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where pino=@pino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pino", li.pino);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpimasterdatafrompinoag(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where pino=@pino and invtype=@invtype and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pino", li.pino);
            da.SelectCommand.Parameters.AddWithValue("@invtype", li.invtype);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpimasterdatafrompinostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where strpino=@strpino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@strpino", li.strpino);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpiitemsfrompino(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIItems where pino=@pino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pino", li.pino);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpiitemsfrompinostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIItems where strpino=@strpino and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@strpino", li.strpino);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertpiitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into PIItems (pino,pidate,itemname,qty,stockqty,rate,basicamount,taxtype,vatp,vatamt,addtaxp,addtaxamt,cstp,cstamt,amount,descr1,descr2,descr3,cno,uname,udate,qtyremain,qtyused,vno,vid,taxdesc,ccode,monyr,strpino) values (@pino,@pidate,@itemname,@qty,@stockqty,@rate,@basicamount,@taxtype,@vatp,@vatamt,@addtaxp,@addtaxamt,@cstp,@cstamt,@amount,@descr1,@descr2,@descr3,@cno,@uname,@udate,@qtyremain,@qtyused,@vno,@vid,@taxdesc,@ccode,@monyr,@strpino)", con);
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@pidate", li.pidate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@stockqty", li.stockqty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@taxtype", li.taxtype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            //cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@vno", li.vnono);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@taxdesc", li.taxdesc);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepiitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIItems set pidate=@pidate,itemname=@itemname,qty=@qty,stockqty=@stockqty,rate=@rate,basicamount=@basicamount,taxtype=@taxtype,vatp=@vatp,vatamt=@vatamt,addtaxp=@addtaxp,addtaxamt=@addtaxamt,cstp=@cstp,cstamt=@cstamt,amount=@amount,descr1=@descr1,descr2=@descr2,descr3=@descr3,uname=@uname,udate=@udate,qtyremain=@qtyremain,qtyused=@qtyused,ccode=@ccode,taxdesc=@taxdesc where id=@id", con);
            //cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@pidate", li.pidate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@stockqty", li.stockqty);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@taxtype", li.taxtype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);
            //cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@taxdesc", li.taxdesc);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertpimasterdata(LogicLayer li)
    {
        try
        {
            //pidate,invtype,billno,billdate,pcno,pcdate,pcno1,pcdate1,acname,purchaseac,totbasicamount,totvat,,totaddtax,totcst,servicetaxp,servicetaxamount,cartage,roundoff,billamount,cno,uname,udate
            cmd = new SqlCommand("insert into PIMaster (pino,pidate,invtype,billno,billdate,pcno,pcdate,pcno1,pcdate1,acname,purchaseac,totbasicamount,totvat,totaddtax,totcst,servicetaxp,servicetaxamount,cartage,roundoff,billamount,type,remainamount,strpino,status,cno,uname,udate,remarks,form,transport,salesman,lrno,lrdate,duedays,monyr,stringpcno) values (@pino,@pidate,@invtype,@billno,@billdate,@pcno,@pcdate,@pcno1,@pcdate1,@acname,@purchaseac,@totbasicamount,@totvat,@totaddtax,@totcst,@servicetaxp,@servicetaxamount,@cartage,@roundoff,@billamount,@type,@remainamount,@strpino,@status,@cno,@uname,@udate,@remarks,@form,@transport,@salesman,@lrno,@lrdate,@duedays,@monyr,@stringpcno)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@pidate", li.pidate);
            cmd.Parameters.AddWithValue("@invtype", li.invtype);
            cmd.Parameters.AddWithValue("@billno", li.billno);
            cmd.Parameters.AddWithValue("@billdate", li.billdate);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@pcdate", li.pcdate);
            cmd.Parameters.AddWithValue("@pcno1", li.pcno1);
            cmd.Parameters.AddWithValue("@pcdate1", li.pcdate1);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@purchaseac", li.purchaseac);
            cmd.Parameters.AddWithValue("@totbasicamount", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddtax", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@servicetaxp", li.servicetaxp);
            cmd.Parameters.AddWithValue("@servicetaxamount", li.servicetaxamount);
            cmd.Parameters.AddWithValue("@cartage", li.cartage);
            cmd.Parameters.AddWithValue("@roundoff", li.roundoff);
            cmd.Parameters.AddWithValue("@billamount", li.billamount);
            cmd.Parameters.AddWithValue("@type", li.type);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@form", li.form);
            cmd.Parameters.AddWithValue("@transport", li.transportname);
            cmd.Parameters.AddWithValue("@salesman", li.salesmanname);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@duedays", li.duedays);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@stringpcno", li.stringpcno);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepimasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIMaster set pidate=@pidate,invtype=@invtype,billno=@billno,billdate=@billdate,pcno=@pcno,pcdate=@pcdate,pcno1=@pcno1,pcdate1=@pcdate1,acname=@acname,purchaseac=@purchaseac,totbasicamount=@totbasicamount,totvat=@totvat,totaddtax=@totaddtax,totcst=@totcst,servicetaxp=@servicetaxp,servicetaxamount=@servicetaxamount,cartage=@cartage,roundoff=@roundoff,billamount=@billamount,remainamount=@remainamount,strpino=@strpino,status=@status,uname=@uname,udate=@udate,remarks=@remarks,form=@form,transport=@transport,salesman=@salesman,lrno=@lrno,lrdate=@lrdate,duedays=@duedays,monyr=@monyr,stringpcno=@stringpcno where pino=@pino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@pidate", li.pidate);
            cmd.Parameters.AddWithValue("@invtype", li.invtype);
            cmd.Parameters.AddWithValue("@billno", li.billno);
            cmd.Parameters.AddWithValue("@billdate", li.billdate);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@pcdate", li.pcdate);
            cmd.Parameters.AddWithValue("@pcno1", li.pcno1);
            cmd.Parameters.AddWithValue("@pcdate1", li.pcdate1);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@purchaseac", li.purchaseac);
            cmd.Parameters.AddWithValue("@totbasicamount", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddtax", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@servicetaxp", li.servicetaxp);
            cmd.Parameters.AddWithValue("@servicetaxamount", li.servicetaxamount);
            cmd.Parameters.AddWithValue("@cartage", li.cartage);
            cmd.Parameters.AddWithValue("@roundoff", li.roundoff);
            cmd.Parameters.AddWithValue("@billamount", li.billamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@form", li.form);
            cmd.Parameters.AddWithValue("@transport", li.transportname);
            cmd.Parameters.AddWithValue("@salesman", li.salesmanname);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@duedays", li.duedays);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@stringpcno", li.stringpcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepimasterdatastring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIMaster set pino=@pino,pidate=@pidate,invtype=@invtype,billno=@billno,billdate=@billdate,pcno=@pcno,pcdate=@pcdate,pcno1=@pcno1,pcdate1=@pcdate1,acname=@acname,purchaseac=@purchaseac,totbasicamount=@totbasicamount,totvat=@totvat,totaddtax=@totaddtax,totcst=@totcst,servicetaxp=@servicetaxp,servicetaxamount=@servicetaxamount,cartage=@cartage,roundoff=@roundoff,billamount=@billamount,remainamount=@remainamount,strpino=@strpino,status=@status,uname=@uname,udate=@udate,remarks=@remarks,form=@form,transport=@transport,salesman=@salesman,lrno=@lrno,lrdate=@lrdate,duedays=@duedays,monyr=@monyr,stringpcno=@stringpcno where strpino=@strpino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@pidate", li.pidate);
            cmd.Parameters.AddWithValue("@invtype", li.invtype);
            cmd.Parameters.AddWithValue("@billno", li.billno);
            cmd.Parameters.AddWithValue("@billdate", li.billdate);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@pcdate", li.pcdate);
            cmd.Parameters.AddWithValue("@pcno1", li.pcno1);
            cmd.Parameters.AddWithValue("@pcdate1", li.pcdate1);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@purchaseac", li.purchaseac);
            cmd.Parameters.AddWithValue("@totbasicamount", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddtax", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@servicetaxp", li.servicetaxp);
            cmd.Parameters.AddWithValue("@servicetaxamount", li.servicetaxamount);
            cmd.Parameters.AddWithValue("@cartage", li.cartage);
            cmd.Parameters.AddWithValue("@roundoff", li.roundoff);
            cmd.Parameters.AddWithValue("@billamount", li.billamount);
            cmd.Parameters.AddWithValue("@remainamount", li.remainamount);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@form", li.form);
            cmd.Parameters.AddWithValue("@transport", li.transportname);
            cmd.Parameters.AddWithValue("@salesman", li.salesmanname);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@duedays", li.duedays);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@stringpcno", li.stringpcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepimasterdatafrompino(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PIMaster where pino=@pino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepimasterdatafrompinostring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PIMaster where strpino=@strpino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepiitemsdatafrompino(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PIItems where pino=@pino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepiitemsdatafrompinostring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PIItems where strpino=@strpino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepiitemsdatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PIItems where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallitemdatafrompcnotexhchange(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PCItems where pcno in (" + li.remarks + ") and cno=@cno and qtyremain>0", con);
            da = new SqlDataAdapter("select * from PCItems inner join ItemMaster on PCItems.itemname=ItemMaster.itemname where PCItems.pcno in (" + li.remarks + ") and PCItems.cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            //da.SelectCommand.Parameters.AddWithValue("@pcno", li.pcno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemdatafrompcnotexhchangeone(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCItems where pcno=@pcno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pcno", li.pcno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemdatafrompcnotexhchangetwo(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCItems where pcno=@pcno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pcno", li.pcno1);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallmasterdatafrompcnotexhchange(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCMaster where pcno=@pcno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pcno", li.pcno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertledgerdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ledger (voucherno,strvoucherno,voucherdate,refid,debitcode,creditcode,description,projectcode,amount,type,istype,cno,uname,udate) values (@voucherno,@strvoucherno,@voucherdate,@refid,@debitcode,@creditcode,@description,@projectcode,@amount,@type,@istype,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@refid", li.refid);
            cmd.Parameters.AddWithValue("@debitcode", li.debitcode);
            cmd.Parameters.AddWithValue("@creditcode", li.creditcode);
            cmd.Parameters.AddWithValue("@description", li.description);
            cmd.Parameters.AddWithValue("@projectcode", li.ccode);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@type", li.istype1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateledgerdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ledger set voucherdate=@voucherdate,debitcode=@debitcode,creditcode=@creditcode,projectcode=@projectcode,amount=@amount,uname=@uname,udate=@udate where voucherno=@voucherno and istype='PI' and description=@description", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@debitcode", li.debitcode);
            cmd.Parameters.AddWithValue("@creditcode", li.creditcode);
            cmd.Parameters.AddWithValue("@projectcode", li.ccode);
            cmd.Parameters.AddWithValue("@description", li.description);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectunusedpurchaseinvoiceno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and ispiused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedpurchaseinvoicenolast(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select pino from PIMaster where cno=@cno order by pino desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisused(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set ispiused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.pino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallpurchasechallanforpopup(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCMaster where status!='Closed' and cno=@cno and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateremainqtyinpurchasechallan(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update PCItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and pcno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PCItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@vno", li.vnono1);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallstatus(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Status' order by name", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallinvtype(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Inv Type' order by name", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectpcdatefrompcno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select convert(varchar(50),pcdate,105) as cdate from PCMaster where pcno=@pcno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pcno", li.pcno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectpcdatefrompcno1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select convert(varchar(50),pcdate,105) as cdate from PCMaster where pcno=@pcno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pcno", li.pcno1);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectqtyremainusedfromscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PCItems where cno=@cno and vno=@scno and itemname=@itemname", con);
            da = new SqlDataAdapter("select * from PCItems where id=@vid", con);
            da.SelectCommand.Parameters.AddWithValue("@vid", li.vid);
            //da.SelectCommand.Parameters.AddWithValue("@scno", li.vnono);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectqtyremainusedfromscno1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PCItems where cno=@cno and pcno=@vno and itemname=@itemname", con);
            da = new SqlDataAdapter("select * from PCItems where id=@vid", con);
            da.SelectCommand.Parameters.AddWithValue("@vid", li.vid);
            //da.SelectCommand.Parameters.AddWithValue("@vno", li.pcno);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectqtyremainusedfromscno11(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PCItems where cno=@cno and pcno=@vno and itemname=@itemname", con);
            da = new SqlDataAdapter("select * from PCItems where id=@vid", con);
            da.SelectCommand.Parameters.AddWithValue("@vid", li.vid);
            //da.SelectCommand.Parameters.AddWithValue("@vno", li.pcno1);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateqtyduringdelete(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update PCItems set qtyremain=@qtyremain,qtyused=@qtyused where itemname=@itemname and pcno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PCItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@vid", con);
            //cmd.Parameters.AddWithValue("@itemname", li.itemname);
            //cmd.Parameters.AddWithValue("@vno", li.vnono);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedn(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set ispiused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.pino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepimasterdataupdatepi(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIMaster set pino=@pino1 where pino=@pino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino1", li.pino1);
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepimasterdataupdatepistring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIMaster set pino=@pino1,invtype=@invtype1,strpino=@strpino1 where strpino=@strpino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino1", li.pino1);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@strpino1", li.strpino1);
            cmd.Parameters.AddWithValue("@invtype1", li.invtype1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepiitemsdataupdatepi(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIItems set pino=@pino1 where pino=@pino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino1", li.pino1);
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepiitemsdataupdatepistring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIItems set pino=@pino1,strpino=@strpino1 where strpino=@strpino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino1", li.pino1);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@strpino1", li.strpino1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepiitemsdatedataupdatepistring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIItems set pidate=@pidate where strpino=@strpino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pidate", li.pidate);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateledgerdataupdatestring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ledger set voucherno=@pino1,strvoucherno=@strpino1 where strvoucherno=@strpino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pino1", li.pino1);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@strpino1", li.strpino1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectalltaxacdesc()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from TaxMaster where vatdesc!='' and addtaxdesc!='' and cstdesc!='' and servicetaxdesc!='' order by id desc", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void deletepurchaseinvoiceledger(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where voucherno=@pino and cno=@cno and istype='PI'", con);
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepurchaseinvoiceledgeraa(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where strvoucherno=@strvoucherno and cno=@cno and istype='PI'", con);
            cmd.Parameters.AddWithValue("@strvoucherno", li.strpino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectalltaxacdescfrommisc()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Tax Type'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatemonyrinpiitems(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SCItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PIItems set monyr=@monyr where pino=@pino and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@vno", li.vnono1);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@pino", li.pino);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatemonyrinpiitemsstring(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SCItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PIItems set monyr=@monyr,pidate=@pidate where strpino=@pino and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@vno", li.vnono1);
            cmd.Parameters.AddWithValue("@monyr", li.monyr);
            cmd.Parameters.AddWithValue("@pino", li.strpino);
            cmd.Parameters.AddWithValue("@pidate", li.pidate);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallprchaseacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where actype='Purchase' order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where isblock='N' order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatepcstatusforallchno(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCMaster set status='Closed' where pcno=@pcno and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcstatusforallchnoa1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCMaster set status='Open' where pcno=@pcno and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcstatusforallchnoopen(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCMaster set status='Open' where pcno in (" + li.stringpcno + ") and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrpinoinpcitems(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PCItems set strpino=@strpino where id=@id", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            //cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrpinoinpcitems1(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PCItems set strpino=@strpino where id=@vid", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            //cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrpinoinpcmaster(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PCMaster set strpino=@strpino where pcno=@pcno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrpinoinpcmastera(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PCItems set strpino=@strpino where pcno=@pcno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectpifrompartybillno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where billno=@billno and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@billno", li.billno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatepimasterdataafterdelete(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PIMaster set totbasicamount=@totbasicamount,totvat=@totvat,totaddtax=@totaddtax,totcst=@totcst,servicetaxp=@servicetaxp,servicetaxamount=@servicetaxamount,cartage=@cartage,roundoff=@roundoff,billamount=@billamount,remainamount=@remainamount,uname=@uname,udate=@udate where strpino=@strpino and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strpino", li.strpino);
            cmd.Parameters.AddWithValue("@totbasicamount", li.totbasicamount);
            cmd.Parameters.AddWithValue("@totvat", li.totvatamt);
            cmd.Parameters.AddWithValue("@totaddtax", li.totaddtaxamt);
            cmd.Parameters.AddWithValue("@totcst", li.totcstamt);
            cmd.Parameters.AddWithValue("@servicetaxp", li.servicetaxp);
            cmd.Parameters.AddWithValue("@servicetaxamount", li.servicetaxamount);
            cmd.Parameters.AddWithValue("@cartage", li.cartage);
            cmd.Parameters.AddWithValue("@roundoff", li.roundoff);
            cmd.Parameters.AddWithValue("@billamount", li.billamount);
            cmd.Parameters.AddWithValue("@remainamount", li.billamount);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}