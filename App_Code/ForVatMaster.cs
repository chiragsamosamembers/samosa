﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForVatMaster
/// </summary>
public class ForVatMaster:ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

	public ForVatMaster()
	{
        con = new SqlConnection(GetConnectionString());
	}

    public DataTable selectallVATdata()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from TaxMaster order by id desc", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallVATdatacnowise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from TaxMaster order by id desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkduplicate(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from TaxMaster where typename=@typename", con);
            da.SelectCommand.Parameters.AddWithValue("@typename", li.typename);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkacnameexistornotvatdesc(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.vatdesc);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkacnameexistornotaddtaxdesc(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.addtaxdesc);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkacnameexistornotcstdesc(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.cstdesc);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkacnameexistornotservicetaxdesc(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.servicetaxdesc);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectVATdatafromid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from TaxMaster where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertVATdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into TaxMaster (typename,vatp,addtaxp,centralsalep,selltaxp,surchargep,servicetaxp,form,uname,udate,vatdesc,addtaxdesc,cstdesc,servicetaxdesc) values (@typename,@vatp,@addtaxp,@centralsalep,@selltaxp,@surchargep,@servicetaxp,@form,@uname,@udate,@vatdesc,@addtaxdesc,@cstdesc,@servicetaxdesc)", con);
            cmd.Parameters.AddWithValue("@typename", li.typename);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@centralsalep", li.centralsalep);
            cmd.Parameters.AddWithValue("@selltaxp", li.selltaxp);
            cmd.Parameters.AddWithValue("@surchargep", li.surchargep);
            cmd.Parameters.AddWithValue("@servicetaxp", li.servicetaxp);
            cmd.Parameters.AddWithValue("@form", li.form);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@vatdesc", li.vatdesc);
            cmd.Parameters.AddWithValue("@addtaxdesc", li.addtaxdesc);
            cmd.Parameters.AddWithValue("@cstdesc", li.cstdesc);
            cmd.Parameters.AddWithValue("@servicetaxdesc", li.servicetaxdesc);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateVATdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update TaxMaster set typename=@typename,vatp=@vatp,addtaxp=@addtaxp,centralsalep=@centralsalep,selltaxp=@selltaxp,surchargep=@surchargep,servicetaxp=@servicetaxp,form=@form,uname=@uname,udate=@udate,vatdesc=@vatdesc,addtaxdesc=@addtaxdesc,cstdesc=@cstdesc,servicetaxdesc=@servicetaxdesc where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@typename", li.typename);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@centralsalep", li.centralsalep);
            cmd.Parameters.AddWithValue("@selltaxp", li.selltaxp);
            cmd.Parameters.AddWithValue("@surchargep", li.surchargep);
            cmd.Parameters.AddWithValue("@servicetaxp", li.servicetaxp);
            cmd.Parameters.AddWithValue("@form", li.form);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@vatdesc", li.vatdesc);
            cmd.Parameters.AddWithValue("@addtaxdesc", li.addtaxdesc);
            cmd.Parameters.AddWithValue("@cstdesc", li.cstdesc);
            cmd.Parameters.AddWithValue("@servicetaxdesc", li.servicetaxdesc);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteVATdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from TaxMaster where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}