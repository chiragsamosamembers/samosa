﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForPurchaseChallan
/// </summary>
public class ForPurchaseChallan : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

	public ForPurchaseChallan()
	{
        con = new SqlConnection(GetConnectionString());
	}

    public DataTable selectallpurchasechallandata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCMaster where cno=@cno order by pcno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpurchaseorderitemdata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems where cno=@cno and pono=@pono", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.strpono);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpurchasechallandatafrompcno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCMaster where pcno=@pcno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pcno", li.pcno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpcitemsfrompcno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCItems where pcno=@pcno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pcno", li.pcno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertpcitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into PCItems (pcno,pcdate,itemname,qty,stockqty,rate,basicamount,ccode,descr1,descr2,descr3,cno,uname,udate,amount,qtyremain,qtyused,vno,vid,unit) values (@pcno,@pcdate,@itemname,@qty,@stockqty,@rate,@amount,@ccode,@descr1,@descr2,@descr3,@cno,@uname,@udate,@amount,@qtyremain,@qtyused,@vno,@vid,@unit)", con);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@pcdate", li.pcdate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@stockqty", li.stockqty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@amount", li.basicamount);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@vno", li.vnono);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain1);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused1);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCItems set pcdate=@pcdate,itemname=@itemname,qty=@qty,stockqty=@stockqty,rate=@rate,basicamount=@basicamount,ccode=@ccode,descr1=@descr1,descr2=@descr2,descr3=@descr3,uname=@uname,udate=@udate,amount=@amount,qtyremain=@qtyremain,qtyused=@qtyused,unit=@unit where id=@id", con);
            //cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@pcdate", li.pcdate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@stockqty", li.stockqty);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@amount", li.basicamount);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertpcmasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into PCMaster (pcno,pcdate,chno,pono,podate,pono1,podate1,acname,status,cno,uname,udate,stringpono) values (@pcno,@pcdate,@chno,@pono,@podate,@pono1,@podate1,@acname,@status,@cno,@uname,@udate,@stringpono)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@pcdate", li.pcdate);
            cmd.Parameters.AddWithValue("@chno", li.challanno);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@podate", li.podate);
            cmd.Parameters.AddWithValue("@pono1", li.strpono1);
            cmd.Parameters.AddWithValue("@podate1", li.podate1);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@stringpono", li.stringpono);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcmasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCMaster set pcdate=@pcdate,chno=@chno,pono=@pono,podate=@podate,pono1=@pono1,podate1=@podate1,acname=@acname,status=@status,uname=@uname,udate=@udate,stringpono=@stringpono where pcno=@pcno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@pcdate", li.pcdate);
            cmd.Parameters.AddWithValue("@chno", li.challanno);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@podate", li.podate);
            cmd.Parameters.AddWithValue("@pono1", li.strpono1);
            cmd.Parameters.AddWithValue("@podate1", li.podate1);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@stringpono", li.stringpono);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepcmasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PCMaster where pcno=@pcno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepcitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PCItems where pcno=@pcno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletepcitemsdatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from PCItems where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallpcitemsfromponotextchange(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono=@pono and PurchaseOrder.cno=@cno", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono in (" + li.remarks + ") and PurchaseOrder.cno=@cno and PurchaseOrderItems.qtyremain>0", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono in (" + li.remarks + ") and PurchaseOrder.cno=@cno and (PurchaseOrderItems.qtyremain>0 or PurchaseOrderItems.qty=0)", con);
            //da.SelectCommand.Parameters.AddWithValue("@pono", li.pono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpcitemsfromponotextchange1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono=@pono and PurchaseOrder.cno=@cno", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono in (" + li.remarks + ") and PurchaseOrder.cno=@cno and PurchaseOrderItems.qtyremain>0", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono in (" + li.remarks + ") and PurchaseOrder.cno=@cno and round((PurchaseOrderItems.qtyused+PurchaseOrderItems.adjqty),2)<round(PurchaseOrderItems.qty,2)", con);
            //da.SelectCommand.Parameters.AddWithValue("@pono", li.pono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpurchaseorderitemdatafromponousingin(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where id in (" + li.remarks + ") and cno=@cno and qtyremain>0", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems where id in (" + li.remarks + ") and cno=@cno", con);
            //da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpcitemsfromponotextchangeone(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono=@pono and PurchaseOrder.cno=@cno", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono=@pono and PurchaseOrder.cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.pono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpcitemsfromponotextchangetwo(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono=@pono and PurchaseOrder.cno=@cno", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono=@pono and PurchaseOrder.cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.pono1);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectpodatefrompono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono=@pono and PurchaseOrder.cno=@cno", con);
            da = new SqlDataAdapter("select convert(varchar(50),podate,105) as cdate from PurchaseOrder where pono=@pono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.pono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectpodatefromponostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono=@pono and PurchaseOrder.cno=@cno", con);
            da = new SqlDataAdapter("select convert(varchar(50),podate,105) as cdate,ccode from PurchaseOrder where pono=@pono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.strpono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectpodatefrompono1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono=@pono and PurchaseOrder.cno=@cno", con);
            da = new SqlDataAdapter("select convert(varchar(50),podate,105) as cdate from PurchaseOrder where pono=@pono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.pono1);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectpodatefrompono1string(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono where PurchaseOrder.pono=@pono and PurchaseOrder.cno=@cno", con);
            da = new SqlDataAdapter("select convert(varchar(50),podate,105) as cdate from PurchaseOrder where pono=@pono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pono", li.strpono1);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedpurchasechallanno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and ispcused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedpurchasechallannolast(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select pcno from PCMaster where cno=@cno order by pcno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisused(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set ispcused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.pcno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateremainqtyinpurchaseorder(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update PurchaseOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and pono=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PurchaseOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@vno", li.vnono1);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallstatus(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Status' order by name", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpurchaseorderforpopup(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrder where status!='Closed' and cno=@cno and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallpurchaseorderforpopup1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrder where pono in (select pono from purchaseorderitems where cno=@cno and acname=@acname and round((qtyused+adjqty),2)<round(qty,2))", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectqtyremainusedfromscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems where cno=@cno and pono=@scno and itemname=@itemname", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems where id=@vid", con);
            da.SelectCommand.Parameters.AddWithValue("@vid", li.vid);
            //da.SelectCommand.Parameters.AddWithValue("@scno", li.vnono);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectqtyremainusedfromscno1(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems where cno=@cno and pono=@vno and itemname=@itemname", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems where id=@vid", con);
            da.SelectCommand.Parameters.AddWithValue("@vid", li.vid);
            //da.SelectCommand.Parameters.AddWithValue("@vno", li.pono);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectqtyremainusedfromscno1cc(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems where cno=@cno and id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@id", li.vid);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectqtyremainusedfromscno11(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from PurchaseOrderItems where cno=@cno and pono=@vno and itemname=@itemname", con);
            da = new SqlDataAdapter("select * from PurchaseOrderItems where id=@vid", con);
            da.SelectCommand.Parameters.AddWithValue("@vid", li.vid);
            //da.SelectCommand.Parameters.AddWithValue("@vno", li.pono1);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateqtyduringdelete(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update PurchaseOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where itemname=@itemname and pono=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PurchaseOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@vid", con);
            //cmd.Parameters.AddWithValue("@itemname", li.itemname);
            //cmd.Parameters.AddWithValue("@vno", li.vnono);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateisusedn(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set ispcused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.pcno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateiscame(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PurchaseOrderItems set iscame=@iscame where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@iscame", li.iscame);
            cmd.Parameters.AddWithValue("@id", li.vid);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcmasterdataupdatepc(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCMaster set pcno=@pcno1 where pcno=@pcno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pcno1", li.pcno1);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcitemsdataupdatepc(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCItems set pcno=@pcno1 where pcno=@pcno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pcno1", li.pcno1);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcitemsdataupdatepcitemsdate(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update PCItems set pcdate=@pcdate where pcno=@pcno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@pcdate", li.pcdate);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where isblock='N' order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectbillinvoicedatafrompcno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIMaster where ((stringpcno like '%'+@pcnos+'%') or pcno=@pcno or pcno1=@pcno) and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@pcno", li.pcno);
            da.SelectCommand.Parameters.AddWithValue("@pcnos", li.stringpcno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectbillinvoicedatafrompcid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PIItems where vid=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updatepcnoinpoitems(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PurchaseOrderItems set pcno=@pcno where id=@id", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            //cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcnoinpoitems1(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PurchaseOrderItems set pcno=@pcno where id=@vid", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            //cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcnoinpomaster(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PurchaseOrder set pcno=@pcno where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatepcnoinpomastera(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update PurchaseOrderItems set pcno=@pcno where pono=@pono and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@pcno", li.pcno);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectpcfrompartychallanno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from PCMaster where chno=@chno and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@chno", li.challanno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}