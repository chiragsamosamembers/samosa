﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForClientMaster
/// </summary>
public class ForClientMaster : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

	public ForClientMaster()
	{
        con = new SqlConnection(GetConnectionString());
	}

    public DataTable selectallclientdata()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ClientMaster order by code desc", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkduplicate(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ClientMaster where cno=@cno and name=@name", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@name", li.name);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkduplicate11(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ClientMaster where cno=@cno and code=@code", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@code", li.ccode);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallclientdatacnowise(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ClientMaster where cno=@cno order by code desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectclientdatafromid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ClientMaster where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void insertclientdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ClientMaster (code,name,add1,add2,add3,city,pincode,mobileno,emailid,cno,uname,udate,opbalance,acname,referencename,handledby,status,remarks,pdate,acyear) values (@code,@name,@add1,@add2,@add3,@city,@pincode,@mobileno,@emailid,@cno,@uname,@udate,@opbalance,@acname,@referencename,@handledby,@status,@remarks,@pdate,@acyear)", con);
            cmd.Parameters.AddWithValue("@code", li.ccode);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@add1", li.add1);
            cmd.Parameters.AddWithValue("@add2", li.add2);
            cmd.Parameters.AddWithValue("@add3", li.add3);
            cmd.Parameters.AddWithValue("@city", li.city);
            cmd.Parameters.AddWithValue("@pincode", li.pincode);
            cmd.Parameters.AddWithValue("@mobileno", li.mobileno);
            cmd.Parameters.AddWithValue("@emailid", li.emailid);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@opbalance", li.opbalance);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@referencename", li.referencename);
            cmd.Parameters.AddWithValue("@handledby", li.handledby);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@pdate", li.pdate);
            cmd.Parameters.AddWithValue("@acyear", li.acyear);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateclientdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update ClientMaster set code=@code,name=@name,add1=@add1,add2=@add2,add3=@add3,city=@city,pincode=@pincode,mobileno=@mobileno,emailid=@emailid,cno=@cno,uname=@uname,udate=@udate,opbalance=@opbalance,acname=@acname,referencename=@referencename,handledby=@handledby,status=@status,remarks=@remarks,pdate=@pdate where id=@id", con);//,acyear=@acyear
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@code", li.ccode);
            cmd.Parameters.AddWithValue("@name", li.name);
            cmd.Parameters.AddWithValue("@add1", li.add1);
            cmd.Parameters.AddWithValue("@add2", li.add2);
            cmd.Parameters.AddWithValue("@add3", li.add3);
            cmd.Parameters.AddWithValue("@city", li.city);
            cmd.Parameters.AddWithValue("@pincode", li.pincode);
            cmd.Parameters.AddWithValue("@mobileno", li.mobileno);
            cmd.Parameters.AddWithValue("@emailid", li.emailid);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@opbalance", li.opbalance);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@referencename", li.referencename);
            cmd.Parameters.AddWithValue("@handledby", li.handledby);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@pdate", li.pdate);
            //cmd.Parameters.AddWithValue("@acyear", li.acyear);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteclientdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ClientMaster where code=@code", con);
            cmd.Parameters.AddWithValue("@code", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectsearchclientname11(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ClientMaster where cno=@cno and name like '%'+@fullname+'%'", con);
            da.SelectCommand.Parameters.AddWithValue("@fullname", li.name);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectsearchclientname11status(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ClientMaster where cno=@cno and status=@status", con);
            da.SelectCommand.Parameters.AddWithValue("@status", li.status);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallclientstatusdata()
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Client Status'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable checkacyearfordata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ClientMaster where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}