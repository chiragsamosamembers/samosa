﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForLedger
/// </summary>
public class ForLedger : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForLedger()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public void insertledgerdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ledger (voucherno,strvoucherno,voucherdate,refid,debitcode,creditcode,description,projectcode,amount,type,istype,cno,uname,udate) values (@voucherno,@strvoucherno,@voucherdate,@refid,@debitcode,@creditcode,@description,@projectcode,@amount,@type,@istype,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@refid", li.refid);
            cmd.Parameters.AddWithValue("@debitcode", li.debitcode);
            cmd.Parameters.AddWithValue("@creditcode", li.creditcode);
            cmd.Parameters.AddWithValue("@description", li.description);
            cmd.Parameters.AddWithValue("@projectcode", li.ccode);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@type", li.istype1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertledgerdataforjvjv(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into Ledger_e (strvoucherno,voucherdate,billno,acname,gstno,hsncode,billvalue,basicamt,cgst,sgst,igst,uname,udate) values (@strvoucherno,@voucherdate,@billno,@acname,@gstno,@hsncode,@billvalue,@basicamt,@cgst,@sgst,@igst,@uname,@udate)", con);           
            cmd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate);
            cmd.Parameters.AddWithValue("@billno", li.strbillno);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@gstno", li.gstno);
            cmd.Parameters.AddWithValue("@hsncode", li.hsncode);
            cmd.Parameters.AddWithValue("@billvalue", li.billamount);
            cmd.Parameters.AddWithValue("@basicamt", li.basicamount);
            cmd.Parameters.AddWithValue("@cgst", li.cgst);
            cmd.Parameters.AddWithValue("@sgst", li.sgst);
            cmd.Parameters.AddWithValue("@igst", li.igst);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertledgerdataforac(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into ledger (voucherno,strvoucherno,voucherdate,refid,debitcode,creditcode,description,projectcode,amount,type,istype,cno,uname,udate) values (@voucherno,@strvoucherno,@voucherdate,@refid,@debitcode,@creditcode,@description,@projectcode,@amount,@type,@istype,@cno,@uname,@udate)", con);
            cmd.Parameters.AddWithValue("@voucherno", li.voucherno);
            cmd.Parameters.AddWithValue("@strvoucherno", li.strvoucherno);
            cmd.Parameters.AddWithValue("@voucherdate", li.voucherdate1);
            cmd.Parameters.AddWithValue("@refid", li.refid);
            cmd.Parameters.AddWithValue("@debitcode", li.debitcode);
            cmd.Parameters.AddWithValue("@creditcode", li.creditcode);
            cmd.Parameters.AddWithValue("@description", li.description);
            cmd.Parameters.AddWithValue("@projectcode", li.ccode);
            cmd.Parameters.AddWithValue("@amount", li.amount);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@type", li.istype1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteledgerdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where id=@id and istype=@istype and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@istype", li.istype);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deleteledgerdataforacmaster(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from ledger where debitcode=@debitcode and istype='OP' and cno=@cno", con);
            cmd.Parameters.AddWithValue("@debitcode", li.debitcode);
            //cmd.Parameters.AddWithValue("@creditcode", li.creditcode);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

}