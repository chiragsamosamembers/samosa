﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Survey.Classes
{
    public class SessionMgt
    {
        
        public enum SessionVariable
        {
            IsAuthenticate,
            IsAdminUser,
            FirstName,
            LastName,
            UserName,
            acname,
            Userid,
            brid,
            fromdate,
            todate,
            TypeofUser,
            ccode,
            InsComName,
            InsId,
            UserID,
            voucherno,
            cno,
            ShopID,
            IsLogIn,
            Ctype,
            page,
            quoteno,
            pcode,
            emailid,
            contactno,
            quono,
            inqno,
            pname,
            invoiceno,
            issipi,
            sino,
            strsino,
            strpino,
            UserRole,
            paidamount,
            ItemNameList,
            id
       }
        public static Int64 quono
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.quono.ToString()] == null)
                {
                    return 0;
                }
                return (Int64)HttpContext.Current.Session[SessionVariable.quono.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.quono.ToString()] = value;
            }
        }

        public static Int64 sino
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.sino.ToString()] == null)
                {
                    return 0;
                }
                return (Int64)HttpContext.Current.Session[SessionVariable.sino.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.sino.ToString()] = value;
            }
        }

        public static string strsino
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.strsino.ToString()] == null)
                {
                    return string.Empty;
                }
                return (string)HttpContext.Current.Session[SessionVariable.strsino.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.strsino.ToString()] = value;
            }
        }

        public static string strpino
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.strpino.ToString()] == null)
                {
                    return string.Empty;
                }
                return (string)HttpContext.Current.Session[SessionVariable.strpino.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.strpino.ToString()] = value;
            }
        }

        public static double paidamount
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.paidamount.ToString()] == null)
                {
                    return 0;
                }
                return (double)HttpContext.Current.Session[SessionVariable.paidamount.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.paidamount.ToString()] = value;
            }
        }

        public static double quoteno
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.quoteno.ToString()] == null)
                {
                    return 0;
                }
                return (double)HttpContext.Current.Session[SessionVariable.quoteno.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.quoteno.ToString()] = value;
            }
        }

        public static Int64 id
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.id.ToString()] == null)
                {
                    return 0;
                }
                return (Int64)HttpContext.Current.Session[SessionVariable.id.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.id.ToString()] = value;
            }
        }

        public static string brid
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.brid.ToString()] == null)
                {
                    return string.Empty;
                }
                return (string)HttpContext.Current.Session[SessionVariable.brid.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.brid.ToString()] = value;
            }
        }

        public static Int64 inqno
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.inqno.ToString()] == null)
                {
                    return 0;
                }
                return (Int64)HttpContext.Current.Session[SessionVariable.inqno.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.inqno.ToString()] = value;
            }
        }

        public static int InsId
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.InsId.ToString()] == null)
                {
                    return 0;
                }
                return (int)HttpContext.Current.Session[SessionVariable.InsId.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.InsId.ToString()] = value;
            }
        }

        public static string InsComName
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.InsComName.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.InsComName.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.InsComName.ToString()] = value;
            }
        }


        public static string UserName
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.UserName.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.UserName.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.UserName.ToString()] = value;
            }
        }

        public static string UserRole
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.UserRole.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.UserRole.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.UserRole.ToString()] = value;
            }
        }

        public static string acname
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.acname.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.acname.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.acname.ToString()] = value;
            }
        }

        public static string fromdate
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.fromdate.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.fromdate.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.fromdate.ToString()] = value;
            }
        }

        public static string todate
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.todate.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.todate.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.todate.ToString()] = value;
            }
        }

        public static string pcode
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.pcode.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.pcode.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.pcode.ToString()] = value;
            }
        }

        public static string pname
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.pname.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.pname.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.pname.ToString()] = value;
            }
        }

        public static string Ctype
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.Ctype.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.Ctype.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.Ctype.ToString()] = value;
            }
        }

        public static string issipi
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.issipi.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.issipi.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.issipi.ToString()] = value;
            }
        }


        public static Int64 Userid
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.Userid.ToString()] == null)
                {
                    return 0;
                }
                return (Int64)HttpContext.Current.Session[SessionVariable.Userid.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.Userid.ToString()] = value;
            }
        }

        public static Int64 ccode
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.ccode.ToString()] == null)
                {
                    return 0;
                }
                return (Int64)HttpContext.Current.Session[SessionVariable.ccode.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.ccode.ToString()] = value;
            }
        }

        public static Int64 voucherno
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.voucherno.ToString()] == null)
                {
                    return 0;
                }
                return (Int64)HttpContext.Current.Session[SessionVariable.voucherno.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.voucherno.ToString()] = value;
            }
        }

        public static Int64 invoiceno
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.invoiceno.ToString()] == null)
                {
                    return 0;
                }
                return (Int64)HttpContext.Current.Session[SessionVariable.invoiceno.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.invoiceno.ToString()] = value;
            }
        }

        public static Int64 cno
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.voucherno.ToString()] == null)
                {
                    return 0;
                }
                return (Int64)HttpContext.Current.Session[SessionVariable.voucherno.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.voucherno.ToString()] = value;
            }
        }

        public static string FirstName
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.FirstName.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.FirstName.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.FirstName.ToString()] = value;
            }
        }

        public static string ItemNameList
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.ItemNameList.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.ItemNameList.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.ItemNameList.ToString()] = value;
            }
        }

        public static string LastName
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.LastName.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.LastName.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.LastName.ToString()] = value;
            }
        }
        public static string emailid
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.emailid.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.emailid.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.emailid.ToString()] = value;
            }
        }
        public static string contactno
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.contactno.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.contactno.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.contactno.ToString()] = value;
            }
        }
        public static string page
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.page.ToString()] == null)
                {
                    return string.Empty;
                }
                return HttpContext.Current.Session[SessionVariable.page.ToString()].ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.page.ToString()] = value;
            }
        }

        public static bool IsAuthenticate
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.IsAuthenticate.ToString()] == null)
                {
                    return false;
                }
                return (bool)(HttpContext.Current.Session[SessionVariable.IsAuthenticate.ToString()]);
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.IsAuthenticate.ToString()] = value;
            }
        }

        public static bool IsAdminUser
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.IsAdminUser.ToString()] == null)
                {
                    return false;
                }
                return (bool)(HttpContext.Current.Session[SessionVariable.IsAdminUser.ToString()]);
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.IsAdminUser.ToString()] = value;
            }
        }

        public static int TypeofUser
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.TypeofUser.ToString()] == null)
                {
                    return 0;
                }
                return (int)(HttpContext.Current.Session[SessionVariable.TypeofUser.ToString()]);
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.TypeofUser.ToString()] = value;
            }
        }

        public static int UserID
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.UserID.ToString()] == null)
                {
                    return 0;
                }
                return (int)(HttpContext.Current.Session[SessionVariable.UserID.ToString()]);
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.UserID.ToString()] = value;
            }
        }

        public static int ShopID
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.ShopID.ToString()] == null)
                {
                    return 0;
                }
                return (int)(HttpContext.Current.Session[SessionVariable.ShopID.ToString()]);
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.ShopID.ToString()] = value;
            }
        }

        public static Boolean IsLogIn
        {
            get
            {
                if (HttpContext.Current.Session[SessionVariable.IsLogIn.ToString()] == null)
                {
                    return false;
                }
                return (Boolean)HttpContext.Current.Session[SessionVariable.IsLogIn.ToString()];
            }
            set
            {
                HttpContext.Current.Session[SessionVariable.IsLogIn.ToString()] = value;
            }
        }
    }
}