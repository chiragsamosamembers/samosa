﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForSalesChallan
/// </summary>
public class ForSalesChallan : ConnectionClass
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForSalesChallan()
    {
        con = new SqlConnection(GetConnectionString());
    }

    public DataTable selectallsaleschallandata(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();//select * from SCMaster inner join SIMaster on SCMaster.strscno=SIMaster.scno
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SCMaster where cno=@cno order by scno desc", con);
            da = new SqlDataAdapter("select * from SCMaster where cno=@cno order by scno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallchallantype(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Challan Type' order by name", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsaleschallandatafromscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where scno=@scno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@scno", li.scno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectbillinvoicedatafromstrscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIMaster where ((stringscno like '%'+@strscno+'%') or scno=@strscno or scno1=@strscno) and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@strscno", li.strscno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectbillinvoicedatafromscid(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SIItems where vid=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsaleschallandatafromscnostrscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where strscno=@strscno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@strscno", li.strscno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsaleschallandatafromscnoag(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCMaster where scno=@scno and ctype=@ctype and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@scno", li.scno);
            da.SelectCommand.Parameters.AddWithValue("@ctype", li.ctype);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallscitemsfromscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCItems where scno=@scno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@scno", li.scno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallscitemsfromscnostring(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCItems where strscno=@strscno and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@strscno", li.strscno);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    //public DataTable selectallscitemsfromscnostring(LogicLayer li)
    //{
    //    DataTable dt = new DataTable();
    //    try
    //    {
    //        con.Open();
    //        //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
    //        da = new SqlDataAdapter("select * from SCItems where strscno=@strscno and cno=@cno", con);
    //        da.SelectCommand.Parameters.AddWithValue("@strscno", li.strscno);
    //        da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
    //        da.Fill(dt);
    //        con.Close();
    //    }
    //    catch (Exception ex)
    //    { }
    //    finally
    //    {
    //        if (con.State == ConnectionState.Open)
    //        {
    //            con.Close();
    //        }
    //    }
    //    return dt;
    //}

    public void insertscitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into SCItems (scno,scdate,itemname,qty,stockqty,rate,basicamount,unit,descr1,descr2,descr3,cno,uname,udate,taxtype,vatp,addtaxp,cstp,vatamt,addtaxamt,cstamt,amount,ccode,qtyremain,qtyused,vno,vid,strscno,remarks,eunit) values (@scno,@scdate,@itemname,@qty,@stockqty,@rate,@basicamount,@unit,@descr1,@descr2,@descr3,@cno,@uname,@udate,@taxtype,@vatp,@addtaxp,@cstp,@vatamt,@addtaxamt,@cstamt,@amount,@ccode,@qtyremain,@qtyused,@vno,@vid,@strscno,@remarks,@eunit)", con);
            cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@scdate", li.scdate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@stockqty", li.stockqty);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@amount", li.amount);

            cmd.Parameters.AddWithValue("@taxtype", li.vattype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);

            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused1);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain1);
            cmd.Parameters.AddWithValue("@vno", li.vnono);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@eunit", li.unitewaybill);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCItems set scdate=@scdate,itemname=@itemname,qty=@qty,stockqty=@stockqty,rate=@rate,basicamount=@basicamount,unit=@unit,descr1=@descr1,descr2=@descr2,descr3=@descr3,uname=@uname,udate=@udate,taxtype=@taxtype,vatp=@vatp,addtaxp=@addtaxp,cstp=@cstp,vatamt=@vatamt,addtaxamt=@addtaxamt,cstamt=@cstamt,amount=@amount,ccode=@ccode,qtyremain=@qtyremain,qtyused=@qtyused,remarks=@remarks,rfs=@rfs,eunit=@eunit where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            //cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@scdate", li.scdate);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@qty", li.qty);
            cmd.Parameters.AddWithValue("@stockqty", li.stockqty);
            cmd.Parameters.AddWithValue("@rfs", li.rfs);
            cmd.Parameters.AddWithValue("@rate", li.rate);
            cmd.Parameters.AddWithValue("@basicamount", li.basicamount);
            cmd.Parameters.AddWithValue("@amount", li.amount);

            cmd.Parameters.AddWithValue("@taxtype", li.vattype);
            cmd.Parameters.AddWithValue("@vatp", li.vatp);
            cmd.Parameters.AddWithValue("@addtaxp", li.addtaxp);
            cmd.Parameters.AddWithValue("@cstp", li.cstp);
            cmd.Parameters.AddWithValue("@vatamt", li.vatamt);
            cmd.Parameters.AddWithValue("@addtaxamt", li.addtaxamt);
            cmd.Parameters.AddWithValue("@cstamt", li.cstamt);

            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@unit", li.unit);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            cmd.Parameters.AddWithValue("@remarks", li.remarks);
            cmd.Parameters.AddWithValue("@eunit", li.unitewaybill);
            //cmd.Parameters.AddWithValue("@retremarks", li.retremarks);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertscmasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into SCMaster (scno,scdate,acname,ccode,sono,sodate,lrno,lrdate,despatchedby,freightrs,sono1,status,cno,uname,udate,ctype,strscno,issigned,signremarks) values (@scno,@scdate,@acname,@ccode,@sono,@sodate,@lrno,@lrdate,@despatchedby,@freightrs,@sono1,@status,@cno,@uname,@udate,@ctype,@strscno,@issigned,@signremarks)", con);// SELECT SCOPE_IDENTITY()
            cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@scdate", li.scdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@sono", li.strsono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@despatchedby", li.dispatchedby);
            cmd.Parameters.AddWithValue("@freightrs", li.freight);
            cmd.Parameters.AddWithValue("@sono1", li.sono1);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@ctype", li.ctype);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@issigned", li.issigned);
            cmd.Parameters.AddWithValue("@signremarks", li.signremark);
            con.Open();
            //var cc = cmd.ExecuteScalar();
            //SessionMgt.voucherno = Convert.ToInt64(cc.ToString());
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescmasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set scdate=@scdate,acname=@acname,ccode=@ccode,sono=@sono,sodate=@sodate,lrno=@lrno,lrdate=@lrdate,despatchedby=@despatchedby,freightrs=@freightrs,sono1=@sono1,status=@status,uname=@uname,udate=@udate,ctype=@ctype,strscno=@strscno where scno=@scno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@scdate", li.scdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@sono", li.strsono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@despatchedby", li.dispatchedby);
            cmd.Parameters.AddWithValue("@freightrs", li.freight);
            cmd.Parameters.AddWithValue("@sono1", li.sono1);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@ctype", li.ctype);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescmasterdatastring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set scdate=@scdate,acname=@acname,ccode=@ccode,sono=@sono,sodate=@sodate,lrno=@lrno,lrdate=@lrdate,despatchedby=@despatchedby,freightrs=@freightrs,sono1=@sono1,status=@status,uname=@uname,udate=@udate,ctype=@ctype,issigned=@issigned,signremarks=@signremarks where strscno=@strscno and cno=@cno", con);
            //cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@scdate", li.scdate);
            cmd.Parameters.AddWithValue("@acname", li.acname);
            cmd.Parameters.AddWithValue("@ccode", li.ccode);
            cmd.Parameters.AddWithValue("@sono", li.strsono);
            cmd.Parameters.AddWithValue("@sodate", li.sodate);
            cmd.Parameters.AddWithValue("@lrno", li.lrno);
            cmd.Parameters.AddWithValue("@lrdate", li.lrdate);
            cmd.Parameters.AddWithValue("@despatchedby", li.dispatchedby);
            cmd.Parameters.AddWithValue("@freightrs", li.freight);
            cmd.Parameters.AddWithValue("@sono1", li.sono1);
            cmd.Parameters.AddWithValue("@status", li.status);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@uname", li.uname);
            cmd.Parameters.AddWithValue("@udate", li.udate);
            cmd.Parameters.AddWithValue("@ctype", li.ctype);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@issigned", li.issigned);
            cmd.Parameters.AddWithValue("@signremarks", li.signremark);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateremainqtyinsalesorder(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatechallantype(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set ctype=@ctype,strscno=@strscno where scno=@scno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@ctype", li.ctype);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletescmasterdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SCMaster where scno=@scno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletescmasterdatastring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SCMaster where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletescitemsdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SCItems where scno=@scno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletescitemsdatastring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SCItems where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void deletescitemsdatafromid(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from SCItems where id=@id and cno=@cno", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updateqtyduringdelete(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where itemname=@itemname and sono=@sono and cno=@cno", con);
            cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id", con);
            //cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@id", li.vid);
            //cmd.Parameters.AddWithValue("@sono", li.sono);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@qtyremain", li.qtyremain);
            cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallsalesorderitemdatafromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno and qtyremain>0", con);
            da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderitemdatafromsonoforcominv(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where sono=@sono and cno=@cno and qtyremain>0", con);
            da = new SqlDataAdapter("select * from SalesOrderItems left join ItemMaster on ItemMaster.itemname=SalesOrderItems.itemname where SalesOrderItems.sono=@sono and SalesOrderItems.cno=@cno order by SalesOrderItems.id", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderitemdatafromsonousingin(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where id in (" + li.remarks + ") and cno=@cno and qtyremain>0", con);
            da = new SqlDataAdapter("select * from SalesOrderItems where id in (" + li.remarks + ") and cno=@cno", con);
            //da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderitemdatafromsonousinginforcomminv(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where id in (" + li.remarks + ") and cno=@cno and qtyremain>0", con);
            da = new SqlDataAdapter("select * from SalesOrderItems left join ItemMaster on ItemMaster.itemname=SalesOrderItems.itemname where SalesOrderItems.id in (" + li.remarks + ") and SalesOrderItems.cno=@cno order by SalesOrderItems.id", con);
            //da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesordermasterdatafromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrder where sono=@sono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedsaleschallanno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from vnogen where cno=@cno and isscused='N' order by id", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectunusedsaleschallannolast(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select scno from SCMaster where cno=@cno order by scno desc", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisused(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isscused='Y' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.scno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectallstatus(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from MiscMaster where type='Status' order by name", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster order by acname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallitemname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where isblock='N' order by itemname", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectallsalesorderforpopup(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SalesOrder where status!='Closed' and cno=@cno and acname=@acname", con);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectpodatefrompono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            da = new SqlDataAdapter("select pono,convert(varchar(50),podate,105) as cdate from SalesOrder where sono=@sono and cno=@cno", con);
            da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectqtyremainusedfromsono(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            //da = new SqlDataAdapter("select * from SalesOrderItems where cno=@cno and sono=@sono and itemname=@itemname", con);
            da = new SqlDataAdapter("select * from SalesOrderItems where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id", li.vid);
            //da.SelectCommand.Parameters.AddWithValue("@sono", li.sono);
            //da.SelectCommand.Parameters.AddWithValue("@itemname", li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateisusedn(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update vnogen set isscused='N' where vno=@vno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@vno", li.scno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescmasterdataupdatesc(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set scno=@scno1 where scno=@scno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@scno1", li.scno1);
            cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescmasterdataupdatescstring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set scno=@scno1,strscno=@strscno1,ctype=@ctype1 where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@scno1", li.scno1);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@strscno1", li.strscno1);
            cmd.Parameters.AddWithValue("@ctype1", li.ctype1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescitemsdataupdatesc(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCItems set scno=@scno1 where scno=@scno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@scno1", li.scno1);
            cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescitemsdataupdatescstring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCItems set scno=@scno1,strscno=@strscno1 where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@scno1", li.scno1);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@strscno1", li.strscno1);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescitemsdatedataupdatescstring(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCItems set scdate=@scdate where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@scdate", li.scdate);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatescitemsdataupdatescstring1(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCItems set strscno=@strscno where scno=@scno and cno=@cno", con);            
            cmd.Parameters.AddWithValue("@scno", li.scno);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrscnoinsalesorderitems(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SalesOrderItems set strscno=@strscno where id=@id", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            //cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrscnoinsalesorderitems1(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SalesOrderItems set strscno=@strscno where id=@vid", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            //cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@vid", li.vid);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrscnoinsalesorder(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SalesOrder set strscno=@strscno where sono=@sono", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            //cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void updatestrscnoinsalesordera(LogicLayer li)
    {
        try
        {
            //cmd = new SqlCommand("update SalesOrderItems set qtyremain=@qtyremain,qtyused=@qtyused where id=@id and vno=@vno and cno=@cno", con);
            cmd = new SqlCommand("update SalesOrderItems set strscno=@strscno where sono=@sono", con);
            //cmd.Parameters.AddWithValue("@scno", li.sono);
            //cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);
            //cmd.Parameters.AddWithValue("@qtyused", li.qtyused);
            //cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@sono", li.sono);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable checkbalqty(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCItems where id=@id and cno=@cno and (isnull(adjqty,0))>0", con);//((isnull(stockqty,0))-(isnull(adjqty,0)))>0
            da.SelectCommand.Parameters.AddWithValue("@id", li.id);
            da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    //For Eway Bill
    public DataTable selectallacdatefromacname(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ACMaster where acname='" + li.acname + "'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectewaybilldatafromstrscno(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select *,convert(varchar(20),ewaybilldate,103) as date1,convert(varchar(20),cewaybilldate,103) as date2,convert(varchar(20),tradate,103) as date3 from SCMaster where strscno='" + li.strscno + "'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public void updateewaybilldetailfromstrscno(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update SCMaster set ewaybillno=@ewaybillno,ewaybilldate=@ewaybilldate,cewaybillno=@cewaybillno,cewaybilldate=@cewaybilldate,supplytype=@supplytype,subtype=@subtype,doctype=@doctype,tramode=@tramode,distanceinkm=@distanceinkm,transportername=@transportername,vehicleno=@vehicleno,vehicletype=@vehicletype,mainhsncode=@mainhsncode,trano=@trano,tradate=@tradate,traid=@traid,consignor=@consignor,consignoradd1=@consignoradd1,consignoradd2=@consignoradd2,consignorplace=@consignorplace,consignorpincode=@consignorpincode,consignorstate=@consignorstate,actualfromstatecode=@actualfromstatecode,consignorgst=@consignorgst,consignee=@consignee,consigneeadd1=@consigneeadd1,consigneeadd2=@consigneeadd2,consigneeplace=@consigneeplace,consigneepincode=@consigneepincode,consigneestate=@consigneestate,actualtostatecode=@actualtostatecode,consigneegst=@consigneegst where strscno=@strscno and cno=@cno", con);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@strscno", li.strscno);

            cmd.Parameters.AddWithValue("@ewaybillno", li.ewaybillno);
            cmd.Parameters.AddWithValue("@ewaybilldate", li.ewaybilldate);
            cmd.Parameters.AddWithValue("@cewaybillno", li.cewaybillno);
            cmd.Parameters.AddWithValue("@cewaybilldate", li.cewaybilldate);
            cmd.Parameters.AddWithValue("@supplytype", li.supplytype);
            cmd.Parameters.AddWithValue("@subtype", li.subtype);
            cmd.Parameters.AddWithValue("@doctype", li.doctype);

            cmd.Parameters.AddWithValue("@tramode", li.tramode);
            cmd.Parameters.AddWithValue("@distanceinkm", li.distanceinkm);
            cmd.Parameters.AddWithValue("@transportername", li.transportername);
            cmd.Parameters.AddWithValue("@vehicleno", li.vehiclenumber);
            cmd.Parameters.AddWithValue("@vehicletype", li.vehicletype);
            cmd.Parameters.AddWithValue("@mainhsncode", li.mainhsncode);
            cmd.Parameters.AddWithValue("@trano", li.trano);
            cmd.Parameters.AddWithValue("@tradate", li.tradate);
            cmd.Parameters.AddWithValue("@traid", li.traid);

            cmd.Parameters.AddWithValue("@consignor", li.consignor);
            cmd.Parameters.AddWithValue("@consignoradd1", li.consignoradd1);
            cmd.Parameters.AddWithValue("@consignoradd2", li.consignoradd2);
            cmd.Parameters.AddWithValue("@consignorplace", li.consignorplace);
            cmd.Parameters.AddWithValue("@consignorpincode", li.consignorpincode);
            cmd.Parameters.AddWithValue("@consignorstate", li.consignorstate);
            cmd.Parameters.AddWithValue("@actualfromstatecode", li.actualfromstate);
            cmd.Parameters.AddWithValue("@consignorgst", li.consignorgst);

            cmd.Parameters.AddWithValue("@consignee", li.consignee);
            cmd.Parameters.AddWithValue("@consigneeadd1", li.consigneeadd1);
            cmd.Parameters.AddWithValue("@consigneeadd2", li.consigneeadd2);
            cmd.Parameters.AddWithValue("@consigneeplace", li.consigneeplace);
            cmd.Parameters.AddWithValue("@consigneepincode", li.consigneepincode);
            cmd.Parameters.AddWithValue("@consigneestate", li.consigneestate);
            cmd.Parameters.AddWithValue("@actualtostatecode", li.actualtostate);
            cmd.Parameters.AddWithValue("@consigneegst", li.consigneegst);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable selectmainhsncode(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from SCItems inner join itemmaster on scitems.itemname=itemmaster.itemname where SCItems.strscno='" + li.strscno + "'", con);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

    public DataTable selectewayunitfromitemmaster(LogicLayer li)
    {
        DataTable dt = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select distinct(sono) from salesorder where cno=@cno and ispi='N'", con);
            da = new SqlDataAdapter("select * from ItemMaster where itemname=@itemname", con);
            da.SelectCommand.Parameters.AddWithValue("@itemname",li.itemname);
            da.Fill(dt);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return dt;
    }

}