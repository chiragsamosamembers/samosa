﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Survey.Classes;

/// <summary>
/// Summary description for ForRegister
/// </summary>
public class ForRegister
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public ForRegister()
    {
        con = new SqlConnection(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
    }

    public void updatepassworddata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update login set password=@password where cno=@cno and username=@username", con);
            cmd.Parameters.AddWithValue("@username", li.username);
            cmd.Parameters.AddWithValue("@password", li.password);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public void insertlogindata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("insert into login (username,password,cno,isactive,role) values (@username,@password,@cno,@isactive,@role)", con);
            cmd.Parameters.AddWithValue("@username", li.username);
            cmd.Parameters.AddWithValue("@password", li.password);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@isactive", li.isactive);
            cmd.Parameters.AddWithValue("@role", li.role);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

    public DataTable checkuserforchangepassword(LogicLayer li)
    {
        DataTable ds = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select * from login where username='" + li.username + "' and password='" + li.password + "' COLLATE SQL_Latin1_General_CP1_CS_AS", con);
            da = new SqlDataAdapter("SELECT * FROM login WHERE username='" + li.username + "' COLLATE SQL_Latin1_General_CP1_CS_AS and cno=" + li.cno + " and password='" + li.password + "' COLLATE SQL_Latin1_General_CP1_CS_AS", con);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return ds;
    }

    public DataTable selectalluser()
    {
        DataTable ds = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select * from login where username='" + li.username + "' and password='" + li.password + "' COLLATE SQL_Latin1_General_CP1_CS_AS", con);
            da = new SqlDataAdapter("SELECT * FROM login order by id desc", con);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return ds;
    }

    public DataTable selectuserdatafromid(LogicLayer li)
    {
        DataTable ds = new DataTable();
        try
        {
            con.Open();
            //da = new SqlDataAdapter("select * from login where username='" + li.username + "' and password='" + li.password + "' COLLATE SQL_Latin1_General_CP1_CS_AS", con);
            da = new SqlDataAdapter("SELECT * FROM login where id=@id", con);
            da.SelectCommand.Parameters.AddWithValue("@id",li.id);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        return ds;
    }

    public void deleteuserdata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("delete from login where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }

   public void updatelogindata(LogicLayer li)
    {
        try
        {
            cmd = new SqlCommand("update login set username=@username,password=@password,cno=@cno,isactive=@isactive,role=@role where id=@id", con);
            cmd.Parameters.AddWithValue("@id", li.id);
            cmd.Parameters.AddWithValue("@username", li.username);
            cmd.Parameters.AddWithValue("@password", li.password);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@isactive", li.isactive);
            cmd.Parameters.AddWithValue("@role", li.role);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        { }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }


}