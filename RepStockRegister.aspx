﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RepStockRegister.aspx.cs" Inherits="RepStockRegister" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Stock Register Report</span><br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    From Item</label></div>
            <div class="col-md-2">
                <asp:DropDownList ID="drpfromitem" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnSelectedIndexChanged="drpfromitem_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    To Item</label></div>
            <div class="col-md-2">
                <asp:DropDownList ID="drptoitem" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    From Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox>
                <%--<asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txtfromdate">
                </asp:MaskedEditExtender>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter from date."
                    Text="*" ControlToValidate="txtfromdate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="From Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txtfromdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    To Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
                <%-- <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99-99-9999"
                    MaskType="Date" TargetControlID="txttodate">
                </asp:MaskedEditExtender>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter to date."
                    Text="*" ControlToValidate="txttodate" ValidationGroup="val"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="To Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                    ControlToValidate="txttodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator></div>
        </div>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnfillledger" runat="server" Text="Fill Ledger" class="btn btn-default forbutton"
                    ValidationGroup="val" OnClick="btnfillledger_Click" />
                <asp:Button ID="btnpreview" runat="server" Text="Preview" class="btn btn-default forbutton"
                    OnClick="btnpreview_Click" ValidationGroup="val" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
                Descr :
                <asp:Label ID="lbldescr" runat="server"></asp:Label>
            </div>
            <div class="col-md-2" style="font-size: large; font-weight: bolder;">
                Received :
                <asp:Label ID="lbltotalreceived" runat="server"></asp:Label>
            </div>
            <div class="col-md-2" style="font-size: large; font-weight: bolder;">
                Issued :
                <asp:Label ID="lbltotalissued" runat="server"></asp:Label>
            </div>
            <div class="col-md-2" style="font-size: large; font-weight: bolder;">
                Closed :
                <asp:Label ID="lblclosedqty" runat="server"></asp:Label>
            </div>
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvaclist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvaclist_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="process" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                        ToolTip="Change Item Name" Height="20px" Width="20px" CausesValidation="False"
                                        CommandArgument='<%# bind("strchno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Redirect" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("strchno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ch. No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblchno" ForeColor="Black" runat="server" Text='<%# bind("strchno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ch. Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblchdate" ForeColor="Black" runat="server" Text='<%# bind("chdate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bill No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbillno" ForeColor="Black" runat="server" Text='<%# bind("billno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bill Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbilldate" ForeColor="Black" runat="server" Text='<%# bind("billdate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Party Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblacname" ForeColor="Black" runat="server" Text='<%# bind("acname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Received" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrqty" ForeColor="Black" runat="server" Text='<%# bind("rqty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Issued" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbliqty" ForeColor="Black" runat="server" Text='<%# bind("iqty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Closing" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblclqty" ForeColor="Black" runat="server" Text='<%# bind("clqty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbltype" ForeColor="Black" runat="server" Text='<%# bind("type") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row">
            <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
            <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
                TargetControlID="lnkopenpopup" BackgroundCssClass="modalBackground" CancelControlID="btnClose">
            </asp:ModalPopupExtender>
            <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="390px" align="center"
                Width="95%" Style="display: none">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <span style="color: Red;"><b>Update Item</b></span></div>
                </div>
                <div class="row">
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-1 text-left">
                        <b>Old Name :</b></div>
                    <div class="col-md-3">
                        <asp:Label ID="lbloldname" runat="server" Font-Bold="true"></asp:Label></div>
                    <div class="col-md-1 text-left">
                        <b>New Name</b></div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="drpnewname" runat="server" CssClass="form-control">
                            <%--AutoPostBack="True"
                            OnSelectedIndexChanged="drpnewname_SelectedIndexChanged"--%>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <%--<div class="col-md-1 text-left">
                        <b>Unit :</b></div>
                    <div class="col-md-1">
                        <asp:Label ID="lblunit" runat="server" Font-Bold="true"></asp:Label></div>--%>
                    <div class="col-md-1">
                        <asp:Label ID="lbltypec" runat="server" Font-Bold="true"></asp:Label></div>
                    <div class="col-md-1">
                        <b>Challan No.:</b></div>
                    <div class="col-md-3">
                        <asp:Label ID="lblchallanno" runat="server" Font-Bold="true"></asp:Label>&nbsp;&nbsp;ID
                        :
                        <asp:Label ID="lblchid" runat="server" Font-Bold="true"></asp:Label></div>
                    <div class="col-md-1">
                        <b>Bill No.:</b></div>
                    <div class="col-md-1">
                        <asp:Label ID="lblbillno" runat="server" Font-Bold="true"></asp:Label>&nbsp;&nbsp;ID
                        :
                        <asp:Label ID="lblbillid" runat="server" Font-Bold="true"></asp:Label></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <asp:Button ID="btnpopupdate" runat="server" Text="Update" AccessKey="c" BackColor="Red"
                            ForeColor="White" OnClick="btnpopupdate_Click" /></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <asp:Label ID="lblalert" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label></div>
                    <asp:Button ID="btnClose" runat="server" Text="Close" AccessKey="c" BackColor="Red"
                        ForeColor="White" />
                </div>
            </asp:Panel>
        </div>
        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
            function getme() {
                $("#<%= txtfromdate.ClientID %>").datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });

                $("#<%= txttodate.ClientID %>").datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });


            }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#<%= txtfromdate.ClientID %>").click(function () {

                    $(this).datepicker({
                        showmonth: true,
                        autoSize: true,
                        showAnim: 'slideDown',
                        duration: 'fast',
                        dateFormat: "dd-mm-yy"
                    });
                });
                $("#<%= txtfromdate.ClientID %>").datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });


                $("#<%= txttodate.ClientID %>").click(function () {

                    $(this).datepicker({
                        showmonth: true,
                        autoSize: true,
                        showAnim: 'slideDown',
                        duration: 'fast',
                        dateFormat: "dd-mm-yy"
                    });
                });
                $("#<%= txttodate.ClientID %>").datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });

            });
        </script>
    </div>
</asp:Content>
