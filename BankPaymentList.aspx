﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BankPaymentList.aspx.cs" Inherits="BankPaymentList" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Bank Payment List</span><br />
        <div class="row">
            <div class="col-md-12">
                <asp:Button ID="btnbankpayment" runat="server" Text="Add New Bank Payment" class="btn btn-default forbutton"
                    OnClick="btnbankpayment_Click" /></div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvbankpaymentlist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvbankpaymentlist_RowCommand" OnRowDeleting="gvbankpaymentlist_RowDeleting"
                        OnRowDataBound="gvbankpaymentlist_RowDataBound" AllowPaging="true" PageSize="10">
                        <%--AllowPaging="True" OnPageIndexChanging="gvbankpaymentlist_PageIndexChanging"--%>
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("voucherno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Voucher NO." SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblvoucherno" ForeColor="Black" runat="server" Text='<%# bind("voucherno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Voucher Date" SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblvoucherdate" ForeColor="Black" runat="server" Text='<%# bind("voucherdate") %>'
                                        Visible="false"></asp:Label>
                                    <%#Convert.ToDateTime(Eval("voucherdate")).ToString("dd-MM-yyyy")%>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Party Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblacname" ForeColor="Black" runat="server" Text='<%# bind("acname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bank Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbankname" ForeColor="Black" runat="server" Text='<%# bind("name") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cheque No" SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblchequeno" ForeColor="Black" runat="server" Text='<%# bind("chequeno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IsType" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblistype" ForeColor="Black" runat="server" Text='<%# bind("istype") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("voucherno") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="print" runat="server" ImageUrl="~/images/buttons/printer.gif"
                                        ToolTip="Print Cheque" CommandArgument='<%# bind("voucherno") %>' Height="20px"
                                        Width="20px" CausesValidation="False" /><%--OnClientClick="return confirmprintchq();"--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1111" CommandName="print" runat="server" ImageUrl="~/images/buttons/printer.gif"
                                        ToolTip="Print Voucher" Height="20px" Width="20px" OnClientClick="return confirmprintvoucher();"
                                        CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
    <div class="row">
        <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
            TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="190px" align="center"
            Width="95%" Style="display: none">
            <div class="row">
                <div class="col-md-12 text-center">
                    <span style="color: Red;"><b>Cheque Format Selection</b></span></div>
            </div>
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-md-1 text-left">
                    <b>Report Type</b></div>
                <div class="col-md-7">
                    <asp:DropDownList ID="drpreporttype" runat="server" CssClass="form-control">
                        <%--AutoPostBack="true" onselectedindexchanged="drpreporttype_SelectedIndexChanged"--%>
                        <asp:ListItem>--SELECT--</asp:ListItem>
                        <asp:ListItem>AXIS BANK CHEQUE</asp:ListItem>
                        <asp:ListItem>AXIS BANK SELF CHEQUE</asp:ListItem>
                        <asp:ListItem>DENA BANK-SBI CHEQUE</asp:ListItem>
                        <asp:ListItem>BANK OF INDIA CHEQUE</asp:ListItem>
                        <asp:ListItem>DENA BANK-SBI SELF CHEQUE</asp:ListItem>
                        <asp:ListItem>BANK OF INDIA SELF CHEQUE</asp:ListItem>
                        <asp:ListItem>BANK PAYMENT VOUCHER</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 text-left">
                    <b>Cheque name</b></div>
                <div class="col-md-7">
                    <asp:TextBox ID="txtchequename" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 text-left">
                    <%--<b>Adj. Amount :</b>--%></div>
                <div class="col-md-2">
                    <asp:Button ID="btngo" runat="server" Text="Go" BackColor="Red" ForeColor="White"
                        OnClick="btngo_Click" /></div>
            </div>
            <asp:Button ID="btnClose" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
        </asp:Panel>
    </div>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
        RenderingDPI="100" PrintMode="ActiveX" />
</asp:Content>
