﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlClient;
using System.Data.SqlTypes;

public partial class ProjectReport : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();
    ForSalesOrder fsclass = new ForSalesOrder();
    ForActivity faclass = new ForActivity();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            // CountryNames.Add(dt.Rows[i][1].ToString());
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }
    protected void txtclientcode_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
            li.ccode = Convert.ToInt64(txtclientcode.Text.ToString());
            DataTable dtdata = new DataTable();
            dtdata = fsclass.selectsodatafromccode(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvprojectreport.Visible = true;
                gvprojectreport.DataSource = dtdata;
                gvprojectreport.DataBind();
            }
            else
            {
                gvprojectreport.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Project Report Data Found.";
            }
        }
        else
        {
            txtclientcode.Text = string.Empty;
            gvprojectreport.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Project Report Data Found.";
        }

    }
    protected void chkfirstquotation_CheckedChanged(object sender, EventArgs e)
    {

        GridViewRow currentRow1 = (GridViewRow)((CheckBox)sender).Parent.Parent;
        CheckBox chk = (CheckBox)currentRow1.FindControl("chkfirstquotation");
        TextBox txt = (TextBox)currentRow1.FindControl("txtfirstquotation");
        if (chk.Checked == true)
        {
            txt.Text = System.DateTime.Now.ToShortDateString();
            chk.Visible = true;

        }

        //CheckBox chek = (CheckBox)gvprojectreport.FindControl("chkfirstquotation");
        //TextBox txt = (TextBox)gvprojectreport.FindControl("txtfirstquotation");
        //if (chek.Checked == true)
        //{
        //    txt.Visible = true;
        //}
        //else
        //{
        //    txt.Visible = false;
        //}
    }
    protected void txtfirstquotation_TextChanged(object sender, EventArgs e)
    {
        //foreach (GridViewRow row in gvprojectreport.Rows)
        //{
        GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        CheckBox chk = (CheckBox)currentRow1.FindControl("chkfirstquotation");
        TextBox txt = (TextBox)currentRow1.FindControl("txtfirstquotation");
        Label lblsono = (Label)currentRow1.FindControl("lblsono");
        if (txt.Text != string.Empty)
        {
            txt.Visible = false;
            chk.Visible = true;

        }
        else
        {
            txt.Visible = true;
            chk.Visible = false;
        }
        //}
    }
    protected void chktechnicaldrawing_CheckedChanged(object sender, EventArgs e)
    {
        //foreach (GridViewRow row in gvprojectreport.Rows)
        //{
        GridViewRow currentRow1 = (GridViewRow)((CheckBox)sender).Parent.Parent;
        CheckBox chk = (CheckBox)currentRow1.FindControl("chktechnicaldrawing");
        TextBox txt = (TextBox)currentRow1.FindControl("txttechnicaldrawing");
        if (chk.Checked == true)
        {
            txt.Text = System.DateTime.Now.ToShortDateString();

        }
        //}
    }
    protected void txttechnicaldrawing_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        CheckBox chk = (CheckBox)currentRow1.FindControl("chktechnicaldrawing");
        TextBox txt = (TextBox)currentRow1.FindControl("txttechnicaldrawing");
        Label lblsono = (Label)currentRow1.FindControl("lblsono");
        if (txt.Text != string.Empty)
        {
            txt.Visible = false;
            chk.Visible = true;

        }
        else
        {
            txt.Visible = true;
            chk.Visible = false;
        }
    }
    protected void chkrewisequotation_CheckedChanged(object sender, EventArgs e)
    {
        //foreach (GridViewRow row in gvprojectreport.Rows)
        //{
        GridViewRow currentRow1 = (GridViewRow)((CheckBox)sender).Parent.Parent;
        CheckBox chk = (CheckBox)currentRow1.FindControl("chkrewisequotation");
        TextBox txt = (TextBox)currentRow1.FindControl("txtrevisequotation");
        if (chk.Checked == true)
        {
            txt.Text = System.DateTime.Now.ToShortDateString();
        }
        // }
    }
    protected void txtrevisequotation_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        CheckBox chk = (CheckBox)currentRow1.FindControl("chkrewisequotation");
        TextBox txt = (TextBox)currentRow1.FindControl("txtrevisequotation");
        Label lblsono = (Label)currentRow1.FindControl("lblsono");
        if (txt.Text != string.Empty)
        {
            txt.Visible = false;
            chk.Visible = true;

        }
        else
        {
            txt.Visible = true;
            chk.Visible = false;
        }
    }
    protected void chkfinaltech_CheckedChanged(object sender, EventArgs e)
    {
        //foreach (GridViewRow row in gvprojectreport.Rows)
        //{
        GridViewRow currentRow1 = (GridViewRow)((CheckBox)sender).Parent.Parent;
        CheckBox chk = (CheckBox)currentRow1.FindControl("chkfinaltech");
        TextBox txt = (TextBox)currentRow1.FindControl("txtfinaltech");
        if (chk.Checked == true)
        {
            txt.Text = System.DateTime.Now.ToShortDateString();
        }
        //}
    }
    protected void txtfinaltech_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        CheckBox chk = (CheckBox)currentRow1.FindControl("chkfinaltech");
        TextBox txt = (TextBox)currentRow1.FindControl("txtfinaltech");
        Label lblsono = (Label)currentRow1.FindControl("lblsono");
        if (txt.Text != string.Empty)
        {
            txt.Visible = false;
            chk.Visible = true;

        }
        else
        {
            txt.Visible = true;
            chk.Visible = false;
        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        if (txtclientcode.Text != string.Empty)
        {
            SqlDateTime sqldatenull = SqlDateTime.Null;
            foreach (GridViewRow row in gvprojectreport.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblid = (Label)row.FindControl("lblid");
                    Label lblsono = (Label)row.FindControl("lblsono");
                    Label lbldescription = (Label)row.FindControl("lbldescription");
                    Label lblunit = (Label)row.FindControl("lblunit");
                    Label lblqty = (Label)row.FindControl("lblqty");
                    CheckBox chkfirstquotation = (CheckBox)row.FindControl("chkfirstquotation");
                    CheckBox chktechnicaldrawing = (CheckBox)row.FindControl("chktechnicaldrawing");
                    CheckBox chkrewisequotation = (CheckBox)row.FindControl("chkrewisequotation");
                    CheckBox chkfinaltech = (CheckBox)row.FindControl("chkfinaltech");
                    TextBox txtfirstquotation = (TextBox)row.FindControl("txtfirstquotation");
                    TextBox txttechnicaldrawing = (TextBox)row.FindControl("txttechnicaldrawing");
                    TextBox txtrevisequotation = (TextBox)row.FindControl("txtrevisequotation");
                    TextBox txtfinaltech = (TextBox)row.FindControl("txtfinaltech");
                    TextBox txtfollowupdate = (TextBox)row.FindControl("txtfollowupdate");
                    DropDownList drppendingorderlyingwith = (DropDownList)row.FindControl("drppendingorderlying");
                    TextBox txtokorderdate = (TextBox)row.FindControl("txtokorderdate");
                    TextBox txttentativedeliverydate = (TextBox)row.FindControl("txttentativedeliverydate");
                    TextBox txtdeliverydate = (TextBox)row.FindControl("txtdeliverydate");
                    TextBox txtremarks = (TextBox)row.FindControl("txtremarks");
                    //if (chkfirstquotation.Checked == true)
                    //{
                    if (txtfirstquotation.Text != string.Empty)
                    {
                        li.firstquotation = Convert.ToDateTime(txtfirstquotation.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    }
                    else
                    {
                        li.firstquotation = sqldatenull;
                    }
                    //}
                    //else
                    //{
                    //    li.firstquotation = sqldatenull;
                    //}
                    //if (chktechnicaldrawing.Checked == true)
                    //{
                    if (txttechnicaldrawing.Text != string.Empty)
                    {
                        li.technicaldrawing = Convert.ToDateTime(txttechnicaldrawing.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    }
                    else
                    {
                        li.technicaldrawing = sqldatenull;
                    }
                    //}
                    //else
                    //{
                    //    li.technicaldrawing = sqldatenull;
                    //}
                    //if (chkrewisequotation.Checked == true)
                    //{
                    if (txtrevisequotation.Text != string.Empty)
                    {
                        li.rewisequotation = Convert.ToDateTime(txtrevisequotation.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    }
                    else
                    {
                        li.rewisequotation = sqldatenull;
                    }
                    //}
                    //else
                    //{
                    //    li.rewisequotation = sqldatenull;
                    //}
                    //if (chkfinaltech.Checked == true)
                    //{
                    if (txtfinaltech.Text != string.Empty)
                    {
                        li.finaltech = Convert.ToDateTime(txtfinaltech.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    }
                    else
                    {
                        li.finaltech = sqldatenull;
                    }
                    //}
                    //else
                    //{
                    //    li.finaltech = sqldatenull;
                    //}
                    if (txtfollowupdate.Text != string.Empty)
                    {
                        li.lastfollowupdate = Convert.ToDateTime(txtfollowupdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    }
                    else
                    {
                        li.lastfollowupdate = sqldatenull;
                    }


                    if (txtokorderdate.Text != string.Empty)
                    {
                        li.okorderdate = Convert.ToDateTime(txtokorderdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    }
                    else
                    {
                        li.okorderdate = sqldatenull;
                    }
                    if (txttentativedeliverydate.Text != string.Empty)
                    {
                        li.tentativedeliverydate = Convert.ToDateTime(txttentativedeliverydate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    }
                    else
                    {
                        li.tentativedeliverydate = sqldatenull;
                    }
                    if (txtdeliverydate.Text != string.Empty)
                    {
                        li.deliverydate = Convert.ToDateTime(txtdeliverydate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    }
                    else
                    {
                        li.deliverydate = sqldatenull;
                    }
                    li.remarks = txtremarks.Text;
                    li.pendingorderlying = drppendingorderlyingwith.SelectedItem.Text;
                    string cz = Request.Cookies["Forcon"]["conc"];
                    cz = cz.Replace(":", ";");
                    SqlConnection con = new SqlConnection(cz);
                    li.sono = Convert.ToInt64(lblsono.Text);
                    li.id = Convert.ToInt64(lblid.Text);
                    SqlCommand cmd = new SqlCommand("update SalesOrderItems set firstquotation=@firstquotation,technicaldrawing=@technicaldrawing,rewisequotation=@rewisequotation,finaltech=@finaltech,lastfollowupdate=@lastfollowupdate,pendingorderlying=@pendingorderlying,okorderdate=@okorderdate,tentativedeliverydate=@tentativedeliverydate,deliverydate=@deliverydate,remarks=@remarks where id=@id", con);
                    cmd.Parameters.AddWithValue("@firstquotation", li.firstquotation);
                    cmd.Parameters.AddWithValue("@technicaldrawing", li.technicaldrawing);
                    cmd.Parameters.AddWithValue("@rewisequotation", li.rewisequotation);
                    cmd.Parameters.AddWithValue("@finaltech", li.finaltech);
                    cmd.Parameters.AddWithValue("@lastfollowupdate", li.lastfollowupdate);
                    cmd.Parameters.AddWithValue("@pendingorderlying", li.pendingorderlying);
                    cmd.Parameters.AddWithValue("@okorderdate", li.okorderdate);
                    cmd.Parameters.AddWithValue("@tentativedeliverydate", li.tentativedeliverydate);
                    cmd.Parameters.AddWithValue("@deliverydate", li.deliverydate);
                    cmd.Parameters.AddWithValue("@remarks", li.remarks);
                    //cmd.Parameters.AddWithValue("@sono", li.sono);
                    cmd.Parameters.AddWithValue("@id", li.id);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = "Project Details Updated for SO NO - " + li.sono + " ,For Client Code " + txtclientcode.Text;
                    faclass.insertactivity(li);
                }
            }
            li.ccode = Convert.ToInt64(txtclientcode.Text.ToString());
            DataTable dtdata = new DataTable();
            dtdata = fsclass.selectsodatafromccode(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvprojectreport.Visible = true;
                gvprojectreport.DataSource = dtdata;
                gvprojectreport.DataBind();
            }
            else
            {
                gvprojectreport.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Project Report Data Found.";
            }
        }
    }
    protected void gvprojectreport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddl = (e.Row.FindControl("drppendingorderlying") as DropDownList);
            Label lblpending = (e.Row.FindControl("lblpending") as Label);
            DataTable dtpending = new DataTable();
            dtpending = fsclass.selectallpendingorderlyingdata();
            if (dtpending.Rows.Count > 0)
            {
                ddl.DataSource = dtpending;
                ddl.DataTextField = "name";
                ddl.DataBind();
                ddl.Items.Insert(0, "--SELECT--");
                ddl.SelectedValue = lblpending.Text;
            }
            else
            {
                ddl.Items.Clear();
                ddl.Items.Insert(0, "--SELECT--");
            }
        }
    }
    protected void btnprint_Click(object sender, EventArgs e)
    {
        if (txtclientcode.Text != string.Empty)
        {
            string Xrepname = "project report";
            //Int64 vno = Convert.ToInt64(e.CommandArgument);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?ccode=" + txtclientcode.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }
}