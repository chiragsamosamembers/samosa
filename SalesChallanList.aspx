﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SalesChallanList.aspx.cs" Inherits="SalesChallanList" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script  type="text/javascript">
    $(function () {
        $("#example").DataTable({
            "pageLength": 10
        });
//        $('#example').DataTable({
//            "pageLength": 30,
//            "paging": true,
//            "lengthChange": false,
//            "searching": true,
//            "ordering": true,
//            "info": true,
//            "autoWidth": false
//        });
    });  
</script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Sales Challan List</span><br />
        <div class="row">
            <div class="col-md-6">
                <asp:Button ID="btnsaleschallan" runat="server" Text="Add New Sales Challan" class="btn btn-default forbutton"
                    OnClick="btnsaleschallan_Click" /></div>
            <div class="col-md-1">
                <label class="control-label">
                    Challan Type</label></div>
            <div class="col-md-5">
                <asp:DropDownList ID="drpchallantype" runat="server" CssClass="form-control" Width="150px"
                    AutoPostBack="True" OnSelectedIndexChanged="drpchallantype_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <%--<div class="row table-responsive">
            <div class="col-md-12">--%>
            <table id="example" class="table table-bordered" style="border : 5px Solid Black !important;">
        <thead>
            <tr>
                <td>
                    SCNO
                </td>
                <td>
                    SCNO
                </td>
                <td>
                    SC Date
                </td>
                <td>
                    ACName
                </td>
                <td>
                    CID
                </td>
                <td>
                    Signed or NotCNO
                </td>
                <td>
                    Remarks
                </td>
                <td></td>
                <td></td>
            </tr>
        </thead>
        <tbody id="tlist" runat="server">
        </tbody>
    </table>
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%" Visible="false">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvsaleschallanlist" runat="server" AutoGenerateColumns="False"
                        Width="95%" BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”"
                        Height="0px" CssClass="table table-bordered" OnRowCommand="gvsaleschallanlist_RowCommand"
                        OnRowDeleting="gvsaleschallanlist_RowDeleting" OnRowDataBound="gvsaleschallanlist_RowDataBound">
                        <%--AllowPaging="True" 
                        onpageindexchanging="gvsaleschallanlist_PageIndexChanging"--%>
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("strscno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SC NO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblstrscno" ForeColor="Black" runat="server" Text='<%# bind("strscno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SC NO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblscno" ForeColor="Black" runat="server" Text='<%# bind("scno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SC Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblscdate" ForeColor="Black" runat="server" Text='<%#Convert.ToDateTime(Eval("scdate")).ToString("dd-MM-yyyy") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblacname" ForeColor="Black" runat="server" Text='<%# bind("acname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CID" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Signed Or Not" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chksigned" runat="server" AutoPostBack="True" OnCheckedChanged="chksigned_CheckedChanged" />
                                    <asp:Label ID="lblsigned" ForeColor="Black" runat="server" Text='<%# bind("issigned") %>'
                                        Visible="false"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtremarks" runat="server" AutoPostBack="True" OnTextChanged="txtremarks_TextChanged"
                                        Text='<%# bind("signremarks") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Bill No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbillno" ForeColor="Black" runat="server" Text='<%# bind("strsino") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <ItemTemplate>                                
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("strscno") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="print" runat="server" ImageUrl="~/images/buttons/printer.gif"
                                        ToolTip="Print" CommandArgument='<%# bind("strscno") %>' Height="20px" Width="20px"
                                        CausesValidation="False" /><%--OnClientClick="return confirmprintchq();"--%>
                                </ItemTemplate>
                                <ItemStyle Width="20px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
           <%-- </div>
        </div>--%>
    </div>
</asp:Content>
