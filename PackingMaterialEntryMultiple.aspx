﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PackingMaterialEntryMultiple.aspx.cs" Inherits="PackingMaterialEntryMultiple"
    Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Packing Material Entry Multiple(Challan)</span><br />
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Voucher No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtvoucherno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtvdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-2">
                        <label class="control-label">
                            User</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" onkeypress="return checkQuote();"
                            ReadOnly="true"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Name<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="393px" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getacname">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Client Code<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" Width="150px"
                            AutoPostBack="true" OnTextChanged="txtname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtclientcode"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetClientname">
                        </asp:AutoCompleteExtender>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Returnable Material Challan No.<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtreturnablematerialchallanno" runat="server" CssClass="form-control"
                            Width="150px" onkeypress="return checkQuote();" placeholder="Alt+r"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtreturnablematerialchallanno"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getrrcno">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Vehicle No.</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtvehicleno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Tools Delivery Challan No.<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txttoolsdeliverychallanno" runat="server" CssClass="form-control"
                            Width="150px" onkeypress="return checkQuote();" placeholder="Alt+t"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txttoolsdeliverychallanno"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getdelcno">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Driver Name</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtdrivername" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Transport Name<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="drptransportname" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                        <%--<asp:TextBox ID="txttransportname" runat="server" CssClass="form-control" Width="393px"
                            onkeypress="return checkQuote();"></asp:TextBox>--%></div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Driver's License No.</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtdriverlicenseno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            LR No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtlrno" runat="server" CssClass="form-control" Width="150px" onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtlrdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Driver Mobile No.</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtdrivermobileno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row" style="height: 10px">
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Material Loading By</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtmateriall" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Material Received By</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtmaterialr" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row" style="height: 10px">
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Delivery Challan No.<span style="color: Red;">*</span></label></div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtdcno" runat="server" CssClass="form-control" Width="150px" onkeypress="return checkQuote();"
                            placeholder="Alt+s"></asp:TextBox></div>
                    <div class="col-md-3">
                        <asp:Button ID="btnadd" runat="server" Text="Add" class="btn btn-default forbutton"
                            OnClick="btnadd_Click" ValidationGroup="valgvtool" /></div>
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                        ShowSummary="false" ValidationGroup="valgvtool" />
                </div>
                <div class="row" style="height: 10px;">
                </div>
                <div class="row table-responsive">
                    <div class="col-md-12">
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                            <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                            <asp:GridView ID="rptlist" runat="server" AutoGenerateColumns="False" Width="100%"
                                BorderStyle="Ridge" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered table-responsive"
                                OnRowDeleting="rptlist_RowDeleting" BackColor="White" BorderColor="White" BorderWidth="2px"
                                CellPadding="3" GridLines="None" CellSpacing="1">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                                ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delivery Challan No." SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldcno" runat="server" ForeColor="#505050" Text='<%# bind("dcno") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                                ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                                CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                <PagerStyle ForeColor="Black" HorizontalAlign="Right" BackColor="#C6C3C6" />
                                <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="#DEDFDE" ForeColor="Black" />
                                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" CssClass="GridViewItemHeader" />
                                <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#33276A" />
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">
                            Remarks</label></div>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control" Height="100px"
                            onkeypress="return checkQuote();" placeholder="Remarks"></asp:TextBox></div>
                </div>
                <asp:Button ID="btnsaveall" runat="server" Text="Save" Height="30px" BackColor="#F05283"
                    ValidationGroup="valtool" OnClick="btnsaveall_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="valtool" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter date."
                    Text="*" ValidationGroup="valtool" ControlToValidate="txtvdate"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="Voucher Date must be dd-MM-yyyy" ValidationGroup="valtool" ForeColor="Red"
                    ControlToValidate="txtvdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                    ErrorMessage="Lr Date must be dd-MM-yyyy" ValidationGroup="valtool" ForeColor="Red"
                    ControlToValidate="txtlrdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter name."
                    Text="*" ValidationGroup="valtool" ControlToValidate="txtname"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter client code."
                    Text="*" ValidationGroup="valtool" ControlToValidate="txtclientcode"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter delivery challan no."
                    Text="*" ValidationGroup="valgvtool" ControlToValidate="txtdcno"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please select transport name."
                    Text="*" ValidationGroup="valtool" ControlToValidate="drptransportname" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter returnable material challan no.If you do not have any returnable material challan No than please enter 0(Zero)."
                    Text="*" ValidationGroup="valtool" ControlToValidate="txtreturnablematerialchallanno"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please enter tools delivery challan no.If you do not have any tools delivery challan No than please enter 0(Zero)."
                    Text="*" ValidationGroup="valtool" ControlToValidate="txttoolsdeliverychallanno"></asp:RequiredFieldValidator>
                <div class="row">
                    <asp:LinkButton ID="lnkselectionpopup" runat="server" AccessKey="s" OnClick="lnkselectionpopup_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
                    <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
                        TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="500px" align="center"
                        Width="95%" Style="display: none">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span style="color: Red;"><b>Sales Challan Selection</b></span></div>
                        </div>
                        <div class="row">
                            <hr />
                        </div>
                        <div class="row">
                            <div class="col-md-1 text-left">
                                <b>A/C Name :</b></div>
                            <div class="col-md-10">
                                <asp:Label ID="lblpopupacname" runat="server" Font-Bold="true"></asp:Label></div>
                        </div>
                        <div class="row">
                            <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                                <asp:Label ID="lblemptysc" runat="server" ForeColor="Red"></asp:Label>
                                <asp:GridView ID="gvsaleschallans" runat="server" AutoGenerateColumns="False" Width="100%"
                                    BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                                    CssClass="table table-bordered" OnRowCommand="gvsaleschallans_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                                    ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("strscno") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sales Chlln No." SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstrscno" ForeColor="Black" runat="server" Text='<%# bind("strscno") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sales Chlln No." SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblscno" ForeColor="Black" runat="server" Text='<%# bind("scno") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                            <ItemTemplate>
                                                <asp:Label ID="lblscdate" runat="server" ForeColor="#505050" Text='<%# bind("scdate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CCode" SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus" ForeColor="Black" runat="server" Text='<%# bind("status") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#4c4c4c" />
                                    <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                                    <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                                </asp:GridView>
                            </asp:Panel>
                            <asp:Button ID="btnClose" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
                        </div>
                    </asp:Panel>
                </div>
                <div class="row">
                    <asp:LinkButton ID="lnkselectionpopupr" runat="server" AccessKey="r" OnClick="lnkselectionpopupr_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lnkopenpopupr" runat="server"></asp:LinkButton>
                    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel4"
                        TargetControlID="lnkopenpopupr" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="Panel4" runat="server" CssClass="modalPopup" Height="500px" align="center"
                        Width="95%" Style="display: none">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span style="color: Red;"><b>Returnable Material Challan Selection</b></span></div>
                        </div>
                        <div class="row">
                            <hr />
                        </div>
                        <div class="row">
                            <div class="col-md-1 text-left">
                                <b>A/C Name :</b></div>
                            <div class="col-md-10">
                                <asp:Label ID="lblpopupacnamer" runat="server" Font-Bold="true"></asp:Label></div>
                        </div>
                        <div class="row">
                            <asp:Panel ID="Panel5" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                                <asp:Label ID="lblemptyscr" runat="server" ForeColor="Red"></asp:Label>
                                <asp:GridView ID="gvsaleschallansr" runat="server" AutoGenerateColumns="False" Width="100%"
                                    BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                                    CssClass="table table-bordered" OnRowCommand="gvsaleschallansr_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                                    ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("strscno") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sales Chlln No." SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstrscno" ForeColor="Black" runat="server" Text='<%# bind("strscno") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sales Chlln No." SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblscno" ForeColor="Black" runat="server" Text='<%# bind("scno") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                            <ItemTemplate>
                                                <asp:Label ID="lblscdate" runat="server" ForeColor="#505050" Text='<%# bind("scdate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CCode" SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus" ForeColor="Black" runat="server" Text='<%# bind("status") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#4c4c4c" />
                                    <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                                    <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                                </asp:GridView>
                            </asp:Panel>
                            <asp:Button ID="btnCloser" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
                        </div>
                    </asp:Panel>
                </div>
                <div class="row">
                    <asp:LinkButton ID="lnkselectionpopupt" runat="server" AccessKey="t" OnClick="lnkselectionpopupt_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lnkopenpopupt" runat="server"></asp:LinkButton>
                    <asp:ModalPopupExtender ID="ModalPopupExtender3" runat="server" PopupControlID="Panel6"
                        TargetControlID="lnkopenpopupt" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="Panel6" runat="server" CssClass="modalPopup" Height="500px" align="center"
                        Width="95%" Style="display: none">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span style="color: Red;"><b>Tools Delivery Challan Selection</b></span></div>
                        </div>
                        <div class="row">
                            <hr />
                        </div>
                        <div class="row">
                            <div class="col-md-1 text-left">
                                <b>A/C Name :</b></div>
                            <div class="col-md-10">
                                <asp:Label ID="lblpopupacnamet" runat="server" Font-Bold="true"></asp:Label></div>
                        </div>
                        <div class="row">
                            <asp:Panel ID="Panel7" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                                <asp:Label ID="lblemptysct" runat="server" ForeColor="Red"></asp:Label>
                                <asp:GridView ID="gvsaleschallanst" runat="server" AutoGenerateColumns="False" Width="100%"
                                    BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                                    CssClass="table table-bordered" OnRowCommand="gvsaleschallanst_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                                    ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("voucherno") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Voucher No." SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblvoucherno" ForeColor="Black" runat="server" Text='<%# bind("voucherno") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                            <ItemTemplate>
                                                <asp:Label ID="lblvoucherdate" runat="server" ForeColor="#505050" Text='<%# bind("voucherdate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ACName" SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblname" ForeColor="Black" runat="server" Text='<%# bind("name") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CCode" SortExpression="Csnm">
                                            <ItemTemplate>
                                                <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("clientcode") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#4c4c4c" />
                                    <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                                    <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                                </asp:GridView>
                            </asp:Panel>
                            <asp:Button ID="Button1" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtlrdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtvdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtvdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });
            $("#<%= txtlrdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtlrdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
