﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class UserRights : System.Web.UI.Page
{
    SqlCommand cmd;
    SqlDataAdapter da;
    DataTable dtdata;
    DataTable dt;
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            filluserrightdrop();
           // selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void filluserrightdrop()
    {
        DataTable dtrole = new DataTable();
        dtrole = furclass.selectallrole();
        if (dtrole.Rows.Count > 0)
        {
            drprole.DataSource = dtrole;
            drprole.DataTextField = "name";
            drprole.DataBind();
            drprole.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drprole.Items.Clear();
            drprole.Items.Insert(0, "--SELECT--");
        }
    }

    protected void chkalladd_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkalladd = (CheckBox)gvRightsDetails.HeaderRow.FindControl("chkalladd");
        if (chkalladd.Checked == true)
        {
            for (int i = 0; i < gvRightsDetails.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkadd");
                chk.Checked = true;
            }
        }
        else
        {
            for (int i = 0; i < gvRightsDetails.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkadd");
                chk.Checked = false;
            }
        }
    }

    protected void chkallupdate_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkalladd = (CheckBox)gvRightsDetails.HeaderRow.FindControl("chkallupdate");
        if (chkalladd.Checked == true)
        {
            for (int i = 0; i < gvRightsDetails.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkupdate");
                chk.Checked = true;
            }
        }
        else
        {
            for (int i = 0; i < gvRightsDetails.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkupdate");
                chk.Checked = false;
            }
        }
    }

    protected void chkalldelete_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkalladd = (CheckBox)gvRightsDetails.HeaderRow.FindControl("chkalldelete");
        if (chkalladd.Checked == true)
        {
            for (int i = 0; i < gvRightsDetails.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkdelete");
                chk.Checked = true;
            }
        }
        else
        {
            for (int i = 0; i < gvRightsDetails.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkdelete");
                chk.Checked = false;
            }
        }
    }

    protected void chkallview_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkalladd = (CheckBox)gvRightsDetails.HeaderRow.FindControl("chkallview");
        if (chkalladd.Checked == true)
        {
            for (int i = 0; i < gvRightsDetails.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkview");
                chk.Checked = true;
            }
        }
        else
        {
            for (int i = 0; i < gvRightsDetails.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkview");
                chk.Checked = false;
            }
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            //if (ViewState["uadd"].ToString() == "Y")
            //{
            ContentPlaceHolder cph = (ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1");
            GridView gv = (GridView)cph.FindControl("gvRightsDetails");

            for (int j = 0; j < gvRightsDetails.Rows.Count; j++)
            {
                Label lblpage = (Label)gvRightsDetails.Rows[j].FindControl("lblid1");
                Label lblpagename = (Label)gvRightsDetails.Rows[j].FindControl("lblpagename");
                CheckBox chkadd = (CheckBox)gvRightsDetails.Rows[j].FindControl("chkadd");
                CheckBox chkupdate = (CheckBox)gvRightsDetails.Rows[j].FindControl("chkupdate");
                CheckBox chkdelete = (CheckBox)gvRightsDetails.Rows[j].FindControl("chkdelete");
                CheckBox chkview = (CheckBox)gvRightsDetails.Rows[j].FindControl("chkview");
                CheckBox chkprint = (CheckBox)gvRightsDetails.Rows[j].FindControl("chkprint");
                //li.XUSERID = txtuserid.Text;                    
                li.pagename = lblpage.Text;
                li.pagename1 = lblpagename.Text;
                li.Role = drprole.SelectedValue;
                //li.Rusername = txtusername.Text;
                //li.PAGETYPE = lblpagetype.Text;
                if (chkadd.Checked == true)
                {
                    li.UADD = "Y";
                }
                else
                {
                    li.UADD = "N";
                }
                if (chkupdate.Checked == true)
                {
                    li.UEDIT = "Y";
                }
                else
                {
                    li.UEDIT = "N";
                }
                if (chkdelete.Checked == true)
                {
                    li.UDELETE = "Y";
                }
                else
                {
                    li.UDELETE = "N";
                }
                if (chkview.Checked == true)
                {
                    li.UVIEW = "Y";
                }
                else
                {
                    li.UVIEW = "N";
                }
                //if (chkprint.Checked == true)
                //{
                li.UPRINT = "Y";
                //}
                //else
                //{
                //    li.UPRINT = "N";
                //}
                furclass.insertrightsdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = "User Rights Inserted for " + drprole.SelectedValue;
                faclass.insertactivity(li);
                btnsave.Text = "Update";

            }
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Rights Inserted Successfully For " + drprole.SelectedValue + "');", true);
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('OOPS!!! No Rights For Add Data');", true);
            //}
        }
        else
        {
            //if (ViewState["uedit"].ToString() == "Y")
            //{
            ContentPlaceHolder cph = (ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1");
            GridView gv = (GridView)cph.FindControl("gvRightsDetails");

            for (int j = 0; j < gvRightsDetails.Rows.Count; j++)
            {
                Label lblid = (Label)gvRightsDetails.Rows[j].FindControl("lblid2");
                Label lblpage = (Label)gvRightsDetails.Rows[j].FindControl("lblid1");
                Label lblpagename = (Label)gvRightsDetails.Rows[j].FindControl("lblpagename");
                CheckBox chkadd = (CheckBox)gvRightsDetails.Rows[j].FindControl("chkadd");
                CheckBox chkupdate = (CheckBox)gvRightsDetails.Rows[j].FindControl("chkupdate");
                CheckBox chkdelete = (CheckBox)gvRightsDetails.Rows[j].FindControl("chkdelete");
                CheckBox chkview = (CheckBox)gvRightsDetails.Rows[j].FindControl("chkview");
                CheckBox chkprint = (CheckBox)gvRightsDetails.Rows[j].FindControl("chkprint");

                li.pagename = lblpage.Text;
                li.pagename1 = lblpagename.Text;
                li.id = Convert.ToInt64(lblid.Text);
                li.Role = drprole.SelectedValue;
                if (chkadd.Checked == true)
                {
                    li.UADD = "Y";
                }
                else
                {
                    li.UADD = "N";
                }
                if (chkupdate.Checked == true)
                {
                    li.UEDIT = "Y";
                }
                else
                {
                    li.UEDIT = "N";
                }
                if (chkdelete.Checked == true)
                {
                    li.UDELETE = "Y";
                }
                else
                {
                    li.UDELETE = "N";
                }
                if (chkview.Checked == true)
                {
                    li.UVIEW = "Y";
                }
                else
                {
                    li.UVIEW = "N";
                }
                //if (chkprint.Checked == true)
                //{
                li.UPRINT = "Y";
                //}
                //else
                //{
                //    li.UPRINT = "N";
                //}
                furclass.updaterightsdata(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = "User Rights Updated for " + drprole.SelectedValue;
                faclass.insertactivity(li);
                btnsave.Text = "Update";

            }
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Rights Updated Successfully For " + drprole.SelectedValue + "');", true);
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('OOPS!!! No Rights For Update Data');", true);
            //}
        }

    }

    protected void drprole_SelectedIndexChanged(object sender, EventArgs e)
    {

        //TabContainer2.Visible = true;
        if (drprole.SelectedItem.Text == "--SELECT--")
        {
            gvRightsDetails.Visible = false;
            btnsave.Visible = false;
        }
        else
        {
            fillrightsgrid();
            btnsave.Visible = true;
            gvRightsDetails.Visible = true;
        }
    }

    public void fillrightsgrid()
    {
        li.role = drprole.SelectedValue;
        DataTable dtdata = new DataTable();
        dtdata = furclass.getuserRole(li);

        if (dtdata.Rows.Count > 0)
        {
            gvRightsDetails.DataSource = dtdata;
            gvRightsDetails.DataBind();
            ContentPlaceHolder cph = (ContentPlaceHolder)this.Master.FindControl("ContentPlaceHolder1");


            for (int i = 0; i < dtdata.Rows.Count; i++)
            {

                CheckBox chkadd = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkadd");
                CheckBox chkupdate = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkupdate");
                CheckBox chkdelete = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkdelete");
                CheckBox chkview = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkview");
                CheckBox chkprint = (CheckBox)gvRightsDetails.Rows[i].FindControl("chkprint");
                if (dtdata.Rows[i]["UADD"].ToString() == "Y")
                {
                    chkadd.Checked = true;
                }
                else
                {
                    chkadd.Checked = false;
                }
                if (dtdata.Rows[i]["UEDIT"].ToString() == "Y")
                {
                    chkupdate.Checked = true;
                }
                else
                {
                    chkupdate.Checked = false;
                }
                if (dtdata.Rows[i]["UDELETE"].ToString() == "Y")
                {
                    chkdelete.Checked = true;
                }
                else
                {
                    chkdelete.Checked = false;
                }
                if (dtdata.Rows[i]["UVIEW"].ToString() == "Y")
                {
                    chkview.Checked = true;
                }
                else
                {
                    chkview.Checked = false;
                }
                //if (dtdata.Rows[i]["UPRINT"].ToString() == "Y")
                //{
                //    chkprint.Checked = true;
                //}
                //else
                //{
                //    chkprint.Checked = false;
                //}
            }
            btnsave.Text = "Update";
        }
        else
        {

            DataTable dt = new DataTable();
            dt = furclass.getuserRoleNull();
            if (dt.Rows.Count > 0)
            {
                gvRightsDetails.DataSource = dt;
                gvRightsDetails.DataBind();
            }
            btnsave.Text = "Save";
        }

    }

}