﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;

public partial class StockTools : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillstockgrid();
        }
    }

    public void fillstockgrid()
    {
        DataTable dtallstock = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select distinct(name) from toolsmaster order by name", con);
        DataTable dtitem = new DataTable();
        da.Fill(dtitem);
        for (int c = 0; c < dtitem.Rows.Count; c++)
        {
            li.name = dtitem.Rows[c]["name"].ToString();
            SqlDataAdapter dac = new SqlDataAdapter("select name as toolname,(select isnull(sum(opstock),0) from toolsmaster where name=@itemname) as opqty,((select isnull(SUM(qty),0) from ToolDelItem where toolname=@itemname)+(select isnull(SUM(qty),0) from ToolsJVItems where itemname=@itemname and inout='OUT')) as tots,((select isnull(SUM(rfs),0) from ToolDelItem where toolname=@itemname)+(select isnull(SUM(qty),0) from ToolsJVItems where itemname=@itemname and inout='IN')) as totp,(((select isnull(sum(opstock),0) from ToolsMaster where name=@itemname)+(select isnull(SUM(rfs),0) from ToolDelItem where toolname=@itemname)+(select isnull(SUM(qty),0) from ToolsJVItems where itemname=@itemname and inout='IN'))-((select isnull(SUM(qty),0) from ToolDelItem where toolname=@itemname)+(select isnull(SUM(qty),0) from ToolsJVItems where itemname=@itemname and inout='OUT'))) as totalqty from ToolsMaster where name=@itemname", con);
            dac.SelectCommand.Parameters.AddWithValue("@itemname", li.name);
            DataTable dtstock = new DataTable();
            dac.Fill(dtstock);
            dtallstock.Merge(dtstock);
        }
        if (dtallstock.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvbankpaymentlist.Visible = true;
            gvbankpaymentlist.DataSource = dtallstock;
            gvbankpaymentlist.DataBind();
        }
        else
        {
            gvbankpaymentlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Stock Data Found.";
        }

    }

    protected void btnstockprint_Click(object sender, EventArgs e)
    {
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        gvbankpaymentlist.RenderControl(hw);
        string gridHTML = sw.ToString().Replace("\"", "'").Replace(System.Environment.NewLine, "");
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type = 'text/javascript'>");
        sb.Append("window.onload = new function(){");
        sb.Append("var printWin = window.open('', '', 'left=0");
        sb.Append(",top=0,width=1000,height=600,status=0');");
        sb.Append("printWin.document.write(\"");
        sb.Append(gridHTML);
        sb.Append("\");");
        sb.Append("printWin.document.close();");
        sb.Append("printWin.focus();");
        sb.Append("printWin.print();");
        sb.Append("printWin.close();};");
        sb.Append("</script>");
        ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

}