﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RepPurchaseOrder.aspx.cs" Inherits="RepPurchaseOrder" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Purchase Order Report</span><br />
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    Account Name</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtacname" runat="server" CssClass="form-control" 
                    Width="200px" AutoPostBack="True" ontextchanged="txtacname_TextChanged"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtacname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetAccountname">
                </asp:AutoCompleteExtender>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                    PO NO.</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtpono" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtpono"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getpono">
                </asp:AutoCompleteExtender>
                </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label">
                </label>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnsaves" runat="server" Text="Preview" class="btn btn-default forbutton"
                    ValidationGroup="val" onclick="btnsaves_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter account name."
                    Text="*" ValidationGroup="val" ControlToValidate="txtacname"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter po no."
                    Text="*" ValidationGroup="val" ControlToValidate="txtpono"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
</asp:Content>
