﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.SqlTypes;

public partial class Quotation : System.Web.UI.Page
{
    ForSalesOrder fsoclass = new ForSalesOrder();
    ForQuotationMaster fqmclass = new ForQuotationMaster();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
            fillacdrop();
            if (Request["mode"].ToString() == "direct")
            {
                txtsodate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemsbifu"] = CreateTemplate();
                fillgrid();
                getquono();
                fillitemnamedrop();
                fillstatusdrop();
                countgv();
                drpdescription3.Items.Insert(0, "--SELECT--");
                txtuser.Text = Request.Cookies["ForLogin"]["username"];
            }
            else if (Request["mode"].ToString() == "insert")
            {
                chkigst.Enabled = false;
                txtsodate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemsbifu"] = CreateTemplate();
                //fillgrid();
                getquono();
                fillitemnamedrop();
                fillstatusdrop();
                //countgv();
                drpdescription3.Items.Insert(0, "--SELECT--");
                txtuser.Text = Request.Cookies["ForLogin"]["username"];
            }
            else if (Request["mode"].ToString() == "update")
            {
                // chkigst.Enabled = false;
                fillitemnamedrop();
                fillstatusdrop();
                filleditdata();
                drpdescription3.Items.Insert(0, "--SELECT--");
            }
            else if (Request["mode"].ToString() == "revised")
            {
                txtsodate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                drpdescription3.Items.Insert(0, "--SELECT--");
                fillitemnamedrop();
                fillstatusdrop();
                //filleditdata();
                Session["dtpitemsbifu"] = CreateTemplate();
                DataTable dt = (DataTable)Session["dtpitemsbifu"];
                li.quono = Convert.ToDouble(Request["quono"].ToString());
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtdata = new DataTable();
                dtdata = fqmclass.selectquodatafromquono(li);
                if (dtdata.Rows.Count > 0)
                {
                    txtquotationno.Text = dtdata.Rows[0]["quono"].ToString();
                    txtsodate.Text = (Convert.ToDateTime(dtdata.Rows[0]["quodate"].ToString())).ToString("dd-MM-yyyy");
                    txtclientcode.Text = dtdata.Rows[0]["ccode"].ToString();
                    drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
                    txtbasicamount.Text = dtdata.Rows[0]["basicamount"].ToString();
                    txtvat.Text = dtdata.Rows[0]["vat"].ToString();
                    txtadvat.Text = dtdata.Rows[0]["advat"].ToString();
                    txtcst.Text = dtdata.Rows[0]["cst"].ToString();
                    txttotalso.Text = dtdata.Rows[0]["amount"].ToString();
                    drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
                    txtannexure.Text = dtdata.Rows[0]["annexure"].ToString();
                    txtquotationno.Text = (Convert.ToDouble(Request["quono"].ToString()) + 0.1).ToString();
                    if (dtdata.Rows[0]["isigst"].ToString() == "Y")
                    {
                        chkigst.Checked = true;
                    }
                    dt.Clear();
                    DataTable dtitems = new DataTable();
                    dtitems = fqmclass.selectquoitemsdatafromquono(li);
                    for (int d = 0; d < dtitems.Rows.Count; d++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["id"] = dtitems.Rows[d]["id"].ToString();
                        dr["srno"] = dtitems.Rows[d]["srno"].ToString();
                        dr["itemname"] = dtitems.Rows[d]["itemname"].ToString();
                        dr["qty"] = dtitems.Rows[d]["qty"].ToString();
                        dr["unit"] = dtitems.Rows[d]["unit"].ToString();
                        dr["rate"] = dtitems.Rows[d]["rate"].ToString();

                        dr["basicamt"] = Convert.ToDouble(dtitems.Rows[d]["basicamt"].ToString());
                        dr["cstp"] = Convert.ToDouble(dtitems.Rows[d]["cstp"].ToString());
                        dr["taxtype"] = dtitems.Rows[d]["taxtype"].ToString();
                        dr["vatp"] = Convert.ToDouble(dtitems.Rows[d]["vatp"].ToString());
                        dr["vatamt"] = Convert.ToDouble(dtitems.Rows[d]["vatamt"].ToString());
                        dr["addtaxp"] = Convert.ToDouble(dtitems.Rows[d]["addtaxp"].ToString());
                        dr["addtaxamt"] = Convert.ToDouble(dtitems.Rows[d]["addtaxamt"].ToString());
                        dr["cstamt"] = Convert.ToDouble(dtitems.Rows[d]["cstamt"].ToString());

                        dr["amount"] = Convert.ToDouble(dtitems.Rows[d]["amount"].ToString());
                        dr["descr1"] = dtitems.Rows[d]["descr1"].ToString();
                        dr["descr2"] = dtitems.Rows[d]["descr2"].ToString();
                        dr["descr3"] = dtitems.Rows[d]["descr3"].ToString();
                        dr["make"] = dtitems.Rows[d]["make"].ToString();
                        dt.Rows.Add(dr);
                        //count1();
                    }
                    Session["dtpitemsbifu"] = dt;
                    txtuser.Text = Request.Cookies["ForLogin"]["username"];
                }
                hideimage();
                txtuser.Text = Request.Cookies["ForLogin"]["username"];
                if (dt.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvslist.Visible = true;
                    gvslist.DataSource = dt;
                    gvslist.DataBind();
                    lblcount.Text = dt.Rows.Count.ToString();
                }
                else
                {
                    gvslist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
            hideimage();
        }
        Page.SetFocus(txtsodate);
    }

    public void hideimage()
    {
        for (int c = 0; c < gvslist.Rows.Count; c++)
        {
            ImageButton img = (ImageButton)gvslist.Rows[c].FindControl("imgbtnselect11");
            if (btnfinalsave.Text == "Update")
            {
                img.Visible = true;
            }
            else
            {
                img.Visible = false;
            }
        }
    }

    public void filleditdata()
    {
        li.quono = Convert.ToDouble(Request["quono"].ToString());
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fqmclass.selectquodatafromquono(li);
        if (dtdata.Rows.Count > 0)
        {
            txtquotationno.Text = dtdata.Rows[0]["quono"].ToString();
            txtsodate.Text = (Convert.ToDateTime(dtdata.Rows[0]["quodate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat)).ToString("dd-MM-yyyy");
            txtclientcode.Text = dtdata.Rows[0]["ccode"].ToString();
            drpacname.SelectedValue = dtdata.Rows[0]["acname"].ToString();
            txtbasicamount.Text = dtdata.Rows[0]["basicamount"].ToString();
            txtvat.Text = dtdata.Rows[0]["vat"].ToString();
            txtadvat.Text = dtdata.Rows[0]["advat"].ToString();
            txtcst.Text = dtdata.Rows[0]["cst"].ToString();
            txttotalso.Text = dtdata.Rows[0]["amount"].ToString();
            drpstatus.SelectedValue = dtdata.Rows[0]["status"].ToString();
            txtannexure.Text = dtdata.Rows[0]["annexure"].ToString();
            txtuser.Text = dtdata.Rows[0]["uname"].ToString();
            if (dtdata.Rows[0]["isigst"].ToString() == "Y")
            {
                chkigst.Checked = true;
            }
            //txtquotationno.Text = (Convert.ToDouble(Request["quono"].ToString()) + 0.1).ToString();
            DataTable dtitems = new DataTable();
            dtitems = fqmclass.selectquoitemsdatafromquono(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvslist.Visible = true;
                gvslist.DataSource = dtitems;
                gvslist.DataBind();
                lblcount.Text = dtitems.Rows.Count.ToString();
            }
            else
            {
                gvslist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Items Found.";
            }
            btnfinalsave.Text = "Update";
        }
    }

    public void fillstatusdrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsoclass.selectallstatus(li);
        if (dtdata.Rows.Count > 0)
        {
            drpstatus.Items.Clear();
            drpstatus.DataSource = dtdata;
            drpstatus.DataTextField = "name";
            drpstatus.DataBind();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpstatus.Items.Clear();
            drpstatus.Items.Insert(0, "--SELECT--");
        }
    }

    public void getquono()
    {
        DataTable dtdata = new DataTable();
        dtdata = fqmclass.selectlastquono();
        if (dtdata.Rows.Count > 0)
        {
            txtquotationno.Text = (Convert.ToDouble(dtdata.Rows[0]["quono1"].ToString()) + 1).ToString();
        }
        else
        {
            txtquotationno.Text = "1";
        }
    }

    public void fillitemnamedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fsoclass.selectallitemname(li);
        if (dtdata.Rows.Count > 0)
        {
            drpitemname.Items.Clear();
            drpitemname.DataSource = dtdata;
            drpitemname.DataTextField = "itemname";
            drpitemname.DataBind();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpitemname.Items.Clear();
            drpitemname.Items.Insert(0, "--SELECT--");
        }
    }

    public void fillgrid()
    {

        li.remarks = SessionMgt.emailid.TrimEnd(',');
        DataTable dtdata = new DataTable();
        dtdata = fqmclass.selectallitemdata(li);

        DataTable dt = (DataTable)Session["dtpitemsbifu"];
        dt.Clear();
        for (int d = 0; d < dtdata.Rows.Count; d++)
        {
            DataRow dr = dt.NewRow();
            dr["id"] = dtdata.Rows[d]["id"].ToString();
            dr["srno"] = "";
            dr["itemname"] = dtdata.Rows[d]["itemname"].ToString();
            dr["qty"] = 1;
            dr["unit"] = dtdata.Rows[d]["unit"].ToString();
            dr["hsncode"] = dtdata.Rows[d]["hsncode"].ToString();
            dr["rate"] = dtdata.Rows[d]["salesrate"].ToString();

            dr["basicamt"] = Convert.ToDouble(dtdata.Rows[d]["salesrate"].ToString());
            if (chkigst.Checked == true)
            {
                if (dtdata.Rows[0]["gsttype"].ToString().IndexOf("-") != -1)
                {
                    dr["cstp"] = (Math.Round(Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[0]) + Convert.ToDouble(dtdata.Rows[d]["gsttype"].ToString().Split('-')[1]), 2)).ToString();
                    dr["taxtype"] = 0;
                }
                else
                {
                    dr["taxtype"] = "";
                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                }
            }
            else
            {
                if (dtdata.Rows[0]["gsttype"].ToString().IndexOf("-") != -1)
                {
                    dr["taxtype"] = dtdata.Rows[d]["gsttype"].ToString();
                    dr["cstp"] = 0;
                }
                else
                {
                    dr["taxtype"] = "";
                    dr["cstp"] = dtdata.Rows[d]["gsttype"].ToString();
                }

            }
            dr["vatp"] = 0;
            dr["vatamt"] = 0;
            dr["addtaxp"] = 0;
            dr["addtaxamt"] = 0;
            dr["cstamt"] = 0;

            dr["amount"] = dtdata.Rows[d]["amount"].ToString();
            dr["descr1"] = "";
            dr["descr2"] = "";
            dr["descr3"] = "";
            dr["make"] = "";
            dt.Rows.Add(dr);
            //count1();
        }
        Session["dtpitemsbifu"] = dt;

        if (dt.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvslist.Visible = true;
            gvslist.DataSource = dt;
            gvslist.DataBind();
            lblcount.Text = dt.Rows.Count.ToString();
        }
        else
        {
            lblempty.Visible = true;
            gvslist.Visible = false;
            lblempty.Text = "No Item Found.";
        }
        countgv();
    }

    public void fillacdrop()
    {
        DataTable dtac = new DataTable();
        dtac = fqmclass.selectallacnamedata();
        if (dtac.Rows.Count > 0)
        {
            drpacname.DataSource = dtac;
            drpacname.DataTextField = "acname";
            drpacname.DataBind();
            drpacname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpacname.Items.Clear();
            drpacname.Items.Insert(0, "--SELECT--");
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtclientcode.Text = string.Empty;
        }
        Page.SetFocus(drpacname);
    }

    protected void gvslist_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            con.Open();
            var ddl = (DropDownList)e.Row.FindControl("drpdescr3");
            var lbl = (Label)e.Row.FindControl("lblitemname");
            var txtdescr3 = (TextBox)e.Row.FindControl("txtdescr3");
            SqlCommand cmd = new SqlCommand("select * from MiscMaster where type='" + lbl.Text + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            con.Close();
            ddl.DataSource = ds;
            ddl.DataTextField = "name";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("--SELECT--", "0"));
            //if (Request["mode"].ToString() == "update" || Request["mode"].ToString() == "revised" || Request["mode"].ToString() == "update")
            //{
            if (txtdescr3.Text != "" && txtdescr3.Text != string.Empty)
            {
                ddl.SelectedValue = txtdescr3.Text;
            }
            //}
            //ddl.SelectedValue = lbl.Text;
            //var gv = (GridView)e.Row.FindControl("gvproductlist");
            //var inqno = (Label)e.Row.FindControl("lblinquiryid");
            //Int64 inquiryno = Convert.ToInt64(inqno.Text);
            //li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
            //SqlDataAdapter da1 = new SqlDataAdapter("select * from inquiryproduct where inquiryno=" + inquiryno + " and cno=" + li.CNO + "", con);
            //DataSet ds1 = new DataSet();
            //da1.Fill(ds1);
            ////if (dtdata.Rows.Count > 0)
            ////{
            //gv.DataSource = ds1;
            //gv.DataBind();
        }
    }
    protected void txtqty_TextChanged(object sender, EventArgs e)
    {
        double qty = 0;
        double rate = 0;
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
        TextBox txtqty = (TextBox)currentRow.FindControl("txtqty");
        TextBox txtrate = (TextBox)currentRow.FindControl("txtrate");
        TextBox txtamount = (TextBox)currentRow.FindControl("txtamount");
        if (txtqty.Text.Trim() != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text.Trim() != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtamount.Text = Math.Round((qty * rate), 2).ToString();
        //counttotal();
        //countgv();

        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgvtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatp = (TextBox)currentRow.FindControl("txttax1");
        TextBox lbladdvatp = (TextBox)currentRow.FindControl("txttax2");
        TextBox lblcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatamt = (TextBox)currentRow.FindControl("txttax1amt");
        TextBox lbladdvatamt = (TextBox)currentRow.FindControl("txttax2amt");
        TextBox lblcstamt = (TextBox)currentRow.FindControl("txttax3amt");
        TextBox lblamount = (TextBox)currentRow.FindControl("txtamount");
        TextBox lblgvamount = (TextBox)currentRow.FindControl("txtgvamount");
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtamount.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }
                else
                {
                    txtgridcstp.Text = "0";
                }
            }
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (lblcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(lblcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        countgv();
        Page.SetFocus(txtrate);
    }

    protected void txtrate_TextChanged(object sender, EventArgs e)
    {
        double qty = 0;
        double rate = 0;
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
        TextBox txtqty = (TextBox)currentRow.FindControl("txtqty");
        TextBox txtrate = (TextBox)currentRow.FindControl("txtrate");
        TextBox txtamount = (TextBox)currentRow.FindControl("txtamount");
        if (txtqty.Text != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtamount.Text = Math.Round((qty * rate), 2).ToString();
        //counttotal();
        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgvtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatp = (TextBox)currentRow.FindControl("txttax1");
        TextBox lbladdvatp = (TextBox)currentRow.FindControl("txttax2");
        TextBox lblcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatamt = (TextBox)currentRow.FindControl("txttax1amt");
        TextBox lbladdvatamt = (TextBox)currentRow.FindControl("txttax2amt");
        TextBox lblcstamt = (TextBox)currentRow.FindControl("txttax3amt");
        TextBox lblamount = (TextBox)currentRow.FindControl("txtamount");
        TextBox lblgvamount = (TextBox)currentRow.FindControl("txtgvamount");
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtamount.Text);
        if (lbltaxtype.Text != string.Empty && lbltaxtype.Text != "0")
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }
                else
                {
                    txtgridcstp.Text = "0";
                }
            }
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (lblcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(lblcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        countgv();
        Page.SetFocus(txtamount);
    }

    public void counttotal()
    {
        double tot = 0;
        for (int c = 0; c < gvslist.Rows.Count; c++)
        {
            TextBox txtamount1 = (TextBox)gvslist.Rows[c].FindControl("txtamount");
            if (txtamount1.Text.Trim() != string.Empty)
            {
                tot = tot + Convert.ToDouble(txtamount1.Text.Trim());
            }
        }
        txtbasicamount.Text = tot.ToString();
    }

    public void countgv()
    {

        DataTable dtsoitem = (DataTable)Session["dtpitemsbifu"];
        //DataTable dtsoitem1 = (DataTable)Session["dtpitems12"];
        //if (dtsoitem1 != null)
        //{
        //    dtsoitem.Rows.Clear();
        //    dtsoitem.Merge(dtsoitem1);
        //}
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double vatp = 0;
        double qty = 0;
        double rate = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        double amount = 0;
        for (int c = 0; c < gvslist.Rows.Count; c++)
        {
            TextBox txtgvqty1 = (TextBox)gvslist.Rows[c].FindControl("txtqty");
            TextBox txtgvrate1 = (TextBox)gvslist.Rows[c].FindControl("txtrate");
            TextBox txtgvamount1 = (TextBox)gvslist.Rows[c].FindControl("txtamount");
            //            txtgvamount1.Text = Math.Round((Convert.ToDouble(txtgvqty1.Text)) * (Convert.ToDouble(txtgvrate1.Text)), 2).ToString();
            TextBox txtgridtaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
            //Label lbltaxtype = (Label)gvslist.Rows[c].FindControl("lbltaxtype");
            TextBox txttax1 = (TextBox)gvslist.Rows[c].FindControl("txttax1");
            TextBox txttax2 = (TextBox)gvslist.Rows[c].FindControl("txttax2");
            TextBox txttax3 = (TextBox)gvslist.Rows[c].FindControl("txttax3");
            TextBox txttax1amt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
            TextBox txttax2amt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
            TextBox txttax3amt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
            TextBox txtgvamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
            baseamt = baseamt + Convert.ToDouble(txtgvqty1.Text) * Convert.ToDouble(txtgvrate1.Text);
            amount = amount + baseamt;
            if (txtgridtaxtype.Text != "" && txtgridtaxtype.Text != "0")
            {
                var cc = txtgridtaxtype.Text.IndexOf("-");
                if (cc != -1)
                {
                    li.typename = txtgridtaxtype.Text;
                    vatp = Convert.ToDouble(txtgridtaxtype.Text.Split('-')[0]);
                    addvatp = Convert.ToDouble(txtgridtaxtype.Text.Split('-')[1]);
                    //txtgvvat.Text = vatp.ToString();
                    //txtgvadvat.Text = addvatp.ToString();
                    txttax1.Text = vatp.ToString();
                    txttax2.Text = addvatp.ToString();
                    txttax1amt.Text = (Math.Round((Convert.ToDouble(txttax1.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100, 2)).ToString();
                    txttax2amt.Text = (Math.Round((Convert.ToDouble(txttax2.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100, 2)).ToString();

                }
                else
                {
                    txttax3amt.Text = (Math.Round(((Convert.ToDouble(txttax3.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();
                }
            }
            else
            {
                txttax3amt.Text = (Math.Round(((Convert.ToDouble(txttax3.Text) * Convert.ToDouble(txtgvamount1.Text)) / 100), 2)).ToString();
            }
            if (txttax1amt.Text != "")
            {
                vatamt = vatamt + Convert.ToDouble(txttax1amt.Text);
            }
            if (txttax2amt.Text != "")
            {
                addvatamt = addvatamt + Convert.ToDouble(txttax2amt.Text);
            }
            if (txttax3.Text != "")
            {
                cstp = cstp + Convert.ToDouble(txttax3.Text);
            }
            if (txttax3amt.Text != "")
            {
                cstamt = cstamt + Convert.ToDouble(txttax3amt.Text);
            }
            if (txttax1.Text != "")
            {
                vatp = Convert.ToDouble(txttax1.Text);
            }
            if (txttax2.Text != "")
            {
                addvatp = Convert.ToDouble(txttax2.Text);
            }
            vatp = vatp + vatp + addvatp;
            txtgvamount.Text = Math.Round((Convert.ToDouble(txtgvamount1.Text) + Convert.ToDouble(txttax1amt.Text) + Convert.ToDouble(txttax2amt.Text) + Convert.ToDouble(txttax3amt.Text)), 2).ToString();
        }
        txtbasicamount.Text = baseamt.ToString();
        //if (txtservicep.Text.Trim() != string.Empty)
        //{
        //    txtserviceamt.Text = Math.Round(((Convert.ToDouble(txttotbasicamount.Text) * Convert.ToDouble(txtservicep.Text)) / 100), 2).ToString();
        //}
        //else
        //{
        //    txtservicep.Text = "0";
        //    txtserviceamt.Text = "0";
        //}
        //txtfcstp.Text = cstp.ToString();
        txtcst.Text = cstamt.ToString();
        txtvat.Text = vatamt.ToString();
        //txtfvatp.Text = vatp.ToString();
        txtadvat.Text = addvatamt.ToString();
        txttotalso.Text = (Convert.ToDouble(txtbasicamount.Text) + Convert.ToDouble(txtcst.Text) + Convert.ToDouble(txtadvat.Text) + Convert.ToDouble(txtvat.Text)).ToString();
        //double bamt = Convert.ToDouble(txttotalso.Text);
        //double round1 = bamt - Math.Truncate(bamt);
        //txtroundoff.Text = Math.Round(round1, 2).ToString();
        //roundoff = Convert.ToDouble(txtroundoff.Text);
        //txtbillamount.Text = (Math.Round(bamt)).ToString();
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("srno1", typeof(Int64));
        dtpitems.Columns.Add("srno", typeof(string));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("basicamt", typeof(double));
        dtpitems.Columns.Add("taxtype", typeof(string));
        dtpitems.Columns.Add("vatp", typeof(double));
        dtpitems.Columns.Add("vatamt", typeof(double));
        dtpitems.Columns.Add("addtaxp", typeof(double));
        dtpitems.Columns.Add("addtaxamt", typeof(double));
        dtpitems.Columns.Add("cstp", typeof(double));
        dtpitems.Columns.Add("cstamt", typeof(double));
        dtpitems.Columns.Add("amount", typeof(double));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("make", typeof(string));
        dtpitems.Columns.Add("hsncode", typeof(string));
        return dtpitems;
    }

    protected void btnfinalsave_Click(object sender, EventArgs e)
    {
        li.quodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if (li.quodate <= yyyear1 && li.quodate >= yyyear)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        li.quono = Convert.ToDouble(txtquotationno.Text);
        li.quodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.ccode = Convert.ToInt64(txtclientcode.Text);
        li.acname = drpacname.SelectedItem.Text;
        li.basicamount = Convert.ToDouble(txtbasicamount.Text);
        li.vatamt = Convert.ToDouble(txtvat.Text);
        li.addtaxamt = Convert.ToDouble(txtadvat.Text);
        li.cstamt = Convert.ToDouble(txtcst.Text);
        li.grandtotal = Convert.ToDouble(txttotalso.Text);
        li.status = drpstatus.SelectedItem.Text;
        li.annexure = txtannexure.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        if (chkigst.Checked == true)
        {
            li.isigst = "Y";
        }
        else
        {
            li.isigst = "N";
        }
        if (btnfinalsave.Text == "Save")
        {
            fqmclass.insertquomasterdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.quono + " New Bifurcation Inserted.";
            faclass.insertactivity(li);
            for (int c = 0; c < gvslist.Rows.Count; c++)
            {
                Label lblitemname = (Label)gvslist.Rows[c].FindControl("lblitemname");
                Label lblunit = (Label)gvslist.Rows[c].FindControl("lblunit");
                TextBox txtgvsrno = (TextBox)gvslist.Rows[c].FindControl("txtsrno");
                TextBox txtgvsrno1 = (TextBox)gvslist.Rows[c].FindControl("txtsrno1");
                TextBox txtqty = (TextBox)gvslist.Rows[c].FindControl("txtqty");
                TextBox txtrate = (TextBox)gvslist.Rows[c].FindControl("txtrate");
                TextBox txtamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
                DropDownList drpdescr3 = (DropDownList)gvslist.Rows[c].FindControl("drpdescr3");
                TextBox txtdescr2 = (TextBox)gvslist.Rows[c].FindControl("txtdescr2");
                TextBox txtdescr3 = (TextBox)gvslist.Rows[c].FindControl("txtdescr3");
                TextBox txtdescr1 = (TextBox)gvslist.Rows[c].FindControl("txtdescr1");
                TextBox txtgvmake = (TextBox)gvslist.Rows[c].FindControl("txtmake");
                TextBox lblhsncode = (TextBox)gvslist.Rows[c].FindControl("txtgvhsncode");

                TextBox lbltaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
                TextBox lblvatp = (TextBox)gvslist.Rows[c].FindControl("txttax1");
                TextBox lbladdvatp = (TextBox)gvslist.Rows[c].FindControl("txttax2");
                TextBox lblcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
                TextBox lblvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
                TextBox lbladdvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
                TextBox lblcstamt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
                TextBox lblgvamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
                if (txtgvsrno.Text.Trim() != string.Empty)
                {
                    li.srno = Convert.ToInt64(txtgvsrno.Text);
                }
                else
                {
                    li.srno = 0;
                }

                if (txtgvsrno1.Text.Trim() != string.Empty)
                {
                    li.srno1 = Convert.ToInt64(txtgvsrno1.Text);
                }
                else
                {
                    li.srno1 = 0;
                }
                li.itemname = lblitemname.Text;
                li.unit = lblunit.Text;
                li.hsncode = lblhsncode.Text;
                li.qty = Convert.ToDouble(txtqty.Text);
                li.rate = Convert.ToDouble(txtrate.Text);
                li.basicamount = Convert.ToDouble(txtamount.Text);
                li.descr1 = txtdescr1.Text;
                li.descr2 = txtdescr2.Text;
                //if (drpdescr3.SelectedItem.Text != "--SELECT--")
                //{
                //    li.descr3 = drpdescr3.SelectedItem.Text;
                //}
                //else if (txtdescr3.Text.Trim() != "" && drpdescr3.SelectedItem.Text == "--SELECT--")
                //{
                //    li.descr3 = txtdescr3.Text.Trim();
                //}
                //else
                //{
                li.descr3 = txtdescr3.Text;
                //}
                li.taxtype = lbltaxtype.Text;
                li.vatp = Convert.ToDouble(lblvatp.Text);
                li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                li.cstp = Convert.ToDouble(lblcstp.Text);
                li.vatamt = Convert.ToDouble(lblvatamt.Text);
                li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                li.cstamt = Convert.ToDouble(lblcstamt.Text);
                li.amount = Convert.ToDouble(lblgvamount.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.make = txtgvmake.Text;
                fqmclass.insertquoitemsdata(li);
            }
        }
        else
        {
            fqmclass.updatequomasterdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.quono + " Bifurcation Updated.";
            faclass.insertactivity(li);
            li.quono = Convert.ToDouble(txtquotationno.Text);
            li.quodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.ccode = Convert.ToInt64(txtclientcode.Text);
            li.acname = drpacname.SelectedItem.Text;
            for (int c = 0; c < gvslist.Rows.Count; c++)
            {
                Label lblid = (Label)gvslist.Rows[c].FindControl("lblid");
                Label lblitemname = (Label)gvslist.Rows[c].FindControl("lblitemname");
                Label lblunit = (Label)gvslist.Rows[c].FindControl("lblunit");
                TextBox lblhsncode = (TextBox)gvslist.Rows[c].FindControl("txtgvhsncode");
                TextBox txtgvsrno = (TextBox)gvslist.Rows[c].FindControl("txtsrno");
                TextBox txtgvsrno1 = (TextBox)gvslist.Rows[c].FindControl("txtsrno1");
                TextBox txtqty = (TextBox)gvslist.Rows[c].FindControl("txtqty");
                TextBox txtrate = (TextBox)gvslist.Rows[c].FindControl("txtrate");
                TextBox txtamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
                DropDownList drpdescr3 = (DropDownList)gvslist.Rows[c].FindControl("drpdescr3");
                TextBox txtdescr2 = (TextBox)gvslist.Rows[c].FindControl("txtdescr2");
                TextBox txtdescr3 = (TextBox)gvslist.Rows[c].FindControl("txtdescr3");
                TextBox txtdescr1 = (TextBox)gvslist.Rows[c].FindControl("txtdescr1");
                TextBox txtgvmake = (TextBox)gvslist.Rows[c].FindControl("txtmake");


                TextBox lbltaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
                TextBox lblvatp = (TextBox)gvslist.Rows[c].FindControl("txttax1");
                TextBox lbladdvatp = (TextBox)gvslist.Rows[c].FindControl("txttax2");
                TextBox lblcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
                TextBox lblvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
                TextBox lbladdvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
                TextBox lblcstamt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
                TextBox lblgvamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
                li.id = Convert.ToInt64(lblid.Text);
                if (txtgvsrno.Text.Trim() != string.Empty)
                {
                    li.srno = Convert.ToInt64(txtgvsrno.Text);
                }
                else
                {
                    li.srno = 0;
                }
                if (txtgvsrno1.Text.Trim() != string.Empty)
                {
                    li.srno1 = Convert.ToInt64(txtgvsrno1.Text);
                }
                else
                {
                    li.srno1 = 0;
                }
                li.itemname = lblitemname.Text;
                li.unit = lblunit.Text;
                li.hsncode = lblhsncode.Text;
                li.qty = Convert.ToDouble(txtqty.Text);
                li.rate = Convert.ToDouble(txtrate.Text);
                li.basicamount = Convert.ToDouble(txtamount.Text);
                li.descr1 = txtdescr1.Text;
                li.descr2 = txtdescr2.Text;
                //if (drpdescr3.SelectedItem.Text != "--SELECT--")
                //{
                //    li.descr3 = drpdescr3.SelectedItem.Text;
                //}
                //else if (txtdescr3.Text.Trim() != "" && drpdescr3.SelectedItem.Text == "--SELECT--")
                //{
                //    li.descr3 = txtdescr3.Text.Trim();
                //}
                //else
                //{
                li.descr3 = txtdescr3.Text;
                //}
                li.taxtype = lbltaxtype.Text;
                li.vatp = Convert.ToDouble(lblvatp.Text);
                li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
                li.cstp = Convert.ToDouble(lblcstp.Text);
                li.vatamt = Convert.ToDouble(lblvatamt.Text);
                li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
                li.cstamt = Convert.ToDouble(lblcstamt.Text);
                li.amount = Convert.ToDouble(lblgvamount.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.make = txtgvmake.Text;
                fqmclass.updatequoitemsdata(li);
            }
        }
        Response.Redirect("~/QuotationList.aspx?pagename=Bifurcation");
    }
    protected void txtgvtaxtype_TextChanged(object sender, EventArgs e)
    {
        double qty = 0;
        double rate = 0;
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
        TextBox txtqty = (TextBox)currentRow.FindControl("txtqty");
        TextBox txtrate = (TextBox)currentRow.FindControl("txtrate");
        TextBox txtamount = (TextBox)currentRow.FindControl("txtamount");
        if (txtqty.Text != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtamount.Text = Math.Round((qty * rate), 2).ToString();
        //counttotal();
        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgvtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatp = (TextBox)currentRow.FindControl("txttax1");
        TextBox lbladdvatp = (TextBox)currentRow.FindControl("txttax2");
        TextBox lblcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatamt = (TextBox)currentRow.FindControl("txttax1amt");
        TextBox lbladdvatamt = (TextBox)currentRow.FindControl("txttax2amt");
        TextBox lblcstamt = (TextBox)currentRow.FindControl("txttax3amt");
        TextBox lblamount = (TextBox)currentRow.FindControl("txtamount");
        TextBox lblgvamount = (TextBox)currentRow.FindControl("txtgvamount");
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtamount.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }
                else
                {
                    txtgridcstp.Text = "0";
                }
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            //txtgridcstp.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (lblcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(lblcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        countgv();
        Page.SetFocus(lblvatp);
    }

    protected void txttax3_TextChanged(object sender, EventArgs e)
    {
        double qty = 0;
        double rate = 0;
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
        TextBox txtqty = (TextBox)currentRow.FindControl("txtqty");
        TextBox txtrate = (TextBox)currentRow.FindControl("txtrate");
        TextBox txtamount = (TextBox)currentRow.FindControl("txtamount");
        if (txtqty.Text != string.Empty)
        {
            qty = Convert.ToDouble(txtqty.Text);
        }
        if (txtrate.Text != string.Empty)
        {
            rate = Convert.ToDouble(txtrate.Text);
        }
        txtamount.Text = Math.Round((qty * rate), 2).ToString();
        //counttotal();
        TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgvtaxtype");
        TextBox txtgridcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatp = (TextBox)currentRow.FindControl("txttax1");
        TextBox lbladdvatp = (TextBox)currentRow.FindControl("txttax2");
        TextBox lblcstp = (TextBox)currentRow.FindControl("txttax3");
        TextBox lblvatamt = (TextBox)currentRow.FindControl("txttax1amt");
        TextBox lbladdvatamt = (TextBox)currentRow.FindControl("txttax2amt");
        TextBox lblcstamt = (TextBox)currentRow.FindControl("txttax3amt");
        TextBox lblamount = (TextBox)currentRow.FindControl("txtamount");
        TextBox lblgvamount = (TextBox)currentRow.FindControl("txtgvamount");
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = Convert.ToDouble(txtamount.Text);
        if (lbltaxtype.Text != string.Empty)
        {
            var cc = lbltaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = lbltaxtype.Text;
                vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                DataTable dtdtax = new DataTable();
                dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                if (dtdtax.Rows.Count > 0)
                {
                    txtgridcstp.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                }
                else
                {
                    txtgridcstp.Text = "0";
                }
            }
            else
            {
                vatp = 0;
                addvatp = 0;
                lblvatp.Text = vatp.ToString();
                lbladdvatp.Text = addvatp.ToString();
                //txtgridcstp.Text = "0";
                lbltaxtype.Text = "0";
            }
        }
        else
        {
            vatp = 0;
            addvatp = 0;
            lblvatp.Text = vatp.ToString();
            lbladdvatp.Text = addvatp.ToString();
            //txtgridcstp.Text = "0";
        }
        if (lblvatp.Text != string.Empty)
        {
            vatp = Convert.ToDouble(lblvatp.Text);
            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            lblvatamt.Text = "0";
            lblvatp.Text = "0";
        }
        if (lbladdvatp.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(lbladdvatp.Text);
            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            lbladdvatp.Text = "0";
            lbladdvatamt.Text = "0";
        }
        if (lblcstp.Text != string.Empty)
        {
            cstp = Convert.ToDouble(lblcstp.Text);
            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        }
        else
        {
            lblcstp.Text = "0";
            lblcstamt.Text = "0";
        }
        vatamt = Convert.ToDouble(lblvatamt.Text);
        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        cstamt = Convert.ToDouble(lblcstamt.Text);
        lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        countgv();
        Page.SetFocus(lblcstamt);
    }

    protected void gvslist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.quono = Convert.ToDouble(txtquotationno.Text);
            li.quodate = Convert.ToDateTime(txtsodate.Text);
            li.ccode = Convert.ToInt64(txtclientcode.Text);
            li.acname = drpacname.SelectedItem.Text;
            li.id = Convert.ToInt64(e.CommandArgument);
            GridViewRow currentRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            Label lblitemname = (Label)currentRow.FindControl("lblitemname");
            Label lblunit = (Label)currentRow.FindControl("lblunit");
            TextBox txtgvsrno11 = (TextBox)currentRow.FindControl("txtsrno1");
            TextBox txtgvsrno = (TextBox)currentRow.FindControl("txtsrno");
            TextBox txtqty = (TextBox)currentRow.FindControl("txtqty");
            TextBox txtrate = (TextBox)currentRow.FindControl("txtrate");
            TextBox txtamount = (TextBox)currentRow.FindControl("txtamount");
            TextBox txtdescr3 = (TextBox)currentRow.FindControl("txtdescr3");
            TextBox txtdescr2 = (TextBox)currentRow.FindControl("txtdescr2");
            TextBox txtdescr1 = (TextBox)currentRow.FindControl("txtdescr1");
            DropDownList drpdescr3 = (DropDownList)currentRow.FindControl("drpdescr3");
            TextBox txtgvmake = (TextBox)currentRow.FindControl("txtmake");
            TextBox lblhsncode = (TextBox)currentRow.FindControl("txtgvhsncode");


            TextBox lbltaxtype = (TextBox)currentRow.FindControl("txtgvtaxtype");
            TextBox lblvatp = (TextBox)currentRow.FindControl("txttax1");
            TextBox lbladdvatp = (TextBox)currentRow.FindControl("txttax2");
            TextBox lblcstp = (TextBox)currentRow.FindControl("txttax3");
            TextBox lblvatamt = (TextBox)currentRow.FindControl("txttax1amt");
            TextBox lbladdvatamt = (TextBox)currentRow.FindControl("txttax2amt");
            TextBox lblcstamt = (TextBox)currentRow.FindControl("txttax3amt");
            TextBox lblgvamount = (TextBox)currentRow.FindControl("txtgvamount");

            if (txtgvsrno11.Text.Trim() != string.Empty)
            {
                li.srno1 = Convert.ToInt64(txtgvsrno11.Text);
            }
            else
            {
                li.srno1 = 0;
            }
            if (txtgvsrno.Text.Trim() != string.Empty)
            {
                li.srno = Convert.ToInt64(txtgvsrno.Text);
            }
            else
            {
                li.srno = 0;
            }
            li.itemname = lblitemname.Text;
            li.unit = lblunit.Text;
            li.hsncode = lblhsncode.Text;
            li.qty = Convert.ToDouble(txtqty.Text);
            li.rate = Convert.ToDouble(txtrate.Text);
            li.basicamount = Convert.ToDouble(txtamount.Text);
            li.descr1 = txtdescr1.Text;
            li.descr2 = txtdescr2.Text;
            //li.descr3 = txtdescr3.Text;
            //if (drpdescr3.SelectedItem.Text == "--SELECT--" && txtdescr3.Text.Trim() != "")
            //{
            //    li.descr3 = txtdescr3.Text;
            //}
            //else if (drpdescr3.SelectedItem.Text != "--SELECT--")
            //{
            //    li.descr3 = drpdescr3.SelectedItem.Text;
            //}
            //else
            //{
            li.descr3 = txtdescr3.Text;
            //}
            li.taxtype = lbltaxtype.Text;
            li.vatp = Convert.ToDouble(lblvatp.Text);
            li.addtaxp = Convert.ToDouble(lbladdvatp.Text);
            li.cstp = Convert.ToDouble(lblcstp.Text);
            li.vatamt = Convert.ToDouble(lblvatamt.Text);
            li.addtaxamt = Convert.ToDouble(lbladdvatamt.Text);
            li.cstamt = Convert.ToDouble(lblcstamt.Text);
            li.amount = Convert.ToDouble(lblgvamount.Text);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.make = txtgvmake.Text;
            fqmclass.updatequoitemsdata(li);
            DataTable dtitems = new DataTable();
            dtitems = fqmclass.selectquoitemsdatafromquono(li);
            if (dtitems.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvslist.Visible = true;
                gvslist.DataSource = dtitems;
                gvslist.DataBind();
                lblcount.Text = dtitems.Rows.Count.ToString();
            }
            else
            {
                gvslist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Items Found.";
            }
            Page.SetFocus(txtqty);
        }
    }

    public void bindgrid()
    {
        DataTable dtsession = Session["dtpitemsbifu"] as DataTable;
        if (dtsession.Rows.Count > 0)
        {
            gvslist.Visible = true;
            lblempty.Visible = false;
            gvslist.DataSource = dtsession;
            gvslist.DataBind();
            lblcount.Text = dtsession.Rows.Count.ToString();
        }
        else
        {
            gvslist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Items Found.";
            //lblcount.Text = "0";
        }
    }

    protected void gvslist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            DataTable dt = Session["dtpitemsbifu"] as DataTable;
            dt.Rows.Remove(dt.Rows[e.RowIndex]);
            //Session["ddc"] = dt;
            Session["dtpitemsbifu"] = dt;
            this.bindgrid();
        }
        else
        {
            if (gvslist.Rows.Count > 1)
            {
                li.quono = Convert.ToDouble(txtquotationno.Text);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                Label lblid = (Label)gvslist.Rows[e.RowIndex].FindControl("lblid");
                Label lblvid = (Label)gvslist.Rows[e.RowIndex].FindControl("lblvid");
                li.id = Convert.ToInt64(lblid.Text);
                fqmclass.deletequoitemsdatafromid(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.id + " Bifurcation Item Deleted.";
                faclass.insertactivity(li);
                DataTable dtitems = new DataTable();
                dtitems = fqmclass.selectquoitemsdatafromquono(li);
                if (dtitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvslist.Visible = true;
                    gvslist.DataSource = dtitems;
                    gvslist.DataBind();
                    lblcount.Text = dtitems.Rows.Count.ToString();
                }
                else
                {
                    txtbasicamount.Text = "0";
                    txtvat.Text = "0";
                    txtadvat.Text = "0";
                    txtcst.Text = "0";
                    txttotalso.Text = "0";
                    gvslist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter New Entry and then try to delete this item because you cant delete entry when there is only 1 entry.');", true);
                return;
            }
        }
        count1();
        hideimage();
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getvattype(string prefixText)
    {
        ForBankReceipt fbrclass = new ForBankReceipt();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = fbrclass.selectallvattype(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void drpitemname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpitemname.SelectedItem.Text != string.Empty)
        {
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.itemname = drpitemname.SelectedItem.Text;
            DataTable dtdata = new DataTable();
            dtdata = fsoclass.selectallitemdatafromitemname(li);
            if (dtdata.Rows.Count > 0)
            {
                txtqty1.Text = "1";
                txtrate1.Text = dtdata.Rows[0]["salesrate"].ToString();
                txtunit.Text = dtdata.Rows[0]["unit"].ToString();
                txthsncode.Text = dtdata.Rows[0]["hsncode"].ToString();
                if (dtdata.Rows[0]["gsttype"].ToString().IndexOf("-") != -1)
                {
                    txttaxtype.Text = dtdata.Rows[0]["gsttype"].ToString();
                    string vattype = dtdata.Rows[0]["gsttype"].ToString();
                    txtgvvat.Text = vattype.Split('-')[0];
                    txtvatamt.Text = "0";
                    txtgvadvat.Text = vattype.Split('-')[1];
                    txtaddtaxamt.Text = "0";
                    count1();
                }
                else
                {
                    string vattype = dtdata.Rows[0]["gsttype"].ToString();
                    txtgvcst.Text = vattype.ToString();
                    txtcstamount.Text = "0";
                    count1();
                }
                li.type = drpitemname.SelectedItem.Text;
                DataTable dtmisc = new DataTable();
                dtmisc = fqmclass.selectmiscdataitemwise(li);
                if (dtmisc.Rows.Count > 0)
                {
                    drpdescription3.DataSource = dtmisc;
                    drpdescription3.DataTextField = "name";
                    drpdescription3.DataBind();
                    drpdescription3.Items.Insert(0, "--SELECT--");
                }
                else
                {
                    drpdescription3.Items.Clear();
                    drpdescription3.Items.Insert(0, "--SELECT--");
                }
            }
            else
            {
                fillitemnamedrop();
                txtqty1.Text = string.Empty;
                txtrate1.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtbasicamt.Text = string.Empty;
                txttaxtype.Text = string.Empty;
                txtgvvat.Text = string.Empty;
                txtgvadvat.Text = string.Empty;
                txtvatamt.Text = string.Empty;
                txtaddtaxamt.Text = string.Empty;
                txtgvcst.Text = string.Empty;
                txtcstamount.Text = string.Empty;
                drpdescription3.Items.Clear();
                drpdescription3.Items.Insert(0, "--SELECT--");
                txtdescription2.Text = string.Empty;
                txtdescription3.Text = string.Empty;
                txtgvamount.Text = string.Empty;
            }
        }
        else
        {
            fillitemnamedrop();
            txtqty1.Text = string.Empty;
            txtrate1.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtbasicamt.Text = string.Empty;
            txttaxtype.Text = string.Empty;
            txtgvvat.Text = string.Empty;
            txtgvadvat.Text = string.Empty;
            txtvatamt.Text = string.Empty;
            txtaddtaxamt.Text = string.Empty;
            txtgvcst.Text = string.Empty;
            txtcstamount.Text = string.Empty;
            drpdescription3.Items.Clear();
            drpdescription3.Items.Insert(0, "--SELECT--");
            txtdescription2.Text = string.Empty;
            txtdescription3.Text = string.Empty;
            txtgvamount.Text = string.Empty;
        }
        Page.SetFocus(txtunit);
    }
    protected void txttaxtype_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvvat);
    }
    protected void txtqty1_TextChanged(object sender, EventArgs e)
    {
        if (txtqty1.Text.Trim() == string.Empty)
        {
            txtqty1.Text = "0";
        }
        if (txtrate1.Text.Trim() == string.Empty)
        {
            txtrate1.Text = "0";
        }
        count1();
        Page.SetFocus(txtrate1);
    }
    protected void txtrate1_TextChanged(object sender, EventArgs e)
    {
        if (txtqty1.Text.Trim() == string.Empty)
        {
            txtqty1.Text = "0";
        }
        if (txtrate1.Text.Trim() == string.Empty)
        {
            txtrate1.Text = "0";
        }
        count1();
        Page.SetFocus(txtbasicamt);
    }
    protected void txtbasicamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txttaxtype);
    }
    protected void txtgvvat_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvadvat);
    }
    protected void txtgvadvat_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvcst);
    }
    protected void txtgvcst_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtcstamount);
    }
    protected void txtcstamount_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txthsncode);
    }
    protected void txtvatamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtaddtaxamt);
    }
    protected void txtaddtaxamt_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtgvamount);
    }
    protected void txtgvamount_TextChanged(object sender, EventArgs e)
    {
        count1();
        Page.SetFocus(txtmake);
    }

    public void count1()
    {
        //txtbasicamt.Text = ((Convert.ToDouble(txtqty.Text)) * (Convert.ToDouble(txtrate.Text))).ToString();
        double qty = 0;
        double rate = 0;
        double vatp = 0;
        double addvatp = 0;
        double cstp = 0;
        double vatamt = 0;
        double addvatamt = 0;
        double cstamt = 0;
        double baseamt = 0;
        if (txtqty1.Text.Trim() != string.Empty)
        {
            qty = Convert.ToDouble(txtqty1.Text);
        }
        if (txtrate1.Text.Trim() != string.Empty)
        {
            rate = Convert.ToDouble(txtrate1.Text);
        }
        txtbasicamt.Text = Math.Round((qty * rate), 2).ToString();
        if (txtbasicamt.Text.Trim() != string.Empty)
        {
            baseamt = Convert.ToDouble(txtbasicamt.Text);
        }

        if (txttaxtype.Text != string.Empty)
        {
            var cc = txttaxtype.Text.IndexOf("-");
            if (cc != -1)
            {
                li.typename = txttaxtype.Text;
                vatp = Convert.ToDouble(txttaxtype.Text.Split('-')[0]);
                addvatp = Convert.ToDouble(txttaxtype.Text.Split('-')[1]);
                txtgvvat.Text = vatp.ToString();
                txtgvadvat.Text = addvatp.ToString();
                //DataTable dtdtax = new DataTable();
                //dtdtax = fsoclass.selectalltaxdatafromtaxname(li);
                //if (dtdtax.Rows.Count > 0)
                //{
                //    txtgvcst.Text = dtdtax.Rows[0]["centralsalep"].ToString();
                //}

            }
            else
            {
                txtgvvat.Text = "0";
                txtgvadvat.Text = "0";
                txttaxtype.Text = "0";
            }
        }
        else
        {
            txtgvvat.Text = "0";
            txtgvadvat.Text = "0";
        }

        if (txtgvvat.Text != string.Empty)
        {
            vatp = Convert.ToDouble(txtgvvat.Text);
            txtvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        }
        else
        {
            txtgvvat.Text = "0";
            txtvatamt.Text = "0";
        }
        if (txtgvadvat.Text != string.Empty)
        {
            addvatp = Convert.ToDouble(txtgvadvat.Text);
            txtaddtaxamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        }
        else
        {
            txtgvadvat.Text = "0";
            txtaddtaxamt.Text = "0";
        }
        if (Convert.ToDouble(txtgvvat.Text) != 0)
        {
            txtgvcst.Text = "0";
            txtcstamount.Text = "0";
        }
        else
        {
            if (txtgvcst.Text != string.Empty)
            {
                cstp = Convert.ToDouble(txtgvcst.Text);
                txtcstamount.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
            }
            else
            {
                txtgvcst.Text = "0";
                txtcstamount.Text = "0";
            }
        }
        vatamt = Convert.ToDouble(txtvatamt.Text);
        addvatamt = Convert.ToDouble(txtaddtaxamt.Text);
        cstamt = Convert.ToDouble(txtcstamount.Text);
        txtgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();

        if (gvslist.Rows.Count > 0)
        {
            double totbasicamt = 0;
            double totcst = 0;
            double totvat = 0;
            double totaddvat = 0;
            double totamount = 0;
            for (int c = 0; c < gvslist.Rows.Count; c++)
            {
                TextBox lblbasicamt = (TextBox)gvslist.Rows[c].FindControl("txtamount");
                TextBox lblsctamt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
                TextBox lblvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
                TextBox lbladdvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
                TextBox lblamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
                totbasicamt = totbasicamt + Convert.ToDouble(lblbasicamt.Text);
                totcst = totcst + Convert.ToDouble(lblsctamt.Text);
                totvat = totvat + Convert.ToDouble(lblvatamt.Text);
                totaddvat = totaddvat + Convert.ToDouble(lbladdvatamt.Text);
                totamount = totamount + Convert.ToDouble(lblamount.Text);
            }
            txtbasicamount.Text = totbasicamt.ToString();
            txtvat.Text = totvat.ToString();
            txtadvat.Text = totaddvat.ToString();
            txtcst.Text = totcst.ToString();
            txttotalso.Text = totamount.ToString();
        }
        else
        {
            txtbasicamount.Text = "0";
            txtvat.Text = "0";
            txtadvat.Text = "0";
            txtcst.Text = "0";
            txttotalso.Text = "0";
        }

    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnfinalsave.Text == "Save")
        {
            DataTable dt = (DataTable)Session["dtpitemsbifu"];
            DataRow dr = dt.NewRow();
            dr["id"] = gvslist.Rows.Count + 1;
            dr["srno1"] = gvslist.Rows.Count + 1;
            dr["srno"] = "0";
            dr["itemname"] = drpitemname.SelectedItem.Text;
            dr["qty"] = txtqty1.Text;
            dr["unit"] = txtunit.Text;
            dr["hsncode"] = txthsncode.Text;
            dr["rate"] = txtrate1.Text;
            dr["basicamt"] = txtbasicamt.Text;
            dr["taxtype"] = txttaxtype.Text;
            dr["vatp"] = txtgvvat.Text;
            dr["addtaxp"] = txtgvadvat.Text;
            dr["cstp"] = txtgvcst.Text;
            dr["vatamt"] = txtvatamt.Text;
            dr["addtaxamt"] = txtaddtaxamt.Text;
            dr["cstamt"] = txtcstamount.Text;
            dr["amount"] = txtgvamount.Text;
            dr["descr1"] = txtdescription1.Text;
            dr["descr2"] = txtdescription2.Text;
            if (drpdescription3.SelectedItem.Text == "--SELECT--" && txtdescription3.Text.Trim() != "")
            {
                dr["descr3"] = txtdescription3.Text;
            }
            else if (drpdescription3.SelectedItem.Text != "--SELECT--")
            {
                dr["descr3"] = drpdescription3.SelectedItem.Text;
            }
            else
            {
                dr["descr3"] = "";
            }
            dr["make"] = txtmake.Text;
            dt.Rows.Add(dr);
            Session["dtpitemsbifu"] = dt;
            this.bindgrid();
            count1();
        }
        else
        {
            li.srno1 = gvslist.Rows.Count + 1;
            li.quono = Convert.ToDouble(txtquotationno.Text);
            li.quodate = Convert.ToDateTime(txtsodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            li.ccode = Convert.ToInt64(txtclientcode.Text);
            li.acname = drpacname.SelectedItem.Text;
            li.itemname = drpitemname.SelectedItem.Text;
            li.qty = Convert.ToDouble(txtqty1.Text);
            li.unit = txtunit.Text;
            li.hsncode = txthsncode.Text;
            li.rate = Convert.ToDouble(txtrate1.Text);
            li.basicamount = Convert.ToDouble(txtbasicamt.Text);
            li.taxtype = txttaxtype.Text;
            li.vatp = Convert.ToDouble(txtgvvat.Text);
            li.addtaxp = Convert.ToDouble(txtgvadvat.Text);
            li.cstp = Convert.ToDouble(txtgvcst.Text);
            li.vatamt = Convert.ToDouble(txtvatamt.Text);
            li.addtaxamt = Convert.ToDouble(txtaddtaxamt.Text);
            li.cstamt = Convert.ToDouble(txtcstamount.Text);
            li.amount = Convert.ToDouble(txtgvamount.Text);
            li.descr1 = txtdescription1.Text;
            li.descr2 = txtdescription2.Text;
            if (drpdescription3.SelectedItem.Text == "--SELECT--" && txtdescription3.Text.Trim() != "")
            {
                li.descr3 = txtdescription3.Text;
            }
            else if (drpdescription3.SelectedItem.Text != "--SELECT--")
            {
                li.descr3 = drpdescription3.SelectedItem.Text;
            }
            else
            {
                li.descr3 = "";
            }
            li.make = txtmake.Text;
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            fqmclass.insertquoitemsdata(li);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.quono + " Bifurcation Item Inserted.";
            faclass.insertactivity(li);
            li.quono = Convert.ToDouble(txtquotationno.Text);
            DataTable dtdata = new DataTable();
            dtdata = fqmclass.selectquoitemsdatafromquono(li);
            if (dtdata.Rows.Count > 0)
            {
                lblempty.Visible = false;
                gvslist.Visible = true;
                gvslist.DataSource = dtdata;
                gvslist.DataBind();
                lblcount.Text = dtdata.Rows.Count.ToString();
            }
            else
            {
                gvslist.Visible = false;
                lblempty.Visible = true;
                lblempty.Text = "No Sales Order Items Found.";
                //lblcount.Text = "0";
            }
            count1();
        }
        fillitemnamedrop();
        txtqty1.Text = string.Empty;
        txtunit.Text = string.Empty;
        txthsncode.Text = string.Empty;
        txtmake.Text = string.Empty;
        txtrate1.Text = string.Empty;
        txtbasicamt.Text = string.Empty;
        txttaxtype.Text = string.Empty;
        txtgvvat.Text = string.Empty;
        txtgvadvat.Text = string.Empty;
        txtgvcst.Text = string.Empty;
        txtcstamount.Text = string.Empty;
        drpdescription3.Items.Clear();
        drpdescription3.Items.Insert(0, "--SELECT--");
        txtdescription2.Text = string.Empty;
        txtdescription3.Text = string.Empty;
        txtmake.Text = string.Empty;
        txtvatamt.Text = string.Empty;
        txtaddtaxamt.Text = string.Empty;
        txtgvamount.Text = string.Empty;
        hideimage();
        Page.SetFocus(drpitemname);

    }

    protected void chkigst_CheckedChanged(object sender, EventArgs e)
    {
        //if (chkigst.Checked == true)
        //{
        //for (int c = 0; c < gvslist.Rows.Count; c++)
        //{

        //    TextBox lbltaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
        //    TextBox lblcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
        //    if (lbltaxtype.Text.Trim() == string.Empty && chkigst.Checked==true)
        //    {
        //        //if()
        //        if (chkigst.Checked == true)
        //        {
        //            chkigst.Checked = false;
        //        }
        //        else
        //        {
        //            chkigst.Checked = true;
        //        }
        //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter GST-IGST Percentage and try again.');", true);
        //        return;
        //    }
        //    if (chkigst.Checked == false && lblcstp.Text.Trim() == string.Empty)
        //    {
        //        //if()
        //        if (chkigst.Checked == true)
        //        {
        //            chkigst.Checked = false;
        //        }
        //        else
        //        {
        //            chkigst.Checked = true;
        //        }
        //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter GST-IGST Percentage and try again.');", true);
        //        return;
        //    }           
        //}
        for (int c = 0; c < gvslist.Rows.Count; c++)
        {
            double qty = 0;
            double rate = 0;
            //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
            TextBox txtqty = (TextBox)gvslist.Rows[c].FindControl("txtqty");
            TextBox txtrate = (TextBox)gvslist.Rows[c].FindControl("txtrate");
            TextBox txtamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
            if (txtqty.Text != string.Empty)
            {
                qty = Convert.ToDouble(txtqty.Text);
            }
            if (txtrate.Text != string.Empty)
            {
                rate = Convert.ToDouble(txtrate.Text);
            }
            txtamount.Text = Math.Round((qty * rate), 2).ToString();
            //counttotal();
            //countgv();

            TextBox lbltaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
            TextBox txtgridcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
            TextBox lblvatp = (TextBox)gvslist.Rows[c].FindControl("txttax1");
            TextBox lbladdvatp = (TextBox)gvslist.Rows[c].FindControl("txttax2");
            TextBox lblcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
            TextBox lblvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
            TextBox lbladdvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
            TextBox lblcstamt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
            TextBox lblamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
            TextBox lblgvamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
            double vatp = 0;
            double addvatp = 0;
            double cstp = 0;
            double vatamt = 0;
            double addvatamt = 0;
            double cstamt = 0;
            double baseamt = Convert.ToDouble(txtamount.Text);
            if (lbltaxtype.Text != string.Empty && lblcstp.Text == "0")
            {
                var cc = lbltaxtype.Text.IndexOf("-");
                if (cc != -1)
                {
                    if (chkigst.Checked == true)
                    {
                        cstp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]) + Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                        lblcstp.Text = cstp.ToString();
                        vatp = 0;
                        addvatp = 0;
                        lblvatp.Text = vatp.ToString();
                        lbladdvatp.Text = addvatp.ToString();
                        lbltaxtype.Text = string.Empty;
                    }
                    else
                    {
                        li.typename = lbltaxtype.Text;
                        vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
                        addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
                        lblvatp.Text = vatp.ToString();
                        lbladdvatp.Text = addvatp.ToString();
                        cstp = 0;
                        lblcstp.Text = cstp.ToString();
                    }
                }
                else
                {
                    if (chkigst.Checked == true)
                    {
                        cstp = Convert.ToDouble(lbltaxtype.Text);
                        lblcstp.Text = cstp.ToString();
                        vatp = 0;
                        addvatp = 0;
                        lblvatp.Text = vatp.ToString();
                        lbladdvatp.Text = addvatp.ToString();
                        lbltaxtype.Text = string.Empty;
                    }
                    else
                    {
                        cstp = 0;
                        lblcstp.Text = cstp.ToString();
                        vatp = Convert.ToDouble(lblcstp.Text) / 2;
                        addvatp = Convert.ToDouble(lblcstp.Text) / 2;
                        lblvatp.Text = vatp.ToString();
                        lbladdvatp.Text = addvatp.ToString();
                        lbltaxtype.Text = string.Empty;
                    }
                }
            }
            else if (lbltaxtype.Text == string.Empty && lblcstp.Text != "0")
            {
                //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter GST-IGST Percentage and try again.');", true);
                //return;
                if (chkigst.Checked == true)
                {
                    cstp = Convert.ToDouble(lbltaxtype.Text);
                    lblcstp.Text = cstp.ToString();
                    vatp = 0;
                    addvatp = 0;
                    lblvatp.Text = vatp.ToString();
                    lbladdvatp.Text = addvatp.ToString();
                    lbltaxtype.Text = string.Empty;
                }
                else
                {

                    vatp = Convert.ToDouble(lblcstp.Text) / 2;
                    addvatp = Convert.ToDouble(lblcstp.Text) / 2;
                    lblvatp.Text = vatp.ToString();
                    lbladdvatp.Text = addvatp.ToString();
                    lbltaxtype.Text = (vatp.ToString()) + "-" + (addvatp.ToString());
                    //lbltaxtype.Text = string.Empty;
                    cstp = 0;
                    lblcstp.Text = cstp.ToString();
                }
            }
            else
            {
                if (chkigst.Checked == true)
                {
                    chkigst.Checked = false;
                }
                else
                {
                    chkigst.Checked = true;
                }
                li.quono = Convert.ToDouble(Request["quono"].ToString());
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtitems = new DataTable();
                dtitems = fqmclass.selectquoitemsdatafromquono(li);
                if (dtitems.Rows.Count > 0)
                {
                    lblempty.Visible = false;
                    gvslist.Visible = true;
                    gvslist.DataSource = dtitems;
                    gvslist.DataBind();
                    lblcount.Text = dtitems.Rows.Count.ToString();
                }
                else
                {
                    gvslist.Visible = false;
                    lblempty.Visible = true;
                    lblempty.Text = "No Items Found.";
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter GST-IGST Percentage and try again.');", true);
                return;
            }
            if (lblvatp.Text != string.Empty)
            {
                vatp = Convert.ToDouble(lblvatp.Text);
                lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
            }
            else
            {
                lblvatamt.Text = "0";
                lblvatp.Text = "0";
            }
            if (lbladdvatp.Text != string.Empty)
            {
                addvatp = Convert.ToDouble(lbladdvatp.Text);
                lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
            }
            else
            {
                lbladdvatp.Text = "0";
                lbladdvatamt.Text = "0";
            }
            if (lblcstp.Text != string.Empty)
            {
                cstp = Convert.ToDouble(lblcstp.Text);
                lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
            }
            else
            {
                lblcstp.Text = "0";
                lblcstamt.Text = "0";
            }
            vatamt = Convert.ToDouble(lblvatamt.Text);
            addvatamt = Convert.ToDouble(lbladdvatamt.Text);
            cstamt = Convert.ToDouble(lblcstamt.Text);
            lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
            countgv();
            Page.SetFocus(txtrate);
        }
        //}
        //else
        //{
        //    for (int c = 0; c < gvslist.Rows.Count; c++)
        //    {
        //        double qty = 0;
        //        double rate = 0;
        //        //Label lblqtyremain = (Label)currentRow.FindControl("lblqtyremain");
        //        TextBox txtqty = (TextBox)gvslist.Rows[c].FindControl("txtqty");
        //        TextBox txtrate = (TextBox)gvslist.Rows[c].FindControl("txtrate");
        //        TextBox txtamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
        //        if (txtqty.Text != string.Empty)
        //        {
        //            qty = Convert.ToDouble(txtqty.Text);
        //        }
        //        if (txtrate.Text != string.Empty)
        //        {
        //            rate = Convert.ToDouble(txtrate.Text);
        //        }
        //        txtamount.Text = Math.Round((qty * rate), 2).ToString();
        //        //counttotal();
        //        //countgv();

        //        TextBox lbltaxtype = (TextBox)gvslist.Rows[c].FindControl("txtgvtaxtype");
        //        TextBox txtgridcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
        //        TextBox lblvatp = (TextBox)gvslist.Rows[c].FindControl("txttax1");
        //        TextBox lbladdvatp = (TextBox)gvslist.Rows[c].FindControl("txttax2");
        //        TextBox lblcstp = (TextBox)gvslist.Rows[c].FindControl("txttax3");
        //        TextBox lblvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax1amt");
        //        TextBox lbladdvatamt = (TextBox)gvslist.Rows[c].FindControl("txttax2amt");
        //        TextBox lblcstamt = (TextBox)gvslist.Rows[c].FindControl("txttax3amt");
        //        TextBox lblamount = (TextBox)gvslist.Rows[c].FindControl("txtamount");
        //        TextBox lblgvamount = (TextBox)gvslist.Rows[c].FindControl("txtgvamount");
        //        double vatp = 0;
        //        double addvatp = 0;
        //        double cstp = 0;
        //        double vatamt = 0;
        //        double addvatamt = 0;
        //        double cstamt = 0;
        //        double baseamt = Convert.ToDouble(txtamount.Text);
        //        if (lbltaxtype.Text != string.Empty)
        //        {
        //            var cc = lbltaxtype.Text.IndexOf("-");
        //            if (cc != -1)
        //            {
        //                if (chkigst.Checked == true)
        //                {
        //                    cstp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]) + Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
        //                    lblcstp.Text = cstp.ToString();
        //                }
        //                else
        //                {
        //                    li.typename = lbltaxtype.Text;
        //                    vatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]);
        //                    addvatp = Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
        //                    lblvatp.Text = vatp.ToString();
        //                    lbladdvatp.Text = addvatp.ToString();
        //                }
        //            }
        //            else
        //            {
        //                if (chkigst.Checked == true)
        //                {
        //                    cstp = Convert.ToDouble(lbltaxtype.Text.Split('-')[0]) + Convert.ToDouble(lbltaxtype.Text.Split('-')[1]);
        //                    lblcstp.Text = cstp.ToString();
        //                }
        //            }
        //        }
        //        if (lblvatp.Text != string.Empty)
        //        {
        //            vatp = Convert.ToDouble(lblvatp.Text);
        //            lblvatamt.Text = Math.Round(((baseamt * vatp) / 100), 2).ToString();
        //        }
        //        else
        //        {
        //            lblvatamt.Text = "0";
        //            lblvatp.Text = "0";
        //        }
        //        if (lbladdvatp.Text != string.Empty)
        //        {
        //            addvatp = Convert.ToDouble(lbladdvatp.Text);
        //            lbladdvatamt.Text = Math.Round(((baseamt * addvatp) / 100), 2).ToString();
        //        }
        //        else
        //        {
        //            lbladdvatp.Text = "0";
        //            lbladdvatamt.Text = "0";
        //        }
        //        if (lblcstp.Text != string.Empty)
        //        {
        //            cstp = Convert.ToDouble(lblcstp.Text);
        //            lblcstamt.Text = Math.Round(((baseamt * cstp) / 100), 2).ToString();
        //        }
        //        else
        //        {
        //            lblcstp.Text = "0";
        //            lblcstamt.Text = "0";
        //        }
        //        vatamt = Convert.ToDouble(lblvatamt.Text);
        //        addvatamt = Convert.ToDouble(lbladdvatamt.Text);
        //        cstamt = Convert.ToDouble(lblcstamt.Text);
        //        lblgvamount.Text = (baseamt + vatamt + addvatamt + cstamt).ToString();
        //        countgv();
        //        Page.SetFocus(txtrate);
        //    }
        //}
    }
}