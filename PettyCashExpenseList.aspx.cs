﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class PettyCashExpenseList : System.Web.UI.Page
{
    ForBankReceipt fbrclass = new ForBankReceipt();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.istype = "PC";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fbrclass.selectallbr1data(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvcashexpenselist.Visible = true;
            gvcashexpenselist.DataSource = dtdata;
            gvcashexpenselist.DataBind();
        }
        else
        {
            gvcashexpenselist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Bank Receipt Data Found.";
        }
    }

    protected void btnpettycashexpense_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/PettyCashExpense.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvcashexpenselist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                Response.Redirect("~/PettyCashExpense.aspx?mode=update&vno=" + li.voucherno + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.istype = "PC";
                DataTable dtitem = new DataTable();
                dtitem = fbrclass.selectbrdatafromvno(li);
                for (int c = 0; c < dtitem.Rows.Count; c++)
                {
                    li.id = Convert.ToInt64(dtitem.Rows[c]["id"].ToString());
                    //fbrclass.deletefromledgertable(li);
                }
                fbrclass.deleteallledgerdatapc(li);
                fbrclass.deletebrdatafromvno(li);
                fbrclass.deletebr1data(li);
                fbrclass.updateisusednpc(li);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.voucherno + " Petty Cash deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvcashexpenselist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvcashexpenselist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvcashexpenselist.PageIndex = e.NewPageIndex;
        fillgrid();
    }
}