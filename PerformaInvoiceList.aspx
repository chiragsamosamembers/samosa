﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PerformaInvoiceList.aspx.cs" Inherits="PerformaInvoiceList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
    <div class="container">
        <span style="color: white; background-color: Red">Quotation List</span><br />
        <div class="row">
            <div class="col-md-2">
                <asp:Button ID="btnquotation" runat="server" Text="Add New Per. Inv." class="btn btn-default forbutton"
                    OnClick="btnquotation_Click" /></div>
            <div class="col-md-2">
                <asp:CheckBox ID="chkexcel" runat="server" Text="Export In $" /><asp:CheckBox ID="chkexcel1"
                    runat="server" Text="Export In €" /></div>
            <div class="col-md-2">
                <asp:RadioButtonList ID="rdotype" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True">PDF</asp:ListItem>
                    <asp:ListItem>Excel</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    Rupees Per Dollar</label>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtdtors" runat="server" CssClass="form-control" Width="70px"></asp:TextBox>
            </div>
            <div class="col-md-2">
            <asp:TextBox ID="txtreporttitle" runat="server" CssClass="form-control" placeholder="Report Title"></asp:TextBox>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvso" runat="server" AutoGenerateColumns="False" Width="100%" BorderStyle="None"
                        AllowSorting="false" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered"
                        OnRowCommand="gvso_RowCommand" OnRowDeleting="gvso_RowDeleting">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("invno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect121" CommandName="revised" runat="server" ImageUrl="~/images/buttons/revised1.jpg"
                                        ToolTip="Edit" Height="20px" Width="40px" CausesValidation="False" CommandArgument='<%# bind("invno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Per. Inv. NO." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblquono" ForeColor="Black" runat="server" Text='<%# bind("invno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText=" Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblquodate" ForeColor="Black" runat="server" Text='<%#Convert.ToDateTime(Eval("invdate")).ToString("dd-MM-yyyy") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Client Code" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AC Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblacname" ForeColor="Black" runat="server" Text='<%# bind("acname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dtors" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldtors" ForeColor="Black" runat="server" Text='<%# bind("dtors") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Etors" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbletors" ForeColor="Black" runat="server" Text='<%# bind("etors") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("invno") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="print" runat="server" ImageUrl="~/images/buttons/printer.gif"
                                        ToolTip="Print" CommandArgument='<%# bind("invno") %>' Height="20px" Width="20px"
                                        CausesValidation="False" /><%--OnClientClick="return confirmprintchq();"--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkpitosc" runat="server" CommandArgument='<%# bind("invno") %>'
                                        CommandName="directpisc" ToolTip="Create Sales Challan From Performa Invoice">SC</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkpitoci" runat="server" CommandArgument='<%# bind("invno") %>'
                                        CommandName="directpici" ToolTip="Create Commercial Invoice From Performa Invoice">CI</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
    <div class="row">
        <asp:LinkButton ID="lnkselectionpopup" runat="server" AccessKey="s" OnClick="lnkselectionpopup_Click"></asp:LinkButton>
        <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
            TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="500px" align="center"
            Width="95%" Style="display: none">
            <div class="row">
                <div class="col-md-12 text-center">
                    <span style="color: Red;"><b>PI-SC Selection</b></span></div>
            </div>
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-md-1 text-left">
                    <b>Invoice No. :</b></div>
                <div class="col-md-7">
                    <asp:Label ID="lblpopupacname" runat="server" Font-Bold="true"></asp:Label></div>
            </div>
            <div class="row">
                <asp:Label ID="lblemptyso" runat="server" ForeColor="Red"></asp:Label>
                <asp:CheckBox ID="chkall" runat="server" Text="Select All" Visible="false" />
                <asp:Button ID="btnselectitem" runat="server" Text="Ok" BackColor="Red" ForeColor="White"
                    OnClick="btnselectitem_Click" />
                <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                    <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvpiitemlistselection" runat="server" AutoGenerateColumns="False"
                        Width="1300px" BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”"
                        Height="0px" CssClass="table table-bordered">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkselect" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblvid" ForeColor="Black" runat="server" Text='<%# bind("vid") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvunit" runat="server" Width="120px" Text='<%# bind("unit") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtqty" runat="server" Width="80px" Text='<%# bind("qty") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtrate" runat="server" Width="120px" Text='<%# bind("rate") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtamount" runat="server" Width="120px" Text='<%# bind("basicamt") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdescr1" runat="server" Text='<%# bind("descr1") %>'></asp:TextBox>
                                    <%--<asp:DropDownList ID="drpdescr1" runat="server">
                                    </asp:DropDownList>--%>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdescr2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdescr3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvtaxtype" runat="server" Text='<%# bind("taxtype") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax1" runat="server" Text='<%# bind("vatp") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax2" runat="server" Text='<%# bind("addtaxp") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax3" runat="server" Text='<%# bind("cstp") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax1 Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax1amt" runat="server" Text='<%# bind("vatamt") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax2 Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax2amt" runat="server" Text='<%# bind("addtaxamt") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax3 Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax3amt" runat="server" Text='<%# bind("cstamt") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvamount" runat="server" Text='<%# bind("amount") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
                <asp:Button ID="btnClose" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
            </div>
        </asp:Panel>
    </div>
    <div class="row">
        <asp:LinkButton ID="lnkselectionpopup1" runat="server" AccessKey="p" OnClick="lnkselectionpopup1_Click"></asp:LinkButton>
        <asp:LinkButton ID="lnkopenpopup1" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel4"
            TargetControlID="lnkopenpopup1" CancelControlID="btnClose1" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel4" runat="server" CssClass="modalPopup" Height="500px" align="center"
            Width="95%" Style="display: none">
            <div class="row">
                <div class="col-md-12 text-center">
                    <span style="color: Red;"><b>PI-CI Selection</b></span></div>
            </div>
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-md-1 text-left">
                    <b>Invoice No. :</b></div>
                <div class="col-md-7">
                    <asp:Label ID="lblpopup1" runat="server" Font-Bold="true"></asp:Label></div>
            </div>
            <div class="row">
                <asp:Label ID="lblempty1" runat="server" ForeColor="Red"></asp:Label>
                <asp:CheckBox ID="chkall1" runat="server" Text="Select All" Visible="false" />
                <asp:Button ID="btnselectitem1" runat="server" Text="Ok" BackColor="Red" ForeColor="White"
                    OnClick="btnselectitem1_Click" />
                <asp:Panel ID="Panel5" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                    <asp:Label ID="Label4" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvpiselect" runat="server" AutoGenerateColumns="False" Width="1300px"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkselect" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblvid" ForeColor="Black" runat="server" Text='<%# bind("vid") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvunit" runat="server" Width="120px" Text='<%# bind("unit") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtqty" runat="server" Width="80px" Text='<%# bind("qty") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtrate" runat="server" Width="120px" Text='<%# bind("rate") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtamount" runat="server" Width="120px" Text='<%# bind("basicamt") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdescr1" runat="server" Text='<%# bind("descr1") %>'></asp:TextBox>
                                    <%--<asp:DropDownList ID="drpdescr1" runat="server">
                                    </asp:DropDownList>--%>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdescr2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdescr3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvtaxtype" runat="server" Text='<%# bind("taxtype") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax1" runat="server" Text='<%# bind("vatp") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax2" runat="server" Text='<%# bind("addtaxp") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax3" runat="server" Text='<%# bind("cstp") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax1 Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax1amt" runat="server" Text='<%# bind("vatamt") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax2 Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax2amt" runat="server" Text='<%# bind("addtaxamt") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="tax3 Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txttax3amt" runat="server" Text='<%# bind("cstamt") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvamount" runat="server" Text='<%# bind("amount") %>' ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
                <asp:Button ID="btnClose1" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
            </div>
        </asp:Panel>
    </div>
</asp:Content>
