﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class PackingMaterialList : System.Web.UI.Page
{
    ForPackingMaterial fpmclass = new ForPackingMaterial();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
        }
    }

    public void fillgrid()
    {
        li.istype = "S";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fpmclass.selectallpmmasterdata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvpackingmateriallist.Visible = true;
            gvpackingmateriallist.DataSource = dtdata;
            gvpackingmateriallist.DataBind();
        }
        else
        {
            gvpackingmateriallist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Packing Material Found.";
        }
    }

    protected void btnpackingmaterial_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/PackingMaterialEntry.aspx?mode=insert");
    }

    protected void gvpackingmateriallist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            li.voucherno = Convert.ToInt64(e.CommandArgument);
            Response.Redirect("~/PackingMaterialEntry.aspx?mode=update&vno="+li.voucherno+"");
        }
        else if (e.CommandName == "delete")
        {
            li.voucherno = Convert.ToInt64(e.CommandArgument);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            fpmclass.deletepmmasteredatafromvoucherno(li);
            fpmclass.deletepmitemsedatafromvoucherno(li);
            fillgrid();
        }
    }

    protected void gvpackingmateriallist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
}