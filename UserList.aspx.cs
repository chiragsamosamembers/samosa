﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class UserList : System.Web.UI.Page
{
    ForRegister frclass = new ForRegister();
    ForActivity faclass = new ForActivity();
    ForUserRight furclass = new ForUserRight();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        DataTable dtdata = new DataTable();
        dtdata = frclass.selectalluser();
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvuserlist.Visible = true;
            gvuserlist.DataSource = dtdata;
            gvuserlist.DataBind();
        }
        else
        {
            gvuserlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No User Data Found.";
        }
    }

    protected void gvuserlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.id = Convert.ToInt64(e.CommandArgument);
                Response.Redirect("~/AddNewUser.aspx?mode=update&code=" + li.id + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.id = Convert.ToInt64(e.CommandArgument);
                frclass.deleteuserdata(li);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.activity = li.id + " User Name Deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
    }
    protected void gvuserlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvuserlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvuserlist.PageIndex = e.NewPageIndex;
        fillgrid();
    }
    protected void btnuser_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddNewUser.aspx?mode=insert");
    }
}