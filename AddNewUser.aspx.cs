﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Data.SqlClient;

public partial class AddNewUser : System.Web.UI.Page
{
    SqlCommand cmd;
    SqlDataAdapter da;
    DataTable dtdata;
    DataTable dt;
    ForUserRight furclass = new ForUserRight();
    ForRegister frclass = new ForRegister();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "update")
            {
                txtcompanyid.Text = Request.Cookies["ForCompany"]["cno"];
                txtcompanyid.ReadOnly = true;
                filluserrightdrop();
                filleditgrid();
            }
            else
            {
                txtpassword.TextMode = TextBoxMode.Password;
                txtconfirmpassword.TextMode = TextBoxMode.Password;
                txtcompanyid.Text = Request.Cookies["ForCompany"]["cno"];
                txtcompanyid.ReadOnly = true;
                filluserrightdrop();
            }
        }
    }

    public void filleditgrid()
    {
        li.id = Convert.ToInt64(Request["code"].ToString());
        DataTable dtdata = new DataTable();
        dtdata = furclass.selectuserdatafromid(li);
        if (dtdata.Rows.Count > 0)
        {
            ViewState["code"] = li.id;
            txtusername.Text = dtdata.Rows[0]["username"].ToString();
            txtpassword.Text = dtdata.Rows[0]["password"].ToString();
            txtconfirmpassword.Text = dtdata.Rows[0]["password"].ToString();
            drprole.SelectedValue = dtdata.Rows[0]["role"].ToString();
            //txtpassword.TextMode = TextBoxMode.Password;
            //txtconfirmpassword.TextMode = TextBoxMode.Password;
            btnsaves.Text = "Update";

        }
    }

    public void filluserrightdrop()
    {
        DataTable dtrole = new DataTable();
        dtrole = furclass.selectallrole();
        if (dtrole.Rows.Count > 0)
        {
            drprole.DataSource = dtrole;
            drprole.DataTextField = "name";
            drprole.DataBind();
            drprole.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drprole.Items.Clear();
            drprole.Items.Insert(0, "--SELECT--");
        }
    }

    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }
    //lblcaketitle.Text = Decrypt(HttpUtility.UrlDecode(Request.QueryString["name"]));
    //lblprice.Text = (Convert.ToDouble(Decrypt(HttpUtility.UrlDecode(Request.QueryString["price"]))) * 50).ToString();

    protected void btnsaves_Click(object sender, EventArgs e)
    {
        li.username = txtusername.Text;
        li.password = txtpassword.Text;
        if (chkactive.Checked == true)
        {
            li.isactive = "Y";
        }
        else
        {
            li.isactive = "N";
        }
        li.role = drprole.SelectedItem.Text;
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        if (btnsaves.Text == "Save")
        {
            frclass.insertlogindata(li);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = "New User added - " + txtusername.Text + ".";
            faclass.insertactivity(li);
        }
        else
        {
            li.id = Convert.ToInt64(ViewState["code"].ToString());
            frclass.updatelogindata(li);
            li.activity = li.id + " User Data Updated.";
            faclass.insertactivity(li);
        }        
        if (Request.Cookies["ForLogin"]["acyear"] != "2017-2018")
        {
            //string dbname = "AT" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0] + Request.Cookies["ForLogin"]["acyear"].Split('-')[1];
            SqlConnection con111 = new SqlConnection("Data Source=CHIRAG-PC;Initial Catalog=AT20172018;Integrated Security=True");
            SqlCommand cmd = new SqlCommand("insert into login (username,password,cno,isactive,role) values (@username,@password,@cno,@isactive,@role)", con111);
            cmd.Parameters.AddWithValue("@username", li.username);
            cmd.Parameters.AddWithValue("@password", li.password);
            cmd.Parameters.AddWithValue("@cno", li.cno);
            cmd.Parameters.AddWithValue("@isactive", li.isactive);
            cmd.Parameters.AddWithValue("@role", li.role);
            con111.Open();
            cmd.ExecuteNonQuery();
            con111.Close();
        }
        txtusername.Text = string.Empty;
        txtpassword.Text = string.Empty;
        chkactive.Checked = false;
        Response.Redirect("~/UserList.aspx?pagename=AddNewUser");
    }
}