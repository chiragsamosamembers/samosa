﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Survey.Classes;

public partial class PackingMaterialEntry : System.Web.UI.Page
{

    ForPackingMaterial fpmclass = new ForPackingMaterial();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["mode"].ToString() == "insert")
            {
                txtvdate.Text = System.DateTime.Now.ToString("dd-MM-yyyy");
                Session["dtpitemspme"] = CreateTemplate();
            }
            else
            {
                filleditdata();
            }
            txtuser.Text = Request.Cookies["ForLogin"]["username"];
        }
    }

    public void filleditdata()
    {
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.voucherno = Convert.ToInt64(Request["vno"].ToString());
        DataTable dtdata = new DataTable();
        dtdata = fpmclass.selectallpmmasterdatafromvoucherno(li);
        if (dtdata.Rows.Count > 0)
        {
            txtvoucherno.Text = dtdata.Rows[0]["voucherno"].ToString();
            txtvdate.Text = Convert.ToDateTime(dtdata.Rows[0]["vdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtname.Text = dtdata.Rows[0]["name"].ToString();
            txtclientcode.Text = dtdata.Rows[0]["clientcode"].ToString();
            txtdcno.Text = dtdata.Rows[0]["dcno"].ToString();
            txtreturnablematerialchallanno.Text = dtdata.Rows[0]["rmcno"].ToString();
            txttoolsdeliverychallanno.Text = dtdata.Rows[0]["tdcno"].ToString();
            txttransportname.Text = dtdata.Rows[0]["transportname"].ToString();
            txtlrno.Text = dtdata.Rows[0]["lrno"].ToString();
            txtlrdate.Text = Convert.ToDateTime(dtdata.Rows[0]["lrdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MM-yyyy");
            txtvehicleno.Text = dtdata.Rows[0]["vehicleno"].ToString();
            txtdrivername.Text = dtdata.Rows[0]["drivername"].ToString();
            txtdriverlicenseno.Text = dtdata.Rows[0]["dlicenseno"].ToString();
            txtdrivermobileno.Text = dtdata.Rows[0]["mobileno"].ToString();
            btnsave.Text = "Update";
        }
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("id", typeof(Int64));
        dtpitems.Columns.Add("dcno", typeof(Int64));
        return dtpitems;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetClientname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallclientname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString() + "-" + dt.Rows[i][2].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getrrcno(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallrrcno(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getdelcno(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectalldelcno(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][0].ToString());
        }
        return CountryNames;
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        li.voucherdate = Convert.ToDateTime(txtvdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.name = txtname.Text;
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            li.ccode = Convert.ToInt64(txtclientcode.Text);
        }
        else
        {
            li.ccode = 0;
        }
        if (txtdcno.Text.Trim() != string.Empty)
        {
            li.dcno = Convert.ToInt64(txtdcno.Text);
        }
        else
        {
            li.dcno = 0;
        }
        if (txtreturnablematerialchallanno.Text.Trim() != string.Empty)
        {
            li.rmcno = Convert.ToInt64(txtreturnablematerialchallanno.Text);
        }
        else
        {
            li.rmcno = 0;
        }
        if (txttoolsdeliverychallanno.Text.Trim() != string.Empty)
        {
            li.tdcno = Convert.ToInt64(txttoolsdeliverychallanno.Text);
        }
        else
        {
            li.tdcno = 0;
        }
        li.transportname = txttransportname.Text;
        li.lrno = txtlrno.Text;
        li.lrdate = Convert.ToDateTime(txtlrdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.vehicleno = txtvehicleno.Text;
        li.drivername = txtdrivername.Text;
        li.licenseno = txtdriverlicenseno.Text;
        li.mobileno = txtdrivermobileno.Text;
        li.istype = "S";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.uname = Request.Cookies["ForLogin"]["username"];
        li.udate = System.DateTime.Now;
        if (btnsave.Text == "Save")
        {
            fpmclass.insertpmmasterdeldata(li);
            li.voucherno = SessionMgt.voucherno;
            fpmclass.insertpmitemserdeldata(li);
        }
        else
        {
            li.voucherno = Convert.ToInt64(Request["vno"].ToString());
            fpmclass.updatepmmasterdeldata(li);
            fpmclass.updatepmitemsdeldata(li);
        }
        Response.Redirect("~/PackingMaterialEntryList.aspx");
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> Getacname(string prefixText)
    {
        ForToolsMaster ftmclass = new ForToolsMaster();
        LogicLayer li = new LogicLayer();
        li.cno = Convert.ToInt64(HttpContext.Current.Request.Cookies["ForCompany"]["cno"]);
        li.name = prefixText;
        DataTable dt = new DataTable();
        dt = ftmclass.selectallacname(li);
        List<string> CountryNames = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountryNames.Add(dt.Rows[i][1].ToString());
        }
        return CountryNames;
    }

    protected void txtname_TextChanged(object sender, EventArgs e)
    {
        if (txtclientcode.Text.Trim() != string.Empty)
        {
            string cc = txtclientcode.Text;
            //txtclientcode.Text = cc.Split('-')[1];
            txtclientcode.Text = cc.Split('-')[0];
        }
        else
        {
            //txtname.Text = string.Empty;
            txtclientcode.Text = string.Empty;
        }
    }

}