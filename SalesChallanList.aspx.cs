﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

public partial class SalesChallanList : System.Web.UI.Page
{
    public ReportDocument rptdoc;
    ForSalesChallan fscclasss = new ForSalesChallan();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillchallantypedrop();
            fillgrid();
            selectrights();
        }
    }

    public void fillchallantypedrop()
    {
        DataTable dtdata = new DataTable();
        dtdata = fscclasss.selectallchallantype(li);
        if (dtdata.Rows.Count > 0)
        {
            drpchallantype.Items.Clear();
            drpchallantype.DataSource = dtdata;
            drpchallantype.DataTextField = "name";
            drpchallantype.DataBind();
            drpchallantype.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drpchallantype.Items.Clear();
            drpchallantype.Items.Insert(0, "--SELECT--");
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        Session["dtpitems"] = CreateTemplate();
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        con.Open();
        string query = "select * from SCMaster order by scno desc";
        SqlCommand cmdn = new SqlCommand(query, con);
        SqlDataReader user = cmdn.ExecuteReader();
        string unreadtext = "";
        Int32 i = 0;
        while (user.Read())
        {
            unreadtext += "<tr>";
            unreadtext += "<td>" + user["strscno"] + "</td>";
            unreadtext += "<td>" + user["scno"] + "</td>";
            unreadtext += "<td>" + user["scdate"] + "</td>";
            unreadtext += "<td>" + user["acname"] + "</td>";
            unreadtext += "<td>" + user["ccode"] + "</td>";
            unreadtext += "<td>" + user["issigned"] + "</td>";
            unreadtext += "<td>" + user["signremarks"] + "</td>";
            unreadtext += "<td><a class=\"btn btn-info\" href=\"SalesChallan.aspx?mode=update&scdate=" + user["scdate"] + "&strscno=" + user["strscno"] + "\">Edit</a></td>";
            unreadtext += "<td><a class=\"btn btn-info\" href=\"Edit.aspx?strscno=" + user["strscno"] + "\">Delete</a></td>";            
            unreadtext += "</tr>";
            tlist.InnerHtml = unreadtext;
            i++;
        }
        con.Close();
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        SqlDataAdapter da111 = new SqlDataAdapter("select ctype from SCMaster where cno=1 group by ctype order by ctype desc", con);
        da111.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
        DataTable dtcc = new DataTable();
        da111.Fill(dtcc);
        DataTable dtall = new DataTable();
        for (int c = 0; c < dtcc.Rows.Count; c++)
        {
            li.ctype = dtcc.Rows[c]["ctype"].ToString();
            SqlDataAdapter da = new SqlDataAdapter("select * from SCMaster where ctype='" + li.ctype + "' and cno=" + li.cno + " order by scno desc", con);
            DataTable dtc = new DataTable();
            da.Fill(dtc);
            if (dtc.Rows.Count > 0)
            {
                dtall.Merge(dtc);
            }
        }
        //DataTable dtq = (DataTable)Session["dtpitems"];
        //for (int c = 0; c < dtcc.Rows.Count; c++)
        //{
        //    DataRow dr1 = dtq.NewRow();
        //    dr1["strscno"] = dtcc.Rows[c]["strscno"].ToString();
        //    dr1["scno"] = dtcc.Rows[c]["scno"].ToString();
        //    dr1["scdate"] = Convert.ToDateTime(dtcc.Rows[c]["scdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //    dr1["ccode"] = dtcc.Rows[c]["ccode"].ToString();
        //    dr1["acname"] = dtcc.Rows[c]["acname"].ToString();
        //    li.strscno = dtcc.Rows[c]["strscno"].ToString();
        //    string cc = "'" + li.strscno + "'";
        //    SqlDataAdapter da = new SqlDataAdapter("select * from SIMaster where (scno=@scno or scno1=@scno1 or stringscno like '%'+@fullname+'%') and cno=@cno", con);
        //    da.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
        //    da.SelectCommand.Parameters.AddWithValue("@scno", li.strscno);
        //    da.SelectCommand.Parameters.AddWithValue("@scno1", li.strscno);
        //    da.SelectCommand.Parameters.AddWithValue("@fullname", li.strscno);
        //    DataTable dtc = new DataTable();
        //    da.Fill(dtc);
        //    if (dtc.Rows.Count > 0)
        //    {
        //        dr1["strsino"] = dtc.Rows[0]["strsino"].ToString();
        //    }
        //    else
        //    {
        //        dr1["strsino"] = "";
        //    }
        //    dtq.Rows.Add(dr1);
        //}
        //Session["dtpitems"] = dtq;



        DataTable dtdata = new DataTable();
        dtdata = fscclasss.selectallsaleschallandata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvsaleschallanlist.Visible = true;
            gvsaleschallanlist.DataSource = dtall;
            gvsaleschallanlist.DataBind();
        }
        else
        {
            gvsaleschallanlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Sales Cahllan Data Found.";
        }
    }

    protected void gvsaleschallanlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.strscno = e.CommandArgument.ToString();
                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                Label lblbscdate = (Label)row.FindControl("lblscdate");
                Response.Redirect("~/SalesChallan.aspx?mode=update&scdate=" + lblbscdate.Text + "&strscno=" + li.strscno + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.strscno = e.CommandArgument.ToString();

                DataTable dtcheck = new DataTable();
                dtcheck = fscclasss.selectbillinvoicedatafromstrscno(li);
                if (dtcheck.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Challan Used in Sales Invoice No. " + dtcheck.Rows[0]["strsino"].ToString() + " So You Cant delete this Challan.First delete that invoice then try to delete this challan.');", true);
                    return;
                }
                else
                {
                    DataTable dtdata1 = new DataTable();
                    dtdata1 = fscclasss.selectallsaleschallandatafromscnostrscno(li);
                    //li.sono =Convert.ToInt64(dtdata1.Rows[0]["sono1"].ToString());
                    //li.strsono = dtdata1.Rows[0]["strscno"].ToString();
                    DataTable dtdata = new DataTable();
                    dtdata = fscclasss.selectallscitemsfromscnostring(li);
                    for (int c = 0; c < dtdata.Rows.Count; c++)
                    {
                        li.vid = Convert.ToInt64(dtdata.Rows[c]["vid"].ToString());
                        li.id = Convert.ToInt64(dtdata.Rows[c]["id"].ToString());
                        li.itemname = dtdata.Rows[c]["itemname"].ToString();
                        li.qty = Convert.ToDouble(dtdata.Rows[c]["qty"].ToString());
                        if (li.vid != 0)
                        {
                            DataTable dtqty = new DataTable();
                            dtqty = fscclasss.selectqtyremainusedfromsono(li);
                            if (dtqty.Rows.Count > 0)
                            {
                                li.qtyremain = Convert.ToDouble(dtqty.Rows[0]["qtyremain"].ToString()) + li.qty;
                                li.qtyused = Convert.ToDouble(dtqty.Rows[0]["qtyused"].ToString()) - li.qty;
                                fscclasss.updateqtyduringdelete(li);
                            }
                        }
                    }

                    li.strscno = "";
                    if (dtdata1.Rows.Count > 0)
                    {
                        li.sono = Convert.ToInt64(dtdata1.Rows[0]["sono1"].ToString());
                        fscclasss.updatestrscnoinsalesorder(li);
                    }
                    li.strscno = e.CommandArgument.ToString();
                    fscclasss.deletescmasterdatastring(li);
                    fscclasss.deletescitemsdatastring(li);
                    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                    li.uname = Request.Cookies["ForLogin"]["username"];
                    li.udate = System.DateTime.Now;
                    li.activity = li.strscno + " Sales Challan Deleted.";
                    faclass.insertactivity(li);
                    fscclasss.updateisusedn(li);
                    fillgrid();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
        else if (e.CommandName == "print")
        {
            rptdoc = new ReportDocument();
            string Xrepname = "sales challan";
            string strscno = e.CommandArgument.ToString();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?strscno=" + strscno + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            string ftow = "";
            li.strscno = strscno;
            DataSet1 imageDataSet = new DataSet1();
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
            //SqlDataAdapter da11 = new SqlDataAdapter("select * from SCMaster inner join SCItems on SCMaster.scno=SCItems.scno inner join ACMaster on ACMaster.acname=SCMaster.acname where SCMaster.scno='" + li.scno + "' and SCMaster.cno='" + li.cno + "'", con);
            SqlDataAdapter da11 = new SqlDataAdapter("select * from SCMaster inner join SCItems on SCMaster.strscno=SCItems.strscno inner join ACMaster on ACMaster.acname=SCMaster.acname where SCMaster.strscno='" + li.strscno + "' and SCMaster.cno='" + li.cno + "' order by SCItems.id", con);
            DataTable dtc = new DataTable();
            da11.Fill(imageDataSet.Tables["DataTable2"]);
            //if (imageDataSet.Tables["DataTable2"].Rows.Count > 0)
            //{
            //    string amount = imageDataSet.Tables["DataTable2"].Rows[0]["grandtotal"].ToString();
            //    ftow = changeToWords(amount);
            //}
            li.acname = imageDataSet.Tables["DataTable2"].Rows[0]["acname"].ToString();
            li.ccode = Convert.ToInt64(imageDataSet.Tables["DataTable2"].Rows[0]["ccode"].ToString());
            con.Open();
            DataTable dtacname = new DataTable();
            SqlDataAdapter das = new SqlDataAdapter("select * from ACMaster where acname=@acname and cno=@cno", con);
            das.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
            das.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            das.Fill(dtacname);
            con.Close();

            con.Open();
            DataTable dtclient = new DataTable();
            SqlDataAdapter das1 = new SqlDataAdapter("select * from ClientMaster where code=@ccode and cno=@cno", con);
            das1.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            das1.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
            das1.Fill(dtclient);
            con.Close();
            string address = dtacname.Rows[0]["add1"].ToString() + " " + dtacname.Rows[0]["add2"].ToString() + " " + dtacname.Rows[0]["add3"].ToString();
            string rptname;
            //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
            //{
            rptname = Server.MapPath(@"~/Reports/DeliveryChallan.rpt");
            //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
            //}
            //else
            //{
            //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
            //    {
            //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
            //    }
            //    else
            //    {
            //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
            //    }
            //}
            rptdoc.Load(rptname);
            if (dtacname.Rows.Count > 0)
            {

            }
            //string title = "";
            //if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
            //{
            //    title = "Tax Invoice";
            //    rptdoc.DataDefinition.FormulaFields["rtitle"].Text = "'" + title + "'";
            //}
            //else
            //{
            //    title = "Retail Invoice";
            //    rptdoc.DataDefinition.FormulaFields["rtitle"].Text = "'" + title + "'";
            //}
            string ccode = dtclient.Rows[0]["name"].ToString();
            rptdoc.DataDefinition.FormulaFields["gst1"].Text = "'" + dtacname.Rows[0]["gsttinno"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["gst2"].Text = "'" + dtacname.Rows[0]["date1"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["cst1"].Text = "'" + dtacname.Rows[0]["csttinno"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["cst2"].Text = "'" + dtacname.Rows[0]["date2"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["email"].Text = "'" + dtacname.Rows[0]["emailid"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["gstno"].Text = "'" + dtacname.Rows[0]["gstno"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["city"].Text = "'" + dtacname.Rows[0]["city"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["clientname"].Text = "'" + ccode + "'";
            //rptdoc.DataDefinition.FormulaFields["ftow"].Text = "'" + ftow + "'";
            //rptdoc.DataDefinition.FormulaFields["cramount"].Text = "'" + dtcc.Rows[0]["cramount"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["add1"].Text = "'" + imageDataSet.Tables["DataTable2"].Rows[0]["add1"].ToString().Trim().Replace("'", "") + "'";
            rptdoc.DataDefinition.FormulaFields["add2"].Text = "'" + imageDataSet.Tables["DataTable2"].Rows[0]["add2"].ToString().Trim().Replace("'", "") + "'";
            rptdoc.DataDefinition.FormulaFields["add3"].Text = "'" + imageDataSet.Tables["DataTable2"].Rows[0]["add3"].ToString().Trim().Replace("'", "") + "'";
            rptdoc.DataDefinition.FormulaFields["cadd1"].Text = "'" + dtclient.Rows[0]["add1"].ToString().Trim().Replace("'", "") + "'";
            rptdoc.DataDefinition.FormulaFields["cadd2"].Text = "'" + dtclient.Rows[0]["add2"].ToString().Trim().Replace("'", "") + "'";
            rptdoc.DataDefinition.FormulaFields["cadd3"].Text = "'" + dtclient.Rows[0]["add3"].ToString().Trim().Replace("'", "") + "'";
            rptdoc.SetDataSource(imageDataSet.Tables["DataTable2"]);
            //rptdoc.PrintOptions.PrinterName = "Canon LBP6000/LBP6018";
            //rptdoc.PrintToPrinter(1, false, 0, 0);
            //CrystalReportViewer1.ReportSource = rptdoc;
            ExportOptions exprtopt = default(ExportOptions);
            string fname = "saleschallan.pdf";
            //create instance for destination option - This one is used to set path of your pdf file save 
            DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
            destiopt.DiskFileName = Server.MapPath(@"~/cheque/" + fname);


            exprtopt = rptdoc.ExportOptions;
            exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

            //use PortableDocFormat for PDF data
            exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
            exprtopt.DestinationOptions = destiopt;

            //finally export your report document
            rptdoc.Export();
            rptdoc.Close();
            rptdoc.Clone();
            rptdoc.Dispose();
            string path = fname;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('SCPrinting.aspx?&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }
    protected void gvsaleschallanlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void btnsaleschallan_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/SalesChallan.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvsaleschallanlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvsaleschallanlist.PageIndex = e.NewPageIndex;
        fillgrid();
    }

    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("scno", typeof(string));
        dtpitems.Columns.Add("strscno", typeof(string));
        dtpitems.Columns.Add("scdate", typeof(DateTime));
        dtpitems.Columns.Add("acname", typeof(string));
        dtpitems.Columns.Add("ccode", typeof(string));
        dtpitems.Columns.Add("strsino", typeof(string));
        return dtpitems;
    }

    protected void chksigned_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow1 = (GridViewRow)((CheckBox)sender).Parent.Parent;
        CheckBox chksigned = (CheckBox)currentRow1.FindControl("chksigned");
        Label lblstrscno = (Label)currentRow1.FindControl("lblstrscno");
        string signedd = "No";
        if (chksigned.Checked == true)
        {
            signedd = "Yes";
        }
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlCommand da = new SqlCommand("update SCMaster set issigned='" + signedd + "' where strscno='" + lblstrscno.Text + "'", con);
        con.Open();
        da.ExecuteNonQuery();
        con.Close();
        fillgrid();
        CheckBox chksigned1 = (CheckBox)gvsaleschallanlist.Rows[currentRow1.RowIndex + 1].FindControl("chksigned");
        Page.SetFocus(chksigned1);
    }
    protected void txtremarks_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox txtremarks = (TextBox)currentRow1.FindControl("txtremarks");
        Label lblstrscno = (Label)currentRow1.FindControl("lblstrscno");
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlCommand da = new SqlCommand("update SCMaster set signremarks='" + txtremarks.Text + "' where strscno='" + lblstrscno.Text + "'", con);
        con.Open();
        da.ExecuteNonQuery();
        con.Close();
        fillgrid();
        TextBox txtremarks1 = (TextBox)gvsaleschallanlist.Rows[currentRow1.RowIndex + 1].FindControl("txtremarks");
        Page.SetFocus(txtremarks1);
    }
    protected void gvsaleschallanlist_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //var gv = (GridView)e.Row.FindControl("gvemember");
            var lblvoucherno = (Label)e.Row.FindControl("lblsigned");
            var chkbox = (CheckBox)e.Row.FindControl("chksigned");
            var txtremarks = (TextBox)e.Row.FindControl("txtremarks");
            if (lblvoucherno.Text == "Yes")
            {
                chkbox.Checked = true;
            }
            if (Request.Cookies["ForLogin"]["username"] != "PSP")
            {
                chkbox.Visible = false;
                txtremarks.Visible = false;
            }
        }
    }

    protected void drpchallantype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpchallantype.SelectedItem.Text == "--SELECT--")
        {
            fillgrid();
        }
        else
        {
            fillgrid1();
        }
    }

    public void fillgrid1()
    {
        Session["dtpitems"] = CreateTemplate();
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        ////SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        //SqlDataAdapter da111 = new SqlDataAdapter("select ctype from SCMaster where cno=1 and ctype='" + drpchallantype.SelectedItem.Text + "'", con);
        //da111.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
        //DataTable dtcc = new DataTable();
        //da111.Fill(dtcc);
        //DataTable dtall = new DataTable();
        //for (int c = 0; c < dtcc.Rows.Count; c++)
        //{
        li.ctype = drpchallantype.SelectedItem.Text;
        SqlDataAdapter da = new SqlDataAdapter("select * from SCMaster where ctype='" + li.ctype + "' and cno=" + li.cno + " order by scno desc", con);
        DataTable dtc = new DataTable();
        da.Fill(dtc);
        //if (dtc.Rows.Count > 0)
        //{
        //    dtall.Merge(dtc);
        //}
        DataTable dtdata = new DataTable();
        dtdata = fscclasss.selectallsaleschallandata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvsaleschallanlist.Visible = true;
            gvsaleschallanlist.DataSource = dtc;
            gvsaleschallanlist.DataBind();
        }
        else
        {
            gvsaleschallanlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Sales Cahllan Data Found.";
        }
    }

}