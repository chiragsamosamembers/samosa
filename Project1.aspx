﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Project1.aspx.cs" Inherits="Project1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Blow Through AHUs</span><br />
        <br />
        <div class="row">
            <div class="col-md-1">
                <label class="control-label">
                    Client Code</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" Width="200px"
                    onkeypress="return checkQuote();" AutoPostBack="True" OnTextChanged="txtclientcode_TextChanged"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender33" runat="server" TargetControlID="txtclientcode"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnprint" runat="server" Text="Print" Height="30px" 
                    BackColor="#F05283" onclick="btnprint_Click" Visible="false"/></div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="400px" Width="100%">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblempty" runat="server" ForeColor="Red" Font-Size="Larger" Font-Bold="true">
                            </asp:Label>
                            <asp:GridView ID="gvproject1" runat="server" AutoGenerateColumns="False" Width="100%"
                                BorderStyle="None" EmptyDataText="”No records found”" Height="0px" CssClass="table table-bordered"
                                OnRowCancelingEdit="gvproject1_RowCancelingEdit" OnRowCommand="gvproject1_RowCommand"
                                OnRowDeleting="gvproject1_RowDeleting" OnRowEditing="gvproject1_RowEditing" OnRowUpdating="gvproject1_RowUpdating"
                                ShowFooter="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Action" SortExpression="companyname">
                                        <EditItemTemplate>
                                            <asp:Button ID="btnupdate" runat="server" CommandName="Update" Text="Update" />
                                            <asp:Button ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Button ID="btnedit" runat="server" CommandName="Edit" Text="Edit" />
                                            <asp:Button ID="btndelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick="return confirmdelete();" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <%--<asp:Button ID="btnadd" runat="server" CommandName="AddNew" Text="AddNew" />--%>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ID" SortExpression="companyname" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblid" runat="server" ForeColor="#505050" Text='<%# bind("id") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AHU No." SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblahuno" runat="server" ForeColor="#505050" Text='<%# bind("ahuno") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtahuno" runat="server" Width="100px" Text='<%# bind("ahuno") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtahunof" runat="server" Width="100px"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Return Air CFM" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblreturnaircfm" runat="server" ForeColor="#505050" Text='<%# bind("returnaircfm") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtreturnaircfm1" runat="server" Width="100px" Text='<%# bind("returnaircfm") %>'
                                                AutoPostBack="true" OnTextChanged="txtreturnaircfm1_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtreturnaircfmf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtreturnaircfmf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Return Air Temp" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblreturnairtemp" runat="server" ForeColor="#505050" Text='<%# bind("returnairtemp") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtreturnairtemp1" runat="server" Width="100px" Text='<%# bind("returnairtemp") %>'
                                                AutoPostBack="true" OnTextChanged="txtreturnairtemp1_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtreturnairtempf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtreturnairtempf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Return Air Grains" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblreturnairgains" runat="server" ForeColor="#505050" Text='<%# bind("returnairgrains") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtreturnairgains1" runat="server" Width="100px" Text='<%# bind("returnairgrains") %>'
                                                AutoPostBack="true" OnTextChanged="txtreturnairgains1_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtreturnairgainsf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtreturnairgainsf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fresh Air CFM" SortExpression="companyname" ItemStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblfreshaircfm" runat="server" ForeColor="#505050" Text='<%# bind("freshaircfm") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtfreshaircfm1" runat="server" Width="100px" Text='<%# bind("freshaircfm") %>'
                                                AutoPostBack="true" OnTextChanged="txtfreshaircfm1_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtfreshaircfmf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtfreshaircfmf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                        <ItemStyle Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fresh Air Temp" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblfreshairtemp" runat="server" ForeColor="#505050" Text='<%# bind("freshairtemp") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtfreshairtemp1" runat="server" Width="100px" Text='<%# bind("freshairtemp") %>'
                                                AutoPostBack="true" OnTextChanged="txtfreshairtemp1_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtfreshairtempf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtfreshairtempf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fresh Air Grains" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblfreshairgrains" runat="server" ForeColor="#505050" Text='<%# bind("freshairgrains") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtfreshairgrains1" runat="server" Width="100px" Text='<%# bind("freshairgrains") %>'
                                                AutoPostBack="true" OnTextChanged="txtfreshairgrains1_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtfreshairgrainsf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtfreshairgrainsf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mixing Air CFM" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblmixingaircfm" runat="server" ForeColor="#505050" Text='<%# bind("mixinaircfm") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtmixingaircfmf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mixing Air Temp" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblmixingairtemp" runat="server" ForeColor="#505050" Text='<%# bind("mixingairtemp") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtmixingairtempf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mixing Air Grains" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblmixingairgrains" runat="server" ForeColor="#505050" Text='<%# bind("mixingairgrains") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtmixingairgrainsf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Fan CFM" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltotalfancfm" runat="server" ForeColor="#505050" Text='<%# bind("totalfancfm") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txttotalfancfmf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Before Fan Air Temp" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbeforefanairtemp" runat="server" ForeColor="#505050" Text='<%# bind("beforefanairtemp") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtbeforefanairtempf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Before Fan Air Grains" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbeforefanairgrains" runat="server" ForeColor="#505050" Text='<%# bind("beforefanairgrains") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtbeforefanairgrainsf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fan Static Pressures" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblfanstaticpressures" runat="server" ForeColor="#505050" Text='<%# bind("fanstaticpressure") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtfanstaticpressures" runat="server" Width="100px" Text='<%# bind("freshairgrains") %>'
                                                AutoPostBack="true" OnTextChanged="txtfanstaticpressures_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtfanstaticpressuresf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtfanstaticpressuresf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Fan KW" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltotalfankw" runat="server" ForeColor="#505050" Text='<%# bind("totalfankw") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txttotalfankwf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Fan BTU/HR" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltotalfanbtuhr" runat="server" ForeColor="#505050" Text='<%# bind("totalfanbtuhr") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txttotalfanbtuhrf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Temp.Diff(Degree F)" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltempdiffdegreef" runat="server" ForeColor="#505050" Text='<%# bind("tempdiffdegreef") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txttempdiffdegreeff" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="After Fan Air Temp" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblafterfanairtemp" runat="server" ForeColor="#505050" Text='<%# bind("afterfabairtemp") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtafterfanairtempf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="After Fan Air Grains" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblafterfanairgrains" runat="server" ForeColor="#505050" Text='<%# bind("afterfanairgrains") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtafterfanairgrainsf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Selected Deh.CFM" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblselecteddehcfm" runat="server" ForeColor="#505050" Text='<%# bind("selecteddehcfm") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtselecteddehcfm1" runat="server" Width="100px" CssClass="txtfirstquotation"
                                                Text='<%# bind("selecteddehcfm") %>' AutoPostBack="true" OnTextChanged="txtselecteddehcfm1_TextChanged"
                                                onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtselecteddehcfmf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtselecteddehcfmf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Actual Deh.Air CFM" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblactualdehaircfm" runat="server" ForeColor="#505050" Text='<%# bind("actualdehaircfm") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtactualdehaircfmf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Deh.Air Temp(After CHW)" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldehairtempafterchw" runat="server" ForeColor="#505050" Text='<%# bind("dehairtempafterchw") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtdehairtempafterchw1" runat="server" Width="100px" CssClass="txtfirstquotation"
                                                Text='<%# bind("dehairtempafterchw") %>' AutoPostBack="true" OnTextChanged="txtdehairtempafterchw1_TextChanged"
                                                onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtdehairtempafterchwf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtdehairtempafterchwf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Deh.Air Grains(After CHW)" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldehairgrainsafterchw" runat="server" ForeColor="#505050" Text='<%# bind("dehairgrainsafterchw") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtdehairgrainsafterchw1" runat="server" Width="100px" CssClass="txtfirstquotation"
                                                Text='<%# bind("dehairgrainsafterchw") %>' AutoPostBack="true" OnTextChanged="txtdehairgrainsafterchw1_TextChanged"
                                                onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtdehairgrainsafterchwf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtdehairgrainsafterchwf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bypass Air CFM" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbypassaircfm" runat="server" ForeColor="#505050" Text='<%# bind("bypassaircfm") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtbypassaircfmf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bypass Air Temp" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbypassairtemp" runat="server" ForeColor="#505050" Text='<%# bind("bypassairtemp") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtbypassairtempf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bypass Air Grains" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbypassairgrains" runat="server" ForeColor="#505050" Text='<%# bind("bypassairgrains") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtbypassairgrainsf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exhaust CFM" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblexhaustcfm" runat="server" ForeColor="#505050" Text='<%# bind("exhaustcfm") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtexhaustcfm1" runat="server" Width="100px" CssClass="txtfirstquotation"
                                                Text='<%# bind("exhaustcfm") %>' AutoPostBack="true" OnTextChanged="txtexhaustcfm1_TextChanged"
                                                onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtexhaustcfmf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtexhaustcfmf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Supply CFM" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsupplycfm" runat="server" ForeColor="#505050" Text='<%# bind("supplycfm") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtsupplycfmf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sensible Load(BTU/HR)from Heat Load" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsensibleloadfromheatload" runat="server" ForeColor="#505050" Text='<%# bind("sensibleloadfromheatload") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtsensibleloadfromheatload1" runat="server" Width="100px" CssClass="txtfirstquotation"
                                                Text='<%# bind("sensibleloadfromheatload") %>' AutoPostBack="true" OnTextChanged="txtsensibleloadfromheatload1_TextChanged"
                                                onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtsensibleloadfromheatloadf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtsensibleloadfromheatloadf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Diversity of Sensible Load(Min.load available in the room)"
                                        SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldiversityofsensibleload" runat="server" ForeColor="#505050" Text='<%# bind("diversityofsensibleloadminload") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtdiversityofsensibleload1" runat="server" Width="100px" CssClass="txtfirstquotation"
                                                Text='<%# bind("diversityofsensibleloadminload") %>' AutoPostBack="true" OnTextChanged="txtdiversityofsensibleload1_TextChanged"
                                                onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtdiversityofsensibleloadf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtdiversityofsensibleloadf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Diversified Sensible Load(50% Diversity)" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldiversifiedsensibleload" runat="server" ForeColor="#505050" Text='<%# bind("diversityofsensibleloaddiversity") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtdiversifiedsensibleloadf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Supply DB" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsupplydb" runat="server" ForeColor="#505050" Text='<%# bind("suppludb") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtsupplydbf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Latern Load(BTU/HR)" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllaterloadbtuhr" runat="server" ForeColor="#505050" Text='<%# bind("latentload") %>'
                                                Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtlaterloadbtuhr1" runat="server" Width="100px" CssClass="txtfirstquotation"
                                                Text='<%# bind("latentload") %>' AutoPostBack="true" OnTextChanged="txtlaterloadbtuhr1_TextChanged"
                                                onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtlaterloadbtuhrf" runat="server" Width="100px" AutoPostBack="true"
                                                OnTextChanged="txtlaterloadbtuhrf_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Supply Grains" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsupplygrains" runat="server" ForeColor="#505050" Text='<%# bind("supplygrains") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtsupplygrainsf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Hot Water Outlet Condition DB" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblhotwateroutletconditiondb" runat="server" ForeColor="#505050" Text='<%# bind("hotwateroutletconditiondb") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txthotwateroutletconditiondbf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Hot Water Outlet Condition Grains" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblhotwateroutletconditiongrains" runat="server" ForeColor="#505050"
                                                Text='<%# bind("hotwateroutletconditiongrains") %>' Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txthotwateroutletconditiongrainsf" runat="server" Width="100px"
                                                onkeypress="javascript:return isNumber (event)" ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sensible Load for Hot Water Coil" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsesibleloadforhotwatercoil" runat="server" ForeColor="#505050"
                                                Text='<%# bind("sensibleloadforhotwatercoil") %>' Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtsesibleloadforhotwatercoilf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Hot Water Coil Capacity of Sensible Load" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblhotwatercoilcapacityofsensibleload" runat="server" ForeColor="#505050"
                                                Text='<%# bind("hotwatercoilcapacityofsensibleload") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txthotwatercoilcapacityofsensibleload1" runat="server" Width="100px"
                                                CssClass="txtfirstquotation" Text='<%# bind("hotwatercoilcapacityofsensibleload") %>'
                                                AutoPostBack="true" OnTextChanged="txthotwatercoilcapacityofsensibleload1_TextChanged"
                                                onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txthotwatercoilcapacityofsensibleloadf" runat="server" Width="100px"
                                                AutoPostBack="true" OnTextChanged="txthotwatercoilcapacityofsensibleloadf_TextChanged"
                                                onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Check Point(For RH)" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcheckpointforrh" runat="server" ForeColor="#505050" Text='<%# bind("checkpointforrh") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtcheckpointforrhf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Check Point(for Temp) 1" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcheckpointfortemp" runat="server" ForeColor="#505050" Text='<%# bind("checkpointfortemp") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtcheckpointfortempf" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Check Point(for Temp) 2" SortExpression="companyname">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcheckpointfortemp2" runat="server" ForeColor="#505050" Text='<%# bind("checkpointfortemp1") %>'
                                                Width="50px"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtcheckpointfortemp2f" runat="server" Width="100px" onkeypress="javascript:return isNumber (event)"
                                                ReadOnly="true"></asp:TextBox>
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" SortExpression="companyname">
                                        <EditItemTemplate>
                                            <%--<asp:Button ID="btnupdate" runat="server" CommandName="Update" Text="Update" />
                                            <asp:Button ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel" />--%>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <%--<asp:Button ID="btnedit" runat="server" CommandName="Edit" Text="Edit" />
                                            <asp:Button ID="btndelete" runat="server" CommandName="Delete" Text="Delete" />--%>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="btnadd" runat="server" CommandName="AddNew" Text="AddNew" />
                                        </FooterTemplate>
                                        <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#4c4c4c" />
                                <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                                <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <%--<span style="color: white; background-color: Red">Activity</span>--%>
            </div>
        </div>
    </div>
</asp:Content>
