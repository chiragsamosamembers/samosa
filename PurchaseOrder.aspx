﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PurchaseOrder.aspx.cs" Inherits="PurchaseOrder" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Purchase Order</span><br />
        <div class="row" id="asasass" runat="server" visible="false">
            <div class="col-md-1">
                <asp:Button ID="btnfirst" runat="server" Text="First" OnClick="btnfirst_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnprevious" runat="server" Text="Previous" OnClick="btnprevious_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnnext" runat="server" Text="Next" OnClick="btnnext_Click" /></div>
            <div class="col-md-1">
                <asp:Button ID="btnlast" runat="server" Text="Last" OnClick="btnlast_Click" /></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            PO No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtpurchaseorderno" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtpodate" runat="server" CssClass="form-control" Width="150px"
                            AutoPostBack="True" OnTextChanged="txtpodate_TextChanged"></asp:TextBox></div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Delivery Days<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtdeliverydays" runat="server" CssClass="form-control" Width="70px"
                            onkeypress="javascript:return isNumber (event)" AutoPostBack="True" OnTextChanged="txtdeliverydays_TextChanged"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            User</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="100px" ReadOnly="true"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Client Code<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtclientcode" runat="server" CssClass="form-control" Width="393px"
                            AutoPostBack="true" OnTextChanged="txtname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtclientcode"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetClientname">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Follow Up Date<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtfollowupdate" runat="server" CssClass="form-control" Width="120px"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Name<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-5">
                        <%-- <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="393px" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txtname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetAccountname">
                        </asp:AutoCompleteExtender>--%>
                        <asp:DropDownList ID="drpacname" runat="server" CssClass="form-control" AutoPostBack="True"
                            OnSelectedIndexChanged="drpacname_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Follow Up Details<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtfollowupdetails" runat="server" CssClass="form-control" Width="250px"
                            TextMode="MultiLine" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Sales Order<span style="color: Red;">*</span></label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtsalesorder" runat="server" CssClass="form-control" Width="150px"
                            placeholder="Alt + s"></asp:TextBox><%--OnTextChanged="txtsalesorder_TextChanged"--%>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtsalesorder"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getsono">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-1">
                        <asp:Label ID="lblcount" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="22px"></asp:Label></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Order Type
                        </label>
                    </div>
                    <div class="col-md-1">
                        <asp:DropDownList ID="drpordertype" runat="server" CssClass="form-control" Width="80px">
                            <asp:ListItem>GST</asp:ListItem>
                            <asp:ListItem>VAT</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">
                            Purchase Challan Details
                        </label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtfollowupdetails1" runat="server" CssClass="form-control" Width="250px"
                            TextMode="MultiLine" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Item Name</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Qty</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Unit</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Rate</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Basic Amount</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Tax Type
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    VAT%
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Ad.Vat%
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    CST%
                </label>
            </div>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    CST</label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 1</label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 2</label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 3</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    VAT</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Add.Vat</label>
            </div>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    Amount</label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2" style="border-right: 1px solid;">
                <%--<asp:TextBox ID="txtitemname" runat="server" CssClass="form-control" placeholder="Item Name"
                    Width="200px" AutoPostBack="True" OnTextChanged="txtitemname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="server" TargetControlID="txtitemname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getitemname">
                </asp:AutoCompleteExtender>--%>
                <asp:DropDownList ID="drpitemname" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnSelectedIndexChanged="drpitemname_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" placeholder="Qty"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtqty_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" placeholder="Unit"
                    Width="90px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtrate" runat="server" CssClass="form-control" placeholder="Rate"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtrate_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtbasicamt" runat="server" CssClass="form-control" placeholder="Basic Amt."
                    Width="90px" AutoPostBack="True" OnTextChanged="txtbasicamt_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txttaxtype" runat="server" CssClass="form-control" placeholder="Tax Type"
                    Width="90px" AutoPostBack="True" OnTextChanged="txttaxtype_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txttaxtype"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getvattype">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvvatp" runat="server" CssClass="form-control" placeholder="VAT%"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtgvvatp_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvadvatp" runat="server" CssClass="form-control" placeholder="Ad.Vat%"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtgvadvatp_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvcst" runat="server" CssClass="form-control" placeholder="CST%"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtgvcst_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtcstamount" runat="server" CssClass="form-control" placeholder="CST"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtcstamount_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescr1" runat="server" CssClass="form-control" placeholder="Description 1"
                    Width="200px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescr2" runat="server" CssClass="form-control" placeholder="Description 2"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescr3" runat="server" CssClass="form-control" placeholder="Description 3"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvvat" runat="server" CssClass="form-control" placeholder="VAT"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtgvvat_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvadvat" runat="server" CssClass="form-control" placeholder="Add.Vat"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtgvadvat_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvamount" runat="server" CssClass="form-control" placeholder="Amount"
                    Width="90px" AutoPostBack="True" ReadOnly="true" OnTextChanged="txtgvamount_TextChanged"
                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnadd" runat="server" Text="Add" Height="30px" BackColor="#F05283"
                    ValidationGroup="valgvpurchaseorder" OnClick="btnadd_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="valgvpurchaseorder" />
            </div>
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvpoitemlist" runat="server" AutoGenerateColumns="False" Width="1300px"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowDeleting="gvpoitemlist_RowDeleting" OnRowCommand="gvpoitemlist_RowCommand">
                        <%--OnRowDataBound="gvpoitemlist_RowDataBound"--%>
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="update1" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete123();"
                                        CommandArgument='<%# bind("vid") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PI No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VNo." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvno" ForeColor="Black" runat="server" Text='<%# bind("vno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'
                                        Visible="false"></asp:Label><asp:TextBox ID="txtgvqty" runat="server" Text='<%# bind("qty") %>'
                                            Width="50px" AutoPostBack="True" OnTextChanged="txtgvqty_TextChanged"></asp:TextBox>--%>
                                    <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                    <asp:Label ID="lblqtyremain" runat="server" ForeColor="Green" Text='<%# bind("qtyremain") %>'></asp:Label>
                                    <asp:Label ID="lblqtyused" runat="server" ForeColor="Red" Text='<%# bind("qtyused") %>'></asp:Label>
                                    <asp:TextBox ID="txtgridqty" runat="server" Text='<%# bind("qty") %>' Width="80px"
                                        AutoPostBack="True" OnTextChanged="txtgridqty_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtgvunit" runat="server" Width="50px" Text='<%# bind("unit") %>'></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender222" runat="server" TargetControlID="txtgvunit"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="Getunit">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'
                                        Visible="false"></asp:Label><asp:TextBox ID="txtgvrate" runat="server" Text='<%# bind("rate") %>'
                                            Width="100px" AutoPostBack="True" OnTextChanged="txtgvrate_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="basic Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbasicamt" ForeColor="Black" runat="server" Text='<%# bind("basicamount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT Type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbltaxtype" ForeColor="Black" runat="server" Text='<%# bind("taxtype") %>'></asp:Label><asp:TextBox
                                        ID="txtgridtaxtype" runat="server" Text='<%# bind("taxtype") %>' Width="120px"
                                        AutoPostBack="True" OnTextChanged="txtgridtaxtype_TextChanged"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtgridtaxtype"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="Getvattype">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvatp" ForeColor="Black" runat="server" Text='<%# bind("vatp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvatamt" ForeColor="Black" runat="server" Text='<%# bind("vatamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add VAT%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladdvatp" ForeColor="Black" runat="server" Text='<%# bind("addtaxp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AddVAT Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladdvatamt" ForeColor="Black" runat="server" Text='<%# bind("addtaxamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CST%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcstp" ForeColor="Black" runat="server" Text='<%# bind("cstp") %>'></asp:Label>
                                    <asp:TextBox ID="txtgridcstp" runat="server" Text='<%# bind("cstp") %>' Width="120px"
                                        AutoPostBack="True" OnTextChanged="txtgridcstp_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CST Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcstamt" ForeColor="Black" runat="server" Text='<%# bind("cstamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc1" ForeColor="Black" runat="server" Text='<%# bind("descr1") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtdesc1" runat="server" Text='<%# bind("descr1") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc2" ForeColor="Black" runat="server" Text='<%# bind("descr2") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtdesc2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc3" ForeColor="Black" runat="server" Text='<%# bind("descr3") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtdesc3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VID" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvid" ForeColor="Black" runat="server" Text='<%# bind("vid") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Received" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbliscame" runat="server" Text='<%# bind("iscame") %>' Visible="false"></asp:Label>
                                    <asp:CheckBox ID="chkreceived" runat="server" AutoPostBack="True" OnCheckedChanged="chkreceived_CheckedChanged" />
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Basic Amount</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtbasicamount" runat="server" CssClass="form-control" Width="175px"
                                    AutoPostBack="True" ReadOnly="true" OnTextChanged="txtbasicamount_TextChanged"
                                    Text="0" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Excise</label>
                            </div>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtfexcisep" runat="server" CssClass="form-control" Width="50px"
                                    AutoPostBack="True" OnTextChanged="txtfexcisep_TextChanged" Text="0" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtfexciseamt" runat="server" CssClass="form-control" Width="100px"
                                    AutoPostBack="True" OnTextChanged="txtfexciseamt_TextChanged" Text="0" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    CST</label>
                            </div>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtfcstp" runat="server" CssClass="form-control" Width="50px" AutoPostBack="True"
                                    OnTextChanged="txtfcstp_TextChanged" ReadOnly="true" Text="0" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtfcstamt" runat="server" CssClass="form-control" Width="100px"
                                    AutoPostBack="True" ReadOnly="true" OnTextChanged="txtfcstamt_TextChanged" Text="0"
                                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    VAT</label>
                            </div>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtfvatp" runat="server" CssClass="form-control" Width="50px" AutoPostBack="True"
                                    OnTextChanged="txtfvatp_TextChanged" ReadOnly="true" Text="0" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtfvatamt" runat="server" CssClass="form-control" Width="100px"
                                    AutoPostBack="True" ReadOnly="true" OnTextChanged="txtfvatamt_TextChanged" Text="0"
                                    onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Grand Total</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtgrandtotal" ReadOnly="true" runat="server" CssClass="form-control"
                                    Width="175px" Text="0" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Status<span style="color: Red;">*</span></label>
                            </div>
                            <div class="col-md-4">
                                <asp:DropDownList ID="drpstatus" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="border-left: 2px solid black">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Delivery Sch</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtdeliverysch" runat="server" CssClass="form-control" Width="150px"
                                    onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Payment</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtpayment" runat="server" CssClass="form-control" Width="150px"
                                    onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Delivery At</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtdeliveryat" runat="server" CssClass="form-control" Width="150px"
                                    onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Taxes</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txttaxes" runat="server" CssClass="form-control" Width="150px" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Octroi</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtoctroi" runat="server" CssClass="form-control" Width="150px"
                                    onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Excise</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtexcise" runat="server" CssClass="form-control" Width="150px"
                                    onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Form</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtform" runat="server" CssClass="form-control" Width="150px" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Trasnsportation</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txttransportation" runat="server" CssClass="form-control" Width="150px"
                                    onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="border-left: 2px solid black">
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Insurance</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtinsurance" runat="server" CssClass="form-control" Width="250px"
                                    onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Packing</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtpacking" runat="server" CssClass="form-control" Width="250px"
                                    onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Descrption 1</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtdescription1" runat="server" CssClass="form-control" Width="250px"
                                    TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Descrption 2</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtdescription2" runat="server" CssClass="form-control" Width="250px"
                                    TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Descrption 3</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtdescription3" runat="server" CssClass="form-control" Width="250px"
                                    TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Descrption 4</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtdescription4" runat="server" CssClass="form-control" Width="250px"
                                    TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Descrption 5</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtdescription5" runat="server" CssClass="form-control" Width="250px"
                                    TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">
                                    Descrption 6</label>
                            </div>
                            <div class="col-md-1">
                                <asp:TextBox ID="txtdescription6" runat="server" CssClass="form-control" Width="250px"
                                    TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <asp:Button ID="btnfinalsave" runat="server" Text="Save" class="btn btn-default forbutton"
                OnClick="btnfinalsave_Click" ValidationGroup="valpurchaseorder" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                ShowSummary="false" ValidationGroup="valpurchaseorder" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter po date."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtpodate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter delivery days."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtdeliverydays"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter client code."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtclientcode"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter followup date."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtfollowupdate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please select name."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="drpacname" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter followup details."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtfollowupdetails"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please enter sales order."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtsalesorder"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please select item name."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="drpitemname"
                InitialValue="--SELECT--"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please enter qty."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please enter unit."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtunit"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please enter rate."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtrate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Please enter basic amount."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtbasicamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Please enter vat%."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtgvvatp"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Please enter vat."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtgvvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Please enter ad.vat%."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtgvadvatp"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Please enter add.vat."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtgvadvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="Please enter amount."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtgvamount"></asp:RequiredFieldValidator>
            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Please enter description1."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtdescr1"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="Please enter description2."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtdescr2"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Please enter description3."
                Text="*" ValidationGroup="valgvpurchaseorder" ControlToValidate="txtdescr3"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter basic amount."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtbasicamount"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please enter excise(%)."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtfexcisep"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Please enter excise amount."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtfexciseamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Please enter cst(%)."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtfcstp"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="Please enter cst amount."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtfcstamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Please enter vat(%)."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtfvatp"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="Please enter vat amount."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtfvatamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Please enter grand total."
                Text="*" ValidationGroup="valpurchaseorder" ControlToValidate="txtgrandtotal"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="Please select status."
                Text="*" ValidationGroup="valpurchaseorder" InitialValue="--SELECT--" ControlToValidate="drpstatus"></asp:RequiredFieldValidator>

                 <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Po Date must be dd-MM-yyyy" ValidationGroup="valpurchaseorder" ForeColor="Red"
                ControlToValidate="txtpodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>

                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Followup Date must be dd-MM-yyyy" ValidationGroup="valpurchaseorder" ForeColor="Red"
                ControlToValidate="txtfollowupdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
        </div>
        <div class="row">
            <asp:LinkButton ID="lnkselectionpopup" runat="server" AccessKey="s" OnClick="lnkselectionpopup_Click"></asp:LinkButton>
            <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
            <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
                TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="500px" align="center"
                Width="95%" Style="display: none">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <span style="color: Red;"><b>Sales Order Selection</b></span></div>
                </div>
                <div class="row">
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-1 text-left">
                        <b>A/C Name :</b></div>
                    <div class="col-md-7">
                        <asp:Label ID="lblpopupacname" runat="server" Font-Bold="true"></asp:Label></div>
                </div>
                <asp:CheckBox ID="chkall" runat="server" Text="Select All" Visible="false" />
                <asp:Button ID="btnselectitem" runat="server" Text="Ok" BackColor="Red" ForeColor="White"
                    OnClick="btnselectitem_Click" />
                <div class="row">
                    <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                        <asp:Label ID="lblemptyso" runat="server" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="gvsalesorder" runat="server" AutoGenerateColumns="False" Width="100%"
                            BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                            CssClass="table table-bordered" OnRowCommand="gvsalesorder_RowCommand">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                            ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("sono") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Purchase Chlln No." SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblscno" ForeColor="Black" runat="server" Text='<%# bind("sono") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                    <ItemTemplate>
                                        <asp:Label ID="lblpcdate" runat="server" ForeColor="#505050" Text='<%# bind("sodate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" ForeColor="Black" runat="server" Text='<%# bind("status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#4c4c4c" />
                            <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                            <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                        </asp:GridView>
                        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="gvsoitemlistselection" runat="server" AutoGenerateColumns="False"
                            Width="1300px" BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”"
                            Height="0px" CssClass="table table-bordered">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%--<asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />--%>
                                        <asp:CheckBox ID="chkselect" runat="server" />
                                    </ItemTemplate>
                                    <%--<HeaderTemplate>
                                    <asp:CheckBox ID="chkall" runat="server" AutoPostBack="True" OnCheckedChanged="chkall_CheckedChanged" /></HeaderTemplate>--%>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ID" SortExpression="Csnm" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtdesc1" runat="server" Text='<%# bind("descr1") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtdesc2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtdesc3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="basic Amt" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblbasicamt" ForeColor="Black" runat="server" Text='<%# bind("basicamount") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#4c4c4c" />
                            <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                            <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                        </asp:GridView>
                    </asp:Panel>
                    <asp:Button ID="btnClose" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
                </div>
            </asp:Panel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtpodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtfollowupdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtpodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtpodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtfollowupdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtfollowupdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });
        });
    </script>
</asp:Content>
