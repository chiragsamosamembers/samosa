﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GSThsn1Excel.aspx.cs" Inherits="GSThsn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">GST Sales HSN Return</span><br />
        <div class="row">
            <div class="col-md-2">
                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox></div>
            <div class="col-md-2">
                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox></div>
            <div class="col-md-4">
                <asp:Button ID="btngsthsn" runat="server" Text="Export HSN Data" class="btn btn-default forbutton"
                    ValidationGroup="val" OnClick="btngsthsn_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="val" />
            </div>
        </div>
        <div class="row">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter from date."
                Text="*" ControlToValidate="txtfromdate" ValidationGroup="val"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="From Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                ControlToValidate="txtfromdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter to date."
                Text="*" ControlToValidate="txttodate" ValidationGroup="val"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="To Date must be dd-MM-yyyy" ValidationGroup="val" ForeColor="Red"
                ControlToValidate="txttodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator></div>
        <asp:GridView ID="gvhsn" runat="server" AutoGenerateColumns="False" Width="100%"
            BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
            CssClass="table table-bordered">
            <Columns>
                <%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                </ItemTemplate>
            </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="HSN" SortExpression="Csnm">
                    <ItemTemplate>
                        <asp:Label ID="lblhsncode" ForeColor="Black" runat="server" Text='<%# bind("hsncode") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description" SortExpression="Csnm">
                    <ItemTemplate>
                        <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="UQC" SortExpression="Csnm">
                    <ItemTemplate>
                        <asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Quantity" SortExpression="Csnm">
                    <ItemTemplate>
                        <asp:Label ID="lbltotqty" ForeColor="Black" runat="server" Text='<%# bind("totqty") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Value" SortExpression="Csnm">
                    <ItemTemplate>
                        <asp:Label ID="lbltotamount" ForeColor="Black" runat="server" Text='<%# bind("totamount") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Taxable Value" SortExpression="Csnm">
                    <ItemTemplate>
                        <asp:Label ID="lbltotbasic" ForeColor="Black" runat="server" Text='<%# bind("totbasic") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Central Tax Amount" SortExpression="Csnm">
                    <ItemTemplate>
                        <asp:Label ID="lbltotvat" ForeColor="Black" runat="server" Text='<%# bind("totvat") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="State/UT Tax Amount" SortExpression="Csnm">
                    <ItemTemplate>
                        <asp:Label ID="lbltotadvat" ForeColor="Black" runat="server" Text='<%# bind("totadvat") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Integrated Tax Amount" SortExpression="Csnm">
                    <ItemTemplate>
                        <asp:Label ID="lbltotcst" ForeColor="Black" runat="server" Text='<%# bind("totcst") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cess Amount" SortExpression="Csnm">
                    <ItemTemplate>
                        <asp:Label ID="lbltaxdesc" ForeColor="Black" runat="server" Text="0"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                </asp:TemplateField>
                <%--<asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                </ItemTemplate>
            </asp:TemplateField>--%>
            </Columns>
            <FooterStyle BackColor="#4c4c4c" />
            <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
            <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
        </asp:GridView>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtfromdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtfromdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


            $("#<%= txttodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txttodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
