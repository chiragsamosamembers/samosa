﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SalesInvoiceExport.aspx.cs" Inherits="SalesInvoiceExport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 780px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Sales Invoice</span><br />
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Invoice No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtinvoiceno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="javascript:return isNumber (event)" AutoPostBack="True" OnTextChanged="txtinvoiceno_TextChanged"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtsidate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Inv Type</label>
                    </div>
                    <div class="col-md-1">
                        <%--<asp:TextBox ID="txtinvtype" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>--%>
                        <asp:DropDownList ID="drpinvtype" runat="server" CssClass="form-control" AutoPostBack="True"
                            OnSelectedIndexChanged="drpinvtype_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1">
                        <asp:CheckBox ID="chkigst" runat="server" Text="IGST" /></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            User</label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" Width="150px" ReadOnly="true"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Order No.</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtorderno" runat="server" CssClass="form-control" Width="150px"
                            onkeypress="return checkQuote();"></asp:TextBox><%--AutoPostBack="True" OnTextChanged="txtorderno_TextChanged"--%>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="server" TargetControlID="txtorderno"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getsono">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Date</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtsodate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Chlln No.</label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtscno" runat="server" CssClass="form-control" Width="150px" onkeypress="javascript:return isNumber (event)"
                            placeholder="Alt + s"></asp:TextBox><%--OnTextChanged="txtscno_TextChanged"--%>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="server" TargetControlID="txtscno"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getscno">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtscno1" runat="server" CssClass="form-control" Width="150px" onkeypress="javascript:return isNumber (event)"
                            placeholder="Alt + s"></asp:TextBox><%--OnTextChanged="txtscno1_TextChanged"--%>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender7" runat="server" TargetControlID="txtscno1"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getscno">
                        </asp:AutoCompleteExtender>
                        <asp:HiddenField ID="hdnscno" runat="server" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Name</label>
                    </div>
                    <div class="col-md-5">
                        <%--<asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="394px" onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="txtname"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="GetAccountname">
                        </asp:AutoCompleteExtender>--%>
                        <asp:DropDownList ID="drpacname" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Chlln Date.</label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtscdate" runat="server" CssClass="form-control" Width="150px"></asp:TextBox></div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtscdate1" runat="server" CssClass="form-control" Width="150px"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label class="control-label">
                            Sales A/C</label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtsalesac" runat="server" CssClass="form-control" Width="394px"
                            onkeypress="return checkQuote();"></asp:TextBox>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender9" runat="server" TargetControlID="txtsalesac"
                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                            ServiceMethod="Getsalesac">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-md-1">
                        <asp:Label ID="lblcount" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="22px"></asp:Label></div>
                    <div class="col-md-1">
                        <label class="control-label">
                            Status</label></div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="drpstatus" runat="server" CssClass="form-control" Width="150px">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtsrtringscno" runat="server" CssClass="form-control" Width="394px"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Item Name</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Bill Qty</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Stock Qty</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Rate</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Basic Amount</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Tax Type
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    VAT/SGST %
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Ad.Tax/CGST%
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    CST/IGST %
                </label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Unit
                </label>
            </div>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    Code
                </label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 1</label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 2</label>
            </div>
            <div class="col-md-2 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Description 3</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Tax Desc</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    VAT/SGST</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    Ad.Tax/CGST</label>
            </div>
            <div class="col-md-1 text-center" style="border-right: 1px solid;">
                <label class="control-label">
                    CST Amt.</label>
            </div>
            <div class="col-md-1 text-center">
                <label class="control-label">
                    Amount</label>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2" style="border-right: 1px solid;">
                <%--<asp:TextBox ID="txtitemname" runat="server" CssClass="form-control" placeholder="Item Name"
                    Width="200px" AutoPostBack="True" OnTextChanged="txtitemname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtitemname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getitemname">
                </asp:AutoCompleteExtender>--%>
                <asp:DropDownList ID="drpitemname" runat="server" CssClass="form-control" AutoPostBack="True"
                    OnSelectedIndexChanged="drpitemname_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtbillqty" runat="server" CssClass="form-control" placeholder="Bill Qty"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtbillqty_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtstockqty" runat="server" CssClass="form-control" placeholder="Stock Qty"
                    Width="90px" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtrate" runat="server" CssClass="form-control" placeholder="Rate"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtrate_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtbasicamt" runat="server" CssClass="form-control" placeholder="Basic Amt."
                    Width="90px" AutoPostBack="True" OnTextChanged="txtbasicamt_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txttaxtype" runat="server" CssClass="form-control" placeholder="Tax Type"
                    Width="90px" AutoPostBack="True" OnTextChanged="txttaxtype_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txttaxtype"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="Getvattype">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvvat" runat="server" CssClass="form-control" placeholder="VAT%"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtgvvat_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvadtax" runat="server" CssClass="form-control" placeholder="Ad.Tax%"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtgvadtax_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtgvcst" runat="server" CssClass="form-control" placeholder="CST%"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtgvcst_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" placeholder="Unit"
                    Width="90px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" placeholder="Code"
                    Width="90px" AutoPostBack="true" OnTextChanged="txtname_TextChanged" onkeypress="return checkQuote();"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="txtcode"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetClientname">
                </asp:AutoCompleteExtender>
            </div>
        </div>
        <div class="row" style="border: 1px solid;">
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription1" runat="server" CssClass="form-control" placeholder="Description 1"
                    Width="200px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription2" runat="server" CssClass="form-control" placeholder="Description 2"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-2" style="border-right: 1px solid;">
                <asp:TextBox ID="txtdescription3" runat="server" CssClass="form-control" placeholder="Description 3"
                    Width="204px" onkeypress="return checkQuote();"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:DropDownList ID="drptaxdesc" runat="server" CssClass="form-control" Width="90px">
                </asp:DropDownList>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtvat" runat="server" CssClass="form-control" placeholder="VAT"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtvat_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtaddtax" runat="server" CssClass="form-control" placeholder="Ad.Tax"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtaddtax_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtcstamount" runat="server" CssClass="form-control" placeholder="CST Amt."
                    Width="90px" AutoPostBack="True" OnTextChanged="txtcstamount_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1" style="border-right: 1px solid;">
                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" placeholder="Amount"
                    Width="90px" AutoPostBack="True" OnTextChanged="txtamount_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnadd" runat="server" Text="Add" Height="30px" BackColor="#F05283"
                    ValidationGroup="valgvsalesinovice" OnClick="btnadd_Click" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="valgvsalesinovice" />
            </div>
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="370px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvsiitemlist" runat="server" AutoGenerateColumns="False" Width="1300px"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowDeleting="gvsiitemlist_RowDeleting" OnRowCommand="gvsiitemlist_RowCommand"
                        OnDataBound="gvsiitemlist_DataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="update1" runat="server" ImageUrl="~/images/buttons/viewer_ico_checkl.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete123();"
                                        CommandArgument='<%# bind("vid") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VNo." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvno" ForeColor="Black" runat="server" Text='<%# bind("vno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblqty" ForeColor="Black" runat="server" Text='<%# bind("qty") %>'></asp:Label>
                                    <asp:Label ID="lblqtyremain" runat="server" ForeColor="Green" Text='<%# bind("qtyremain") %>'></asp:Label>
                                    <asp:Label ID="lblqtyused" runat="server" ForeColor="Red" Text='<%# bind("qtyused") %>'></asp:Label>
                                    <asp:TextBox ID="txtgridqty" runat="server" Text='<%# bind("qty") %>' Width="50px"
                                        AutoPostBack="True" OnTextChanged="txtgvqty_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Qty" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtgvstockqty" runat="server" Text='<%# bind("stockqty") %>' Width="120px"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'
                                        Visible="false"></asp:Label>
                                    <asp:TextBox ID="txtgvunit" runat="server" Text='<%# bind("unit") %>' Width="50px"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rate" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblrate" ForeColor="Black" runat="server" Text='<%# bind("rate") %>'
                                        Visible="false"></asp:Label><asp:TextBox ID="txtgridrate" runat="server" Text='<%# bind("rate") %>'
                                            Width="120px" AutoPostBack="True" OnTextChanged="txtgvrate_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="basic Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblbasicamount" ForeColor="Black" runat="server" Text='<%# bind("basicamount") %>'></asp:Label>
                                    <asp:TextBox ID="txtgridamount" runat="server" Text='<%# bind("basicamount") %>'
                                        Width="120px"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT Type" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbltaxtype" ForeColor="Black" runat="server" Text='<%# bind("taxtype") %>'></asp:Label><asp:TextBox
                                        ID="txtgridtaxtype" runat="server" Text='<%# bind("taxtype") %>' Width="120px"
                                        AutoPostBack="True" OnTextChanged="txtgridtaxtype_TextChanged"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtgridtaxtype"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="Getvattype">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvatp" ForeColor="Black" runat="server" Text='<%# bind("vatp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add VAT%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladdvatp" ForeColor="Black" runat="server" Text='<%# bind("addtaxp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CST%" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcstp" ForeColor="Black" runat="server" Text='<%# bind("cstp") %>'></asp:Label>
                                    <asp:TextBox ID="txtgridcstp" runat="server" Text='<%# bind("cstp") %>' Width="120px"
                                        AutoPostBack="True" OnTextChanged="txtgridcstp_TextChanged"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CST Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblcstamt" ForeColor="Black" runat="server" Text='<%# bind("cstamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VAT Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvatamt" ForeColor="Black" runat="server" Text='<%# bind("vatamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Add VAT Amt" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbladdvatamt" ForeColor="Black" runat="server" Text='<%# bind("addtaxamt") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblamount" ForeColor="Black" runat="server" Text='<%# bind("amount") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ccode" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description1" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc1" ForeColor="Black" runat="server" Text='<%# bind("descr1") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtdesc1" runat="server" Text='<%# bind("descr1") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description2" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc2" ForeColor="Black" runat="server" Text='<%# bind("descr2") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtdesc2" runat="server" Text='<%# bind("descr2") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description3" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbldesc3" ForeColor="Black" runat="server" Text='<%# bind("descr3") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtdesc3" runat="server" Text='<%# bind("descr3") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VID" SortExpression="Csnm" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblvid" ForeColor="Black" runat="server" Text='<%# bind("vid") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tax Desc" SortExpression="Csnm">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lbltaxdesc" ForeColor="Black" runat="server" Text='<%# bind("taxdesc") %>'></asp:Label>--%>
                                    <asp:TextBox ID="txtgridtaxdesc" runat="server" Text='<%# bind("taxdesc") %>' Width="120px"></asp:TextBox><%--AutoPostBack="True" OnTextChanged="txtgridtaxtype_TextChanged"--%>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender222" runat="server" TargetControlID="txtgridtaxdesc"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="Getvatdesc">
                                    </asp:AutoCompleteExtender>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="tab-block margin-bottom-lg">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab6" data-toggle="tab">Bill Details</a></li>
                <li><a href="#tab7" data-toggle="tab"><i class="fa fa-bolt text-blue2"></i>Other Details</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab6" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Basic Amount</label>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txttotbasicamount" runat="server" CssClass="form-control" Width="150px"
                                        AutoPostBack="True" OnTextChanged="txttotbasicamount_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        CST</label>
                                </div>
                                <div class="col-md-1">
                                    <asp:TextBox ID="txttotcst" runat="server" CssClass="form-control" Width="155px"
                                        AutoPostBack="True" OnTextChanged="txttotcst_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <label class="control-label">
                                        VAT</label>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txttotvat" runat="server" CssClass="form-control" Width="150px"
                                        AutoPostBack="True" OnTextChanged="txttotvat_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Service Tax</label>
                                </div>
                                <div class="col-md-1" style="width: 4% !important">
                                    <asp:TextBox ID="txtservicep" runat="server" CssClass="form-control" Width="50px"
                                        AutoPostBack="True" OnTextChanged="txtservicep_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                                <div class="col-md-1" style="width: 12.6% !important">
                                    <asp:TextBox ID="txtserviceamt" runat="server" CssClass="form-control" Width="100px"
                                        AutoPostBack="True" OnTextChanged="txtserviceamt_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Round Dff</label>
                                </div>
                                <div class="col-md-1">
                                    <asp:TextBox ID="txtroundoff" runat="server" CssClass="form-control" Width="150px"
                                        AutoPostBack="True" OnTextChanged="txtroundoff_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Ad.Tax</label>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txttotaddtax" runat="server" CssClass="form-control" Width="150px"
                                        AutoPostBack="True" OnTextChanged="txttotaddtax_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Cartage</label>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtcartage" runat="server" CssClass="form-control" Width="155px"
                                        AutoPostBack="True" OnTextChanged="txtcartage_TextChanged" onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Bill Amount</label>
                                </div>
                                <div class="col-md-1">
                                    <asp:TextBox ID="txtbillamount" runat="server" CssClass="form-control" Width="150px"
                                        onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab7" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Remarks</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:TextBox ID="txtremakrs" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox></div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Form</label>
                                </div>
                                <div class="col-md-1">
                                    <asp:TextBox ID="txtform" runat="server" CssClass="form-control" Width="155px" onkeypress="return checkQuote();"></asp:TextBox></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Transport</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:TextBox ID="txttransportname" runat="server" CssClass="form-control" onkeypress="return checkQuote();"></asp:TextBox></div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Sales Man</label>
                                </div>
                                <div class="col-md-1">
                                    <asp:TextBox ID="txtsalesman" runat="server" CssClass="form-control" Width="150px"
                                        onkeypress="return checkQuote();"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender8" runat="server" TargetControlID="txtsalesman"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                                        ServiceMethod="Getsalesman">
                                    </asp:AutoCompleteExtender>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <label class="control-label">
                                        L.R. No</label>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtlrno" runat="server" CssClass="form-control" Width="150px" onkeypress="return checkQuote();"></asp:TextBox></div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Date</label>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtlrdate" runat="server" CssClass="form-control" Width="155px"></asp:TextBox></div>
                                <div class="col-md-1">
                                    <label class="control-label">
                                        Due Days</label>
                                </div>
                                <div class="col-md-1">
                                    <asp:TextBox ID="txtduedays" runat="server" CssClass="form-control" Width="150px"
                                        onkeypress="return checkQuote();"></asp:TextBox></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <asp:Button ID="btnsave" runat="server" Text="Save" Height="30px" BackColor="#F05283"
                ValidationGroup="valsalesinovice" OnClick="btnsave_Click" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                ShowSummary="false" ValidationGroup="valsalesinovice" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter sales invoice date."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtsidate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select inv type."
                InitialValue="--SELECT--" Text="*" ValidationGroup="valsalesinovice" ControlToValidate="drpinvtype"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter order no."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtorderno"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter sc no."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtscno"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter sc no."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtscno1"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please select name."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="drpacname" InitialValue="--SELECT--"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter so date."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtsodate"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please enter sc date."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtscdate"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please enter sc date."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtscdate1"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="Please enter sales ac."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtsalesac"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="Please enter basic amount."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txttotbasicamount"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="Please enter total cst."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txttotcst"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="Please enter total vat."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txttotvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ErrorMessage="Please enter service tax%."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtservicep"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ErrorMessage="Please enter service amount."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtserviceamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="Please enter round off."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtroundoff"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ErrorMessage="Please enter total add.tax."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txttotaddtax"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ErrorMessage="Please enter cartage."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtcartage"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ErrorMessage="Please enter bill amount."
                Text="*" ValidationGroup="valsalesinovice" ControlToValidate="txtbillamount"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please select item name."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="drpitemname"
                InitialValue="--SELECT--"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please enter bill qty."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtbillqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please enter stock qty."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtstockqty"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please enter rate."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtrate"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Please enter basic amount."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtbasicamt"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Please enter tax type."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txttaxtype"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Please enter vat%."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtgvvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Please enter ad.vat%."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtgvadtax"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Please enter cst%."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtgvcst"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Please enter unit."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtunit"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please enter client code."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtcode"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Please select status."
                Text="*" ValidationGroup="valsalesinovice" InitialValue="--SELECT--" ControlToValidate="drpstatus"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please select inv type."
                Text="*" ValidationGroup="valsalesinovice" InitialValue="--SELECT--" ControlToValidate="drpinvtype"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Please enter description1."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtdescription1"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="Please enter description2."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtdescription2"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Please enter description3."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtdescription3"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select tax desc."
                Text="*" ValidationGroup="valgvsalesinovice" InitialValue="--SELECT--" ControlToValidate="drptaxdesc"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="Please enter vat."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtvat"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Please enter add.tax."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtaddtax"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="Please enter cst amount."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtcstamount"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="Please enter amount."
                Text="*" ValidationGroup="valgvsalesinovice" ControlToValidate="txtamount"></asp:RequiredFieldValidator>

                 <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Sales invoice Date must be dd-MM-yyyy" ValidationGroup="valsalesinovice"
                ForeColor="Red" ControlToValidate="txtsidate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Sales Order Date must be dd-MM-yyyy" ValidationGroup="valsalesinovice"
                ForeColor="Red" ControlToValidate="txtsodate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Sales Challan Date must be dd-MM-yyyy" ValidationGroup="valsalesinovice"
                ForeColor="Red" ControlToValidate="txtscdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Sales Challan Date1 must be dd-MM-yyyy" ValidationGroup="valsalesinovice"
                ForeColor="Red" ControlToValidate="txtscdate1" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" Text="DD-MM-YYYY"
                ErrorMessage="Lr Date must be dd-MM-yyyy" ValidationGroup="valsalesinovice" ForeColor="Red"
                ControlToValidate="txtlrdate" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)[0-9]{2}"></asp:RegularExpressionValidator>
        </div>
        <div class="row">
            <asp:LinkButton ID="lnkselectionpopup" runat="server" AccessKey="s" OnClick="lnkselectionpopup_Click"></asp:LinkButton>
            <asp:LinkButton ID="lnkopenpopup" runat="server"></asp:LinkButton>
            <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Panel2"
                TargetControlID="lnkopenpopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" Height="500px" align="center"
                Width="95%" Style="display: none">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <span style="color: Red;"><b>Sales Challan Selection</b></span></div>
                </div>
                <div class="row">
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-1 text-left">
                        <b>A/C Name :</b></div>
                    <div class="col-md-7">
                        <asp:Label ID="lblpopupacname" runat="server" Font-Bold="true"></asp:Label></div>
                </div>
                <div class="row">
                    <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Height="395px" Width="98%">
                        <asp:Label ID="lblemptysc" runat="server" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="gvsaleschallans" runat="server" AutoGenerateColumns="False" Width="100%"
                            BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                            CssClass="table table-bordered" OnRowCommand="gvsaleschallans_RowCommand">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                            ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("strscno") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales Chlln No." SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstrscno" ForeColor="Black" runat="server" Text='<%# bind("strscno") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales Chlln No." SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblscno" ForeColor="Black" runat="server" Text='<%# bind("scno") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                    <ItemTemplate>
                                        <asp:Label ID="lblscdate" runat="server" ForeColor="#505050" Text='<%# bind("scdate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CCode" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" ForeColor="Black" runat="server" Text='<%# bind("status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#4c4c4c" />
                            <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                            <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                        </asp:GridView>
                        <asp:Label ID="lblemptyso" runat="server" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="gvsalesorders" runat="server" AutoGenerateColumns="False" Width="100%"
                            BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                            CssClass="table table-bordered" OnRowCommand="gvsalesorders_RowCommand">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                            ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("sono") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales Order No." SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblsono" ForeColor="Black" runat="server" Text='<%# bind("sono") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date" SortExpression="companyname">
                                    <ItemTemplate>
                                        <asp:Label ID="lblsodate" runat="server" ForeColor="#505050" Text='<%# bind("sodate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CCode" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblccode" ForeColor="Black" runat="server" Text='<%# bind("ccode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bill Amount" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lbltotsoamt" ForeColor="Black" runat="server" Text='<%# bind("totsoamt") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" SortExpression="Csnm">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" ForeColor="Black" runat="server" Text='<%# bind("status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#4c4c4c" />
                            <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                            <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                        </asp:GridView>
                    </asp:Panel>
                    <asp:Button ID="btnClose" runat="server" Text="Close" BackColor="Red" ForeColor="White" />
                </div>
            </asp:Panel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(getme);
        function getme() {
            $("#<%= txtsidate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtsodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtscdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtscdate1.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtlrdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtsidate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtsidate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtsodate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtsodate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtscdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtscdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

            $("#<%= txtscdate1.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtscdate1.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });


            $("#<%= txtlrdate.ClientID %>").click(function () {

                $(this).datepicker({
                    showmonth: true,
                    autoSize: true,
                    showAnim: 'slideDown',
                    duration: 'fast',
                    dateFormat: "dd-mm-yy"
                });
            });
            $("#<%= txtlrdate.ClientID %>").datepicker({
                showmonth: true,
                autoSize: true,
                showAnim: 'slideDown',
                duration: 'fast',
                dateFormat: "dd-mm-yy"
            });

        });
    </script>
</asp:Content>
