﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
//using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlTypes;

public partial class GSTB2B_P : System.Web.UI.Page
{
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //fillgridb2b();
            //ExportGridToExcel();
        }
    }

    public void fillgridb2b()
    {
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        Session["dtpitems"] = CreateTemplate();
        DataTable dtq = new DataTable();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da = new SqlDataAdapter("select strpino,pidate,SUM(basicamount) as totbasic,(sum(PIItems.basicamount)+sum(PIItems.vatamt)+sum(PIItems.addtaxamt)+sum(PIItems.cstamt)) as totbillamt,taxdesc,sum(PIItems.vatamt) as tcgst,sum(PIItems.addtaxamt) as tsgst,sum(PIItems.cstamt) as tigst from PIItems where pidate between @fromdate and @todate group by taxdesc,strpino,pidate,pino order by pino", con);
        da.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        DataTable dtdata = new DataTable();
        da.Fill(dtdata);
        if (dtdata.Rows.Count > 0)
        {
            for (int c = 0; c < dtdata.Rows.Count; c++)
            {
                dtq = (DataTable)Session["dtpitems"];
                DataRow dr1 = dtq.NewRow();                
                dr1["sidate"] = Convert.ToDateTime(dtdata.Rows[c]["pidate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat).ToString("dd-MMM-yy");
                dr1["totbasicamount"] = dtdata.Rows[c]["totbasic"].ToString();

                dr1["sgst"] = dtdata.Rows[c]["tsgst"].ToString();
                dr1["cgst"] = dtdata.Rows[c]["tcgst"].ToString();
                dr1["igst"] = dtdata.Rows[c]["tigst"].ToString();
                dr1["asgst"] = dtdata.Rows[c]["tsgst"].ToString();
                dr1["acgst"] = dtdata.Rows[c]["tcgst"].ToString();
                dr1["aigst"] = dtdata.Rows[c]["tigst"].ToString();

                var cc = dtdata.Rows[c]["taxdesc"].ToString().IndexOf("-");
                if (cc != -1)
                {
                    double vatp = Convert.ToDouble(dtdata.Rows[c]["taxdesc"].ToString().Split('-')[0]);
                    double addvatp = Convert.ToDouble(dtdata.Rows[c]["taxdesc"].ToString().Split('-')[1]);
                    dr1["taxdesc"] = (vatp + addvatp).ToString();
                }
                else
                {
                    dr1["taxdesc"] = dtdata.Rows[c]["taxdesc"].ToString();
                }

                li.strsino = dtdata.Rows[c]["strpino"].ToString();
                SqlDataAdapter dasi = new SqlDataAdapter("select * from pimaster where strpino='" + li.strsino + "'", con);
                DataTable dtsi = new DataTable();
                dasi.Fill(dtsi);
                dr1["billamount"] = dtsi.Rows[0]["billamount"].ToString();
                SqlDataAdapter daa = new SqlDataAdapter("select * from PIMaster inner join ACMaster on PIMaster.acname=ACMaster.acname where PIMaster.strpino='" + li.strsino + "' and ACMaster.gstno!=''", con);
                DataTable dta = new DataTable();
                daa.Fill(dta);
                if (dta.Rows.Count > 0)
                {
                    dr1["gstno"] = dta.Rows[0]["gstno"].ToString();
                    if (dta.Rows[0]["gstno"].ToString() != "")
                    {
                        dr1["rcm"] = "N";
                    }
                    else
                    {
                        dr1["rcm"] = "Y";
                    }
                    dr1["city"] = dta.Rows[0]["city"].ToString();
                    dr1["acname"] = dta.Rows[0]["acname"].ToString();
                }
                else
                {
                    dr1["gstno"] = "";
                    dr1["city"] = "";
                    dr1["rcm"] = "";
                    dr1["acname"] = "";
                }
                dr1["strsino"] = dtsi.Rows[0]["billno"].ToString();
                if (dta.Rows.Count > 0)
                {
                    dtq.Rows.Add(dr1);
                }
            }
            Session["dtpitems"] = dtq;
            gvaclist.DataSource = dtq;
            gvaclist.DataBind();
        }
    }



    private void ExportGridToExcel()
    {
        ////string path = string.Concat(Server.MapPath("~/GST Return/a4.xlsx"));
        //string dd = System.DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx";
        //if (!File.Exists(@"d:\\" + dd + ""))
        //{
        //    //
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not created.Create Excel Sheet @d:\\a4.xlsx');", true);
        //    //MessageBox.Show("Excel is not properly installed!!");
        //    return;
        //    //File.Delete(Server.MapPath("~/GST Return/a4.xlsx"));
        //}


        ////Microsoft.Office.Interop.Excel.DataTable dtq = (Microsoft.Office.Interop.Excel.DataTable)Session["dtpitems"];
        //Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

        //if (xlApp == null)
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Excel is not properly installed!!.');", true);
        //    //MessageBox.Show("Excel is not properly installed!!");
        //    return;
        //}

        //xlApp.DisplayAlerts = false;
        ////string filePath = @"d:\\a4.xlsx";
        //string filePath = @"d:\\" + dd + "";
        //Microsoft.Office.Interop.Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        //Microsoft.Office.Interop.Excel.Sheets worksheets = xlWorkBook.Worksheets;
        ////1 sheet
        //var xlNewSheet = (Microsoft.Office.Interop.Excel.Worksheet)worksheets.Add(worksheets[1], Type.Missing, Type.Missing, Type.Missing);
        //xlNewSheet.Name = "Chigs11111";
        //xlNewSheet.Cells[1, 1] = "Chirag";
        //xlNewSheet.Cells[1, 2] = "Chirag 1";
        //xlNewSheet.Cells[1, 3] = "Chirag 2";
        //xlNewSheet.Cells[2, 1] = "Chirag";
        //xlNewSheet.Cells[2, 2] = "Chirag 1";
        //xlNewSheet.Cells[2, 3] = "Chirag 2";

        //xlNewSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //xlNewSheet.Select();

        ////2 Sheet
        //Microsoft.Office.Interop.Excel.Sheets worksheets1 = xlWorkBook.Worksheets;
        //var xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)worksheets1.Add(worksheets1[1], Type.Missing, Type.Missing, Type.Missing);
        //xlNewSheet1.Name = "P22222";
        //xlNewSheet1.Cells[1, 1] = "P";
        //xlNewSheet1.Cells[1, 2] = "P 1";
        //xlNewSheet1.Cells[1, 3] = "P 2";
        //xlNewSheet1.Cells[2, 1] = "P";
        //xlNewSheet1.Cells[2, 2] = "P 1";
        //xlNewSheet1.Cells[2, 3] = "P 2";

        ////        xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets1.get_Item(2);
        //xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
        //xlNewSheet1.Select();

        ////3 Sheet
        //Microsoft.Office.Interop.Excel.Sheets worksheets2 = xlWorkBook.Worksheets;
        //var xlNewSheet2 = (Microsoft.Office.Interop.Excel.Worksheet)worksheets1.Add(worksheets1[1], Type.Missing, Type.Missing, Type.Missing);
        //xlNewSheet2.Name = "Q22222";
        //xlNewSheet2.Cells[1, 1] = "Q";
        //xlNewSheet2.Cells[1, 2] = "Q 1";
        //xlNewSheet2.Cells[1, 3] = "Q 2";
        //xlNewSheet2.Cells[2, 1] = "Q";
        //xlNewSheet2.Cells[2, 2] = "Q 1";
        //xlNewSheet2.Cells[2, 3] = "Q 2";

        ////        xlNewSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets1.get_Item(2);
        //xlNewSheet2 = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(3);
        //xlNewSheet2.Select();

        //xlWorkBook.Save();
        //xlWorkBook.Close();

        //releaseObject(xlNewSheet);
        //releaseObject(worksheets);
        //releaseObject(xlWorkBook);
        //releaseObject(xlApp);

        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('New Worksheet Created!');", true);
        //Microsoft.Office.Interop.Excel.DataTable dtq = (Microsoft.Office.Interop.Excel.DataTable)Session["dtpitems"];
        DataTable dtq = (DataTable)Session["dtpitems"];
        if (dtq.Rows.Count > 0)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "GSTB2B_P" + DateTime.Now + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            gvaclist.GridLines = GridLines.Both;
            gvaclist.HeaderStyle.Font.Bold = true;
            gvaclist.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No B2B data available for export.');", true);
            return;
        }
    }



    public DataTable CreateTemplate()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("gstno", typeof(string));
        dtpitems.Columns.Add("strsino", typeof(string));
        dtpitems.Columns.Add("acname", typeof(string));
        dtpitems.Columns.Add("sidate", typeof(string));
        dtpitems.Columns.Add("billamount", typeof(double));
        dtpitems.Columns.Add("city", typeof(string));
        dtpitems.Columns.Add("taxdesc", typeof(string));
        dtpitems.Columns.Add("totbasicamount", typeof(double));
        dtpitems.Columns.Add("rcm", typeof(string));

        dtpitems.Columns.Add("igst", typeof(double));
        dtpitems.Columns.Add("cgst", typeof(double));
        dtpitems.Columns.Add("sgst", typeof(double));
        dtpitems.Columns.Add("aigst", typeof(double));
        dtpitems.Columns.Add("acgst", typeof(double));
        dtpitems.Columns.Add("asgst", typeof(double));
        //dtpitems.Columns.Add("gstno", typeof(string));
        //dtpitems.Columns.Add("strsino", typeof(DateTime));
        //dtpitems.Columns.Add("sidate", typeof(string));
        //dtpitems.Columns.Add("billamount", typeof(double));
        //dtpitems.Columns.Add("city", typeof(double));
        //dtpitems.Columns.Add("taxdesc", typeof(double));
        //dtpitems.Columns.Add("totbasicamount", typeof(string));
        return dtpitems;
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //
    }

    protected void btngstb2b_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        //if (txtfromdate.Text.Trim() != string.Empty && txttodate.Text.Trim() != string.Empty)
        //{
            fillgridb2b();
            ExportGridToExcel();
       // }
    }

    private void releaseObject(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }
        catch (Exception ex)
        {
            obj = null;
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Exception Occured while releasing object " + ex.ToString() + "');", true);
            //MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
        }
        finally
        {
            GC.Collect();
        }
    }
}