﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StockTools.aspx.cs" Inherits="StockTools" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Tools Stock</span><br />
        <div class="row">
            <%--<div class="col-md-1">
                <label class="control-label">
                    From Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-1">
                <label class="control-label">
                    To Date</label></div>
            <div class="col-md-2">
                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
            </div>--%>
            <div class="col-md-2">
                <asp:Button ID="btnstockform201c" runat="server" Text="Form 201C" class="btn btn-default forbutton"
                    Visible="false" /></div>
            <div class="col-md-2">
                <asp:Button ID="Button1" runat="server" Text="Print Stock" class="btn btn-default forbutton"
                    OnClick="btnstockprint_Click" /></div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvbankpaymentlist" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered">
                        <Columns>
                            <asp:TemplateField HeaderText="item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("toolname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Op. Qty." SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblopqty" ForeColor="Black" runat="server" Text='<%# bind("opqty") %>'></asp:Label>
                                    <%--<%#Convert.ToDateTime(Eval("voucherdate")).ToString("dd-MM-yyyy")%>--%>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inward" SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblsqty" ForeColor="Black" runat="server" Text='<%# bind("totp") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Outward" SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblpqty" ForeColor="Black" runat="server" Text='<%# bind("tots") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cl. Qty." SortExpression="Csnm" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblclqty" ForeColor="Black" runat="server" Text='<%# bind("totalqty") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>    
</asp:Content>
