﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Survey.Classes;
using System.Data.SqlTypes;

public partial class AdjustPendingPO : System.Web.UI.Page
{
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //fillpendingpo();
        }
    }

    public void fillpendingpo()
    {
        Session["dtpitemsapp"] = CreateTemplatepo();
        li.acname = "";
        //if (Request["ccode"].ToString() != "" && Request["ccode"].ToString() != string.Empty)
        //{
        //    li.ccode = Convert.ToInt64(Request["ccode"].ToString());
        //}
        //else
        //{
        li.ccode = 0;
        //}
        DataTable dtq = new DataTable();
        string id = "";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string ftow = "";
        DataSet1 imageDataSet = new DataSet1();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da11 = new SqlDataAdapter();
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from PurchaseOrder inner join PurchaseOrderItems on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrder.status!='Closed' and PurchaseOrder.cno='" + li.cno + "' and (PurchaseOrderItems.iscame!='Yes' or PurchaseOrderItems.iscame is NULL) and PurchaseOrder.podate between @fromdate and @todate", con);
        if (li.acname == "" && li.ccode == 0)
        {
            da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.podate between @fromdate and @todate and PurchaseOrderItems.cno=@cno order by PurchaseOrder.pono", con);
        }
        else if (li.acname != "" && li.ccode == 0)
        {
            da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.podate between @fromdate and @todate and PurchaseOrderItems.cno=@cno and PurchaseOrder.acname=@acname order by PurchaseOrder.pono", con);
            da11.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
        }
        else if (li.acname == "" && li.ccode != 0)
        {
            da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.podate between @fromdate and @todate and PurchaseOrderItems.cno=@cno and PurchaseOrder.ccode=@ccode order by PurchaseOrder.pono", con);
            da11.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
        }
        else if (li.acname != "" && li.ccode != 0)
        {
            da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.podate between @fromdate and @todate and PurchaseOrderItems.cno=@cno and PurchaseOrder.ccode=@ccode and PurchaseOrder.acname=@acname order by PurchaseOrder.pono", con);
            da11.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            da11.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
        }
        da11.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        da11.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
        DataTable dtc = new DataTable();
        da11.Fill(dtc);
        if (dtc.Rows.Count > 0)
        {
            for (int c = 0; c < dtc.Rows.Count; c++)
            {
                //if (dtc.Rows[c]["pono"].ToString() == "17180292")
                //{
                //    string cc = "";
                //}                
                li.id = Convert.ToInt64(dtc.Rows[c]["id"].ToString());
                //if (li.id == 2988 || li.id == 3032)
                //{
                //    string cc = "";
                //}
                li.qty = Convert.ToDouble(dtc.Rows[c]["qty"].ToString());
                if (dtc.Rows[c]["adjqty"].ToString() != string.Empty)
                {
                    li.adjqty = Convert.ToDouble(dtc.Rows[c]["adjqty"].ToString());
                }
                else
                {
                    li.adjqty = 0;
                }
                SqlDataAdapter da11c = new SqlDataAdapter("select isnull(sum(qty) ,0) as totqty from PCItems where vid=@id and cno=@cno", con);
                da11c.SelectCommand.Parameters.AddWithValue("@id", li.id);
                da11c.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
                DataTable dtcc = new DataTable();
                da11c.Fill(dtcc);
                if (dtcc.Rows.Count > 0)
                {
                    li.qtypc = Convert.ToDouble(dtcc.Rows[0]["totqty"].ToString());
                    li.qtyremain = Math.Round((li.qty - (li.qtypc + li.adjqty)), 2);
                    if (li.qtyremain > 0)
                    {
                        id = id + li.id + ",";
                        dtq = (DataTable)Session["dtpitemsapp"];
                        DataRow dr1 = dtq.NewRow();
                        dr1["pono"] = dtc.Rows[c]["pono"].ToString();
                        dr1["podate"] = Convert.ToDateTime(dtc.Rows[c]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr1["acname"] = dtc.Rows[c]["acname"].ToString();
                        dr1["ccode"] = Convert.ToInt64(dtc.Rows[c]["ccode"].ToString());
                        dr1["name"] = dtc.Rows[c]["name"].ToString();
                        dr1["followupdate"] = Convert.ToDateTime(dtc.Rows[c]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        dr1["followupdetails"] = dtc.Rows[c]["followupdetails"].ToString();
                        dr1["followupdetails1"] = dtc.Rows[c]["followupdetails1"].ToString();

                        dr1["itemname"] = dtc.Rows[c]["itemname"].ToString();
                        dr1["descr1"] = dtc.Rows[c]["descr1"].ToString();
                        dr1["descr2"] = dtc.Rows[c]["descr2"].ToString();
                        dr1["descr3"] = dtc.Rows[c]["descr3"].ToString();
                        dr1["qty"] = li.qtyremain;
                        dr1["unit"] = dtc.Rows[c]["unit"].ToString();
                        dr1["rate"] = Convert.ToDouble(dtc.Rows[c]["rate"].ToString());
                        dr1["basicamount"] = Convert.ToDouble(dtc.Rows[c]["basicamount"].ToString());
                        dr1["adjqty"] = Convert.ToDouble(dtc.Rows[c]["adjqty"].ToString());
                        dtq.Rows.Add(dr1);
                    }
                }
                else
                {
                    id = id + li.id + ",";
                    dtq = (DataTable)Session["dtpitemsapp"];
                    DataRow dr1 = dtq.NewRow();
                    dr1["pono"] = dtc.Rows[c]["pono"].ToString();
                    dr1["podate"] = Convert.ToDateTime(dtc.Rows[c]["podate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["acname"] = dtc.Rows[c]["acname"].ToString();
                    dr1["ccode"] = Convert.ToInt64(dtc.Rows[c]["ccode"].ToString());
                    dr1["name"] = dtc.Rows[c]["name"].ToString();
                    dr1["followupdate"] = Convert.ToDateTime(dtc.Rows[c]["followupdate"].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    dr1["followupdetails"] = dtc.Rows[c]["followupdetails"].ToString();
                    dr1["followupdetails1"] = dtc.Rows[c]["followupdetails1"].ToString();

                    dr1["itemname"] = dtc.Rows[c]["itemname"].ToString();
                    dr1["descr1"] = dtc.Rows[c]["descr1"].ToString();
                    dr1["descr2"] = dtc.Rows[c]["descr2"].ToString();
                    dr1["descr3"] = dtc.Rows[c]["descr3"].ToString();
                    dr1["qty"] = Math.Round(Convert.ToDouble(dtc.Rows[c]["qty"].ToString()), 2);
                    dr1["unit"] = dtc.Rows[c]["unit"].ToString();
                    dr1["rate"] = Convert.ToDouble(dtc.Rows[c]["rate"].ToString());
                    dr1["basicamount"] = Convert.ToDouble(dtc.Rows[c]["basicamount"].ToString());
                    dr1["adjqty"] = Convert.ToDouble(dtc.Rows[c]["adjqty"].ToString());
                    dtq.Rows.Add(dr1);
                }
            }
            gvaclist.DataSource = dtq;
            gvaclist.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No pending PO for this Account Name or Period.');", true);
            return;
        }
    }

    public void fillallpodata()
    {
        Session["dtpitemsapp"] = CreateTemplatepo();
        li.acname = "";
        //if (Request["ccode"].ToString() != "" && Request["ccode"].ToString() != string.Empty)
        //{
        //    li.ccode = Convert.ToInt64(Request["ccode"].ToString());
        //}
        //else
        //{
        li.ccode = 0;
        //}
        DataTable dtq = new DataTable();
        string id = "";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        li.fromdated = Convert.ToDateTime(li.fromdate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(li.todate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        string ftow = "";
        DataSet1 imageDataSet = new DataSet1();
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        SqlDataAdapter da11 = new SqlDataAdapter();
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        //SqlDataAdapter da11 = new SqlDataAdapter("select * from PurchaseOrder inner join PurchaseOrderItems on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrder.status!='Closed' and PurchaseOrder.cno='" + li.cno + "' and (PurchaseOrderItems.iscame!='Yes' or PurchaseOrderItems.iscame is NULL) and PurchaseOrder.podate between @fromdate and @todate", con);
        if (li.acname == "" && li.ccode == 0)
        {
            da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.podate between @fromdate and @todate and PurchaseOrderItems.cno=@cno order by PurchaseOrder.pono", con);
        }
        else if (li.acname != "" && li.ccode == 0)
        {
            da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.podate between @fromdate and @todate and PurchaseOrderItems.cno=@cno and PurchaseOrder.acname=@acname order by PurchaseOrder.pono", con);
            da11.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
        }
        else if (li.acname == "" && li.ccode != 0)
        {
            da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.podate between @fromdate and @todate and PurchaseOrderItems.cno=@cno and PurchaseOrder.ccode=@ccode order by PurchaseOrder.pono", con);
            da11.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
        }
        else if (li.acname != "" && li.ccode != 0)
        {
            da11 = new SqlDataAdapter("select * from PurchaseOrderItems inner join PurchaseOrder on PurchaseOrder.pono=PurchaseOrderItems.pono inner join ClientMaster on ClientMaster.code=PurchaseOrder.ccode where PurchaseOrderItems.podate between @fromdate and @todate and PurchaseOrderItems.cno=@cno and PurchaseOrder.ccode=@ccode and PurchaseOrder.acname=@acname order by PurchaseOrder.pono", con);
            da11.SelectCommand.Parameters.AddWithValue("@ccode", li.ccode);
            da11.SelectCommand.Parameters.AddWithValue("@acname", li.acname);
        }
        da11.SelectCommand.Parameters.AddWithValue("@fromdate", li.fromdated);
        da11.SelectCommand.Parameters.AddWithValue("@todate", li.todated);
        da11.SelectCommand.Parameters.AddWithValue("@cno", li.cno);
        DataTable dtc = new DataTable();
        da11.Fill(dtc);
        if (dtc.Rows.Count > 0)
        {            
            gvaclist.DataSource = dtc;
            gvaclist.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No pending PO for this Account Name or Period.');", true);
            return;
        }
    }

    public DataTable CreateTemplatepo()
    {
        DataTable dtpitems = new DataTable();
        dtpitems.Columns.Add("pono", typeof(string));
        dtpitems.Columns.Add("podate", typeof(DateTime));
        dtpitems.Columns.Add("acname", typeof(string));
        dtpitems.Columns.Add("ccode", typeof(Int64));
        dtpitems.Columns.Add("name", typeof(string));
        dtpitems.Columns.Add("followupdate", typeof(DateTime));
        dtpitems.Columns.Add("followupdetails", typeof(string));
        dtpitems.Columns.Add("followupdetails1", typeof(string));
        dtpitems.Columns.Add("itemname", typeof(string));
        dtpitems.Columns.Add("descr1", typeof(string));
        dtpitems.Columns.Add("descr2", typeof(string));
        dtpitems.Columns.Add("descr3", typeof(string));
        dtpitems.Columns.Add("unit", typeof(string));
        dtpitems.Columns.Add("qty", typeof(double));
        dtpitems.Columns.Add("rate", typeof(double));
        dtpitems.Columns.Add("basicamount", typeof(double));
        dtpitems.Columns.Add("adjqty", typeof(double));
        return dtpitems;
    }

    protected void txtadjqty_TextChanged(object sender, EventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        GridViewRow currentRow1 = (GridViewRow)((TextBox)sender).Parent.Parent;
        Label lblpono = (Label)currentRow1.FindControl("lblpono");
        Label lblitemname = (Label)currentRow1.FindControl("lblitemname");
        Label lbldescr1 = (Label)currentRow1.FindControl("lbldescr1");
        Label lbldescr2 = (Label)currentRow1.FindControl("lbldescr2");
        Label lbldescr3 = (Label)currentRow1.FindControl("lbldescr3");
        TextBox txtadjqty = (TextBox)currentRow1.FindControl("txtadjqty");
        if (txtadjqty.Text.Trim() != string.Empty)
        {
            li.adjqty = Math.Round(Convert.ToDouble(txtadjqty.Text), 2);
            li.strpono = lblpono.Text;
            li.itemname = lblitemname.Text;
            li.descr1 = lbldescr1.Text;
            li.descr2 = lbldescr2.Text;
            li.descr3 = lbldescr3.Text;
            SqlCommand cmd = new SqlCommand("update PurchaseOrderItems set adjqty=@adjqty where pono=@pono and itemname=@itemname and descr1=@descr1 and descr2=@descr2 and descr3=@descr3", con);
            cmd.Parameters.AddWithValue("@adjqty", li.adjqty);
            cmd.Parameters.AddWithValue("@pono", li.strpono);
            cmd.Parameters.AddWithValue("@itemname", li.itemname);
            cmd.Parameters.AddWithValue("@descr1", li.descr1);
            cmd.Parameters.AddWithValue("@descr2", li.descr2);
            cmd.Parameters.AddWithValue("@descr3", li.descr3);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            //fillpendingpo();
            if (chkalldata.Checked)
            {
                fillallpodata();
            }
            else
            {
                fillpendingpo();
            }
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.uname = Request.Cookies["ForLogin"]["username"];
            li.udate = System.DateTime.Now;
            li.activity = li.strpono + "-" + li.itemname + " Pending PO Adjusted.";
            faclass.insertactivity(li);
        }
        Page.SetFocus(txtadjqty);
    }
    protected void btnadjustpo_Click(object sender, EventArgs e)
    {
        string xxyear = Request.Cookies["ForLogin"]["currentyear"];
        SqlDateTime yyyear = Convert.ToDateTime("01-04-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[0], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        SqlDateTime yyyear1 = Convert.ToDateTime("31-03-" + Request.Cookies["ForLogin"]["acyear"].Split('-')[1], System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.fromdated = Convert.ToDateTime(txtfromdate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        li.todated = Convert.ToDateTime(txttodate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //int zz = xxyear.IndexOf(yyyear);
        if ((li.fromdated <= yyyear1 && li.fromdated >= yyyear) && (li.todated <= yyyear1 && li.todated >= yyyear))
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Date must be in current period.');", true);
            return;
        }
        if (chkalldata.Checked)
        {
            fillallpodata();
        }
        else
        {
            fillpendingpo();
        }

    }
}