﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SalesmanMaster.aspx.cs" Inherits="SalesmanMaster" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Salesman Master</span><br />
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Code</label></div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" Width="200px" ReadOnly="true"
                            placeholder="Auto"></asp:TextBox></div>
                            <div class="col-md-2">
                        <asp:TextBox ID="txtuser" runat="server" CssClass="form-control" ReadOnly="true"
                            onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Name<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Width="200px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Mobile<span style="color: Red; font-size: 20px;">*</span></label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtmobile" runat="server" CssClass="form-control" Width="200px"
                            onkeypress="javascript:return isNumber (event)"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            E-Mail</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" Width="200px"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                            Address</label></div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtaddress" runat="server" CssClass="form-control" Width="200px"
                            TextMode="MultiLine" Height="40px" onkeypress="return checkQuote();"></asp:TextBox></div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-md-3">
                        <asp:Button ID="btnsaves" runat="server" Text="Save" class="btn btn-default forbutton"
                            ValidationGroup="valsalesman" OnClick="btnsaves_Click" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                            ShowSummary="false" ValidationGroup="valsalesman" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter account name."
                            Text="*" ValidationGroup="valsalesman" ControlToValidate="txtname"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter mobile."
                            Text="*" ValidationGroup="valsalesman" ControlToValidate="txtmobile"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please Enter Proper Email ID. e.g. ( test@test.com )"
                            Text="*" ValidationGroup="valsalesman" ControlToValidate="txtemail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
