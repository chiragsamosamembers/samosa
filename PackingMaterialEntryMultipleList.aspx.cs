﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class PackingMaterialEntryMultipleList : System.Web.UI.Page
{
    ForPackingMaterial fpmclass = new ForPackingMaterial();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            filltransportname();
            selectrights();
        }
    }

    public void filltransportname()
    {
        DataTable dtdata = new DataTable();
        dtdata = fpmclass.selectalltransportname(li);
        if (dtdata.Rows.Count > 0)
        {
            drptransportname.DataSource = dtdata;
            drptransportname.DataTextField = "name";
            drptransportname.DataBind();
            drptransportname.Items.Insert(0, "--SELECT--");
        }
        else
        {
            drptransportname.Items.Clear();
            drptransportname.Items.Insert(0, "--SELECT--");
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.istype = "M";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fpmclass.selectallpmmasterdata(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvpackingmultiplelist.Visible = true;
            gvpackingmultiplelist.DataSource = dtdata;
            gvpackingmultiplelist.DataBind();
        }
        else
        {
            gvpackingmultiplelist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Packing Material Found.";
        }
    }

    protected void btnpacking_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/PackingMaterialEntryMultiple.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvpackingmultiplelist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                Response.Redirect("~/PackingMaterialEntryMultiple.aspx?mode=update&vno=" + li.voucherno + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);

                DataTable dtdata = new DataTable();
                dtdata = fpmclass.selectallpmmasterdatafromvoucherno(li);
                if (dtdata.Rows.Count > 0)
                {
                    li.ispacking = "Y";
                    li.strscno = dtdata.Rows[0]["rmcno"].ToString();
                    fpmclass.updateispackinginscmastersetnull(li);
                    li.vnono1 = Convert.ToInt64(dtdata.Rows[0]["tdcno"].ToString());
                    fpmclass.updateispackingintoodelmastersetnull(li);
                }
                else
                {
                    DataTable dtdataq = new DataTable();
                    dtdataq = fpmclass.selectallpmmasterdatafromvoucherno2(li);
                    if (dtdataq.Rows.Count > 0)
                    {
                        li.ispacking = "Y";
                        li.strscno = dtdataq.Rows[0]["rmcno"].ToString();
                        fpmclass.updateispackinginscmastersetnull(li);
                        li.vnono1 = Convert.ToInt64(dtdataq.Rows[0]["tdcno"].ToString());
                        fpmclass.updateispackingintoodelmastersetnull(li);
                    }
                }

                DataTable dtitems = new DataTable();
                dtitems = fpmclass.selectallpmitemsdata(li);
                for (int c = 0; c < dtitems.Rows.Count; c++)
                {
                    li.dcno1 = dtitems.Rows[c]["dcno"].ToString();
                    li.strscno = li.dcno1.ToString();
                    li.ispacking = "Y";
                    fpmclass.updateispackinginscmastersetnull(li);
                }



                fpmclass.deletepmmasteredatafromvoucherno(li);
                fpmclass.deletepmitemsedatafromvoucherno(li);
                fpmclass.updateisusedn(li);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.voucherno + " Packing Material Data deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
        else if (e.CommandName == "print")
        {
            string Xrepname = "packing slip report";
            Int64 vno = Convert.ToInt64(e.CommandArgument);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?voucherno=" + vno + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
    }
    protected void gvpackingmultiplelist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void btnreport_Click(object sender, EventArgs e)
    {
        string Xrepname = "Packing Slip From To Report";
        string bank = drptransportname.SelectedItem.Text;
        li.fromdate = txtfromdate.Text;
        li.todate = txttodate.Text;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?billno=" + billno + "&custname=" + ViewState["clientname"].ToString() + "&mobile=" + ViewState["clientmobile"].ToString() + "&email=" + ViewState["clientemail"].ToString() + "&grandtotal=" + ViewState["total"].ToString() + "&billtype=" + rdobilltype.SelectedItem.Text + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?bank=" + bank + "&fromdate=" + li.fromdate + "&todate=" + li.todate + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
    }
}