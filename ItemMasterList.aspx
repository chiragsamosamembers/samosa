﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ItemMasterList.aspx.cs" Inherits="ItemMasterList" Culture="hi-IN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <span style="color: white; background-color: Red">Item Master List</span><br />
        <div class="row">
            <div class="col-md-1">
                <asp:Button ID="btnitem" runat="server" Text="Add New Item" class="btn btn-default forbutton"
                    OnClick="btnitem_Click" /></div>
            <div class="col-md-3">
                <asp:TextBox ID="txtsearchitemname" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtsearchitemname"
                    MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="100"
                    ServiceMethod="GetItemname">
                </asp:AutoCompleteExtender>
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnsearch" runat="server" Text="Search" class="btn btn-default forbutton"
                    OnClick="btnsearch_Click" />
            </div>
            <div class="col-md-1">
                <asp:Button ID="btnprintitem" runat="server" Text="Print Item List" class="btn btn-default forbutton"
                    OnClick="btnprintitem_Click" />
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnperformainvoice" runat="server" Text="Create Per. Inv." 
                    class="btn btn-default forbutton" onclick="btnperformainvoice_Click" />
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnquotation" runat="server" Text="Create Quotation" 
                    class="btn btn-default forbutton" onclick="btnquotation_Click" />
            </div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvitem" runat="server" AutoGenerateColumns="False" Width="100%"
                        BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”" Height="0px"
                        CssClass="table table-bordered" OnRowCommand="gvitem_RowCommand" OnRowDeleting="gvitem_RowDeleting">
                        <%--AllowPaging="True" OnPageIndexChanging="gvitem_PageIndexChanging"--%>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkall" runat="server" AutoPostBack="True" OnCheckedChanged="chkall_CheckedChanged" /></HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkselect" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ID" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" ForeColor="Black" runat="server" Text='<%# bind("id") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemname" ForeColor="Black" runat="server" Text='<%# bind("itemname") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldescription" ForeColor="Black" runat="server" Text='<%# bind("description") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unit" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblunit" ForeColor="Black" runat="server" Text='<%# bind("unit") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="HSN Code" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txthsncode" runat="server" Text='<%# bind("hsncode") %>' 
                                        AutoPostBack="True" ontextchanged="txthsncode_TextChanged" Width="80px"></asp:TextBox>
                                    <%--<asp:Label ID="lblhsncode" ForeColor="Black" runat="server" Text='<%# bind("hsncode") %>'></asp:Label>--%>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ACYear" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblacyear" ForeColor="Black" runat="server" Text='<%# bind("acyear") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("id") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
