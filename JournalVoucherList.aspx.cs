﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

public partial class JournalVoucherList : System.Web.UI.Page
{
    public ReportDocument rptdoc;
    ForBankReceipt fbrclass = new ForBankReceipt();
    ForUserRight furclass = new ForUserRight();
    ForActivity faclass = new ForActivity();
    LogicLayer li = new LogicLayer();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillgrid();
            selectrights();
        }
    }

    public void selectrights()
    {
        DataTable dtr = new DataTable();
        li.pagename = Request["pagename"].ToString();
        dtr = furclass.selectuserrights(li);
        if (dtr.Rows.Count > 0)
        {
            ViewState["uadd"] = dtr.Rows[0]["UADD"].ToString();
            ViewState["uedit"] = dtr.Rows[0]["UEDIT"].ToString();
            ViewState["udelete"] = dtr.Rows[0]["UDELETE"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('First GoTo User Rights And Give Rights To This User.');", true);
        }
    }

    public void fillgrid()
    {
        li.istype = "JV";
        li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        DataTable dtdata = new DataTable();
        dtdata = fbrclass.selectallbr1data(li);
        if (dtdata.Rows.Count > 0)
        {
            lblempty.Visible = false;
            gvjournalvoucherlist.Visible = true;
            gvjournalvoucherlist.DataSource = dtdata;
            gvjournalvoucherlist.DataBind();
        }
        else
        {
            gvjournalvoucherlist.Visible = false;
            lblempty.Visible = true;
            lblempty.Text = "No Journal Voucher Data Found.";
        }
    }

    protected void gvjournalvoucherlist_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void gvjournalvoucherlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            if (ViewState["uedit"].ToString() == "Y")
            {
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                Response.Redirect("~/JournalVoucher.aspx?mode=update&vno=" + li.voucherno + "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Edit New Data.');", true);
            }
        }
        else if (e.CommandName == "delete")
        {
            if (ViewState["udelete"].ToString() == "Y")
            {
                li.istype = "JV";
                li.voucherno = Convert.ToInt64(e.CommandArgument);
                li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
                DataTable dtitemdata = new DataTable();
                dtitemdata = fbrclass.selectbrdatafromvno(li);
                for (int c = 0; c < dtitemdata.Rows.Count; c++)
                {
                    li.id = Convert.ToInt64(dtitemdata.Rows[c]["id"].ToString());
                    DataTable dtamount = new DataTable();
                    dtamount = fbrclass.selectamounttodebitfromacfromaccountname(li);
                    if (dtamount.Rows.Count > 0)
                    {
                        for (int q = 0; q < dtamount.Rows.Count; q++)
                        {
                            li.amount = Convert.ToDouble(dtamount.Rows[q]["amount1"].ToString());
                            //li.invoiceno = Convert.ToInt64(dtamount.Rows[0]["invoiceno"].ToString());
                            li.acname = dtamount.Rows[q]["acname"].ToString();
                            li.issipi = dtamount.Rows[q]["issipi1"].ToString();
                            if (li.issipi == "SI")
                            {
                                li.strsino = dtamount.Rows[q]["invoiceno"].ToString();
                                DataTable dtsi = new DataTable();
                                dtsi = fbrclass.selectamountforupdationstring(li);
                                li.receivedamount = Convert.ToDouble(dtsi.Rows[0]["receivedamount"].ToString()) - li.amount;
                                li.remainamount = Convert.ToDouble(dtsi.Rows[0]["remainamount"].ToString()) + li.amount;
                                fbrclass.updatesidata11string(li);
                            }
                            else
                            {
                                li.strpino = dtamount.Rows[q]["invoiceno"].ToString();
                                DataTable dtsi = new DataTable();
                                dtsi = fbrclass.selectamountforupdationpistring(li);
                                li.receivedamount = Convert.ToDouble(dtsi.Rows[0]["receivedamount"].ToString()) - li.amount;
                                li.remainamount = Convert.ToDouble(dtsi.Rows[0]["remainamount"].ToString()) + li.amount;
                                fbrclass.updatepidata11string(li);
                            }
                        }
                    }
                    li.istype = "JV";
                    //fbrclass.deletebrdatafromvno(li);                
                    fbrclass.deletebrdata(li);
                    fbrclass.deletefromshadowtable(li);
                    //fbrclass.deletefromledgertable(li);
                }
                fbrclass.deletebr1data(li);
                fbrclass.deleteallfromshadowtable(li);
                fbrclass.deletebr1datajv(li);
                li.strvoucherno = "JV" + li.voucherno.ToString();
                fbrclass.deleteledger_edata(li);
                fbrclass.updateisusednjv(li);
                li.uname = Request.Cookies["ForLogin"]["username"];
                li.udate = System.DateTime.Now;
                li.activity = li.voucherno + " Journal Voucher deleted.";
                faclass.insertactivity(li);
                fillgrid();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Delete New Data.');", true);
            }
        }
        else if (e.CommandName == "print")
        {
            rptdoc = new ReportDocument();
            string Xrepname = "JVV Report";
            Int64 vno = Convert.ToInt64(e.CommandArgument);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?vno=" + vno + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
            li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
            li.voucherno = vno;
            string ftow = "";
            DataSet1 imageDataSet = new DataSet1();
            string cz = Request.Cookies["Forcon"]["conc"];
            cz = cz.Replace(":", ";");
            SqlConnection con = new SqlConnection(cz);
            //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
            SqlDataAdapter da11 = new SqlDataAdapter("select BankACMaster.name as achead,BankACMaster.voucherno,BankACMaster.voucherdate,BankACMaster.acname,BankACMaster.remarks,(CASE WHEN (BankACMaster.iscd = 'D') THEN BankACMaster.amount ELSE NULL END) as Debitamt,(CASE WHEN (BankACMaster.iscd = 'C') THEN BankACMaster.amount ELSE NULL END) as Creditamt from BankACMaster where BankACMaster.cno=" + li.cno + " and BankACMaster.voucherno=@voucherno and BankACMaster.istype='JV'", con);
            da11.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            DataTable dtc = new DataTable();
            da11.Fill(imageDataSet.Tables["DataTable3"]);
            string rptname;
            //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
            //{
            rptname = Server.MapPath(@"~/Reports/JVVoucher.rpt");

            SqlDataAdapter dchead = new SqlDataAdapter("select * from BankACMaster1 where cno=" + li.cno + " and voucherno=@voucherno and istype='JV'", con);
            dchead.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            DataTable dtophead = new DataTable();
            dchead.Fill(dtophead);

            //SqlDataAdapter dc = new SqlDataAdapter("select (CASE WHEN (type = 'D') THEN amount ELSE NULL END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE NULL END) as Creditamt from ledger where type='C'", con);
            SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from BankACMaster where cno=" + li.cno + " and iscd='C' and voucherno=@voucherno and istype='JV'", con);
            dc.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            DataTable dtop = new DataTable();
            dc.Fill(dtop);

            SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from BankACMaster where cno=" + li.cno + " and iscd='D' and voucherno=@voucherno and istype='JV'", con);
            dc1.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
            DataTable dtop1 = new DataTable();
            dc1.Fill(dtop1);
            //////double opbal = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString()) - (Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString()) + 500);
            //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
            //}
            //else
            //{
            //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
            //    {
            //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
            //    }
            //    else
            //    {
            //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
            //    }
            //}
            rptdoc.Load(rptname);
            double ch = 0;
            double dh = 0;
            if (dtophead.Rows[0]["type"].ToString() == "C")
            {
                ch = Convert.ToDouble(dtophead.Rows[0]["total"].ToString());
                if (ch < 0)
                {
                    ch = Convert.ToDouble(dtophead.Rows[0]["total"].ToString()) * (-1);
                }
            }
            else
            {
                dh = Convert.ToDouble(dtophead.Rows[0]["total"].ToString());
                if (dh < 0)
                {
                    dh = Convert.ToDouble(dtophead.Rows[0]["total"].ToString()) * (-1);
                }
            }
            double totc = 0;
            double totd = 0;
            totc = (ch + (Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString()) * (-1)));
            totd = (dh + Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString()));
            if (totc < 0)
            {
                totc = totc * (-1);
            }
            if (totd < 0)
            {
                totd = totd * (-1);
            }
            //string fdate = "From : " + li.fromdated.ToString("dd-MM-yyyy") + " To : " + li.todated.ToString("dd-MM-yyyy");
            //rptdoc.DataDefinition.FormulaFields["fdate"].Text = "'" + fdate + "'";
            rptdoc.DataDefinition.FormulaFields["totcredit"].Text = "'" + dtop.Rows[0]["totcredit"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["totdebit"].Text = "'" + dtop1.Rows[0]["totdebit"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["achead"].Text = "'" + dtophead.Rows[0]["name"].ToString() + "'";
            rptdoc.DataDefinition.FormulaFields["camt"].Text = "'" + ch.ToString("#0.00") + "'";
            rptdoc.DataDefinition.FormulaFields["damt"].Text = "'" + dh.ToString("#0.00") + "'";
            rptdoc.DataDefinition.FormulaFields["totc"].Text = "'" + totc.ToString("#0.00") + "'";
            rptdoc.DataDefinition.FormulaFields["totd"].Text = "'" + totd.ToString("#0.00") + "'";
            //////if (opbal >= 0)
            //////{
            //////    rptdoc.DataDefinition.FormulaFields["debit"].Text = "'" + opbal + "'";
            //////}
            //////else
            //////{
            //////    rptdoc.DataDefinition.FormulaFields["credit"].Text = "'" + opbal * (-1) + "'";
            //////}

            rptdoc.SetDataSource(imageDataSet.Tables["DataTable3"]);
            ExportOptions exprtopt = default(ExportOptions);
            string fname = "jvv.pdf";
            //create instance for destination option - This one is used to set path of your pdf file save 
            DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
            destiopt.DiskFileName = Server.MapPath(@"~/cheque/" + fname);


            exprtopt = rptdoc.ExportOptions;
            exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

            //use PortableDocFormat for PDF data
            exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
            exprtopt.DestinationOptions = destiopt;

            //finally export your report document
            rptdoc.Export();
            rptdoc.Close();
            rptdoc.Clone();
            rptdoc.Dispose();
            string path = fname;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('JVVPrinting.aspx?&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        }
        //else if (e.CommandName == "print")
        //{
        //    rptdoc = new ReportDocument();
        //    string Xrepname = "JVV Report";
        //    Int64 vno = Convert.ToInt64(e.CommandArgument);
        //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('ReportViewer.aspx?vno=" + vno + "&Xrepname=" + Xrepname + "'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //    li.cno = Convert.ToInt64(Request.Cookies["ForCompany"]["cno"]);
        //    li.voucherno = vno;
        //    string ftow = "";
        //    DataSet1 imageDataSet = new DataSet1();
        //    string cz = Request.Cookies["Forcon"]["conc"];
        //    cz = cz.Replace(":", ";");
        //    SqlConnection con = new SqlConnection(cz);
        //    //SqlDataAdapter da11 = new SqlDataAdapter("select * from mmast where fullname='" + li.fullname + "'", con);
        //    SqlDataAdapter da11 = new SqlDataAdapter("select BankACMaster.name as achead,BankACMaster.voucherno,BankACMaster.voucherdate,BankACMaster.acname,BankACMaster.remarks,(CASE WHEN (BankACMaster1.type = 'C') THEN BankACMaster.amount ELSE NULL END) as Debitamt,(CASE WHEN (BankACMaster1.type = 'D') THEN BankACMaster.amount ELSE NULL END) as Creditamt from BankACMaster inner join BankACMaster1 on BankACMaster.istype+convert(varchar(50),BankACMaster.voucherno)=BankACMaster1.istype+convert(varchar(50),BankACMaster1.voucherno) where BankACMaster.cno=" + li.cno + " and BankACMaster.voucherno=@voucherno and BankACMaster.istype='JV'", con);
        //    da11.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
        //    DataTable dtc = new DataTable();
        //    da11.Fill(imageDataSet.Tables["DataTable3"]);
        //    string rptname;
        //    //if (imageDataSet.Tables["DataTable1"].Rows.Count < 11)
        //    //{
        //    rptname = Server.MapPath(@"~/Reports/JVVoucher.rpt");

        //    SqlDataAdapter dchead = new SqlDataAdapter("select * from BankACMaster1 where cno=" + li.cno + " and voucherno=@voucherno and istype='JV'", con);
        //    dchead.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
        //    DataTable dtophead = new DataTable();
        //    dchead.Fill(dtophead);

        //    //SqlDataAdapter dc = new SqlDataAdapter("select (CASE WHEN (type = 'D') THEN amount ELSE NULL END) as Debitamt,(CASE WHEN (type = 'C') THEN amount ELSE NULL END) as Creditamt from ledger where type='C'", con);
        //    SqlDataAdapter dc = new SqlDataAdapter("select isnull(sum(amount),0) as totcredit from BankACMaster where cno=" + li.cno + " and iscd='C' and voucherno=@voucherno and istype='JV'", con);
        //    dc.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
        //    DataTable dtop = new DataTable();
        //    dc.Fill(dtop);

        //    SqlDataAdapter dc1 = new SqlDataAdapter("select isnull(sum(amount),0) as totdebit from BankACMaster where cno=" + li.cno + " and iscd='D' and voucherno=@voucherno and istype='JV'", con);
        //    dc1.SelectCommand.Parameters.AddWithValue("@voucherno", li.voucherno);
        //    DataTable dtop1 = new DataTable();
        //    dc1.Fill(dtop1);
        //    //////double opbal = Convert.ToDouble(dtop1.Rows[0]["totdebit"].ToString()) - (Convert.ToDouble(dtop.Rows[0]["totcredit"].ToString()) + 500);
        //    //rptname = Server.MapPath(@"~/Reports/invoiceonbill.rpt");
        //    //}
        //    //else
        //    //{
        //    //    if (Convert.ToDouble(imageDataSet.Tables["DataTable1"].Rows[0]["taxamount"].ToString()) > 0)
        //    //    {
        //    //        rptname = Server.MapPath(@"~/Reports/invoice.rpt");
        //    //    }
        //    //    else
        //    //    {
        //    //        rptname = Server.MapPath(@"~/Reports/retailinvoice.rpt");
        //    //    }
        //    //}
        //    rptdoc.Load(rptname);
        //    double ch = 0;
        //    double dh = 0;
        //    if (dtophead.Rows[0]["type"].ToString() == "C")
        //    {
        //        ch = Convert.ToDouble(dtophead.Rows[0]["total"].ToString());
        //    }
        //    else
        //    {
        //        dh = Convert.ToDouble(dtophead.Rows[0]["total"].ToString());
        //    }
        //    //string fdate = "From : " + li.fromdated.ToString("dd-MM-yyyy") + " To : " + li.todated.ToString("dd-MM-yyyy");
        //    //rptdoc.DataDefinition.FormulaFields["fdate"].Text = "'" + fdate + "'";
        //    rptdoc.DataDefinition.FormulaFields["totcredit"].Text = "'" + dtop.Rows[0]["totcredit"].ToString() + "'";
        //    rptdoc.DataDefinition.FormulaFields["totdebit"].Text = "'" + dtop1.Rows[0]["totdebit"].ToString() + "'";
        //    if (dh != 0)
        //    {
        //        rptdoc.DataDefinition.FormulaFields["totc1"].Text = "'" + dh.ToString("#0.00") + "'";
        //        rptdoc.DataDefinition.FormulaFields["totd1"].Text = "'" + dh.ToString("#0.00") + "'";
        //        if (dh < 0)
        //        {
        //            rptdoc.DataDefinition.FormulaFields["totc1"].Text = "'" + (dh * (-1)).ToString("#0.00") + "'";
        //            rptdoc.DataDefinition.FormulaFields["totd1"].Text = "'" + (dh * (-1)).ToString("#0.00") + "'";
        //        }
        //    }
        //    else if (ch != 0)
        //    {
        //        rptdoc.DataDefinition.FormulaFields["totc1"].Text = "'" + (ch * (-1)).ToString("#0.00") + "'";
        //        rptdoc.DataDefinition.FormulaFields["totd1"].Text = "'" + (ch * (-1)).ToString("#0.00") + "'";
        //        if (ch > 0)
        //        {
        //            rptdoc.DataDefinition.FormulaFields["totc1"].Text = "'" + (ch).ToString("#0.00") + "'";
        //            rptdoc.DataDefinition.FormulaFields["totd1"].Text = "'" + ch.ToString("#0.00") + "'";
        //        }
        //    }

        //    rptdoc.DataDefinition.FormulaFields["achead"].Text = "'" + dtophead.Rows[0]["name"].ToString() + "'";
        //    if (ch >= 0)
        //    {
        //        rptdoc.DataDefinition.FormulaFields["camt"].Text = "'" + (ch).ToString("#0.00") + "'";
        //    }
        //    else
        //    {
        //        rptdoc.DataDefinition.FormulaFields["camt"].Text = "'" + (ch * (-1)).ToString("#0.00") + "'";
        //    }
        //    if (dh >= 0)
        //    {
        //        rptdoc.DataDefinition.FormulaFields["damt"].Text = "'" + dh.ToString("#0.00") + "'";
        //    }
        //    else
        //    {
        //        rptdoc.DataDefinition.FormulaFields["damt"].Text = "'" + (dh * (-1)).ToString("#0.00") + "'";
        //    }
        //    //////if (opbal >= 0)
        //    //////{
        //    //////    rptdoc.DataDefinition.FormulaFields["debit"].Text = "'" + opbal + "'";
        //    //////}
        //    //////else
        //    //////{
        //    //////    rptdoc.DataDefinition.FormulaFields["credit"].Text = "'" + opbal * (-1) + "'";
        //    //////}

        //    rptdoc.SetDataSource(imageDataSet.Tables["DataTable3"]);
        //    ExportOptions exprtopt = default(ExportOptions);
        //    string fname = "jvv.pdf";
        //    //create instance for destination option - This one is used to set path of your pdf file save 
        //    DiskFileDestinationOptions destiopt = new DiskFileDestinationOptions();
        //    destiopt.DiskFileName = Server.MapPath(@"~/cheque/" + fname);


        //    exprtopt = rptdoc.ExportOptions;
        //    exprtopt.ExportDestinationType = ExportDestinationType.DiskFile;

        //    //use PortableDocFormat for PDF data
        //    exprtopt.ExportFormatType = ExportFormatType.PortableDocFormat;
        //    exprtopt.DestinationOptions = destiopt;

        //    //finally export your report document
        //    rptdoc.Export();
        //    rptdoc.Close();
        //    rptdoc.Clone();
        //    rptdoc.Dispose();
        //    string path = fname;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "<Script>window.open('JVVPrinting.aspx?&mode=insert'" + ",'',' left=0,top=0,width=1500,height=750,toolbar=0,scrollbars=1,status=0');</Script>", false);
        //}
    }
    protected void gvjournalvoucherlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvjournalvoucherlist.PageIndex = e.NewPageIndex;
        fillgrid();
    }
    protected void btnjournalvoucher_Click(object sender, EventArgs e)
    {
        if (ViewState["uadd"].ToString() == "Y")
        {
            Response.Redirect("~/JournalVoucher.aspx?mode=insert");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Rights To Add New Data.');", true);
        }
    }
    protected void gvjournalvoucherlist_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string cz = Request.Cookies["Forcon"]["conc"];
        cz = cz.Replace(":", ";");
        SqlConnection con = new SqlConnection(cz);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //var gv = (GridView)e.Row.FindControl("gvemember");
            var lblvoucherno = (Label)e.Row.FindControl("lblvoucherno");
            string vno = lblvoucherno.Text;
            string istype = "JV";
            con.Open();
            var ddl = (DropDownList)e.Row.FindControl("drpstatus");
            var lbl = (Label)e.Row.FindControl("lblstatus");
            SqlCommand cmd = new SqlCommand("select * from BankACMaster where voucherno=" + lblvoucherno.Text + " and istype='" + istype + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            da.Fill(ds);
            con.Close();

            var lnkmove = (ImageButton)e.Row.FindControl("imgbtnselect");
            for (int i = 0; i < ds.Rows.Count; i++)
            {
                if (ds.Rows[i]["agbill"].ToString() == "Y")
                {
                    lnkmove.Visible = false;
                }
            }
        }
    }
}