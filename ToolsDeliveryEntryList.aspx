﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ToolsDeliveryEntryList.aspx.cs" Inherits="ToolsDeliveryEntryList" Culture="hi-IN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <span style="color: white; background-color: Red">Tools Delivery Entry List</span><br />
        <div class="row">
            <div class="col-md-12">
                <asp:Button ID="btntoolsdeliveryentry" runat="server" Text="Add New Tool Delivery"
                    class="btn btn-default forbutton" OnClick="btntoolsdeliveryentry_Click" /></div>
        </div>
        <div class="row" style="height: 20px;">
        </div>
        <div class="row table-responsive">
            <div class="col-md-12">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="460px" Width="98%">
                    <asp:Label ID="lblempty" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvtoolsdeliveryentrylist" runat="server" AutoGenerateColumns="False"
                        Width="100%" BorderStyle="None" AllowSorting="false" EmptyDataText="”No records found”"
                        Height="0px" CssClass="table table-bordered" OnRowCommand="gvtoolsdeliveryentrylist_RowCommand"
                        OnRowDeleting="gvtoolsdeliveryentrylist_RowDeleting" 
                        onrowdatabound="gvtoolsdeliveryentrylist_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect" CommandName="select" runat="server" ImageUrl="~/images/buttons/edit.jpg"
                                        ToolTip="Edit" Height="20px" Width="20px" CausesValidation="False" CommandArgument='<%# bind("voucherno") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Voucher No." SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvoucherno" ForeColor="Black" runat="server" Text='<%# bind("voucherno") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Voucher Date" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblvoucherdate" ForeColor="Black" runat="server" Text='<%#Convert.ToDateTime(Eval("voucherdate")).ToString("dd-MM-yyyy") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblname" ForeColor="Black" runat="server" Text='<%# bind("name") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Client Code" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lblclientcode" ForeColor="Black" runat="server" Text='<%# bind("clientcode") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Despatched By" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:Label ID="lbldispatchedby" ForeColor="Black" runat="server" Text='<%# bind("dispatchedby") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Signed Or Not" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chksigned" runat="server" AutoPostBack="True" OnCheckedChanged="chksigned_CheckedChanged" />
                                    <asp:Label ID="lblsigned" ForeColor="Black" runat="server" Text='<%# bind("issigned") %>'
                                        Visible="false"></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks" SortExpression="Csnm">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtremarks" runat="server" AutoPostBack="True" OnTextChanged="txtremarks_TextChanged"
                                        Text='<%# bind("signremarks") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridViewItemHeader"></HeaderStyle>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect1" CommandName="delete" runat="server" ImageUrl="~/images/buttons/delete-icon.png"
                                        ToolTip="Delete" Height="20px" Width="20px" OnClientClick="return confirmdelete();"
                                        CommandArgument='<%# bind("voucherno") %>' CausesValidation="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnselect11" CommandName="print" runat="server" ImageUrl="~/images/buttons/printer.gif"
                                        ToolTip="Print" CommandArgument='<%# bind("voucherno") %>' Height="20px"
                                        Width="20px" CausesValidation="False" /><%--OnClientClick="return confirmprintchq();"--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#4c4c4c" />
                        <RowStyle Font-Size="13px" CssClass="GridViewBorderRight" BackColor="white" />
                        <HeaderStyle BackColor="white" Font-Bold="True" ForeColor="black" CssClass="GridViewItemHeader" />
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
